package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShowTime {

    @SerializedName("ShowDetails")
    @Expose
    private String showDetails;

    public String getShowDetails() {
        return showDetails;
    }

    public void setShowDetails(String showDetails) {
        this.showDetails = showDetails;
    }

}