package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Row {

    @SerializedName("intGridRowID")
    @Expose
    private String intGridRowID;
    @SerializedName("strRowPhyID")
    @Expose
    private String strRowPhyID;
    @SerializedName("seats")
    @Expose
    private List<Seat> seats = null;

    public String getIntGridRowID() {
        return intGridRowID;
    }

    public void setIntGridRowID(String intGridRowID) {
        this.intGridRowID = intGridRowID;
    }

    public String getStrRowPhyID() {
        return strRowPhyID;
    }

    public void setStrRowPhyID(String strRowPhyID) {
        this.strRowPhyID = strRowPhyID;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

}