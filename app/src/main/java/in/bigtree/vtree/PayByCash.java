package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

public class PayByCash extends AppCompatActivity implements AsyncResponse{
    String strTempTransId = "", strSessionId = "", strTicketType = "", strSeatInfo = "", strQty = "", strIP = "", strFileName = "", strFileContent = "", strGAction = "", strBookingId = "",
            strComments = "", strPhone = "", strWCODE = "", strFilmName = "", strShowDateTime = "", strPrice = "", strCinemaname = "", strScreenName = "", strAreaCat = "", strSeats = "", strAppMode = "",
            strJsonArray ="", strJItems = "", strPickupType = "";
    TextInputEditText edComments, edPhone;
    TextView tvFilmName, tvShowDate, tvPrice, tvSkip;
    AlertDialog dialogProg;
    Button btnProceed;
    JSONArray jArray;
    JSONObject jObject;
    LinearLayout llBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_by_cash);
        try{

            strJsonArray = getIntent().getStringExtra("jsonArray");
            Bundle poBundle = getIntent().getExtras();
            strAppMode = poBundle.getString("appmode");
            strTempTransId = poBundle.getString("temptransid");
            strSessionId = poBundle.getString("sessionid");
            strTicketType = poBundle.getString("ttype");
            strSeatInfo = poBundle.getString("seatinfo");
            strQty = poBundle.getString("qty");
            strBookingId = poBundle.getString("bookingid");
            strFilmName = poBundle.getString("filmname");
            strShowDateTime = poBundle.getString("showdatetime");
            strPrice = poBundle.getString("price");
            strCinemaname = poBundle.getString("cinemaname");
            strScreenName = poBundle.getString("screen");
            strAreaCat = poBundle.getString("areacat");
            strSeats = poBundle.getString("seats");
            strPickupType = poBundle.getString("pickuptype");
            llBack = (LinearLayout)findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToPaymentSelection(v));

            FileIO obFile = new FileIO();
            strFileName = getResources().getString(R.string.connection_config_file);
            strFileContent = obFile.readFile(strFileName,getApplicationContext());
            strIP = strFileContent.split("\\|")[0];
            strWCODE = strFileContent.split("\\|")[1];
            edComments = (TextInputEditText) findViewById(R.id.tvComments);
            edPhone = (TextInputEditText)findViewById(R.id.tvMobileNo);
            tvFilmName = (TextView)findViewById(R.id.tvFilmName);
            tvShowDate = (TextView) findViewById(R.id.tvShowDateTime);
            tvPrice = (TextView) findViewById(R.id.tvPrice);
            btnProceed = (Button)findViewById(R.id.btnProceed);
            btnProceed.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
            btnProceed.setClickable(false);

            tvShowDate.setText(strShowDateTime);
            tvFilmName.setText("Payment for " + strFilmName);
            tvPrice.setText(strPrice);
            //tvSkip = (TextView) findViewById(R.id.tvSkip);
            //callAddSeats();


            if(!strJsonArray.equals("")){
                jArray = new JSONArray(strJsonArray);
                for(int iCount = 0; iCount < jArray.length(); iCount++){
                    jObject = (JSONObject) jArray.get(iCount);
                    strJItems += jObject.getString("itemid") + "_" +jObject.getString("qty") + "|";
                }
                strJItems = strJItems.substring(0,strJItems.length()-1);
            }

            edPhone.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        if (s.length() < 10) {
                            btnProceed.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
                            btnProceed.setClickable(false);
                        }
                        else{
                            btnProceed.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                            btnProceed.setClickable(true);
                            closeKeyboard();
                        }
                        /*
                        if(s.length() == 10){
                            closeKeyboard();
                        }
                        */
                    } catch (Exception Ex) {
                        Ex.toString();
                    }
                }
            });

            /*
            if(strAppMode.equals("fnb")) {
                tvSkip.setVisibility(View.VISIBLE);
            }
            */
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCash.this,"Error","Something went wrong");
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void proceedCommit(View v){
        try{
            String strCustDetails = "", strUID = "", strTransType = "", strProgTitle = "", strLoc = "";
            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            strUID = obShared.getUID();
            if(strAppMode.equals("fnb")) {
                strTransType = "C";
                strProgTitle = "Commiting transaction";
                strSessionId = strJItems;
                strLoc = obShared.getLOC();
            }
            else{
                strTransType = "T";
                strProgTitle = "Booking your ticket";
            }
            strComments = edComments.getText().toString();
            strPhone = edPhone.getText().toString();
            strCustDetails = strComments + "|" +strPhone + "|" + "VTREE" + "|" + strComments;
            strGAction = "COMMIT";

            SoapCall obSc = new SoapCall(PayByCash.this);
            obSc.delegate = this;
            ShowProgressDialog(PayByCash.this,strProgTitle,"");
            obSc.execute("COMMITTRANS", strIP, strSessionId, strTempTransId, "TRUE","5123456789012346|VISA|05|2021|123",strCustDetails,"C",strUID,strWCODE,strBookingId,strTicketType,strQty,strTransType,strLoc,strPickupType,"");
            /*
            Intent intBookingConfirmation = new Intent(PayByCash.this,BookingConfirmation.class);
            startActivity(intBookingConfirmation);
            */
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCash.this,"Error","Something went wrong");
        }
    }

    @Override
    public void processFinish(String output) {
        try{
            dialogProg.dismiss();
            if(!output.split("\\|")[0].equals("1")){
                if(strGAction.equals("COMMIT")){
                    proceedConfirmation(output);
                }
                if(strGAction.equals("CANCELTRANS")){
                    if(strAppMode.equals("fnb")){
                        Intent intent = new Intent(PayByCash.this, FnbMain.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                    }
                    else if (strAppMode.equals("ticket")){
                        Intent intent = new Intent(PayByCash.this, Movies.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                    }
                    finish();
                }
            }
            else {
                ShowErrorDialog(PayByCash.this,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            //dialogErr.dismiss();
            ShowErrorDialog(PayByCash.this,"Error","Something went wrong");
        }
    }

    private void proceedConfirmation(String output) {
        try{
            String strTotal = "", strCat = "", strTotalAmount = "", strResp = "", strFSummData = "", strFnBPrintData = "", strTransNo = "";
            JCResp obJsonCommit = new Gson().fromJson(output, JCResp.class);
            if(obJsonCommit.getRESPONSECODE().equals("0")){
                strResp = obJsonCommit.getRESPONSEDATA();
                strFSummData = strResp.split("\\@")[0];
                strFnBPrintData = strResp.split("\\@")[1];
                Bundle pcBundle = new Bundle();
                strTotal = strFSummData.split("\\|")[8];
                if(strAppMode.equals("fnb")) {
                    Intent intBookingConfirmation = new Intent(PayByCash.this,FnbConfirmation.class);
                    pcBundle.putString("price",strTotal);
                    pcBundle.putString("printdata",strFnBPrintData);
                    pcBundle.putString("wcode",strWCODE);
                    pcBundle.putString("transno",strFSummData.split("\\|")[11]);
                    pcBundle.putString("bookingno",strFSummData.split("\\|")[10]);
                    pcBundle.putString("receiptno",strFSummData.split("\\|")[13]);
                    intBookingConfirmation.putExtra("jsonArray",strJsonArray);
                    intBookingConfirmation.putExtras(pcBundle);
                    startActivity(intBookingConfirmation);
                }
                else if(strAppMode.equals("ticket")){
                    pcBundle.putString("filmname",strFilmName);
                    pcBundle.putString("seats",strSeats);
                    pcBundle.putString("cinemaname",strCinemaname);
                    pcBundle.putString("showdatetime",strShowDateTime);
                    pcBundle.putString("qty",strQty);
                    pcBundle.putString("price",strTotal);
                    pcBundle.putString("screen",strScreenName);
                    pcBundle.putString("areacat",strAreaCat);
                    Intent intBookingConfirmation = new Intent(PayByCash.this,BookingConfirmation.class);
                    intBookingConfirmation.putExtras(pcBundle);
                    startActivity(intBookingConfirmation);
                }
            }
            else {
                ShowErrorDialog(PayByCash.this,"Error",obJsonCommit.getRESPONSEDATA());
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCash.this,"Error","Something went wrong");
        }
    }

    private void subCancelTrans() {
        String strFileContent = "";
        try{
            ShowProgressDialog(PayByCash.this,"Going back","");
            strGAction = "CANCELTRANS";
            SoapCall obSc = new SoapCall(PayByCash.this);
            obSc.delegate = this;
            obSc.execute("CANCELTRANS", strIP, strTempTransId, "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCash.this,"Error","Something went wrong");
        }
    }


    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    subCancelTrans();
                }
            });
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCash.this,"Error","Something went wrong");
        }
    }

    public void backToPaymentSelection(View view) {
        try{
            //subCancelTrans();
            finish();
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCash.this,"Error","Something went wrong");
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogProg = mBuilder.create();
            dialogProg.show();

            dialogProg.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(PayByCash.this,"Error",Ex.toString());
        }
    }

    @Override
    public void onBackPressed() {

    }
}
