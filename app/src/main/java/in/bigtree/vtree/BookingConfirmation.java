package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.tscdll.TSCActivity;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import in.bigtree.vtree.PayU_Printer.Printer;

public class BookingConfirmation extends AppCompatActivity {
    String strFilmName = "", strPrintDataFromIntent = "", strQty = "", strDateTime = "", strCinemaName = "", strBookingId = "", strCat = "", strScreen = "", strSeats = "", strTotal = "", strPrint = "";
    TextView tvFilmName, tvFilmDesc, tvTkQty, tvDateTime, tvCinemaName, tvCat, tvScreen, tvSeatQty, tvSeats, tvTotal, tvBid;
    Timer t;
    LinearLayout llBack;
    AlertDialog dialogProg;
    SharedPreferences obPreferences;
    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_confirmation);
        try{
            obPreferences = getSharedPreferences(getResources().getString(R.string.appPrefs), Context.MODE_PRIVATE);
            tvFilmName = (TextView)findViewById(R.id.tvFilmName);
            //tvFilmDesc = (TextView)findViewById(R.id.tvTkLang);
            tvTkQty = (TextView)findViewById(R.id.tvTkQty);
            tvDateTime = (TextView)findViewById(R.id.tvDateTime);
            //tvCinemaName = (TextView)findViewById(R.id.tvCinema);
            tvCat = (TextView)findViewById(R.id.tvCat);
            //tvScreen = (TextView)findViewById(R.id.tvScreen);
            tvSeatQty = (TextView)findViewById(R.id.tvSeatQty);
            tvSeats = (TextView)findViewById(R.id.tvSeats);
            tvTotal = (TextView)findViewById(R.id.tvTotal);
            tvBid = (TextView)findViewById(R.id.tvBID);

            llBack = (LinearLayout) findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToHome(v));
            btnBack = (Button)findViewById(R.id.bBack);
            btnBack.setOnClickListener((v) -> backToHome(v));

            Bundle pcBundle = getIntent().getExtras();
            strFilmName = pcBundle.getString("filmname");
            strQty = pcBundle.getString("qty");
            strDateTime = pcBundle.getString("showdatetime");
            strCinemaName = pcBundle.getString("cinemaname");
            strBookingId = pcBundle.getString("bookingid");
            strPrint = pcBundle.getString("print");
            //strCat = pcBundle.getString("areacat");
            //strScreen = pcBundle.getString("screen");
            strSeats = pcBundle.getString("seats");
            strTotal = pcBundle.getString("price");
            strPrintDataFromIntent = pcBundle.getString("printData");

            tvFilmName.setText(strFilmName);
            //tvCinemaName.setText(strCinemaName);
            //tvScreen.setText(strScreen);
            tvTkQty.setText(strQty);
            tvBid.setText(strBookingId);
            //tvSeatQty.setText("(" + strQty + ")seats");
            tvTotal.setText(strTotal);
            //tvSeats.setText(strSeats);
            //tvCat.setText(strCat);
            tvDateTime.setText(strDateTime);

            strFilmName = strPrintDataFromIntent.split("\\|")[1];

            //strDateTime = strDateTime.split("\\|")[0] + "|" + strPrintDataFromIntent.split("\\|")[4];
            //Print logic

            if(strPrint.equals("Y")) {
                String printData = strFilmName + "|" + strDateTime +"|"+ strQty + "|" + strBookingId;
                Context ctx = getApplicationContext();
                SharedVariable obShared = ((SharedVariable) ctx);
                ShowProgressDialog(ctx, "Printing", "This should take less than a minute");
                if (obShared.getIbPrinting() == true) {
                    Printer printer = new Printer(ctx);
                    printer.Print(printData);
                }
                if (obShared.getPrintChecked() == true) {
                    String strPrinterName = obPreferences.getString("printer", "");
                    ShowProgressDialog(ctx, "Printing", "Connecting to "+strPrinterName);
                    in.bigtree.vtree.tsc_bt.Printer btPrinter = new in.bigtree.vtree.tsc_bt.Printer(ctx);
                    btPrinter.Print(strPrinterName,printData);
                }
                if(dialogProg != null){dialogProg.dismiss();}
            }
            initTimer();
        }
        catch (Exception Ex){
            ShowErrorDialog(BookingConfirmation.this,"Error",Ex.getMessage());
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogProg = mBuilder.create();
            dialogProg.show();

            dialogProg.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error",Ex.toString());
        }
    }

    private void initTimer() {
        try{
            //final Timer t = new Timer();
            t = new Timer();
            TimerTask tt = new TimerTask() {
                @Override
                public void run() {
                    scheduledBackToHome();
                };
            };
            t.schedule(tt,4000);
        }
        catch (Exception Ex){
            ShowErrorDialog(BookingConfirmation.this,"Error","Something went wrong");
        }
    }

    private void scheduledBackToHome() {
        try{
            t.cancel();
            t.purge();
            Intent intent = new Intent(BookingConfirmation.this, Movies.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        }
        catch (Exception Ex){
            ShowErrorDialog(BookingConfirmation.this,"Error","Something went wrong");
        }
    }

    public void backToHome(View v){
        try{
            t.cancel();
            t.purge();
            Intent intent = new Intent(BookingConfirmation.this, Movies.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        }
        catch (Exception Ex){
            ShowErrorDialog(BookingConfirmation.this,"Error","Something went wrong");
        }
    }

    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    finish();

                }
            });
        }
        catch (Exception Ex){
            Toast.makeText(BookingConfirmation.this,Ex.toString(), Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onBackPressed() {

    }

}
