package in.bigtree.vtree;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mswipetech.wisepad.sdk.Print;

public class ReprintDetailsCon extends AppCompatActivity {

    TextView tvBID,tvFnbPaymentTotal;
    AlertDialog dialogErr;
    LinearLayout llBack,llPaidItems;
    String strBookingNo, strItemField, strTotalPrice, strWCode,gstinfo, strReceiptNo, strTransNo;
    Context ctx;
    SharedPreferences obPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reprint_details_con);
        try{
            tvBID = (TextView)findViewById(R.id.tvBID);
            tvFnbPaymentTotal = (TextView)findViewById(R.id.tvFnbPaymentTotal);
            llBack = (LinearLayout)findViewById(R.id.llBack);
            llPaidItems = (LinearLayout)findViewById(R.id.llPaidItems);
            llBack.setOnClickListener((v) -> backToLastTrans(v));
            Bundle rtBundle = getIntent().getExtras();
            strBookingNo = rtBundle.getString("bookingno");
            strItemField = rtBundle.getString("items");
            strTotalPrice = rtBundle.getString("totalprice");
            strWCode = rtBundle.getString("wcode");
            gstinfo = rtBundle.getString("gstinfo");
            strReceiptNo = rtBundle.getString("receiptno");
            strTransNo = rtBundle.getString("transno");
            ctx = this;
            tvBID.setText(strBookingNo);
            tvFnbPaymentTotal.setText(strTotalPrice);
            showItemsSummary(strItemField);

        }
        catch (Exception Ex){

        }
    }

    private void showItemsSummary(String strItemField){
        try{
            String[] arrItemsField = strItemField.split("\\,");
            for(int iCount = 0; iCount < arrItemsField.length; iCount++){

                LinearLayout llItemRow = new LinearLayout(this);
                LinearLayout.LayoutParams llRowParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                llRowParams.setMargins((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()),(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics()),
                        (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()),(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics()));
                llItemRow.setLayoutParams(llRowParams);
                llItemRow.setPadding(0,0,0,(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics()));
                llItemRow.setBackground(ContextCompat.getDrawable(ctx,R.drawable.border_bottom));

                LinearLayout llItemName = new LinearLayout(this);
                llItemName.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,2.0f));

                Typeface tfPS = ResourcesCompat.getFont(ctx, R.font.panton_semibold);
                TextView tvItemName = new TextView(this);
                LinearLayout.LayoutParams tvItemNameParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                tvItemNameParams.setMarginStart((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()));
                tvItemName.setLayoutParams(tvItemNameParams);
                tvItemName.setTextSize(15);
                tvItemName.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvItemName.setTypeface(tfPS);
                tvItemName.setText(arrItemsField[iCount].split("\\~")[0]);

                llItemName.addView(tvItemName);
                llItemRow.addView(llItemName);

                LinearLayout llQty = new LinearLayout(this);
                llQty.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));
                llQty.setGravity(Gravity.CENTER);

                TextView tvQty = new TextView(this);
                tvQty.setText(arrItemsField[iCount].split("\\~")[1]);
                tvQty.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvQty.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvQty.setTextSize(15);
                tvQty.setTypeface(tfPS);

                llQty.addView(tvQty);
                llItemRow.addView(llQty);

                LinearLayout llPrice = new LinearLayout(this);
                LinearLayout.LayoutParams llPriceParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
                llPriceParams.setMarginEnd((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics()));
                llPrice.setLayoutParams(llPriceParams);
                llPrice.setGravity(Gravity.END);

                LinearLayout llPriceSymbParent = new LinearLayout(this);
                llPriceSymbParent.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                Typeface tfRM = ResourcesCompat.getFont(ctx, R.font.roboto_medium);

                TextView tvRsSymb = new TextView(this);
                tvRsSymb.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvRsSymb.setText(R.string.Rs);
                tvRsSymb.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvRsSymb.setTextSize(15);
                tvRsSymb.setTypeface(tfRM);

                TextView tvPrice = new TextView(this);
                tvPrice.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvPrice.setText(arrItemsField[iCount].split("\\~")[5]);
                tvPrice.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvPrice.setTextSize(16);
                tvPrice.setTypeface(tfPS);

                llPriceSymbParent.addView(tvRsSymb);
                llPriceSymbParent.addView(tvPrice);
                llPrice.addView(llPriceSymbParent);
                llItemRow.addView(llPrice);

                llPaidItems.addView(llItemRow);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(ReprintDetailsCon.this,"Error","Something went wrong");
        }
    }


    public void backToLastTrans(View view) {
        try{
            finish();
        }
        catch (Exception Ex){
            ShowErrorDialog(ReprintDetailsCon.this,"Error","Something went wrong");
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            Ex.toString();
        }
    }

    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    finish();

                }
            });
        }
        catch (Exception Ex){
            Toast.makeText(ReprintDetailsCon.this,Ex.toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void proceedReprint(View view) {
        try{
            String strCinemaName = "",strPrinterName="",strPrintChk="",strPrintData="";
            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            strPrintData = strItemField.replace(',','|');
            strPrintData = strPrintData.replace('~','_');
            strPrintData = strPrintData+"#"+gstinfo;
            if(obShared.getIbPrinting() == true){
                SharedVariable obShare = ((SharedVariable)getApplicationContext());
                strCinemaName = obShare.getCID();
                try {
                    //Print.StartPrinting();
                    MSwipePrinter obMSwipePrinter = new MSwipePrinter(getApplicationContext());
                    obMSwipePrinter.printVoucher(strPrintData, strCinemaName, strWCode, strTransNo,strBookingNo,strReceiptNo);
                }
                catch (RuntimeException iEx) {
                    Log.d("Inbuilt printer",iEx.toString());
                }
            }
            if(obShared.getPrintChecked() == true){
                ShowProgressDialog(ctx,"Printing","This should take less than a minute");
                strPrinterName = obPreferences.getString("printer","");
                PrintFnB obPrint = new PrintFnB();
                strPrintChk = obPrint.checkDevice(ReprintDetailsCon.this,strPrinterName);
                if(strPrintChk.equals("")){
                    Toast.makeText(getApplicationContext(),"No printer found",Toast.LENGTH_SHORT).show();
                }
                else if(strPrintChk.split("\\|")[0].equals("0")){
                    SharedVariable obShare = ((SharedVariable)getApplicationContext());
                    strCinemaName = obShare.getCID();
                    obPrint.openDevice(strPrintData,strCinemaName,strWCode,strBookingNo,strPrinterName);
                }
                dialogErr.dismiss();
            }
        }
        catch (Exception Ex){
            Toast.makeText(ReprintDetailsCon.this,Ex.toString(), Toast.LENGTH_LONG).show();
        }
    }
}
