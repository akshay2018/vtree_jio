package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JCResp {
    @SerializedName("RESPONSE_CODE")
    @Expose
    private String rESPONSECODE;
    @SerializedName("RESPONSE_DATA")
    @Expose
    private String rESPONSEDATA;

    public String getRESPONSECODE() {
        return rESPONSECODE;
    }

    public void setRESPONSECODE(String rESPONSECODE) {
        this.rESPONSECODE = rESPONSECODE;
    }

    public String getRESPONSEDATA() {
        return rESPONSEDATA;
    }

    public void setRESPONSEDATA(String rESPONSEDATA) {
        this.rESPONSEDATA = rESPONSEDATA;
    }
}
