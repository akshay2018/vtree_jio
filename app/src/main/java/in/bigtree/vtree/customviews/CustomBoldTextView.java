package in.bigtree.vtree.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import in.bigtree.vtree.SharedVariable;

public class CustomBoldTextView extends TextView {
	SharedVariable applicationData;
	public CustomBoldTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		this.setTypeface(applicationData.fontBold);
	}
	
	public CustomBoldTextView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		this.setTypeface(applicationData.fontBold);
	}
	
}
