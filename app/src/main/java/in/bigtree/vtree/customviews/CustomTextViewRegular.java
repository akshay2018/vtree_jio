package in.bigtree.vtree.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import in.bigtree.vtree.SharedVariable;

public class CustomTextViewRegular extends TextView {
	SharedVariable applicationData;
	public CustomTextViewRegular(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		applicationData = (SharedVariable) context.getApplicationContext();
		this.setTypeface(applicationData.font);
	}

	public CustomTextViewRegular(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		applicationData = (SharedVariable) context.getApplicationContext();
		this.setTypeface(applicationData.font);
	}
	
}
