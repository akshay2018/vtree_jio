package in.bigtree.vtree.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import in.bigtree.vtree.SharedVariable;

public class CustomButton extends Button {
	SharedVariable applicationData;
	public CustomButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		applicationData = SharedVariable.getApplicationDataSharedInstance();this.setTypeface(applicationData.font);
	
	}
	
	public CustomButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		this.setTypeface(applicationData.font);
	}
	
}
