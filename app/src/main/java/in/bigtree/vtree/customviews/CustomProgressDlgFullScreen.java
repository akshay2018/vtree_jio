package in.bigtree.vtree.customviews;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.util.Logs;


public class CustomProgressDlgFullScreen extends Dialog
{

	String mTitle = "";
	private CustomProgressAnimView mProgressAnim;
	private TextView mTxttTitle;
	String mTitlieProcessing;
	private TextView mTxtProcessing;
	boolean mShowCancel = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_dialog_processings);

		mProgressAnim = (CustomProgressAnimView) findViewById(R.id.progess_dlg_CPA_processing);

		mTxttTitle = (TextView)findViewById(R.id.progess_dlg_TXT_title);
		mTxtProcessing = (TextView)findViewById(R.id.progess_dlg_LBL_processing);
		mTxtProcessing.setText(mTitlieProcessing);

		if(mShowCancel)
			((RelativeLayout)findViewById(R.id.progess_dlg_REL_cancel)).setVisibility(View.VISIBLE);
		else
			((RelativeLayout)findViewById(R.id.progess_dlg_REL_cancel)).setVisibility(View.GONE);


		mTxttTitle.setText(mTitle);
		this.setCancelable(false);
		this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

	}

	public CustomProgressDlgFullScreen(Context context, String title) {
		super(context, R.style.StyleCustomProgressDlg_white);
		this.mTitle = title;
		this.mTitlieProcessing = (context.getResources().getString(R.string.processing));
		this.getWindow().setBackgroundDrawable(new ColorDrawable(0));

	}

	public CustomProgressDlgFullScreen(Context context) {
		super(context, R.style.StyleCustomProgressDlg_white);
		this.mTitlieProcessing = (context.getResources().getString(R.string.creditsalesignatureactivity_processing_chargeslip));
		this.getWindow().setBackgroundDrawable(new ColorDrawable(0));

	}

	public CustomProgressDlgFullScreen(Context context, String title, String message) {
		super(context, R.style.StyleCustomProgressDlg_white);
		this.mTitle = title;
		this.mTitlieProcessing = message;
		this.getWindow().setBackgroundDrawable(new ColorDrawable(0));

	}

	public CustomProgressDlgFullScreen(Context context, String title, String message, boolean showCancel) {
		super(context, R.style.StyleCustomProgressDlg_white);
		this.mTitle = title;
		this.mTitlieProcessing = message;
		this.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		this.mShowCancel = showCancel;

	}

	public void setProgress(int percentage) {

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "values " +percentage, true, true);


		mTxtProcessing.setText("processing ("+percentage+"%)");


	}


	public void setProcessMessage(String msg){

		((TextView)findViewById(R.id.progess_dlg_LBL_processing)).setText(msg);
	}

	@Override
	public void show() {
		super.show();
		mProgressAnim.startAnimation();
	}

	@Override
	public void dismiss() {
		mProgressAnim.stopAnimation();
		super.dismiss();
	}

	@Override
	public void hide() {
		mProgressAnim.stopAnimation();
		super.hide();
	}
}
