package in.bigtree.vtree.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

import in.bigtree.vtree.SharedVariable;

public class CustomCheckBox extends CheckBox {
	SharedVariable applicationData;
	public CustomCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		this.setTypeface(applicationData.font);
	}
	
	public CustomCheckBox(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		this.setTypeface(applicationData.font);
	}
}
