package in.bigtree.vtree.customviews;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.bigtree.vtree.R;


public class CardSaleCustomDlg extends Dialog implements android.view.View.OnClickListener
{

	private Button btnOk= null;
	private String status = "";
	private String authcode ="";
	private String rrno= "";
	private String title = "";
	private  boolean misCardSale = true;
	
	
	public CardSaleCustomDlg(Context context, String title, String status, String authcode, String rrno, boolean isCardSale){
		super(context,R.style.styleCustDlg);
		
		this.status = status;
		this.authcode = authcode;
		this.rrno  =rrno;
		this.title=title;
		this.misCardSale = isCardSale;
		//this.setTitle(title);
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(misCardSale)
			setContentView(R.layout.customcardsaledlg);
		else
			setContentView(R.layout.cashsaledlg);
        this.setCanceledOnTouchOutside(false);
		initUI();	
		
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId()) {
		
			case R.id.cardsalecustomdlg_BTN_ok:
				CardSaleCustomDlg.this.dismiss();
				break;
	
		}
	}


    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {

            return true;

        }else{
            return super.onKeyDown(keyCode, event);
        }
    }

	private void initUI(){
		TextView lblTitle = (TextView) findViewById(R.id.cardsalecustomdlg_LBL_title);
		lblTitle.setText(title);
		

		btnOk=(Button)findViewById(R.id.cardsalecustomdlg_BTN_ok);
		btnOk.setOnClickListener(this);
		
		TextView lblStatus =(TextView)findViewById(R.id.cardsalecustomdlg_LBL_TxStatus);
	
		
		if(status.toString().equalsIgnoreCase("approved"))
		{
			lblStatus.setTextColor(Color.rgb(0,176,80));
			
		}else{
			lblStatus.setTextColor(Color.rgb(255,0,0));
		}
		lblStatus.setText(status);
		
		TextView lblAuthCode =(TextView)findViewById(R.id.cardsalecustomdlg_LBL_AuthNo);
		lblAuthCode.setText(authcode);
		
		TextView lblRrno =(TextView)findViewById(R.id.cardsalecustomdlg_LBL_RRNo);
		lblRrno.setText(rrno);
		


	}

}
