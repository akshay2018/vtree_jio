package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JSessions {

    @SerializedName("SYNC_RESPONSE_CODE")
    @Expose
    private String sYNCRESPONSECODE;
    @SerializedName("SYNC_RESPONSE_DATA")
    @Expose
    private SYNCRESPONSEDATA sYNCRESPONSEDATA;

    public String getSYNCRESPONSECODE() {
        return sYNCRESPONSECODE;
    }

    public void setSYNCRESPONSECODE(String sYNCRESPONSECODE) {
        this.sYNCRESPONSECODE = sYNCRESPONSECODE;
    }

    public SYNCRESPONSEDATA getSYNCRESPONSEDATA() {
        return sYNCRESPONSEDATA;
    }

    public void setSYNCRESPONSEDATA(SYNCRESPONSEDATA sYNCRESPONSEDATA) {
        this.sYNCRESPONSEDATA = sYNCRESPONSEDATA;
    }
}