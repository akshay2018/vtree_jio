package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import static android.view.Gravity.CENTER;
import static android.view.Gravity.CENTER_HORIZONTAL;
import static android.view.Gravity.CENTER_VERTICAL;

public class MovieFragment extends Fragment {
    String[] arrListItems;
    String strSessDate = "", strJson = "";
    AbsListView listview;
    private List<Film> listFilms;
    public Context context;
    public List<Film> FilmList;

    public MovieFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_movie_list,container,false);
        try {
            listview = (AbsListView) view.findViewById(R.id.list);
            Bundle bundle = this.getArguments();
            List<Film> FilmList = (List<Film>) getArguments().getSerializable("FilmList");
            strSessDate = getArguments().getString("sDate");
            strJson = getArguments().getString("strjson");
            getArguments().remove("FilmList");
            getArguments().remove("sDate");
            getArguments().remove("strjson");

            CustomListAdapter adapter = new CustomListAdapter(getActivity(), FilmList, strSessDate);
            listview.setAdapter(adapter);
/*
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String strItemTag = "", strFilmCode = "", strFilmName = "", strEInfo = "", strSessDate = "";
                    strItemTag = view.getTag().toString();
                    strFilmCode = strItemTag.split("\\_")[0];
                    strFilmName = strItemTag.split("\\_")[1];
                    strEInfo = strItemTag.split("\\_")[2];
                    strSessDate = strItemTag.split("\\_")[3];

                    Bundle fBundle = new Bundle();
                    fBundle.putString("fcode", strFilmCode);
                    fBundle.putString("fname", strFilmName);
                    fBundle.putString("finfo", strEInfo);
                    fBundle.putString("sdate",strSessDate);
                    fBundle.putString("strjson",strJson);

                    Intent intentShow = new Intent(parent.getContext(), Shows.class);
                    intentShow.putExtras(fBundle);
                    startActivity(intentShow);
                }
            });

 */
        }
        catch (Exception Ex){
            Ex.toString();
        }
        return view;
    }
}

 class CustomListAdapter extends BaseAdapter{

     Context mContext;
     String[] arrListItems;
     String strCinemaName;
     String strSessDate;
     public List<Film> FilmList;
     GridLayout grdFragShow;
     public CustomListAdapter(FragmentActivity context, List<Film> FilmList, String strSessDate) {
         //this.strCinemaName = (String) ((Movies) context).tvCinema.getText();
         this.mContext = context;
         this.FilmList = FilmList;
         this.strSessDate = strSessDate;
     }

    @Override
    public int getCount() {
        return FilmList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.fragment_movie,null);
        grdFragShow = (GridLayout) convertView.findViewById(R.id.grd_inline_show);
        final TextView tvMName = (TextView)convertView.findViewById(R.id.tvMovieName);
        //final TextView tvLangGenre = (TextView)convertView.findViewById(R.id.tvMovieGenreLang);
        final LinearLayout llParentItem = (LinearLayout)convertView.findViewById(R.id.llItemContainer);



        String strMovieName = "", strLang = "", strFilmCode = "";
        strMovieName = FilmList.get(position).getFilmName();
        strLang = FilmList.get(position).getFilmDuration();
        strFilmCode = FilmList.get(position).getFilmCode();

        llParentItem.setTag(strFilmCode + "_" + strMovieName + "_" + strLang + "_" + strSessDate);

        tvMName.setText(strMovieName);
        //tvLangGenre.setText(strLang);

        for(int s=0; s<FilmList.get(position).getShowTimes().size();s++){
            String showDetails = "";


            //Typeface tsemibold = ResourcesCompat.getFont(mContext,R.font.calibre_semibold);
            Typeface tMontserrat_Bold = ResourcesCompat.getFont(mContext,R.font.montserrat_bold);
            showDetails = FilmList.get(position).getShowTimes().get(s).getShowDetails();
            LinearLayout llShowFragParent = new LinearLayout(mContext);
            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, mContext.getResources().getDisplayMetrics());
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, mContext.getResources().getDisplayMetrics());
            int marginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, mContext.getResources().getDisplayMetrics());
            int marginSides = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, mContext.getResources().getDisplayMetrics());
            LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(width,height);
            lParams.setMargins(marginSides,marginTop,marginSides,marginTop);
            llShowFragParent.setLayoutParams(lParams);
            llShowFragParent.setBackgroundResource(R.drawable.ripple);
            llShowFragParent.setOrientation(LinearLayout.VERTICAL);
            llShowFragParent.setGravity(CENTER_HORIZONTAL);

            LinearLayout llTextContainer = new LinearLayout(mContext);
            llTextContainer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            llTextContainer.setOrientation(LinearLayout.VERTICAL);
            llTextContainer.setGravity(CENTER);

            TextView tvShowTime = new TextView(mContext);
            tvShowTime.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            tvShowTime.setText(showDetails.split("\\|")[1]);
            tvShowTime.setTextSize(16);
            tvShowTime.setTypeface(tMontserrat_Bold);
            LinearLayout.LayoutParams tParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            int marginTTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, mContext.getResources().getDisplayMetrics());
            tParams.setMargins(0,marginTTop,0,0);
            tvShowTime.setLayoutParams(tParams);
            tvShowTime.setTextColor(ContextCompat.getColor(mContext,R.color.colorText));

            TextView tvAvailability = new TextView(mContext);
            tvAvailability.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            if((showDetails.split("\\|")[3].equals("A"))){
                tvAvailability.setText("AVAILABLE");
                tvAvailability.setTextColor(ContextCompat.getColor(mContext,R.color.colorJwcGreen));
            }
            tvAvailability.setTextSize(10);
            tvAvailability.setLetterSpacing(0.1f);
            tvAvailability.setTypeface(tMontserrat_Bold);
            LinearLayout.LayoutParams tvAvailParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            int marginTAvailTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, mContext.getResources().getDisplayMetrics());
            tvAvailParams.setMargins(0,marginTAvailTop,0,0);
            tvAvailability.setLayoutParams(tvAvailParams);

            /*
            LinearLayout llBottomPart = new LinearLayout(mContext);
            int marginTopBPart = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, mContext.getResources().getDisplayMetrics());
            LinearLayout.LayoutParams lBParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            lBParams.setMargins(0,marginTopBPart,0,0);
            llBottomPart.setLayoutParams(lBParams);
            llBottomPart.setBackgroundResource(R.drawable.border_show_top);
            //llBottomPart.setOrientation(LinearLayout.VERTICAL);
            llBottomPart.setGravity(CENTER_VERTICAL);

            TextView tvScreenName = new TextView(mContext);
            tvScreenName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            tvScreenName.setText(showDetails.split("\\|")[2]);
            tvScreenName.setTextSize(12);
            tvScreenName.setTypeface(tRegular);
            tvScreenName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            tvScreenName.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
            tvScreenName.setLetterSpacing(0.13f);
            if(!(showDetails.split("\\|")[3].equals("A"))){
                tvScreenName.setTextColor(ContextCompat.getColor(mContext,R.color.colorJwcRed));
            }
            else {

            }
            */
            //llBottomPart.addView(tvScreenName);
            llTextContainer.addView(tvShowTime);
            llTextContainer.addView(tvAvailability);
            llShowFragParent.addView(llTextContainer);
            //llShowFragParent.addView(llBottomPart);
            llShowFragParent.setTag(showDetails);
            grdFragShow.addView(llShowFragParent);

            String finalStrMovieName = strMovieName;
            llShowFragParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String strSLayout = "";
                    String strSessionId = view.getTag().toString().split("\\|")[0];
                    String strShowTime = view.getTag().toString().split("\\|")[1];
                    String strScreen = view.getTag().toString().split("\\|")[2];
                    String strShowDate = strSessDate.split("\\|")[1];
                    String isFreeSeating = view.getTag().toString().split("\\|")[6];
                    String ticketTypeCodes = view.getTag().toString().split("\\|",-1)[7];
                    FileIO obFileIO = new FileIO();
                    String strFileContent = obFileIO.readFile(mContext.getResources().getString(R.string.advance_config_file),mContext);
                    if(strFileContent.split("\\|").length > 0){ strSLayout = strFileContent.split("\\|")[0]; }
                    if(isFreeSeating.equals("Y")){
                        showFreeSeatingBottomSheetDialog(mContext,ticketTypeCodes, strSessionId, strCinemaName, strShowTime, finalStrMovieName, strShowDate, strScreen);
                        //if (parent.getContext() instanceof Movies) {
                        //    ((Movies) mContext).passDataToActivity(ticketTypeCodes, strSessionId, strCinemaName, strShowTime, finalStrMovieName, strShowDate, strScreen);
                        //}
                        //showFreeSeatingDialog(mContext, ticketTypeCodes, strSessionId, strCinemaName, strShowTime, finalStrMovieName, strShowDate, strScreen);
                    }
                    else {
                        switch (strSLayout) {
                            case "full":
                                GoToSeatLayout(mContext, strSessionId, strCinemaName, strShowTime, strScreen, finalStrMovieName, strShowDate);
                                break;
                            case "partial":
                                GoToTicketTypeSelection(mContext, strSessionId, strCinemaName, strShowTime, strScreen, finalStrMovieName, strShowDate);
                                break;
                            case "":
                                GoToSeatLayout(mContext, strSessionId, strCinemaName, strShowTime, strScreen, finalStrMovieName, strShowDate);
                                break;
                        }
                    }
                }
            });

        }

        return convertView;
    }

    public void showFreeSeatingBottomSheetDialog(Context ctx, String ticketTypeCodes, String strSessionId, String strCinemaName, String strShowTime, String strEventName, String strShowDate, String strScreen){
         try {
             final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(ctx);
             bottomSheetDialog.setContentView(R.layout.bottom_freeseating_popup);
             Button btnProceed;
             btnProceed = (Button) bottomSheetDialog.findViewById(R.id.btnProceedFreeSeating);
             LinearLayout llFSeatQty = (LinearLayout)bottomSheetDialog.findViewById(R.id.llFSeatQty);
             llFSeatQty.removeAllViews();
             RadioGroup rdGrpTTpye = (RadioGroup)bottomSheetDialog.findViewById(R.id.rdGrpTTpye);
             String[] arrTTypes = ticketTypeCodes.split("\\$");
             final String[] TQty = {""};
             final String[] TType = {""};
             for(int t = 0; t < arrTTypes.length; t++){
                 //RadioButton rdTButton = new RadioButton(ctx,null,R.style.Widget_AppCompat_CompoundButton_CheckBox);
                 RadioButton rdTButton = new RadioButton(ctx,null);
                 rdTButton.setId(t+999);
                 rdTButton.setTag(arrTTypes[t].split("\\~")[0]);
                 rdTButton.setText(arrTTypes[t].split("\\~")[1]);
                 if(t == 0){
                     rdTButton.setChecked(true);
                 }
                 else {
                     RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                     params.setMargins(10, 0, 0, 0);
                     rdTButton.setLayoutParams(params);
                 }
                 rdGrpTTpye.addView(rdTButton);
             }
             View vg = (ViewGroup)llFSeatQty;
             for(int q = 1; q <= 8; q++){
                 LinearLayout llQtyP = new LinearLayout(ctx);
                 int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, mContext.getResources().getDisplayMetrics());
                 llQtyP.setLayoutParams(new LinearLayout.LayoutParams(width,width));
                 llQtyP.setTag(q+"");

                 TextView txtQty = new TextView(ctx);
                 txtQty.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                 txtQty.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER);
                 txtQty.setText(q+"");
                 txtQty.setTag("tqty_"+q);
                 txtQty.setTextSize(16);
                 txtQty.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                 if(q==2){
                     TQty[0] = "2";
                     llQtyP.setBackground(ContextCompat.getDrawable(ctx,R.drawable.back_btn_primary));
                     txtQty.setTextColor(ContextCompat.getColor(ctx,R.color.colorWhite));
                 }
                 llQtyP.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         LinearLayout llPrevSelected = (vg).findViewWithTag(TQty[0]+"");
                         llPrevSelected.setBackground(ContextCompat.getDrawable(ctx,R.color.colorWhite));
                         TextView tOldView = (vg).findViewWithTag("tqty_"+TQty[0]);
                         tOldView.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));

                         TQty[0] = (String)view.getTag();
                         view.setBackground(ContextCompat.getDrawable(ctx,R.drawable.back_btn_primary));
                         TextView tNewView = (vg).findViewWithTag("tqty_"+TQty[0]);
                         tNewView.setTextColor(ContextCompat.getColor(ctx, R.color.colorWhite));

                     }
                 });
                 llQtyP.addView(txtQty);
                 llFSeatQty.addView(llQtyP);
             }

             btnProceed.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     int selectedId = rdGrpTTpye.getCheckedRadioButtonId();
                     // find the radiobutton by returned id
                     RadioButton rdButtonSelected = (RadioButton) bottomSheetDialog.findViewById(selectedId);
                     TType[0] = (String) rdButtonSelected.getTag();
                     Log.d("","Qty :" + TQty[0] + ", TType : " + TType[0]);
                     bottomSheetDialog.dismiss();
                     ProceedToFreeSeating(ctx, TQty[0], TType[0], strSessionId, strCinemaName, strShowTime, strScreen, strEventName, strShowDate);
                 }
             });

             bottomSheetDialog.show();
         }
         catch (Exception Ex){

         }
    }


     public void showFreeSeatingDialog(Context ctx, String ticketTypeCodes, String strSessionId, String strCinemaName, String strShowTime, String strFilmName, String strShowDate, String strScreen){
         AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
         try{
             Button btnProceed;
             LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             View mView = inflater.inflate(R.layout.free_seating_layout, null);
             btnProceed = (Button) mView.findViewById(R.id.btnProceed);
             LinearLayout llFSeatQty = (LinearLayout)mView.findViewById(R.id.llFSeatQty);
             llFSeatQty.removeAllViews();
             RadioGroup rdGrpTTpye = (RadioGroup)mView.findViewById(R.id.rdGrpTTpye);
             String[] arrTTypes = ticketTypeCodes.split("\\$");
             final String[] TQty = {""};
             final String[] TType = {""};
             for(int t = 0; t < arrTTypes.length; t++){
                 //RadioButton rdTButton = new RadioButton(ctx,null,R.style.Widget_AppCompat_CompoundButton_CheckBox);
                 RadioButton rdTButton = new RadioButton(ctx,null);
                 rdTButton.setId(t+999);
                 rdTButton.setTag(arrTTypes[t].split("\\~")[0]);
                 rdTButton.setText(arrTTypes[t].split("\\~")[1]);
                 if(t == 0){
                     rdTButton.setChecked(true);
                 }
                 else {
                     RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                     params.setMargins(10, 0, 0, 0);
                     rdTButton.setLayoutParams(params);
                 }
                 rdGrpTTpye.addView(rdTButton);
             }
             for(int q = 1; q <= 8; q++){
                 LinearLayout llQtyP = new LinearLayout(ctx);
                 int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, mContext.getResources().getDisplayMetrics());
                 llQtyP.setLayoutParams(new LinearLayout.LayoutParams(width,width));
                 llQtyP.setTag(q+"");

                 TextView txtQty = new TextView(ctx);
                 txtQty.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                 txtQty.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER);
                 txtQty.setText(q+"");
                 txtQty.setTag("tqty_"+q);
                 txtQty.setTextSize(16);
                 txtQty.setTextColor(ContextCompat.getColor(ctx,R.color.black));
                 if(q==2){
                     TQty[0] = "2";
                     llQtyP.setBackground(ContextCompat.getDrawable(ctx,R.drawable.btn_curved_back_filled));
                     txtQty.setTextColor(ContextCompat.getColor(ctx,R.color.colorWhite));
                 }
                 llQtyP.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         LinearLayout llPrevSelected = (mView).findViewWithTag(TQty[0]+"");
                         llPrevSelected.setBackground(ContextCompat.getDrawable(ctx,R.color.colorWhite));
                         TextView tOldView = (mView).findViewWithTag("tqty_"+TQty[0]);
                         tOldView.setTextColor(ContextCompat.getColor(ctx,R.color.black));

                         TQty[0] = (String)view.getTag();
                         view.setBackground(ContextCompat.getDrawable(ctx,R.drawable.btn_curved_back_filled));
                         TextView tNewView = (mView).findViewWithTag("tqty_"+TQty[0]);
                         tNewView.setTextColor(ContextCompat.getColor(ctx, R.color.colorWhite));

                     }
                 });
                 llQtyP.addView(txtQty);
                 llFSeatQty.addView(llQtyP);
             }
             mBuilder.setView(mView);
             final AlertDialog dialogErr = mBuilder.create();
             btnProceed.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     int selectedId = rdGrpTTpye.getCheckedRadioButtonId();
                     // find the radiobutton by returned id
                     RadioButton rdButtonSelected = (RadioButton) mView.findViewById(selectedId);
                     TType[0] = (String) rdButtonSelected.getTag();
                     Log.d("","Qty :" + TQty[0] + ", TType : " + TType[0]);
                     dialogErr.dismiss();
                     ProceedToFreeSeating(ctx, TQty[0], TType[0], strSessionId, strCinemaName, strShowTime, strScreen, strFilmName, strShowDate);
                 }
             });


             dialogErr.show();
         }
         catch (Exception Ex){
             Ex.toString();
         }
     }

     private void ProceedToFreeSeating(Context ctx, String qty, String strTktType, String strSessionId, String strCinemaName, String strShowTime, String strScreenName, String strFilmName, String strShowDate) {
         try{
             Bundle stBundle = new Bundle();
             stBundle.putString("cinemaname",strCinemaName);
             stBundle.putString("showtime",strShowTime);
             stBundle.putString("screen",strScreenName);
             stBundle.putString("filmname",strFilmName);
             stBundle.putString("areacat","");
             stBundle.putString("ttype",strTktType); // tickettype code
             stBundle.putString("seats","");
             stBundle.putString("qty",qty);
             stBundle.putString("temptransid","");
             stBundle.putString("sessionid",strSessionId);
             stBundle.putString("seatinfo","");
             stBundle.putString("showdate",strShowDate);
             stBundle.putString("ispackage","");
             stBundle.putString("isFreeSeating","Y");

             Intent intConfirm = new Intent(ctx,PaySelectionConfirmation.class);
             intConfirm.putExtras(stBundle);
             mContext.startActivity(intConfirm);
         }
         catch (Exception Ex){
             //ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
         }
     }

     private void GoToSeatLayout(Context context, String strSessionId, String strCinemaName, String strShowTime, String strScreen, String strFilmName, String strShowDate) {
         try{
             Bundle stBundle = new Bundle();
             stBundle.putString("sessionid",strSessionId);
             stBundle.putString("cinemaname",strCinemaName);
             stBundle.putString("showtime",strShowTime);
             stBundle.putString("screen",strScreen);
             stBundle.putString("filmname",strFilmName);
             stBundle.putString("showdate",strShowDate);
             stBundle.putString("stype","full");
             Intent intSeatLayout = new Intent(context,SeatLayout.class);
             intSeatLayout.putExtras(stBundle);
             mContext.startActivity(intSeatLayout);
         }
         catch (Exception Ex){
             Ex.toString();
         }
     }

     private void GoToTicketTypeSelection(Context context, String strSessionId, String strCinemaName, String strShowTime, String strScreen, String strFilmName, String strShowDate) {
         try{
             Bundle stBundle = new Bundle();
             stBundle.putString("sessionid",strSessionId);
             stBundle.putString("cinemaname",strCinemaName);
             stBundle.putString("showtime",strShowTime);
             stBundle.putString("screen",strScreen);
             stBundle.putString("filmname",strFilmName);
             stBundle.putString("showdate",strShowDate);
             Intent intSeatLayout = new Intent(context,TicketTypeSelection.class);
             intSeatLayout.putExtras(stBundle);
             mContext.startActivity(intSeatLayout);
         }
         catch (Exception Ex){
             Ex.toString();
         }
     }

     static class ViewHolder{

     }
}



