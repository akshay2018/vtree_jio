package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class Movies extends AppCompatActivity implements AsyncResponse {
    TabLayout tbDateLayout;
    //MovieViewAdapter adapter;
    //DateAdapter dtAdapter;
    //MovieFragment fragment;
    //TextView tvCinema;
    FragmentAdapter adapter;
    ViewPager mPager;
    String strJsonSession="", strIP = "", strFileContent = "", strFileName = "", strGAction = "";
    SoapCall obSc;
    LinearLayout llBack;
    private BottomSheetBehavior bottomSheetBehavior;
    //private int[] intLayout = {R.layout.sample_view};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);

        try{
            tbDateLayout = (TabLayout)findViewById(R.id.tbDateLayout);
            llBack = (LinearLayout) findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToMain(v));
            //tvCinema = (TextView)findViewById(R.id.tvCinema);
            FileIO obFile = new FileIO();
            strFileContent = obFile.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];

            //strJsonSession = obFile.readLocalJson(Movies.this,"sessions");
            fetchData();

            bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomfreeseating));
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });
            //initGetData();
        }
        catch (Exception Ex){
            ShowErrorDialog(Movies.this,"Error","Something went wrong");
        }
    }

    private void fetchData() {
        try{
            if(strIP.equals("")){
                Toast toast = Toast.makeText(getApplicationContext(), "URL empty, check settings.", Toast.LENGTH_LONG);
                return;
            }
            strGAction = "SESSION";
            obSc = new SoapCall(Movies.this);
            obSc.delegate = this;
            obSc.execute("SESSIONS", strIP, "", "", "","","","","","","","","","","","","");

        }
        catch (Exception Ex){
            ShowErrorDialog(Movies.this,"Error","Something went wrong");
        }
    }

    private void initGetData(String output) {
        try{
            String strTabName = "", strCinemaName = "", strCinemaBranch = "";
            int intShowDateCount = 0;
            mPager = (ViewPager)findViewById(R.id.vpPager);
            if (!output.split("\\|")[0].equals("1")) {
                JSessions obJsonSession = new Gson().fromJson(output, JSessions.class);
                if(obJsonSession.getSYNCRESPONSECODE().equals("0")) {
                    intShowDateCount = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().size();
                    strCinemaName = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getName();
                    strCinemaBranch = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getBranch();
                    SharedVariable obShared = ((SharedVariable)getApplicationContext());
                    obShared.setBranchCode(strCinemaBranch);

                    //tvCinema.setText(strCinemaName);
                    for (int i = 0; i < intShowDateCount; i++) {
                        strTabName = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(i).getSessionDate().split("\\|")[0];
                        tbDateLayout.addTab(tbDateLayout.newTab().setText(strTabName));
                    }
                    adapter = new FragmentAdapter(getSupportFragmentManager(), intShowDateCount, obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates(),output);
                    mPager.setAdapter(adapter);
                    int intLimit = adapter.getCount();
                    mPager.setOffscreenPageLimit(intLimit);

                    tbDateLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            mPager.setCurrentItem(tab.getPosition());
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {}

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {}
                    });

                    mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int i, float v, int i1) {}

                        @Override
                        public void onPageSelected(int i) {
                            tbDateLayout.getTabAt(i).select();
                        }

                        @Override
                        public void onPageScrollStateChanged(int i) {}
                    });
                }
                else{
                    ShowErrorDialog(Movies.this,"Error",obJsonSession.getSYNCRESPONSEDATA().toString());
                }
            }
            else{
                ShowErrorDialog(Movies.this,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(Movies.this,"Error",output + " | " + "Something went wrong");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //Bundle bundle;
        try {

        } catch (Exception Ex) {
            ShowErrorDialog(Movies.this,"Error","Something went wrong on new intent");
        }
    }

    public void backToMain(View view) {
        try{
            finish();
        }
        catch (Exception Ex){
            ShowErrorDialog(Movies.this,"Error","Something went wrong");
        }
    }

    @Override
    public void processFinish(String output) {
        try{
            if(strGAction.equals("SESSION")){
                initGetData(output);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(Movies.this,"Error","Something went wrong");
        }
    }


    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    Intent intent = new Intent(Movies.this, MainScreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                }
            });
        }
        catch (Exception Ex){
            Ex.toString();
        }
    }

    @Override
    public void onBackPressed() {

    }



}
