package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupRow {

    @SerializedName("rowId")
    @Expose
    private String rowId;
    @SerializedName("groupSeats")
    @Expose
    private List<GroupSeat> groupSeats = null;

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public List<GroupSeat> getGroupSeats() {
        return groupSeats;
    }

    public void setGroupSeats(List<GroupSeat> groupSeats) {
        this.groupSeats = groupSeats;
    }

}