package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cinema {

    @SerializedName("strCinemaCode")
    @Expose
    private String strCinemaCode;
    @SerializedName("strCinemaName")
    @Expose
    private String strCinemaName;
    @SerializedName("ItemDetails")
    @Expose
    private List<ItemDetail> itemDetails = null;
    @SerializedName("strCinemaBranch")
    @Expose
    private String strCinemaBranch;

    public String getStrCinemaCode() {
        return strCinemaCode;
    }

    public void setStrCinemaCode(String strCinemaCode) {
        this.strCinemaCode = strCinemaCode;
    }

    public String getStrCinemaBranch() {
        return strCinemaBranch;
    }

    public void setStrCinemaBranch(String strCinemaBranch) {
        this.strCinemaBranch = strCinemaBranch;
    }

    public String getStrCinemaName() {
        return strCinemaName;
    }

    public void setStrCinemaName(String strCinemaName) {
        this.strCinemaName = strCinemaName;
    }

    public List<ItemDetail> getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }

}