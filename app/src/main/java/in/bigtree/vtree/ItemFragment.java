package in.bigtree.vtree;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class ItemFragment extends Fragment {
    String strClassCode = "", strClassName="", strOSModifiedData = "";
    AbsListView listView;
    GridView gridView;
    public Context context;
    private List<Item> lstItems;
    public ItemFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_items_grid,container,false);
        try{
            //listView = (AbsListView)view.findViewById(R.id.listItems);
            gridView = (GridView) view.findViewById(R.id.gridItems);

            List<Item> ItemList = (List<Item>) getArguments().getSerializable("lstitems");
            strClassCode = getArguments().getString("classcode");
            strClassName = getArguments().getString("classname");
            strOSModifiedData = getArguments().getString("ordersummarydata");
            getArguments().remove("lstitems");
            getArguments().remove("classcode");
            getArguments().remove("classname");
            getArguments().remove("ordersummarydata");

            CustomListItemAdapter adapter = new CustomListItemAdapter(getActivity(),ItemList,strClassCode, strOSModifiedData);
            //listView.setAdapter(adapter);
            if(gridView.getAdapter() == null){ //Adapter not set yet.
                gridView.setAdapter(adapter);
            }
            else{ //Already has an adapter
                adapter.notifyDataSetChanged();
            }
        }
        catch (Exception Ex){
            Ex.toString();
        }
        return view;
    }
}

class CustomListItemAdapter extends BaseAdapter {

    Context mContext;
    private List<Item> lstItems;
    String strOSModifiedData;
    JSONArray jOSArray;
    JSONObject jOSObject;

    static class ViewHolder{
        TextView tvItemDesc;
        ImageView imgAdd, imgRemove;
        TextView tvItemPrice, tvQty, tvStockQty;
        LinearLayout llParent;
    }

    public CustomListItemAdapter(FragmentActivity context, List<Item> itemList, String strClassCode, String strOSModifiedData) {
        this.mContext = context;
        this.lstItems = itemList;
        this.strOSModifiedData = strOSModifiedData;
    }

    @Override
    public int getCount() {
        return lstItems.size();
    }

    @Override
    public Object getItem(int position) {
        //Log.d("Item Fragment","Item position: " + position + ", Item name: "+lstItems.get(position).getStrItemDesc());
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
         String strItemDesc="";
         String strItemPrice="";
         String strItemCode="";
         String strStockQuantity = "";
        try{
            //Log.d("Item Fragment","Item position: " + position + ", Item name: "+lstItems.get(position).getStrItemDesc());
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //if (itemView != null){ return itemView;}                  //this prevents view refreshing/add remove bug, but duplicates view
            //if(itemView == null){itemView = inflater.inflate(R.layout.fragment_item,null);}         //this doesnt duplicates view, but it causes refresh bug
            final ViewHolder viewHolder;
            if(itemView == null){
                itemView = inflater.inflate(R.layout.fragment_item_gridstyle,null);
                viewHolder = new ViewHolder();
                viewHolder.tvItemDesc = (TextView)itemView.findViewById(R.id.tvItemDesc);
                viewHolder.tvItemPrice = (TextView)itemView.findViewById(R.id.tvItemPrice);
                viewHolder.tvQty = (TextView)itemView.findViewById(R.id.tvItemQty);
                viewHolder.llParent = (LinearLayout) itemView.findViewById(R.id.lstItemParent);
                viewHolder.imgAdd = (ImageView)itemView.findViewById(R.id.ivAdd);
                viewHolder.imgRemove = (ImageView)itemView.findViewById(R.id.ivRemove);
                viewHolder.tvStockQty = (TextView) itemView.findViewById(R.id.tvStockQty);

                viewHolder.tvQty.setText("0");
                viewHolder.tvStockQty.setText(lstItems.get(position).getStrQuantity());
                viewHolder.llParent.setTag(strItemCode + "|" + strItemDesc + "|" + strItemPrice);

                if(!strOSModifiedData.equals("")){
                    jOSArray = new JSONArray(strOSModifiedData);
                    for(int iCount = 0; iCount < jOSArray.length(); iCount++) {
                        jOSObject = (JSONObject) jOSArray.get(iCount);
                        if(lstItems.get(position).getStrItemCode().equals(jOSObject.getString("itemid"))){
                            viewHolder.tvQty.setText(jOSObject.getString("qty"));
                            viewHolder.tvStockQty.setText(jOSObject.getString("liststockqty"));
                            viewHolder.imgRemove.setImageResource(R.drawable.remove_circle_active);
                        }
                    }
                }
                itemView.setTag(viewHolder);
            }
            else {
                int intListQ = 0;
                viewHolder = (ViewHolder)itemView.getTag();
                intListQ = Integer.parseInt(viewHolder.tvQty.getTag().toString());
                viewHolder.tvQty.setText(Integer.toString(intListQ));
                //itemView.setTag(viewHolder);
                //return itemView;
            }

            strItemDesc = lstItems.get(position).getStrItemDesc();
            strItemPrice = lstItems.get(position).getStrItemPrice();
            strItemCode = lstItems.get(position).getStrItemCode();
            viewHolder.tvItemDesc.setText(strItemDesc);
            viewHolder.tvItemPrice.setText(strItemPrice);
            final String finalStrItemCode = strItemCode;
            final String finalStrItemDesc = strItemDesc;
            final String finalStrItemPrice = strItemPrice;
            final String finalStockQty = lstItems.get(position).getStrQuantity();
            viewHolder.imgAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        viewHolder.imgRemove.setImageResource(R.drawable.remove_circle_active);
                        int intListQ = Integer.parseInt(viewHolder.tvQty.getText().toString());
                        double dblListStockQty = Double.parseDouble(viewHolder.tvStockQty.getText().toString());
                        intListQ = intListQ + 1;
                        /*
                        dblListStockQty = dblListStockQty - 1;

                        if(dblListStockQty < 0) {
                            return;
                        }
                        */
                        viewHolder.tvQty.setText(Integer.toString(intListQ));
                        viewHolder.tvQty.setTag(Integer.toString(intListQ));

                        viewHolder.tvStockQty.setText(Double.toString(dblListStockQty));
                        viewHolder.tvStockQty.setTag(Double.toString(dblListStockQty));
                        //callActivityMethod(mContext,"add",1,strItemCode,strItemDesc,strItemPrice);
                        if (mContext instanceof FnbMain) {
                            ((FnbMain) mContext).passDataToActivity("add", 1, finalStrItemCode, finalStrItemDesc, finalStrItemPrice, finalStockQty, Double.toString(dblListStockQty));
                        }
                    }
                    catch (Exception Ex){
                        Ex.toString();
                    }
                }
            });

            final String finalStrItemCode1 = strItemCode;
            final String finalStrItemDesc1 = strItemDesc;
            final String finalStrItemPrice1 = strItemPrice;
            viewHolder.imgRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        int intListQ = Integer.parseInt(viewHolder.tvQty.getText().toString());
                        double dblListStockQty = Double.parseDouble(viewHolder.tvStockQty.getText().toString());
                        if (intListQ > 0) {
                            intListQ = intListQ - 1;
                            //dblListStockQty = dblListStockQty + 1;

                            viewHolder.tvQty.setText(Integer.toString(intListQ));
                            viewHolder.tvQty.setTag(Integer.toString(intListQ));

                            viewHolder.tvStockQty.setText(Double.toString(dblListStockQty));
                            viewHolder.tvStockQty.setTag(Double.toString(dblListStockQty));
                            if (mContext instanceof FnbMain) {
                                ((FnbMain) mContext).passDataToActivity("remove", 1, finalStrItemCode1, finalStrItemDesc1, finalStrItemPrice1, finalStockQty, Double.toString(dblListStockQty));
                            }
                        }
                        if (intListQ == 0) {
                            viewHolder.imgRemove.setImageResource(R.drawable.remove_circle_inactive);
                            //((FnbMain) mContext).passDataToActivity("remove", 1, strItemCode, strItemDesc, strItemPrice);
                        }
                    }
                    catch (Exception Ex){
                        Ex.toString();
                    }
                    //notifyDataSetChanged();
                }
            });
        }
        catch (Exception Ex){
            Ex.toString();
        }
        return itemView;
    }

    public void callActivityMethod(Context mContext, String strOp, int i, String strItemCode, String strItemDesc, String strItemPrice){
        //((FnbMain) mContext).passDataToActivity(strOp,i,strItemCode,strItemDesc,strItemPrice, finalItemView);
    }

}


