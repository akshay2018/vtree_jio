package in.bigtree.vtree;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class ShowFragmentAdapter extends FragmentStatePagerAdapter {
    public int noOfTabs;
    private ArrayList<String> lstDates;
    public String strCinemaNames, strFilmName;

    public ShowFragmentAdapter(FragmentManager fm, int noOfTabs, ArrayList<String> lstDate, String strCinemaNames, String strFilmName) {
        super(fm);
        this.noOfTabs = noOfTabs;
        this.lstDates = lstDate;
        this.strCinemaNames = strCinemaNames;
        this.strFilmName = strFilmName;
    }

    @Override
    public Fragment getItem(int position) {
        ShowFrgment showFrgment = new ShowFrgment();
        try {
            Bundle bundle;
            String strShowTimes = "", strShowDate = "";
            for (int iCount = 0; iCount < noOfTabs; iCount++) {
                if (iCount == position) {
                    bundle = new Bundle();
                    strShowTimes = lstDates.get(position).split("\\_")[1];
                    strShowDate = lstDates.get(position).split("\\_")[2];
                    bundle.putString("shows", strShowTimes);
                    bundle.putString("cinemaname",strCinemaNames);
                    bundle.putString("filmname",strFilmName);
                    bundle.putString("showdate",strShowDate);
                    showFrgment.setArguments(bundle);
                    break;
                }
            }
        }
        catch (Exception Ex){
            Ex.toString();
        }
        return showFrgment;
    }

    @Override
    public int getCount() {
        return noOfTabs;
    }
}
