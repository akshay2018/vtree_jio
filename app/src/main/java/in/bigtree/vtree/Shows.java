package in.bigtree.vtree;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

public class Shows extends AppCompatActivity {

    TabLayout tbDateLayout;
    ShowFragmentAdapter adapter;
    ViewPager vPager;
    String strJsonSession = "", strBFilmCode = "", strFilmName = "", strSDate = "", strJson = "";
    TextView tvFilmName;
    LinearLayout llBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shows);
        try {
            Bundle bundle = getIntent().getExtras();
            strBFilmCode = bundle.getString("fcode");
            strFilmName = bundle.getString("fname");
            strSDate = bundle.getString("sdate");
            strJsonSession = bundle.getString("strjson");
            bundle.remove("fcode");
            bundle.remove("fname");
            bundle.remove("sdate");
            /*
            strBFilmCode = "0000000001";
            strFilmName = "AVENGERS(U)";
            */
            tvFilmName = (TextView)findViewById(R.id.tvFilmName);
            llBack = (LinearLayout)findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToMovies(v));
            tvFilmName.setText(strFilmName);
            FileIO obFile = new FileIO();
            //strJsonSession = obFile.readLocalJson(Shows.this,"sessions");
            populateShows(strBFilmCode);
        }
        catch (Exception Ex) {
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(Shows.this,"Error","Something went wrong");
        }
    }

    private void populateShows(String strFilmCode ) {
        try{
            String strDate = "", strShowTime = "", strFilmCodeFound = "", strTabName = "", strCinemaName = "";
            ArrayList<String> lstDates = new ArrayList<>();

            ArrayList<String> arrLstFilmDate = new ArrayList<>();

            tbDateLayout = (TabLayout)findViewById(R.id.tbShowDateLayout);
            vPager = (ViewPager)findViewById(R.id.vpShowPager);

            JSessions obJsonSession = new Gson().fromJson(strJsonSession, JSessions.class);
            strCinemaName = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getName();
            for(int iCount = 0; iCount < obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().size(); iCount++){
                strDate = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(iCount).getSessionDate();
                for(int fCount = 0; fCount < obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(iCount).getFilms().size(); fCount++){
                    strFilmCodeFound = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(iCount).getFilms().get(fCount).getFilmCode();
                    if(strFilmCodeFound.equals(strFilmCode)){
                        strShowTime = "";
                        for(int sCount = 0; sCount < obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(iCount).getFilms().get(fCount).getShowTimes().size(); sCount++){
                            strShowTime += obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(iCount).getFilms().get(fCount).getShowTimes().get(sCount).getShowDetails() + "$";
                        }
                        strShowTime = strShowTime.substring(0, strShowTime.length() - 1);
                        // strDate + "_" + strShowTime;
                        lstDates.add(strDate.split("\\|")[0] + "_" + strShowTime + "_" + strDate.split("\\|")[1]);
                        strTabName = strDate.split("\\|")[0];
                        if(!arrLstFilmDate.contains(strDate)){
                            arrLstFilmDate.add(strDate);
                        }
                        tbDateLayout.addTab(tbDateLayout.newTab().setText(strTabName));
                    }
                }
            }

            adapter = new ShowFragmentAdapter(getSupportFragmentManager(),lstDates.size(),lstDates,strCinemaName, strFilmName);
            vPager.setAdapter(adapter);
            int intLimit = adapter.getCount();
            vPager.setOffscreenPageLimit(intLimit);

            tbDateLayout.getTabAt(arrLstFilmDate.indexOf(strSDate)).select();
            vPager.setCurrentItem(arrLstFilmDate.indexOf(strSDate));
            //tbDateLayout.getTabAt()
            tbDateLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    vPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            vPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    tbDateLayout.getTabAt(i).select();
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(Shows.this,"Error","Something went wrong");
        }
    }


    public void backToMovies(View view) {
        try{
            finish();
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(Shows.this,"Error",Ex.toString());
        }
    }

    @Override
    public void onBackPressed() {

    }
}
