package in.bigtree.vtree;

import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import in.bigtree.vtree.PayU_Printer.Printer;

public class MainScreen extends AppCompatActivity implements AsyncResponse {

    String strUID = "", strPWD = "", strWCode = "", strCinemaName = "", strGAction = "", strAdvFileContent = "", strAdvConfigFile = "", strGStockLoc = "", strGSLayout = "", strGPrefMode = "", strCounterChk="",
            strGPrintChk = "";
    TextView tvCinemaName;
    RelativeLayout rlSParent, rlFnbOptionParent, rlMovieTicketOptionParent, rlshowLastTransReprint;
    Handler handler;
    Runnable runnable;
    boolean doubleBackToExitPressedOnce = false;
    //TextView signOut;
    LinearLayout signOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        try{
            Bundle lBundle = getIntent().getExtras();
            strUID = lBundle.getString("uid");
            strPWD = lBundle.getString("pwd");
            strWCode = lBundle.getString("wcode");
            strCinemaName = lBundle.getString("cinema");

            tvCinemaName = (TextView)findViewById(R.id.tvCinemaName);
            signOut = (LinearLayout)findViewById(R.id.signOut);
            //rlBottomMessage = (RelativeLayout)findViewById(R.id.rlBottomMessage);
            rlSParent = (RelativeLayout)findViewById(R.id.rlSuperParent);
            //rlFnbOptionParent = (RelativeLayout)findViewById(R.id.rlFnbOptionParent);
            rlMovieTicketOptionParent = (RelativeLayout)findViewById(R.id.rlMovieTicketOptionParent);
            rlshowLastTransReprint = (RelativeLayout)findViewById(R.id.rlLastPrint);
            tvCinemaName.setText(strCinemaName);

            Snackbar snackbar = (Snackbar) Snackbar.make(rlSParent, "LOGIN SUCCESSFULL", Snackbar.LENGTH_SHORT);
            snackbar.show();

            FileIO obFile = new FileIO();
            //strFileName = getResources().getString(R.string.config_file_name);
            strAdvConfigFile = getResources().getString(R.string.advance_config_file);
            strAdvFileContent = obFile.readFile(strAdvConfigFile,getApplicationContext());
            if(strAdvFileContent.split("\\|").length > 0){strGSLayout = (strAdvFileContent.split("\\|")[0]); } else { strGSLayout = ""; }
            if(strAdvFileContent.split("\\|").length > 1) {strGPrintChk = strAdvFileContent.split("\\|")[1];} else {strGPrintChk="";}
            if(strAdvFileContent.split("\\|").length > 2) {strCounterChk = strAdvFileContent.split("\\|")[2];} else {strCounterChk="";}
            //if(strAdvFileContent.split("\\|").length > 2) {strGPrefMode = strAdvFileContent.split("\\|")[2];} else {strGPrefMode="";}
            //if(strAdvFileContent.split("\\|").length > 3) {strGPrintChk = strAdvFileContent.split("\\|")[3];} else {strGPrintChk="";}

            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            obShared.setCID(strCinemaName);
            if(strGSLayout.equals("")){
                obShared.setSeatLayout("full");
            }
            else{
                obShared.setSeatLayout("partial");
            }
/*
            if(strGPrefMode.equals("")){
                obShared.setMode("both");
            }
            else {
                obShared.setMode(strGPrefMode);
            }
*/
            if(strGPrintChk.equals("Y")){
                obShared.setPrintChecked(true);
            }
            else {
                obShared.setPrintChecked(false);
            }

            if(obShared.getMode().equals("C")){
                rlMovieTicketOptionParent.setVisibility(View.GONE);
                rlshowLastTransReprint.setVisibility(View.GONE);
            }
            if(obShared.getMode().equals("T")){
                rlFnbOptionParent.setVisibility(View.GONE);
            }
            else{

            }

            if(strCounterChk.equals("Y")){
                obShared.setCounterPickupStat(true);
            }
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(MainScreen.this,"Error","Something went wrong");
        }
    }

    public void getMovies(View v){
        try{
            Intent intMovies = new Intent(MainScreen.this,Movies.class);
            startActivity(intMovies);
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(MainScreen.this,"Error","Something went wrong");
        }
    }


    public void showLastTransReprint(View view) {
        try{
            Bundle rLbundle = new Bundle();
            rLbundle.putString("cinema",strCinemaName);
            //create logic to get history on the basis of selected mode i.e tickets or concession

            //Intent intReprintLast = new Intent(MainScreen.this,ReprintLastTrans.class);
            Intent intReprintLast = new Intent(MainScreen.this,ReprintLastCon.class);
            intReprintLast.putExtras(rLbundle);
            startActivity(intReprintLast);
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(MainScreen.this,"Error","Something went wrong");
        }
    }

    public void logOut(View view) {
        String strUID = "", strPWD = "", strResponse = "", strWCode = "", strCId = "", strFIP = "", strFileName = "", strFileContent="", strIP ="";
        try {
            FileIO obFile = new FileIO();
            //strFileName = getResources().getString(R.string.config_file_name);
            strFileName = getResources().getString(R.string.connection_config_file);
            strFileContent = obFile.readFile(strFileName,getApplicationContext());
            strIP = strFileContent.split("\\|")[0];
            strCId = strFileContent.split("\\|")[2];

            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            strUID = obShared.getUID();

            Bundle mBundle = getIntent().getExtras();
            strPWD = this.strPWD;
            strWCode = this.strWCode;

            WifiManager wifiMan = (WifiManager) getApplicationContext().getSystemService(getApplicationContext().WIFI_SERVICE);
            WifiInfo wifiInf = wifiMan.getConnectionInfo();
            //int ipAddress = wifiInf.getIpAddress();
            //strCId = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));

            strGAction = "LOGOUT";
            SoapCall obSc = new SoapCall(MainScreen.this);
            obSc.delegate = this;
            obSc.execute("LOGOUT", strIP, strUID, strPWD, strCId, strWCode, "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(MainScreen.this,"Error","Something went wrong");
        }
    }

    @Override
    public void processFinish(String output) {
        try{
            if(doubleBackToExitPressedOnce){
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
            if(strGAction.equals("LOGOUT")){
                if(!output.split("\\|")[0].equals("1")){
                    Intent obIntent = new Intent(getApplicationContext(), Login.class);
                    obIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(obIntent);
                }
            }
            //finish();
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(MainScreen.this,"Error","Something went wrong");
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            signOut.performClick();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please tap BACK again to log out and exit", Toast.LENGTH_SHORT).show();
        //Snackbar snackbar = (Snackbar) Snackbar.make(rlSParent, "Please tap BACK again to log out and exit", Snackbar.LENGTH_SHORT);
        //snackbar.show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void getConsessions(View view) {
        try{
            Intent intFNB = new Intent(MainScreen.this,FnbMain.class);
            startActivity(intFNB);
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(MainScreen.this,"Error","Something went wrong");
        }
    }
    /*
        public void voidthis(View view) {
            Intent intent = new Intent(MainScreen.this, VoidSaleActivity.class);
            startActivity(intent);
        }
    */
}
