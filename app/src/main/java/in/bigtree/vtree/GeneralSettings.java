package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.google.gson.Gson;

public class GeneralSettings extends AppCompatActivity implements AsyncResponse{

    String strFileContent = "", strFIP = "", strWcode ="",isHttps="", strSLayout = "", strLocDesc = "", strLocCode = "", strLocString ="", strConnection ="", strGAction = "", strCid= "";
    EditText edIp, edWcode, tvCId;
    Switch swHttps;
    Button btnClearData,btnSave;
    Context ctx;
    LinearLayout llIvBack,llBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_settings);
        try {

            llBack = (LinearLayout) findViewById(R.id.llIvBack);
            llBack.setOnClickListener((v) -> backFromSettings(v));

            ctx = GeneralSettings.this;
            Bundle bundle = getIntent().getExtras();
            if(bundle.getString("connection") != null){
                strConnection = bundle.getString("connection");
            }
            if(!strConnection.equals("")){
                CustomDialog cDiag= new CustomDialog();
                cDiag.ShowErrorDialog(ctx,"No Connection","SERVER'S IP IS NOT REACHABLE");
            }

            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file), getApplicationContext());

            if (strFileContent.split("\\|").length > 0) {
                strFIP = strFileContent.split("\\|")[0];
            } else {
                strFIP = "";
            }
            if (strFileContent.split("\\|").length > 1) {
                strWcode = strFileContent.split("\\|")[1];
            } else {
                strWcode = "";
            }
            if(strFileContent.split("\\|").length > 2){
                strCid = strFileContent.split("\\|")[2];
            }
            if(strFileContent.split("\\|").length > 3){
                isHttps = strFileContent.split("\\|")[3];
            }

            edIp = (EditText) findViewById(R.id.tvSSIP);
            edWcode = (EditText) findViewById(R.id.tvSWCODE);
            tvCId = (EditText) findViewById(R.id.tvCId);
            btnSave = (Button) findViewById(R.id.btnSSave);
            btnClearData = (Button) findViewById(R.id.btnClear);
            llIvBack = (LinearLayout)findViewById(R.id.llIvBack);
            swHttps = (Switch)findViewById(R.id.swHttps);

            edIp.setText(strFIP);
            edWcode.setText(strWcode);
            tvCId.setText(strCid);

            if (isHttps.equals("N"))
                swHttps.setChecked(false);
             else
                swHttps.setChecked(true);

            if (!bundle.getString("prev").equals("launch")) {
                edIp.setEnabled(false);
                edWcode.setEnabled(false);
                tvCId.setEnabled(false);
                btnClearData.setVisibility(View.VISIBLE);
                btnSave.setVisibility(View.GONE);
            }
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }

    public void SaveSettings(View view) {
        boolean blnWorking = false;
        String strIp = "", strWCode = "", strClientID = "", strFileContent = "", strFileName = "", etBlank = "",isHttps="";
        try{
            strFileName = getResources().getString(R.string.connection_config_file);
            strIp = edIp.getText().toString();
            strWCode = edWcode.getText().toString();
            strClientID = tvCId.getText().toString();
            isHttps = swHttps.isChecked() ? "Y" : "N";
            if(strIp.equals("") || strWCode.equals("")){
                etBlank = strIp.equals("") ? "Ip address" : "Workstation code";
                CustomDialog cDiag= new CustomDialog();
                cDiag.ShowErrorDialog(ctx,"Values are blank","Kindly enter " + etBlank);
            }
            else if (strClientID.equals("")){
                CustomDialog cDiag= new CustomDialog();
                cDiag.ShowErrorDialog(ctx,"Values are blank","Kindly enter client id");
            }
            else {
                FileIO obFileIO = new FileIO();
                //check for ip connectivity
                blnWorking = obFileIO.checkURL(strIp);
                if (!blnWorking) {
                    CustomDialog cDialog = new CustomDialog();
                    cDialog.ShowErrorDialog(ctx, "No Connection", "SERVER'S IP IS NOT REACHABLE");
                    return;
                }

                //strFileContent = obFileIO.readFile(strFileName,getApplicationContext());
                //if (strFileContent.split("\\|").length > 0) {strFIP = strFileContent.split("\\|")[0];} else {strFIP = "";}
                //if (strFileContent.split("\\|").length > 1) {strWcode = strFileContent.split("\\|")[1];} else {strWcode = "";}

                //strFileContent = strIp + "|" + strWCode ;
                strFileContent += String.format("%s|%s|%s|%s", strIp, strWCode, strClientID,isHttps);
                obFileIO.writeFile(strFileName, strFileContent, getApplicationContext());
            }

            Bundle bundle = getIntent().getExtras();
            if(bundle != null) {
                if(bundle.getString("prev").equals("login")){
                    finish();
                }
                if(bundle.getString("prev").equals("launch")){
                    Intent intLogin = new Intent(ctx,Login.class);
                    startActivity(intLogin);
                }
            }
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }

    public void backFromSettings(View view) {
        try{
            //this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
            //this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            finish();
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }

    public void ClearSettings(View view) {
        try{
            strGAction = "CLEARDATA";
            SoapCall obSc = new SoapCall(ctx);
            obSc.delegate = this;
            obSc.execute("CLEARDATA", strFIP, strWcode, "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Unable to clear the data");
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void processFinish(String output) {
        try{
            if(strGAction.equals("CLEARDATA")) {
                processClearData(output);
            }
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Unable to finish the clear call");
        }
    }

    private void processClearData(String output) {
        try{
            String strResponseCode = "", strResponseData = "", strFileName = "";
            if (!output.split("\\|")[0].equals("1")) {
                JCResp obJsonResponse = new Gson().fromJson(output, JCResp.class);
                strResponseCode = obJsonResponse.getRESPONSECODE();
                strResponseData = obJsonResponse.getRESPONSEDATA();
                if(strResponseCode.equals("0")) {
                    llIvBack.setVisibility(View.GONE);
                    btnClearData.setVisibility(View.GONE);
                    btnSave.setVisibility(View.VISIBLE);

                    edIp.setText("");
                    edWcode.setText("");
                    tvCId.setText("");
                    edIp.setEnabled(true);
                    edWcode.setEnabled(true);
                    tvCId.setEnabled(true);

                    strFileName = getResources().getString(R.string.connection_config_file);
                    FileIO fileIO = new FileIO();
                    fileIO.deleteFile(strFileName,getApplicationContext());
                }
                else {
                    CustomDialog cDialog = new CustomDialog();
                    cDialog.ShowErrorDialog(ctx,"Error",strResponseData);
                }
            }
            else{
                CustomDialog cDialog = new CustomDialog();
                cDialog.ShowErrorDialog(ctx,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Unable to finish the clear call");
        }
    }
}
