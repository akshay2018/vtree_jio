package in.bigtree.vtree;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.io.File;

public class Launcher extends AppCompatActivity {

    String strFileContent = "";
    private int REQUEST_CODE_PERMISSIONS = 2002;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        //subChckFileStatus();
        //requestPermissions();
        new CheckNetworkCall().execute();
    }


    public class CheckNetworkCall extends AsyncTask<Void, Void, Void>{
        boolean blnConnection;
        String strFileIP = "";
        String strApiPackage = "";
        String strFileStat = "";
        @Override
        protected Void doInBackground(Void... voids) {
            //subChckFileStatus();
            String strFileName = "", strFileContent = "", strFileCID = "", strFileWCODE = "";
            File configFile;
            blnConnection = false;
            try {
                //strFileName = getResources().getString(R.string.config_file_name);
                strFileName = getResources().getString(R.string.connection_config_file);
                configFile = new File(Launcher.this.getFilesDir(), strFileName);
                Bundle bundle = new Bundle();
                if (!configFile.exists()) {
                    strFileStat = "N";
                }
                else {
                    strFileStat = "Y";
                    FileIO obFileIO = new FileIO();
                    strFileContent = obFileIO.readFile(strFileName,getApplicationContext());
                    if(strFileContent.equals("")){
                        strFileStat = "B";
                    }
                    else{
                        strFileIP = strFileContent.split("\\|")[0];
                        strApiPackage = getApplicationContext().getResources().getString(R.string.webservice_name);
                        blnConnection = obFileIO.checkURL(strFileIP + strApiPackage);
                    }
                }
            }
            catch (Exception Ex) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try{
                Bundle bundle = new Bundle();
                switch (strFileStat){
                    case "N":
                        bundle.putString("fileExists","N");
                        Intent intentSetup = new Intent(getApplicationContext(), SetUp.class);
                        intentSetup.putExtras(bundle);
                        startActivity(intentSetup);
                        break;
                    case "Y":
                        if(blnConnection == true){
                            Intent obIntent = new Intent(getApplicationContext(), Login.class);
                            startActivity(obIntent);
                        }
                        else {
                            //Toast toast = Toast.makeText(getApplicationContext(), "SERVER'S IP IS NOT REACHABLE", Toast.LENGTH_LONG);
                            //toast.show();
                            //Intent obIntent = new Intent(getApplicationContext(), Settings.class);
                            Intent obIntent = new Intent(getApplicationContext(), GeneralSettings.class);
                            bundle.putString("connection","f"); //blank
                            bundle.putString("fileIP",strFileIP); //blank
                            bundle.putString("prev","launch");
                            obIntent.putExtras(bundle);
                            startActivity(obIntent);
                        }
                        break;
                    case "B":
                        //bundle.putString("fileExists","B");
                        Intent intentSetup1 = new Intent(getApplicationContext(), SetUp.class);
                        //intentSetup1.putExtras(bundle);
                        startActivity(intentSetup1);
                        break;
                }
            }
            catch (Exception Ex){
                Ex.toString();
            }
        }
    }
/*
    protected void requestPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {

            if ((ContextCompat.checkSelfPermission(this, "android.permission.BBPOS") != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.VIBRATE) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                // Permission is not granted

                Intent intent = new Intent(this, PermissionsActivity.class);
                startActivityForResult(intent, REQUEST_CODE_PERMISSIONS);

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        Logs.v(ApplicationData.packName, "requestCode: " + requestCode, true, true);
        Logs.v(ApplicationData.packName, "resultCode: " + resultCode, true, true);

        if (requestCode == REQUEST_CODE_PERMISSIONS) {


        } else if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                this.finish();
                startActivity(this.getIntent());
            }
        }

        /*
        else if (requestCode == CHANGE_PASSWORD) {
            if (resultCode == RESULT_OK) {

                clearDeviceCache();
                Intent intent = new Intent(Launcher.this, LoginView.class);
                startActivity(intent);
            }
        }

    }

         */

}
