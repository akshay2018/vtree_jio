package in.bigtree.vtree;

public interface ActivityCommunicator {
    void passDataToActivity(String strOp, int val, String strItemId, String strItemName, String strItemPrice, String finalStockQty, String finalListStockQty);
}
