package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShowDate {

    @SerializedName("SessionDate")
    @Expose
    private String sessionDate;
    @SerializedName("Films")
    @Expose
    private List<Film> films = null;

    public String getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }

    public List<Film> getFilms() {
        return films;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }

}