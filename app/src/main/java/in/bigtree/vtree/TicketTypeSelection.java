package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

public class TicketTypeSelection extends AppCompatActivity implements AsyncResponse {
    String strSessionId = "", strIP ="",strCinemaName = "",strShowTime = "", strScreenName = "", strFilmName ="", strShowDate = "";
    Context ctx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_type_selection);
        try{
            Bundle bundle = getIntent().getExtras();
            strSessionId = bundle.getString("sessionid") != null ? bundle.getString("sessionid") : "";
            strCinemaName = bundle.getString("cinemaname") != null ? bundle.getString("cinemaname") : "";
            strShowTime = bundle.getString("showtime") != null ? bundle.getString("showtime") : "";
            strScreenName = bundle.getString("screen") != null ? bundle.getString("screen") : "";
            strFilmName = bundle.getString("filmname") != null ? bundle.getString("filmname") : "";
            strShowDate = bundle.getString("showdate") != null ? bundle.getString("showdate") : "";
            ctx = this;
            getTicketTypes(strSessionId);
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(TicketTypeSelection.this,"Error","Something went wrong");
        }
    }

    private void getTicketTypes(String strSessionId) {
        String strFileContent = "";
        try{
            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];

            SoapCall obSc = new SoapCall(TicketTypeSelection.this);
            obSc.delegate = this;
            //strResponse = obCs.Call("TICKETTYPES", strIP, strSession, "", "","","","","","","","","","","","");
            obSc.execute("TICKETTYPES", strIP, strSessionId, "", "","","","","","","","","","","","","");
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(TicketTypeSelection.this,"Error","Something went wrong");
        }
    }

    public void backToShows(View view) {
        try{
            finish();
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(TicketTypeSelection.this,"Error","Something went wrong");
        }
    }

    public void goToSeatLayout(View view){
        try{
            String strAreaCode = "",strAreaDesc = "", strTicketType = "";

            strAreaCode = view.getTag().toString().split("\\_")[0];
            strAreaDesc = view.getTag().toString().split("\\_")[1];
            strTicketType = view.getTag().toString().split("\\_")[2];
            Bundle stBundle = new Bundle();
            stBundle.putString("sessionid",strSessionId);

            stBundle.putString("cinemaname",strCinemaName);
            stBundle.putString("showtime",strShowTime);
            stBundle.putString("screen",strScreenName);
            stBundle.putString("filmname",strFilmName);
            stBundle.putString("showdate",strShowDate);
            stBundle.putString("areacode",strAreaCode);
            stBundle.putString("areadesc",strAreaDesc);
            stBundle.putString("ttype",strTicketType);
            stBundle.putString("stype","partial");


            Intent intSeatLayout = new Intent(getApplicationContext(),SeatLayout.class);
            intSeatLayout.putExtras(stBundle);
            startActivity(intSeatLayout);
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(TicketTypeSelection.this,"Error","Something went wrong");
        }
    }

    @Override
    public void processFinish(String output) {
        try{
            if(!output.split("\\|")[0].equals("1")) {
                showTicketTypes(output);
            }
            else {
                CustomDialog cDiag= new CustomDialog();
                cDiag.ShowErrorDialog(TicketTypeSelection.this,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(TicketTypeSelection.this,"Error","Something went wrong");
        }
    }

    public void showTicketTypes(String output){
        String strAreaDesc = "", strAreaCode = "", strTktCode = "", strTktDesc = "", strAvailTkt = "", strPrice = "", strIsPackage = "";
        int intAWidth = 0, intAHeight = 0, intTAreaSize = 0;
        try{
            JArea obAreaJson = new Gson().fromJson(output, JArea.class);
            int intAreaCount = obAreaJson.getTTYPERESPONSEDATA().get(0).getAreaQty().size();
            LinearLayout llParent = (LinearLayout)findViewById(R.id.llTtypeParent);
            for (int AreaCount = 0; AreaCount < intAreaCount; AreaCount++) {
                strAreaCode = obAreaJson.getTTYPERESPONSEDATA().get(0).getAreaQty().get(AreaCount).getAreaCode().toString();
                strAreaDesc = obAreaJson.getTTYPERESPONSEDATA().get(0).getAreaQty().get(AreaCount).getAreaDesc().toString();
                strAvailTkt = obAreaJson.getTTYPERESPONSEDATA().get(0).getAreaQty().get(AreaCount).getAvailableSeats().toString();
                strTktCode = obAreaJson.getTTYPERESPONSEDATA().get(0).getAreaQty().get(AreaCount).getTCode();
                strTktDesc = obAreaJson.getTTYPERESPONSEDATA().get(0).getAreaQty().get(AreaCount).getTDesc();
                strPrice = obAreaJson.getTTYPERESPONSEDATA().get(0).getAreaQty().get(AreaCount).getTPrice();

                int topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                int startMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                int endMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                int topPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());;
                int bottomPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());;
                LinearLayout llTContainer = new LinearLayout(this);

                //llTContainer.setL
                LinearLayout.LayoutParams llTContainerParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                llTContainerParams.setMargins(startMargin,topMargin,endMargin,0);
                llTContainer.setLayoutParams(llTContainerParams);
                llTContainer.setPadding(0,topPadding,0,bottomPadding);
                llTContainer.setClickable(true);
                llTContainer.setGravity(Gravity.CENTER_VERTICAL);
                llTContainer.setOrientation(LinearLayout.HORIZONTAL);
                llTContainer.setBackground(ContextCompat.getDrawable(ctx,R.drawable.ripple));
                llTContainer.setTag(strAreaCode+"_"+strAreaDesc+"_"+strTktCode);
                //llTContainer.setOnClickListener(goToSeatLayout);
                llTContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goToSeatLayout(v);
                    }
                });

                LinearLayout llBlank = new LinearLayout(this);
                llBlank.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1.0f));

                LinearLayout llTextContainer = new LinearLayout(this);
                llTextContainer.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,6.0f));
                llTextContainer.setOrientation(LinearLayout.VERTICAL);

                Typeface areaTypeFace = ResourcesCompat.getFont(ctx, R.font.calibre_semibold);
                TextView tvAreaName = new TextView(this);
                tvAreaName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvAreaName.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvAreaName.setTextSize(20);
                tvAreaName.setText(strAreaDesc);
                tvAreaName.setTypeface(areaTypeFace);

                LinearLayout llPriceQtyContainer = new LinearLayout(this);
                llPriceQtyContainer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                llPriceQtyContainer.setOrientation(LinearLayout.HORIZONTAL);

                Typeface calRegTypeFace = ResourcesCompat.getFont(ctx, R.font.calibre_regular);
                TextView tvINR = new TextView(this);
                tvINR.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvINR.setText(R.string.Rs);
                tvINR.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvINR.setTextSize(14);
                tvINR.setTypeface(calRegTypeFace);

                TextView tvPrice = new TextView(this);
                tvPrice.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvPrice.setText(strPrice);
                tvPrice.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvPrice.setTextSize(16);
                tvPrice.setTypeface(calRegTypeFace);

                TextView tvQty = new TextView(this);
                tvQty.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvQty.setText(" - " + strAvailTkt + " left");
                tvQty.setTextColor(ContextCompat.getColor(ctx,R.color.colorDisabledText));
                tvQty.setTextSize(16);
                tvQty.setTypeface(calRegTypeFace);

                llPriceQtyContainer.addView(tvINR);
                llPriceQtyContainer.addView(tvPrice);
                llPriceQtyContainer.addView(tvQty);

                llTextContainer.addView(tvAreaName);
                llTextContainer.addView(llPriceQtyContainer);

                LinearLayout llArrowContainer = new LinearLayout(this);
                llArrowContainer.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1.5f));
                llArrowContainer.setGravity(Gravity.CENTER);

                ImageView ivArrow = new ImageView(this);
                ivArrow.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                ivArrow.setImageResource(R.drawable.ic_arrow_right);

                llArrowContainer.addView(ivArrow);

                llTContainer.addView(llBlank);
                llTContainer.addView(llTextContainer);
                llTContainer.addView(llArrowContainer);

                llParent.addView(llTContainer);
            }
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(TicketTypeSelection.this,"Error","Something went wrong");
        }
    }
}
