package in.bigtree.vtree.view.paybylink.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.PayByLinkResponseData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.paybylink.PayByLinkActivity;
import in.bigtree.vtree.view.paybylink.PayByLinkEnum;

public class MobileNoFragment extends android.support.v4.app.Fragment {

    Context mContext;
    PayByLinkActivity mPayByLinkActivity = null;
    SharedVariable applicationData = null;

    //fields for payby link screen
    private EditText mTxtAmt = null;
    private EditText mTxtMobileNo = null;
    private EditText mTxtEmail = null;
    private EditText mTxtInvoiceNo = null;
    private ImageButton mIBtnNext = null;

    EditText mTxtExtraNote1 = null;
    EditText mTxtExtraNote2 = null;
    EditText mTxtExtraNote3 = null;
    EditText mTxtExtraNote4 = null;
    EditText mTxtExtraNote5 = null;
    EditText mTxtExtraNote6 = null;
    EditText mTxtExtraNote7 = null;
    EditText mTxtExtraNote8 = null;
    EditText mTxtExtraNote9 = null;
    EditText mTxtExtraNote10 = null;

    private CustomProgressDialog mProgressActivity;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        mContext = activity;
        mPayByLinkActivity = ((PayByLinkActivity) mContext);
        applicationData = SharedVariable.getApplicationDataSharedInstance();
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.paybylink_mobile_view, container, false);

        initViews(view);
        return view;
    }

    public void initViews(View view) {
        // TODO Auto-generated method stub

        mTxtAmt = (EditText) view.findViewById(R.id.payby_link_inputdetailsview_TXT_amount);
        mTxtMobileNo = (EditText) view.findViewById(R.id.payby_link_TXT_mobileno);
        mTxtInvoiceNo = (EditText) view.findViewById(R.id.payby_link_TXT_receipt);
        mTxtEmail = (EditText) view.findViewById(R.id.payby_link_TXT_email);
        mIBtnNext = (ImageButton) view.findViewById(R.id.payby_link_inputdetailsview_BTN_next);
        mTxtExtraNote1 = (EditText) view.findViewById(R.id.payby_link_TXT_extra_one);
        mTxtExtraNote2 = (EditText) view.findViewById(R.id.payby_link_TXT_extra_two);
        mTxtExtraNote3 = (EditText) view.findViewById(R.id.payby_link_TXT_extra_three);
        mTxtExtraNote4 = (EditText) view.findViewById(R.id.payby_link_TXT_extra_four);
        mTxtExtraNote5 = (EditText) view.findViewById(R.id.payby_link_TXT_extra_five);
        mTxtExtraNote6 = (EditText) view.findViewById(R.id.payby_link_TXT_extra_six);
        mTxtExtraNote7 = (EditText) view.findViewById(R.id.payby_link_TXT_extra_seven);
        mTxtExtraNote8 = (EditText) view.findViewById(R.id.payby_link_TXT_extra_eight);
        mTxtExtraNote9 = (EditText) view.findViewById(R.id.payby_link_TXT_extra_nine);
        mTxtExtraNote10 = (EditText) view.findViewById(R.id.payby_link_TXT_extra_ten);

        mIBtnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                double miAmountDisplay = 0;
                try {
                    if (mTxtAmt.getText().toString().length() > 0)
                        miAmountDisplay = Double.parseDouble(removeChar(mTxtAmt.getText().toString(), ','));


                } catch (Exception ex) {
                    miAmountDisplay = 0;
                }

                if (SharedVariable.IS_DEBUGGING_ON)
                    Logs.v(SharedVariable.packName,  "mTxtAmt " + mTxtAmt.getText().toString(), true, true);

                if (SharedVariable.IS_DEBUGGING_ON)
                    Logs.v(SharedVariable.packName,  "mTxtMobileNo " + mTxtMobileNo.getText().toString(), true, true);


                if (miAmountDisplay < 1) {
                    Constants.showDialog(mPayByLinkActivity,"payby link",
                            String.format(getString(R.string.voidsaleamountfragment_you_need_to_enter_a_minimum_amount_of_at_least_inr_001_to_proceed), applicationData.mCurrency));

                    return;
                }
                else if(mTxtMobileNo.getText().toString().trim().length() != applicationData.PhoneNoLength) {

                    String phoneLength = String.format(Constants.CARDSALE_ERROR_mobilenolen, applicationData.PhoneNoLength);
                    Constants.showDialog(mPayByLinkActivity ,"payby link", phoneLength);
                    mTxtMobileNo.requestFocus();
                    return;

                }
                else if(mTxtMobileNo.getText().toString().trim().startsWith("0")) {

                    Constants.showDialog(mPayByLinkActivity ,"payby link", Constants.CARDSALE_ERROR_mobileformat);
                    mTxtMobileNo.requestFocus();
                    return;

                }
                else if(mTxtEmail.getText().toString().trim().length() != 0) {

                    if (emailCheck(mTxtEmail.getText().toString())) {

                    } else {
                        mTxtEmail.requestFocus();
                        return;
                    }

                }

                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(mPayByLinkActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);


                mPayByLinkActivity.mTransactionData.mBaseAmount = mTxtAmt.getText().toString();
                mPayByLinkActivity.mTransactionData.mPhoneNo = mTxtMobileNo.getText().toString();
                mPayByLinkActivity.mTransactionData.mEmail = mTxtEmail.getText().toString();
                mPayByLinkActivity.mTransactionData.mCustomerName = mTxtInvoiceNo.getText().toString();

                getPayByLinkData();
            }
        });
    }

    public String removeChar(String s, char c) {

        String r = "";

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c)
                r += s.charAt(i);
        }

        return r;
    }

    public boolean emailCheck(String str) {

        if (!Constants.isValidEmail(str)) {
            Constants.showDialog(mPayByLinkActivity, "payby link", "invalid email address");

            return false;
        } else {
            return true;
        }
    }
    public boolean getPayByLinkData() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mPayByLinkActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            wisepadController.generatePayByLink(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getCustCode(),
                    mPayByLinkActivity.mTransactionData.mBaseAmount,
                    mPayByLinkActivity.mTransactionData.mPhoneNo,
                    mPayByLinkActivity.mTransactionData.mEmail,
                    mPayByLinkActivity.mTransactionData.mCustomerName,
                    "",
                    mTxtExtraNote1.getText().toString(),
                    mTxtExtraNote2.getText().toString(),
                    mTxtExtraNote3.getText().toString(),
                    mTxtExtraNote4.getText().toString(),
                    mTxtExtraNote5.getText().toString(),
                    mTxtExtraNote6.getText().toString(),
                    mTxtExtraNote7.getText().toString(),
                    mTxtExtraNote8.getText().toString(),
                    mTxtExtraNote9.getText().toString(),
                    mTxtExtraNote10.getText().toString(),
                    new PayByLinkListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mPayByLinkActivity, getString(R.string.processing));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mPayByLinkActivity, "payby link",
                   "the payby link transaction was not processed successfully. please try again");

        }

        return true;
    }

    class PayByLinkListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            PayByLinkResponseData payByLinkResponseData = (PayByLinkResponseData) aMSDataStore;
            if(payByLinkResponseData.getResponseStatus())
            {

                mPayByLinkActivity.mTrnxId = payByLinkResponseData.getInvoiceID();
                mPayByLinkActivity.showScreen(PayByLinkEnum.PayByLinkScreens.PayByLink_Screen_Aproved_Transaction);
            }
            else{

                final Dialog dialog = Constants.showDialog(mPayByLinkActivity, getString(R.string.mcard_sale), payByLinkResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });
                dialog.show();

            }
        }
    }
}
