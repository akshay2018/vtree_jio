package in.bigtree.vtree.view.cashorbanksale;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.mswipetech.sdk.network.MSGatewayConnectionListener;
import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.CashSaleResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.Calendar;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.BaseTitleActivity;

public class BankSaleView extends BaseTitleActivity
{

    //fields for cash sale screen
    EditText mTxtCreditAmountDollars=null;
    
    //fields for Credit Notes
    EditText mTxtPhoneNum=null;
    EditText mTxtEmail=null;
    EditText mTxtReceipt=null;
    EditText mTxtChequeNo=null;
    
    EditText mTxtNotes=null;
    
    String mNotes="";
    String mEmail="";
    String mReceipt="";
    String mPhoneNum="";

	EditText mTxtExtraOne = null;
	EditText mTxtExtraTwo = null;
	EditText mTxtExtraThree = null;
	EditText mTxtExtraFour = null;
	EditText mTxtExtraFive = null;
	EditText mTxtExtraSix = null;
	EditText mTxtExtraSeven = null;
	EditText mTxtExtraEight = null;
	EditText mTxtExtraNine = null;
	EditText mTxtExtraTen = null;

    private static final String[] MONTHS = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	private int year;
	private int month;
	private int day;
    public String mCheckDate="";
	static final int DATE_DIALOG_ID = 999;
	TextView mLblCheckDate = null;

	protected WisePadGatewayConncetionListener mWisePadGatewayConncetionListener;
    CustomProgressDialog mProgressActivity  =null;
    SharedVariable applicationData = null;

	private CashSaleResponseData mBanksaleResponseData;

	// this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
	private long lastRequestTime = 0;
    
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.banksale);
		applicationData = (SharedVariable) getApplicationContext();

		mWisePadGatewayConncetionListener = new WisePadGatewayConncetionListener();
		MSWisepadController.getSharedMSWisepadController(this,
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
				new WisePadGatewayConncetionListener());

		MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());



		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		initViews();
	    

	       
	}

	@Override
	public void onStart() {


		MSWisepadController.getSharedMSWisepadController(BankSaleView.this,
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
				new WisePadGatewayConncetionListener()).startMSGatewayConnection(BankSaleView.this);
		super.onStart();
	}

	@Override
	public void onStop() {


		MSWisepadController.getSharedMSWisepadController(this,
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
				new WisePadGatewayConncetionListener()).stopMSGatewayConnection();
		super.onStop();
	}
	
	
	private void initViews() 
	{
		((TextView)findViewById(R.id.topbar_LBL_heading)).setText(getResources().getString(R.string.bank_sale));
		((TextView)findViewById(R.id.topbar_LBL_heading)).setTypeface(applicationData.font);

		//sms prefix from the setting
		((TextView)findViewById(R.id.banksale_LBL_countrycodeprefix)).setText(applicationData.smsCode);
		((TextView)findViewById(R.id.banksale_LBL_countrycodeprefix)).setTypeface(applicationData.font);


// The fields for the notes
        mTxtPhoneNum = (EditText)findViewById(R.id.banksale_TXT_mobileno);
        mTxtPhoneNum.setFilters( new InputFilter[] { new InputFilter.LengthFilter(SharedVariable.PhoneNoLength) } );

        mTxtEmail = (EditText) findViewById(R.id.banksale_TXT_email);
        
        mTxtReceipt = (EditText) findViewById(R.id.banksale_TXT_receipt);
        mTxtChequeNo= (EditText) findViewById(R.id.banksale_TXT_checqueno);;
        
        mLblCheckDate = (TextView) findViewById(R.id.banksale_Lbl_checkdate);
        mTxtNotes = (EditText) findViewById(R.id.banksale_TXT_notes);

		mTxtExtraOne = (EditText) findViewById(R.id.banksale_TXT_extra_one);
		mTxtExtraTwo = (EditText) findViewById(R.id.banksale_TXT_extra_two);
		mTxtExtraThree = (EditText) findViewById(R.id.banksale_TXT_extra_three);
		mTxtExtraFour = (EditText) findViewById(R.id.banksale_TXT_extra_four);
		mTxtExtraFive = (EditText) findViewById(R.id.banksale_TXT_extra_five);
		mTxtExtraSix = (EditText) findViewById(R.id.banksale_TXT_extra_six);
		mTxtExtraSeven = (EditText) findViewById(R.id.banksale_TXT_extra_seven);
		mTxtExtraEight = (EditText) findViewById(R.id.banksale_TXT_extra_eight);
		mTxtExtraNine = (EditText) findViewById(R.id.banksale_TXT_extra_nine);
		mTxtExtraTen = (EditText) findViewById(R.id.banksale_TXT_extra_ten);

        mTxtPhoneNum.setTypeface(applicationData.font);
        mTxtEmail.setTypeface(applicationData.font);
        mTxtReceipt.setTypeface(applicationData.font);
        mTxtChequeNo.setTypeface(applicationData.font);
        mTxtNotes.setTypeface(applicationData.font);
        mLblCheckDate.setTypeface(applicationData.font);
       
        ((TextView) findViewById(R.id.banksale_Lbl_lblcheckdate)).setTypeface(applicationData.font);
		
        mLblCheckDate.setText(day + " " + MONTHS[month] + " " + year);
		mCheckDate =day + "/" + (month+1) + "/" + year;
		
		Button btnDate = (Button) findViewById(R.id.banksale_BTN_checkdate);
		btnDate.setTypeface(applicationData.font);
		btnDate.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				showDialog(DATE_DIALOG_ID);
				
			}
		});


//The screen are for the amount		
		
		mTxtCreditAmountDollars= (EditText) findViewById(R.id.banksale_TXT_amount);

		mTxtCreditAmountDollars.setTag("1");
		
		mTxtCreditAmountDollars.setTypeface(applicationData.font);

		mTxtCreditAmountDollars.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				if(mTxtCreditAmountDollars.isFocused())
				{
				  if(mTxtCreditAmountDollars.getText().length()!=0)
					return true;
				}
				return false;
				
			}
		} );
		mTxtCreditAmountDollars.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(v.getTag().toString().equals("1"))
				{
					mTxtCreditAmountDollars.setSelection(mTxtCreditAmountDollars.getText().length());
				}
				
			}
		});
		
        mTxtCreditAmountDollars.addTextChangedListener(new TextWatcher()
		{
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				String oldString = mTxtCreditAmountDollars.getText().toString();
				String newString = removeChar(oldString, '.');
				int ilen= newString.length();
				if(ilen==1 || ilen ==2){
					newString = "." + newString;
				}else if(ilen>2){ 
					newString=newString.substring(0, ilen-2) + "." + newString.substring(ilen-2,ilen);
				}
				if(!newString.equals(oldString))
				{	
						mTxtCreditAmountDollars.setText(newString);
						mTxtCreditAmountDollars.setSelection(mTxtCreditAmountDollars.getText().length());
					
				}

			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

        Button btnSubmit = (Button)findViewById(R.id.banksale_BTN_submit);
        btnSubmit.setTypeface(applicationData.font);

        btnSubmit.setOnClickListener(new OnClickListener()
        {
			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub


				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(BankSaleView.this.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);


				double miAmountDisplay = 0;
				try{
					if(mTxtCreditAmountDollars.length()>0)
						miAmountDisplay = Double.parseDouble(mTxtCreditAmountDollars.getText().toString());


				}catch(Exception ex){
					miAmountDisplay = 0;
				}

				if(miAmountDisplay < 1)
				{
					Constants.showDialog(BankSaleView.this, Constants.BANKSALE_DIALOG_MSG, Constants.CARDSALE_ERROR_INVALIDAMT);
					return;
				}else if(mTxtChequeNo.getText().toString().trim().length() != 6 ){
					Constants.showDialog(BankSaleView.this, Constants.BANKSALE_DIALOG_MSG, Constants.CASHSALE_ERROR_invalidChequeNO);
					return;
				}else if(mTxtPhoneNum.getText().toString().trim().length()!= applicationData.PhoneNoLength ){
					String phoneLength= String.format(Constants.CARDSALE_ERROR_mobilenolen, applicationData.PhoneNoLength);
					Constants.showDialog(BankSaleView.this, Constants.BANKSALE_DIALOG_MSG, phoneLength);
					mTxtPhoneNum.requestFocus();
					return;
				}else if(mTxtPhoneNum.getText().toString().trim().startsWith("0")  ) {

					Constants.showDialog(BankSaleView.this, Constants.BANKSALE_DIALOG_MSG, Constants.CARDSALE_ERROR_mobileformat);

					mTxtPhoneNum.requestFocus();
					return;

				} else if(mTxtReceipt.getText().toString().trim().length() == 0) {

					Constants.showDialog(BankSaleView.this, Constants.BANKSALE_DIALOG_MSG,  Constants.CARDSALE_ERROR_receiptvalidation);
					return;
				}
				else{

					if(mTxtEmail.getText().toString().trim().length()!=0)
					{

						if (!Constants.isValidEmail(mTxtEmail.getText().toString().trim()))
						{
							Constants.showDialog(BankSaleView.this ,Constants.BANKSALE_DIALOG_MSG, Constants.CARDSALE_ERROR_email);
							mTxtEmail.requestFocus();
							return;
						}

					}

					mPhoneNum=mTxtPhoneNum.getText().toString();
					mEmail= mTxtEmail.getText().toString();
					mNotes=mTxtNotes.getText().toString();
					mReceipt=mTxtReceipt.getText().toString();

					String dlgMsg = String.format(Constants.BANKSALE_ALERT_AMOUNTMSG, applicationData.Currency_Code);
					final Dialog dialog = Constants.showDialog(BankSaleView.this, Constants.BANKSALE_DIALOG_MSG,
							dlgMsg + mTxtCreditAmountDollars.getText().toString(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_CONFIRMATION);


					Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
					yes.setOnClickListener(new OnClickListener()
					{

						public void onClick(View v)
						{


							dialog.dismiss();
							getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

							try {

								if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

									MSWisepadController.getSharedMSWisepadController(BankSaleView.this,
											AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
											AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
											new WisePadGatewayConncetionListener()).processBankSaleOnline(
											AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
											AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
											mTxtCreditAmountDollars.getText().toString(),
											mTxtReceipt.getText().toString(),
											SharedVariable.smsCode + mTxtPhoneNum.getText().toString(),
											mTxtEmail.getText().toString(),
											mTxtNotes.getText().toString(),
											mCheckDate,
											mTxtChequeNo.getText().toString(),
											mTxtExtraOne.getText().toString(),
											mTxtExtraTwo.getText().toString(),
											mTxtExtraThree.getText().toString(),
											mTxtExtraFour.getText().toString(),
											mTxtExtraFive.getText().toString(),
											mTxtExtraSix.getText().toString(),
											mTxtExtraSeven.getText().toString(),
											mTxtExtraEight.getText().toString(),
											mTxtExtraNine.getText().toString(),
											mTxtExtraTen.getText().toString(),
											new MSWisepadControllerResponseListenerObserver());

									mProgressActivity = new CustomProgressDialog(BankSaleView.this, "Processing Bank Tx...");
									mProgressActivity.show();
								}

								lastRequestTime = System.currentTimeMillis();


							} catch (Exception ex) {

								Constants.showDialog(BankSaleView.this, Constants.CASHSALE_DIALOG_MSG, Constants.CARDSALE_ERROR_PROCESSIING_DATA, Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_SHOW_DLG_INFO);
							} finally {

							}

						}
					});

					Button no = (Button) dialog.findViewById(R.id.customdlg_BTN_no);
					no.setOnClickListener(new OnClickListener()
					{
						public void onClick(View v)
						{

							mTxtCreditAmountDollars.requestFocus();
							dialog.dismiss();
						}
					});
					dialog.show();
				}

			}// end of the funcation
		});
         
	}


	/**
	 * MSWisepadControllerResponseListenerObserver
	 * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests
	 */
	class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener {

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 *
		 * @param aMSDataStore the generic mswipe data store, this instance is refers to InvoiceTrxData or CardSaleResponseData, so this
		 *                     need be converted back to access the relavant data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{

			if(mProgressActivity != null)
				mProgressActivity.dismiss();

			if (aMSDataStore instanceof CashSaleResponseData) {

				mBanksaleResponseData = (CashSaleResponseData) aMSDataStore;

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "Status: " + mBanksaleResponseData.getResponseStatus() + " FailureReason: " + mBanksaleResponseData.getResponseFailureReason() , true, true);

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "ExceptoinStackTrace: " + mBanksaleResponseData.getExceptoinStackTrace(), true, true);

				if(!mBanksaleResponseData.getResponseStatus()){


					final Dialog dialog = Constants.showDialog(BankSaleView.this, Constants.BANKSALE_DIALOG_MSG, mBanksaleResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
					Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
					yes.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							dialog.dismiss();
							doneWithBankSale();

						}
					});
					dialog.show();
				}
				else {

					showSignature();
				}
			}
		}
	}


	public void showSignature()
	{

		Intent intent = new Intent(this, CashSaleSignatureView.class);
		intent.putExtra("title", getString(R.string.bank_sale));
		intent.putExtra("mAmt", mTxtCreditAmountDollars.getText().toString());
		intent.putExtra("cashSaleResponseData", mBanksaleResponseData);

		startActivity(intent);

		doneWithBankSale();

	}

	public void doneWithBankSale() {

		finish();

	}

	public String removeChar(String s, char c) {

	   String r = "";

	   for (int i = 0; i < s.length(); i ++) {
	      if (s.charAt(i) != c) r += s.charAt(i);
	   }

	   return r;
	}


	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
		   // set date picker as current date
		   return new DatePickerDialog(this, datePickerListener,
                         year, month,day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener()
	{

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;
			mLblCheckDate.setText(day + " " + MONTHS[month] + " " + year);
			mCheckDate =day + "/" + (month+1) + "/" + year;

	
	
		}
	};



	class WisePadGatewayConncetionListener implements MSGatewayConnectionListener {

		@Override
		public void Connected(String msg) {


		}

		@Override
		public void Connecting(String msg) {


		}

		@Override
		public void disConnect(String msg) {


		}
	}

	
}
