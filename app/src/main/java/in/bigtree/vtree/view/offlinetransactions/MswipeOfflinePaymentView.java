package in.bigtree.vtree.view.offlinetransactions;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Logs;


public class MswipeOfflinePaymentView extends Activity
{
	EditText mTxtCreditAmount = null;
	EditText mTxtPhoneNum = null;
	EditText mTxtReceipt = null;
	EditText mTxtNotes = null;
	EditText mTxtFirstSixDigits = null;
	LinearLayout mLINFirstSixDigits = null;
	SharedVariable applicationData = null;

    public static final int MSWIPE_CARDSALE_ACTIVITY_REQUEST_CODE = 1003;

	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState
	 */

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mswipe_offline_payment_view);

		applicationData = SharedVariable.getApplicationDataSharedInstance();
		initViews();
	}


	/**
	 *@description
	 *      All fields intilising here.
	 *
	 */
	private void initViews()
	{
		mTxtCreditAmount = (EditText) findViewById(R.id.payment_TXT_amount);
		mTxtPhoneNum = (EditText) findViewById(R.id.payment_TXT_mobileno);
		mTxtReceipt = (EditText) findViewById(R.id.payment_TXT_receipt);
		mTxtNotes = (EditText) findViewById(R.id.payment_TXT_notes);
		mLINFirstSixDigits = (LinearLayout) findViewById(R.id.payment_LNR_firstsidigits);
		mTxtFirstSixDigits = (EditText) findViewById(R.id.payment_TXT_firstsidigits);
		mTxtCreditAmount.requestFocus();


		TextView txtHeading = ((TextView) findViewById(R.id.topbar_LBL_heading));

			txtHeading.setText(getResources().getString(R.string.card_sale));

		//The screen are for the amount

		Button btnAmtNext = (Button) findViewById(R.id.payment_BTN_amt_next);
		btnAmtNext.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				if(imm != null)
					imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

				double amount = 0;
				try
				{
					if (mTxtCreditAmount.length() > 0)
						amount = Double.parseDouble(removeChar(mTxtCreditAmount.getText().toString(),','));
				}
				catch (Exception ex) {
					amount = 0;
				}

				if (amount < 1)
				{
					showDialog("invalid amount! minimum amount should be inr 1.00 to proceed.");
					return;
				}

				else if (mTxtPhoneNum.getText().toString().trim().length() != 10) {

					showDialog("required length of the mobile number is 10 digits.");
					mTxtPhoneNum.requestFocus();
					return;
				}
				else if (mTxtPhoneNum.getText().toString().trim().startsWith("0")) {

					showDialog("the mobile number cannot start with 0.");
					mTxtPhoneNum.requestFocus();
					return;

				}else if (AppSharedPrefrences.getAppSharedPrefrencesInstace().isReceiptEnabled())
				{
					if (mTxtReceipt.getText().toString().length() == 0) {
					showDialog("enter invoice no.");
					mTxtReceipt.requestFocus();
					return;
					}

				}

				processCardSale();

			}
		});

	}

	private void processCardSale() {

		double amt = Double.parseDouble(mTxtCreditAmount.getText().toString().trim());
		String totalAmt = String.format("%.2f", amt);

		Intent intent_cardsale = new Intent(MswipeOfflinePaymentView.this, MswipeOfflineCardSaleActivity.class);
		intent_cardsale.putExtra("cardsale", true);

		intent_cardsale.putExtra("amount", totalAmt);
		intent_cardsale.putExtra("mobileno", mTxtPhoneNum.getText().toString().trim());
		intent_cardsale.putExtra("receiptno", mTxtReceipt.getText().toString().trim());
		intent_cardsale.putExtra("notes", mTxtNotes.getText().toString().trim());

		startActivityForResult(intent_cardsale, MSWIPE_CARDSALE_ACTIVITY_REQUEST_CODE);



    }
	private void showDialog(String message){

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setMessage(message);
		builder1.setCancelable(false);

		builder1.setPositiveButton(
				"Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();

					}
				});

		AlertDialog alert11 = builder1.create();
		alert11.show();


	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);


        if(requestCode == MSWIPE_CARDSALE_ACTIVITY_REQUEST_CODE)
		{

			boolean status = data.getBooleanExtra("status", false);
			String statusMessage = data.getStringExtra("statusMessage");

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, " statusMessage  "+ statusMessage, true, true);


			if(resultCode == RESULT_OK)
			{

				AlertDialog.Builder builder1 = new AlertDialog.Builder(MswipeOfflinePaymentView.this);
				builder1.setMessage(statusMessage);
				builder1.setCancelable(false);

				builder1.setPositiveButton(
						"Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								finish();
								/*Intent intent = new Intent(MswipeOfflinePaymentView.this, MenuView.class);
								startActivity(intent);*/

							}
						});


				AlertDialog alert11 = builder1.create();
				alert11.show();
					//showapproveDialog("","","",statusMessage);

			}else {

				AlertDialog.Builder builder1 = new AlertDialog.Builder(MswipeOfflinePaymentView.this);
				builder1.setMessage(statusMessage);
				builder1.setCancelable(false);

				builder1.setPositiveButton(
						"Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								finish();
								/*Intent intent = new Intent(MswipeOfflinePaymentView.this, MenuView.class);
								startActivity(intent);*/

							}
						});


				AlertDialog alert11 = builder1.create();
				alert11.show();
			}

		}
	}
	/**
	 * @description
	 *        We are removing specific character form original string.
	 * @param s original string
	 * @param c specific character.
	 * @return
	 */
	public String removeChar(String s, char c) {

		String r = "";

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c)
				r += s.charAt(i);
		}

		return r;
	}

}