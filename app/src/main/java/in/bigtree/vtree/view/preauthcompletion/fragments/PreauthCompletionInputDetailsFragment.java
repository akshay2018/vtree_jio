package in.bigtree.vtree.view.preauthcompletion.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.Calendar;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.preauthcompletion.PreauthCompletionActivity;

public class PreauthCompletionInputDetailsFragment extends Fragment {

	Context mContext;
	PreauthCompletionActivity mPreauthCompletionInputDetailsView = null;
	SharedVariable applicationData = null;

	public String mSelectedDate = "";
	View viewamount;

	private EditText mTXTPreauthAmt = null;
	private EditText mTXTLast4Digits = null;
	private ImageButton mIMGButtonSubmit = null;
	String mLblTot;

	// this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
	private long lastRequestTime = 0;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		
		mContext = activity;
		mPreauthCompletionInputDetailsView = ((PreauthCompletionActivity) mContext);
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		viewamount = inflater.inflate(R.layout.preauthcompletion_inputdetailsview,container, false);

		initViews(viewamount);
		setPreauthSaleData();

		return viewamount;
	}
	
	private void setPreauthSaleData() {

		if(!mPreauthCompletionInputDetailsView.mTransactionData.mBaseAmount.equalsIgnoreCase("0.00"))
			mTXTPreauthAmt.setText(mPreauthCompletionInputDetailsView.mTransactionData.mBaseAmount);

		if(mPreauthCompletionInputDetailsView.mTransactionData.mLast4Digits.length()>0)
			mTXTLast4Digits.setText(mPreauthCompletionInputDetailsView.mTransactionData.mLast4Digits);

	}

	public void initViews(View view) {
		// TODO Auto-generated method stub


		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		mSelectedDate = year + "-" + (month + 1) + "-" + day;

		mTXTPreauthAmt = (EditText) view.findViewById(R.id.preauthcompletion_inputdetailsview_TXT_amount);
		mTXTLast4Digits = (EditText) view.findViewById(R.id.preauthcompletion_inputdetailsview_TXT_last4digits);

		mIMGButtonSubmit = (ImageButton)view.findViewById(R.id.preauthcompletion_inputdetailsview_BTN_next);

		mTXTPreauthAmt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {


			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

				calculateTotalAmt();

			}
		});

		mTXTLast4Digits.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {

				String searchString = s.toString();
				int textLength = searchString.length();
				mTXTLast4Digits.setSelection(textLength);

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});



		mIMGButtonSubmit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
				if(imm != null)
					imm.hideSoftInputFromWindow(mPreauthCompletionInputDetailsView.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

				double miAmountDisplay = 0;
				try {
					if (mTXTPreauthAmt.getText().toString().length() > 0)
						miAmountDisplay = Double.parseDouble(removeChar(mTXTPreauthAmt.getText().toString(), ','));


				} catch (Exception ex) {
					miAmountDisplay = 0;
				}


				if (miAmountDisplay < 1) {
					Constants.showDialog(mPreauthCompletionInputDetailsView, getString(R.string.preauth_completion),
							String.format(getString(R.string.preauthcompletioninputdetailsfragment_you_need_to_enter_a_minimum_amount_of_at_least_inr_001_to_proceed), applicationData.mCurrency));

					return;
				} else if (mTXTLast4Digits.getText().toString().trim().length() < 3) {
					Constants.showDialog(mPreauthCompletionInputDetailsView, getString(R.string.preauth_completion),
							getString(R.string.preauthcompletioninputdetailsfragment_invalid_last_4_digits));
					mTXTLast4Digits.requestFocus();
					return;
				}
				else {

					mPreauthCompletionInputDetailsView.mTransactionData.mBaseAmount = mTXTPreauthAmt.getText().toString();
					mPreauthCompletionInputDetailsView.mTransactionData.mLast4Digits = mTXTLast4Digits.getText().toString();

					if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

						mPreauthCompletionInputDetailsView.getPreauthCompletionDetails();
					}

					lastRequestTime = System.currentTimeMillis();

				}

			}
		});

	}


	public void calculateTotalAmt() {

		//remove the decimal since in j2me it does not exists
		String strAmount = mTXTPreauthAmt.getText().toString().trim();
		strAmount = removeChar(strAmount, ',');

		if(strAmount.length() == 0 ){
			strAmount = "0.0";
		}


		double baseAmount = Double.parseDouble(strAmount);


		String totalAmount;

		totalAmount = String.format("%.2f", baseAmount);

		String text = totalAmount;
		int ilen = text.length();
		if (ilen > 6){
			text = text.substring(0, ilen - 6)+","+ text.substring(ilen - 6, ilen);
		}

		ilen = text.length();
		if (ilen > 9){
			text = text.substring(0, ilen - 9)+","+ text.substring(ilen - 9, ilen);
		}

		mLblTot = text;
	}


	public String removeChar(String s, char c) {

		String r = "";

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c)
				r += s.charAt(i);
		}

		return r;
	}

}
