package in.bigtree.vtree.view.offlinetransactions;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.view.BaseTitleActivity;

/**
 * Created by mswipe on 5/11/17.
 */

public class OfflineTransactionMenu extends BaseTitleActivity
{

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.offline_transaction_view);

        initViews();
    }

    /**
     * Initializes the UI elements for the login screen
     * @param
     *
     * @return
     *
     */
    private void initViews()
    {

        ((TextView) findViewById(R.id.topbar_LBL_heading)).setText("Offline Transactions");


        ((Button) findViewById(R.id.offline_btn_cardsale)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent intent = new Intent(OfflineTransactionMenu.this,MswipeOfflinePaymentView.class);
                startActivity(intent);
                finish();


            }
        });


        ((Button) findViewById(R.id.offline_btn_cashsale)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(OfflineTransactionMenu.this,OfflineCashSaleView.class);
                startActivity(intent);
                finish();

            }
        });


        ((Button) findViewById(R.id.offline_btn_banksale)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent intent = new Intent(OfflineTransactionMenu.this,OfflineBankSaleView.class);
                startActivity(intent);
                finish();

            }
        });



    }



}