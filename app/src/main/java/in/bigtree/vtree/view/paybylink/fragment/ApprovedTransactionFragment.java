package in.bigtree.vtree.view.paybylink.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.PayByLinkResponseData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.paybylink.PayByLinkActivity;
import in.bigtree.vtree.view.paybylink.PayByLinkEnum;

public class ApprovedTransactionFragment extends android.support.v4.app.Fragment

{
	private TextView mLBLAmtMsg = null;
	private TextView mTXTProgMsg = null;

	private CustomProgressDialog mProgressActivity;

	Context mContext;
	PayByLinkActivity mPayByLinkActivity = null;
	SharedVariable applicationData = null;

	@Override
	public void onAttach(Activity activity) {

		mContext = activity;
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		mPayByLinkActivity = ((PayByLinkActivity)mContext);
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		View view =  inflater.inflate(R.layout.paybylink_approved_transactionview, container, false);
		initViews(view);
		return view;
	}


	private void initViews(View view)
	{

		try {
			InputMethodManager imm = (InputMethodManager) mPayByLinkActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (imm != null)
				imm.hideSoftInputFromWindow(mPayByLinkActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
		catch (Exception e){

		}


		mLBLAmtMsg = (TextView) view.findViewById(R.id.paybylink_totalamountview_LBL_totalamount);
		mTXTProgMsg = (TextView) view.findViewById(R.id.paybylink_TXT_mobileno_progmsg);

		mTXTProgMsg.setText("payment link has been sent to your referred mobile number \"" + mPayByLinkActivity.mTransactionData.mPhoneNo+"\"");

		if(mPayByLinkActivity.mTransactionData.mBaseAmount.toString().length()>= 7)
			mLBLAmtMsg.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.font_size_RLH15));
		else
			mLBLAmtMsg.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.font_size_RLH20));


		mLBLAmtMsg.setText(mPayByLinkActivity.mTransactionData.mBaseAmount);

		((ImageButton)view.findViewById(R.id.paybylink_approved_BTN_swiper)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				mPayByLinkActivity.showScreen(PayByLinkEnum.PayByLinkScreens.PayByLink_Screen_Selection);

			}
		});

		((Button)view.findViewById(R.id.paybylink_BTN_check_status)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				checkPayByLinkStatus();

			}
		});


	}

	public boolean checkPayByLinkStatus() {

		try {

			final MSWisepadController wisepadController = MSWisepadController.
					getSharedMSWisepadController(mPayByLinkActivity,
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

			wisepadController.checkPayByLinkStatus(
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
					mPayByLinkActivity.mTrnxId,
					new PayByLinkCheckStatusListener());

			mProgressActivity = null;
			mProgressActivity = new CustomProgressDialog(mPayByLinkActivity, getString(R.string.processing));
			mProgressActivity.show();

		} catch (Exception ex) {

			Constants.showDialog(mPayByLinkActivity, "payby link",
					"the payby link transaction was not processed successfully. please try again");

		}

		return true;
	}

	class PayByLinkCheckStatusListener implements MSWisepadControllerResponseListener
	{

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
		 * need be type cast back to LastTransactionResponseData to access the  response data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{
			if (mProgressActivity != null)
				mProgressActivity.dismiss();

			PayByLinkResponseData payByLinkResponseData = (PayByLinkResponseData) aMSDataStore;
			if(payByLinkResponseData.getResponseStatus())
			{
                String date = Constants.getDateWithFormate(payByLinkResponseData.getTrxDate(), "MM/dd/yyyy hh:mm:ss a", "dd-MMM-yyyy hh:mma");

				showapproveDialog(payByLinkResponseData.getTrxStatus(), payByLinkResponseData.getAmount(),
						payByLinkResponseData.getTrxId(), date.toLowerCase());
			}
			else{

				showfailedDialog("failed" ,payByLinkResponseData.getResponseFailureReason());
			}
		}
	}


	public void showapproveDialog(String status, String amount, String trnxId, String date)
	{

		final Dialog dialog = new Dialog(mPayByLinkActivity, R.style.styleCustDlg);
		dialog.setContentView(R.layout.payby_link_status_customdlg);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(true);

		((LinearLayout) dialog.findViewById(R.id.customdlg_LNR_reason)).setVisibility(View.GONE);

		TextView txtstatusmsg = (TextView) dialog.findViewById(R.id.customdlg_Txt_status);
		txtstatusmsg.setText(status);

		TextView txtauthcode = (TextView) dialog.findViewById(R.id.customdlg_Txt_amount);
		txtauthcode.setText(amount);

		TextView txtrrno = (TextView) dialog.findViewById(R.id.customdlg_Txt_trnx_id);
		txtrrno.setText(trnxId);

		TextView txtreason = (TextView) dialog.findViewById(R.id.customdlg_Txt_date);
		txtreason.setText(date);

		Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
		yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				mPayByLinkActivity.showScreen(PayByLinkEnum.PayByLinkScreens.PayByLink_Screen_Selection);
			}
		});

		dialog.show();
	}
	public void showfailedDialog(String status , String reason)
	{

		final Dialog dialog = new Dialog(mPayByLinkActivity, R.style.styleCustDlg);
		dialog.setContentView(R.layout.payby_link_status_customdlg);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(true);

		TextView txtstatusmsg = (TextView) dialog.findViewById(R.id.customdlg_Txt_status);
		txtstatusmsg.setText(status);

		((LinearLayout) dialog.findViewById(R.id.customdlg_LNR_amount)).setVisibility(View.GONE);
		((LinearLayout) dialog.findViewById(R.id.customdlg_LNR_trnx_id)).setVisibility(View.GONE);
		((LinearLayout) dialog.findViewById(R.id.customdlg_LNR_date)).setVisibility(View.GONE);

		TextView txtreason = (TextView) dialog.findViewById(R.id.customdlg_Txt_reason);
		txtreason.setText(reason);

		Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
		yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

}
