package in.bigtree.vtree.view.summary;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.view.BaseTitleActivity;

public class CashSummaryDetails extends BaseTitleActivity
{
	SharedVariable applicationData = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.cashbanksummarydetails);
		applicationData = (SharedVariable)getApplicationContext();

        initViews();
    }
	
	private void initViews() 
	{
		((TextView)findViewById(R.id.topbar_LBL_heading)).setText("cash summary");
		((TextView)findViewById(R.id.topbar_LBL_heading)).setTypeface(applicationData.font);
		
		
		Intent intent = getIntent();
		
		String cashSaleAmount = intent.getStringExtra("CashSaleAmount");
		String totalCount = intent.getStringExtra("CashSaleCount");
		String summaryDate = intent.getStringExtra("SummaryDate");
		
		
		TextView lblSummaryDate = (TextView)  findViewById(R.id.cashsummarydetails_LBL_TxDateTime);
		lblSummaryDate.setTypeface(applicationData.font);
		((TextView)  findViewById(R.id.cashsummarydetails_LBL_lblTxDateTime)).setTypeface(applicationData.font);
		
		if(summaryDate!=null)
			lblSummaryDate.setText(summaryDate);


		TextView cashAmt = (TextView) findViewById(R.id.cashsummarydetails_LBL_cashamount);
		cashAmt.setTypeface(applicationData.font);
		((TextView)  findViewById(R.id.cashsummarydetails_LBL_lblcashamount)).setTypeface(applicationData.font);

		if(cashSaleAmount!=null)
			cashAmt.setText(cashSaleAmount);

		((TextView) findViewById(R.id.cashsummarydetails_LBL_Amtprefixcashamount)).setTypeface(applicationData.font);
		((TextView) findViewById(R.id.cashsummarydetails_LBL_Amtprefixcashamount)).setText(SharedVariable.mCurrency);


		((TextView)findViewById(R.id.cashsummarydetails_LBL_lblNoofVoids)).setText("no. of cash sale");
		((TextView)findViewById(R.id.cashsummarydetails_LBL_lblNoofVoids)).setTypeface(applicationData.font);
		TextView txtVoid = (TextView) findViewById(R.id.cashsummarydetails_LBL_NoofVoids);
		txtVoid.setTypeface(applicationData.font);
		((TextView)  findViewById(R.id.cashsummarydetails_LBL_lblNoofVoids)).setTypeface(applicationData.font);
		
		if(totalCount!=null)
			txtVoid.setText(totalCount);
    	
				
	}	
}