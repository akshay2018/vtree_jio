package in.bigtree.vtree.view.offlinehistory;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.OfflineTransactionUploadService;
import com.mswipetech.wisepad.sdk.OfflineTransactionUploadService.LocalBinder;
import com.mswipetech.wisepad.sdk.data.CardSaleResponseData;
import com.mswipetech.wisepad.sdk.data.CashSaleResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.OfflineResponseData;
import com.mswipetech.wisepad.sdk.data.OfflineTransationData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.ArrayList;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDlgFullScreen;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.MenuView;


public class OfflineHistoryActivity extends Activity {

	private CustomProgressDlgFullScreen mProgressActivity = null;
	private int mProcessCount = 0 ;
	private ArrayList<OfflineTransationData> mCardSaleOfflineDataList = new ArrayList<OfflineTransationData>();

	public MSWisepadControllerResponseListener  mOfflineListener = null;
	ImageView mIMGUpload;

	private OfflineTransactionUploadService  mOfflineTransactionUploadService;

	private ListView mLVOfflineData = null;
	private HistoryAdapter mOfflineDataAdapter = null;
	private TextView mTXTNoOfRecords = null;
	private TextView mTXTTotalAmount = null;

	private static final int GATEWAY_REQUEST_THRESHOLD_TIME = 1200;

	private static long mProcessedEndTime = 0;
	boolean isBound;

	/**
	 * Called when the activity is first created.
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.cardsale_offline_view);

		mOfflineListener = new OfflineServiceResponseListenerObserver();

		initViews();
	}

	@Override
	protected void onStart() {

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "onStart", true, true);

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {

				/**
				 * binding the service will start the service, and notifies back to the bound objects and this will
				 * unable the interactions with the service
				 *
				 */

				try
				{
                  bindService();
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		}, 1000);

		super.onStart();


	}


	private void initViews() {

		((ImageView)findViewById(R.id.topbar_IMG_position1)).setVisibility(View.GONE);
		if(mProgressActivity != null) {
			mProgressActivity = new CustomProgressDlgFullScreen(OfflineHistoryActivity.this, getString(R.string.offline_trx));
			mProgressActivity.dismiss();
		}

		((TextView)findViewById(R.id.topbar_LBL_heading)).setText(getResources().getString(R.string.offline_trx));

		mIMGUpload = (ImageView)findViewById(R.id.cardsale_offline_IMG_upload_action);

		mLVOfflineData = (ListView) findViewById(R.id.cardsale_offline_LV_history);
		mTXTNoOfRecords = (TextView) findViewById(R.id.cardsale_offline_TXT_no_of_trx);
		mTXTTotalAmount = (TextView) findViewById(R.id.cardsale_offline_TXT_trx_amount);


		((RelativeLayout)findViewById(R.id.cardsale_offline_REL_upload_action)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "upload button click", true, true);

				if (isNetworkConnected())
				{
					if(System.currentTimeMillis() - mProcessedEndTime >= GATEWAY_REQUEST_THRESHOLD_TIME) {

						if (mOfflineTransactionUploadService != null)
							mOfflineTransactionUploadService.startSyncProcess();

						mProgressActivity = null;
						mProgressActivity = new CustomProgressDlgFullScreen(OfflineHistoryActivity.this, "offline trnx","processing transactions"+0+"/"+mCardSaleOfflineDataList.size(),true);
						mProgressActivity.show();

						Button dialog_btn = (Button) mProgressActivity.findViewById(R.id.progess_dlg_BTN_cancel);
						dialog_btn.setOnClickListener(new View.OnClickListener()
						{
							@Override
							public void onClick(View v) {

								if (System.currentTimeMillis() - mProcessedEndTime >= GATEWAY_REQUEST_THRESHOLD_TIME) {

									mProgressActivity.setProcessMessage("cancelling process");

									if (mOfflineTransactionUploadService != null)
										mOfflineTransactionUploadService.stopSyncProcess();

								}

								mProcessedEndTime = System.currentTimeMillis();
							}
						});
					}

					mProcessedEndTime = System.currentTimeMillis();

				}else
				{


					AlertDialog.Builder builder1 = new AlertDialog.Builder(OfflineHistoryActivity.this);
					builder1.setMessage("data service is not available, please check your Wifi connection or mobile data connection");
					builder1.setCancelable(false);

					builder1.setPositiveButton(
							"Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.dismiss();
									finish();
									Intent intent = new Intent(OfflineHistoryActivity.this, MenuView.class);
									startActivity(intent);

								}
							});


					AlertDialog alert11 = builder1.create();
					alert11.show();
				}
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			finish();
			startActivity(new Intent(OfflineHistoryActivity.this, MenuView.class));
		}
		return super.onKeyDown(keyCode, event);
	}

	public class HistoryAdapter extends BaseAdapter {

		ArrayList<OfflineTransationData> listData = null;
		Context context;

		public HistoryAdapter(Context context, ArrayList<OfflineTransationData> listData) {
			this.listData = listData;
			this.context = context;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listData.size();
		}

		@Override
		public OfflineTransationData getItem(int position) {
			// TODO Auto-generated method stub
			return listData.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			int type = getItemViewType(position);

			LayoutInflater inflater = LayoutInflater.from(context);

			convertView = inflater.inflate(R.layout.card_offlinedata_listrow, null);


			OfflineTransationData cardSaleOfflineData = (OfflineTransationData) listData.get(position);

			TextView lblStatus = (TextView) convertView.findViewById(R.id.cardtxtoffline_txt_status);
			String statusMsg = (cardSaleOfflineData.statusmessage).toLowerCase();
			lblStatus.setText(statusMsg);

			CheckBox radiotNewAlertsEdit = (CheckBox)convertView.findViewById(R.id.cardtxtoffline_RDB_new_alertedit);
			String date = Constants.getDateWithFormate(cardSaleOfflineData.trxdate.toLowerCase(), "yy MMM dd hh:mm a", "dd-MMM-yyyy hh:mma");

			((TextView) convertView.findViewById(R.id.cardtxtoffline_txt_date)).setText(date);

			if((cardSaleOfflineData.statusmessage).toLowerCase().equalsIgnoreCase("failed"))
				((TextView) convertView.findViewById(R.id.cardtxtoffline_txt_status)).setTextColor(getResources().getColor(R.color.red));
			else if((cardSaleOfflineData.statusmessage).toLowerCase().equalsIgnoreCase("pending"))
				((TextView) convertView.findViewById(R.id.cardtxtoffline_txt_status)).setTextColor(getResources().getColor(R.color.light_grey));
			else
				((TextView) convertView.findViewById(R.id.cardtxtoffline_txt_status)).setTextColor(getResources().getColor(R.color.green));


			if (!cardSaleOfflineData.trxtype.equalsIgnoreCase("card"))
				((LinearLayout) convertView.findViewById(R.id.offline_lin_last_digits)).setVisibility(View.GONE);

			((TextView) convertView.findViewById(R.id.cardtxtoffline_txt_totalamount)).setText(SharedVariable.mCurrency + " " + cardSaleOfflineData.totalamount.toString());
			((TextView) convertView.findViewById(R.id.cardtxtoffline_txt_trnxtype)).setText( cardSaleOfflineData.trxtype);
			((TextView) convertView.findViewById(R.id.cardtxtoffline_txt_last4digits)).setText( cardSaleOfflineData.lastfourdigits);
			return convertView;
		}
	}


	protected ServiceConnection mServerConn = new ServiceConnection() {


		@Override
		public void onServiceConnected(ComponentName className, IBinder service)
		{
			try
			{
				LocalBinder binder = (LocalBinder)service;
				mOfflineTransactionUploadService = binder.getService();
				isBound = true;
				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "onServiceConnected.", true, true);

			if (mOfflineTransactionUploadService != null)
			{

				mOfflineTransactionUploadService.initOfflineService(
						AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
						AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
						AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
						AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
						mOfflineListener);

				getOfflineData();
			}

			}
			catch (Exception e) {

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "exception."+e.toString(), true, true);
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mOfflineTransactionUploadService = null;
			isBound = false;
			Log.d("", "onServiceDisconnected");
		}
	};


	private void getOfflineData(){

		mProgressActivity = null;
		mProgressActivity = new CustomProgressDlgFullScreen(OfflineHistoryActivity.this, getString(R.string.offline_trx));
		mProgressActivity.show();

		if(mOfflineTransactionUploadService != null)
			mOfflineTransactionUploadService.getOfflinedata();

	}

	void bindService()
	{
		bindService(new Intent(this, OfflineTransactionUploadService.class), mServerConn, Context.BIND_AUTO_CREATE);
		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "bindService.", true, true);

	}


	@Override
	protected void onStop() {

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "onStop", true, true);

	  try{
		  doUnbindOfflineTransactionService();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		super.onStop();
	}

	public void doUnbindOfflineTransactionService()
	{

		if (isBound)
		{
			unbindService(mServerConn);
			isBound = false;
		}
	}


	private void refreshOffileData(){

		mCardSaleOfflineDataList.clear();
		getOfflineData();

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "mCardSaleOfflineDataList.size() " + mCardSaleOfflineDataList.size(), false, true);

	}


	class OfflineServiceResponseListenerObserver implements MSWisepadControllerResponseListener {

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 *
		 * @param aMSDataStore the generic mswipe data store, this instance is refers to Receipt information, so this
		 *                     need be converted back to CardSaleReceiptResponseData to access the receipt response data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{

			if(aMSDataStore instanceof CardSaleResponseData){

				CardSaleResponseData cardSaleResponseData = (CardSaleResponseData) aMSDataStore;

				mProcessCount++;
				mProgressActivity.setProcessMessage("processing transactions"+mProcessCount+"/"+mCardSaleOfflineDataList.size());


				if (cardSaleResponseData.getErrorNo() == 801)
				{
					mProcessCount = 0;

					if (mProgressActivity != null)
					{
						mProgressActivity.dismiss();
						mProgressActivity = null;
					}

					final Dialog dialog = Constants.showDialog(OfflineHistoryActivity.this, getString(R.string.offline_trx),
							cardSaleResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
					Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
					yes.setOnClickListener(new View.OnClickListener() {

						public void onClick(View v) {
							dialog.dismiss();

							refreshOffileData();

						}
					});

					dialog.show();

				}else if (mCardSaleOfflineDataList.size() ==  mProcessCount)
				{

					if (mProgressActivity != null)
					{
						mProgressActivity.dismiss();
						mProgressActivity = null;
					}

					final Dialog dialog = Constants.showDialog(OfflineHistoryActivity.this, getString(R.string.offline_trx),
							getString(R.string.no_offline_transactions_exist), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
					Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
					yes.setOnClickListener(new View.OnClickListener() {

						public void onClick(View v) {
							dialog.dismiss();
							finish();
							startActivity(new Intent(OfflineHistoryActivity.this, MenuView.class));

						}
					});

					dialog.show();

				}

			}else if(aMSDataStore instanceof CashSaleResponseData){

				CashSaleResponseData cashSaleResponseData = (CashSaleResponseData) aMSDataStore;

				mProcessCount++;
                mProgressActivity.setProcessMessage(" processing transactions"+mProcessCount+"/"+mCardSaleOfflineDataList.size());

				if (cashSaleResponseData.getErrorNo() == 801)
				{
					mProcessCount = 0;

					if (mProgressActivity != null)
					{
						mProgressActivity.dismiss();
						mProgressActivity = null;
					}

					final Dialog dialog = Constants.showDialog(OfflineHistoryActivity.this, getString(R.string.offline_trx),
							cashSaleResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
					Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
					yes.setOnClickListener(new View.OnClickListener() {

						public void onClick(View v) {
							dialog.dismiss();
							refreshOffileData();

						}
					});

					dialog.show();


				}
				else if (mCardSaleOfflineDataList.size() ==  mProcessCount)
				{

					if (mProgressActivity != null)
					{

                        mProgressActivity.dismiss();
						mProgressActivity = null;
					}

					final Dialog dialog = Constants.showDialog(OfflineHistoryActivity.this, getString(R.string.offline_trx),
							getString(R.string.no_offline_transactions_exist), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
					Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
					yes.setOnClickListener(new View.OnClickListener() {

						public void onClick(View v) {
							dialog.dismiss();
							finish();
							startActivity(new Intent(OfflineHistoryActivity.this, MenuView.class));

						}
					});

					dialog.show();

				}


			}
			else if(aMSDataStore instanceof OfflineResponseData){

				if (mProgressActivity != null)
				{
					mProgressActivity.dismiss();
					mProgressActivity = null;
				}

				OfflineResponseData offlineResponseData = (OfflineResponseData) aMSDataStore;

				if (offlineResponseData.getResponseStatus())
				{
					mCardSaleOfflineDataList = offlineResponseData.getOfflineTransactions();

					if (mCardSaleOfflineDataList.size() ==  0 ){

						final Dialog dialog = Constants.showDialog(OfflineHistoryActivity.this, getString(R.string.offline_trx),
								getString(R.string.no_offline_transactions_exist), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
						Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
						yes.setOnClickListener(new View.OnClickListener() {

							public void onClick(View v) {
								dialog.dismiss();
								finish();
								startActivity(new Intent(OfflineHistoryActivity.this, MenuView.class));

							}
						});
						dialog.show();


					}
					else{


						runOnUiThread(new Runnable() {

							@Override
							public void run() {

								// Stuff that updates the UI

							}
						});
						mOfflineDataAdapter = new HistoryAdapter(OfflineHistoryActivity.this, mCardSaleOfflineDataList);
						mLVOfflineData.setAdapter(mOfflineDataAdapter);
						mOfflineDataAdapter.notifyDataSetChanged();

						if (SharedVariable.IS_DEBUGGING_ON)
							Logs.v(SharedVariable.packName, "mCardSaleOfflineDataList.size() " + mCardSaleOfflineDataList.size(), false, true);

						try {

							mProcessCount = 0;

							double totalAmount = 0.0;
							for(int i= 0; i< mCardSaleOfflineDataList.size();i++){

								totalAmount = totalAmount+ Double.parseDouble(mCardSaleOfflineDataList.get(i).totalamount);
							}

							mTXTNoOfRecords.setText(""+mCardSaleOfflineDataList.size());

							if(totalAmount > 0)
								mTXTTotalAmount.setText(""+ String.format("%.2f", totalAmount));
							else
								mTXTTotalAmount.setText("0.0");

						}

						catch (Exception e){

						}
					}
				}else {

					final Dialog dialog = Constants.showDialog(OfflineHistoryActivity.this, getString(R.string.offline_trx),
							offlineResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
					Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
					yes.setOnClickListener(new View.OnClickListener() {

						public void onClick(View v) {
							dialog.dismiss();

							refreshOffileData();

						}
					});
					dialog.show();

				}
			}
		}
	}


	public boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

		return cm.getActiveNetworkInfo() != null;
	}
}