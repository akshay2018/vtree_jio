package in.bigtree.vtree.view.bqrsale.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.BQRResponseData;
import com.mswipetech.wisepad.sdk.data.BQRTrxListData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.ArrayList;

import in.bigtree.vtree.R;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.bqrsale.BQRSaleActivity;
import in.bigtree.vtree.view.bqrsale.BQRSaleEnum;


/**
 * Created by Mswipe on 5/10/2017.
 */

public class CheckStatusListFragment extends Fragment
{

    BQRSaleActivity mBQRSaleActivity = null;

    ArrayList<BQRTrxListData> listData = null;
    private CustomProgressDialog mProgressActivity;
    ListView listView;

    public void onAttach(Activity activity) {
        mBQRSaleActivity = (BQRSaleActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.paybylink_bank_selection_fragment, container, false);
        initViews(view);
        return view;
    }

    public void initViews(View view)
    {

        listView = (ListView) view.findViewById(R.id.payby_link_bank_view_LST_options);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

            }
        });

        getTrxList();
    }

    public boolean getTrxList() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mBQRSaleActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            wisepadController.getBQRTrxList(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    new BQRStatusListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mBQRSaleActivity, getString(R.string.processing));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mBQRSaleActivity, getString(R.string.mcard_sale),
                    getString(R.string.voidsaleactivity_the_otp_was_not_processed_successfully_please_try_again));

        }

        return true;
    }

    class BQRStatusListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            BQRResponseData bqrResponseData = (BQRResponseData) aMSDataStore;
            if(bqrResponseData.getResponseStatus())
            {
                listData = bqrResponseData.getQRTrxList();

                PayByLinkBankViewAdapter adapter = new PayByLinkBankViewAdapter(mBQRSaleActivity, listData);
                listView.setAdapter(adapter);

            }
            else{

                final Dialog dialog = Constants.showDialog(mBQRSaleActivity, getString(R.string.payby_link), bqrResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        mBQRSaleActivity.showScreen(BQRSaleEnum.BQRSaleScreens.BQR_SELECTION_SCREEN);
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        }
    }


    public class PayByLinkBankViewAdapter extends BaseAdapter {

        ArrayList<BQRTrxListData> listData = null;
        Context context;

        public PayByLinkBankViewAdapter(Context context, ArrayList<BQRTrxListData> listData) {
            this.listData = listData;
            this.context = context;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.upisale_statuslist_row_view, null);
            }
            final BQRTrxListData listDataModel = listData.get(position);

            TextView txt_date = (TextView) convertView.findViewById(R.id.upi_TXT_dateandtime);
            TextView txt_amount = (TextView) convertView.findViewById(R.id.upi_TXT_amount);
            TextView txt_status = (TextView) convertView.findViewById(R.id.upi_TXT_status);
            TextView txt_transactionId = (TextView) convertView.findViewById(R.id.upi_TXT_transactionid);
            TextView txt_transactiontype = (TextView) convertView.findViewById(R.id.upi_TXT_transactiontype);
            TextView txt_checkstatus = (TextView) convertView.findViewById(R.id.upisale_LBL_check_status);

            txt_amount.setText(Constants.getAmountWithComma(listDataModel.getAmount()));
            txt_status.setText(listDataModel.getTrxStatus().toLowerCase());
            txt_transactionId.setText(listDataModel.getTxnID());
            txt_transactiontype.setText(listDataModel.getTrxType());

            String trxDate = Constants.getDateWithFormate(listDataModel.getTrxDate(), "MM/dd/yyyy hh:mm:ss a", "dd-MMM-yyyy hh:mma");
            txt_date.setText(trxDate);

            txt_checkstatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            checkBQRSaleStatus();
                        }
                    }, 100);
                }
            });

            return convertView;
        }
    }




    public boolean checkBQRSaleStatus() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mBQRSaleActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            wisepadController.getBQRSaleStatus(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    mBQRSaleActivity.mBQRTrxId,
                    new BQRSChecktatusListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mBQRSaleActivity, getString(R.string.processing));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mBQRSaleActivity, getString(R.string.mcard_sale),
                    getString(R.string.voidsaleactivity_the_otp_was_not_processed_successfully_please_try_again));

        }

        return true;
    }

    class BQRSChecktatusListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            BQRResponseData bqrResponseData = (BQRResponseData) aMSDataStore;
            if(bqrResponseData.getResponseStatus())
            {
                mBQRSaleActivity.mTrnxDate = bqrResponseData.getTrnx_date();
                mBQRSaleActivity.mAuthcode = bqrResponseData.getAuthcode();
                mBQRSaleActivity.mRRNo = bqrResponseData.getRrno();
                mBQRSaleActivity.mVocherNo = bqrResponseData.getVocherno();
                mBQRSaleActivity.mTrnxType = bqrResponseData.getTrnx_type();

                showapproveDialog("approved",mBQRSaleActivity.mAuthcode ,mBQRSaleActivity.mRRNo, "" );

            }
            else{

                mBQRSaleActivity.mErrorMsg = bqrResponseData.getResponseFailureReason();

                showfailedDialog("failed" ,mBQRSaleActivity.mErrorMsg);
            }
        }
    }

    public void showapproveDialog(String status, String authcode, String rrno, String reason)
    {

        final Dialog dialog = new Dialog(mBQRSaleActivity, R.style.styleCustDlg);
        dialog.setContentView(R.layout.cardsale_status_customdlg);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        TextView txtstatusmsg = (TextView) dialog.findViewById(R.id.customdlg_Txt_status);
        txtstatusmsg.setText(status);

        TextView txtauthcode = (TextView) dialog.findViewById(R.id.customdlg_Txt_authcode);
        txtauthcode.setText(authcode);

        TextView txtrrno = (TextView) dialog.findViewById(R.id.customdlg_Txt_rrno);
        txtrrno.setText(rrno);

        TextView txtreason = (TextView) dialog.findViewById(R.id.customdlg_Txt_reason);
        txtreason.setText(reason);

        Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mBQRSaleActivity.finish();
                Intent intent = new Intent(mBQRSaleActivity, MenuView.class);
                startActivity(intent);
            }
        });

        dialog.show();
    }
    public void showfailedDialog(String status , String reason)
    {

        final Dialog dialog = new Dialog(mBQRSaleActivity, R.style.styleCustDlg);
        dialog.setContentView(R.layout.cardsale_status_customdlg);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        TextView txtstatusmsg = (TextView) dialog.findViewById(R.id.customdlg_Txt_status);
        txtstatusmsg.setText(status);

        ((LinearLayout) dialog.findViewById(R.id.customdlg_LNR_authcode)).setVisibility(View.GONE);
        ((LinearLayout) dialog.findViewById(R.id.customdlg_LNR_rrn)).setVisibility(View.GONE);

        TextView txtreason = (TextView) dialog.findViewById(R.id.customdlg_Txt_reason);
        txtreason.setText(reason);

        Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}