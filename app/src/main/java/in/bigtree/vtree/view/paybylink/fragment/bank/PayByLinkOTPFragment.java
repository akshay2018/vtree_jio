package in.bigtree.vtree.view.paybylink.fragment.bank;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.IPGBankResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.OTPResponseData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.paybylink.PayByLinkActivity;
import in.bigtree.vtree.view.paybylink.PayByLinkEnum;


/**
 * Created by mSwipe on 2/1/2016.
 */
public class PayByLinkOTPFragment extends Fragment {

    Context mContext;
    PayByLinkActivity mPayByLinkActivity = null;
    SharedVariable applicationData = null;

    EditText mTXTotp = null;
    ImageButton mBTNext = null;
    TextView mLBLResendOtp;

    private long lastRequestTime = 0;

    private CustomProgressDialog mProgressActivity = null;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        mContext = activity;
        mPayByLinkActivity = ((PayByLinkActivity) mContext);
        applicationData = SharedVariable.getApplicationDataSharedInstance();

        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.paybylink_otp_fragment,container, false);

        initViews(view);
        return view;
    }

    protected void initViews(View view) {

        sendOTP();

        mTXTotp =(EditText)view.findViewById(R.id.payby_link_otp_verification_TXT_otp);
        mLBLResendOtp =(TextView)view.findViewById(R.id.payby_link_LBL_resendotp);


        mBTNext =(ImageButton)view.findViewById(R.id.payby_link_otp_verification_BTN_next);
        mBTNext.setEnabled(false);

        mTXTotp.requestFocus();

        mLBLResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               sendOTP();

            }
        });

        mTXTotp.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                String searchString = s.toString();
                int textLength = searchString.length();
                mTXTotp.setSelection(textLength);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (mTXTotp.getText().toString().length() >= 4) {

                    mBTNext.setEnabled(true);

                    mLBLResendOtp.setTextColor(getResources().getColor(R.color.light_grey));
                    mLBLResendOtp.setClickable(false);

                } else {

                    mBTNext.setEnabled(false);
                    mLBLResendOtp.setTextColor(getResources().getColor(R.color.blue));
                    mLBLResendOtp.setClickable(true);
                }
            }
        });

        mBTNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

                    updateIPGBankList();
                }

                lastRequestTime = System.currentTimeMillis();

            }
        });
    }

    public boolean sendOTP() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mPayByLinkActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

            wisepadController.sendIPGBankOTP(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    new SendOTPListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mPayByLinkActivity, getString(R.string.processing));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mPayByLinkActivity, getString(R.string.mcard_sale),
                    getString(R.string.voidsaleactivity_the_otp_was_not_processed_successfully_please_try_again));

        }

        return true;
    }

    /**
     * MswipeWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

     */
    class SendOTPListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            OTPResponseData otpResponseData = (OTPResponseData) aMSDataStore;
            if(otpResponseData.getResponseStatus())
            {
                final Dialog dialog = Constants.showDialog(mPayByLinkActivity, getString(R.string.payby_link), String.format(getResources().getString(R.string.ipg_otp_sent_message)), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

          }
          else{

                final Dialog dialog = Constants.showDialog(mPayByLinkActivity, getString(R.string.payby_link), otpResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        mPayByLinkActivity.showScreen(PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_Terms_Conditions);
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        }
    }

    public boolean updateIPGBankList() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mPayByLinkActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

            wisepadController.updateIPGBankDetails(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    mPayByLinkActivity.mBankCode ,
                    mPayByLinkActivity.mLocationCode ,
                    mTXTotp.getText().toString(),
                    new UpdateIPGListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mPayByLinkActivity, getString(R.string.processing));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mPayByLinkActivity, getString(R.string.mcard_sale),
                    getString(R.string.voidsaleactivity_the_otp_was_not_processed_successfully_please_try_again));

        }

        return true;
    }

    class UpdateIPGListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            final IPGBankResponseData responseData = (IPGBankResponseData) aMSDataStore;

            if(responseData.getResponseStatus())
            {
                final Dialog dialog = Constants.showDialog(mPayByLinkActivity, getString(R.string.payby_link), "details saved successfully", Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        AppSharedPrefrences.getAppSharedPrefrencesInstace().setPayByLinkBank(responseData.getmBankName());
                        Intent intent = new Intent(mPayByLinkActivity , MenuView.class);
                        startActivity(intent);
                        mPayByLinkActivity.finish();
                        dialog.dismiss();

                    }
                });
                dialog.show();
            }
            else{

                final Dialog dialog = Constants.showDialog(mPayByLinkActivity, getString(R.string.payby_link), responseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });
                dialog.show();

            }
        }
    }
}
