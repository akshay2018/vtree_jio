package in.bigtree.vtree.view.mcardsale.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.mcardsale.McardSaleActivity;
import in.bigtree.vtree.view.mcardsale.McardSaleEnum;

public class McardSaleAmountFragment extends Fragment {

    Context mContext;
    McardSaleActivity mMcardSaleActivity = null;
    SharedVariable applicationData = null;
    View viewamount;

    //fields for card sale screen
    private EditText mTxtAmt = null;
    private ImageButton mImageButtonSubmit = null;

    private CustomProgressDialog mProgressActivity;

    // this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
    private long lastRequestTime = 0;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        mContext = activity;
        mMcardSaleActivity = ((McardSaleActivity) mContext);
        applicationData = SharedVariable.getApplicationDataSharedInstance();
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        viewamount = inflater.inflate(R.layout.mcardsale_amountview, container, false);

        initViews(viewamount);
        setDefaultData();

        return viewamount;
    }


    private void setDefaultData() {

        if (!mMcardSaleActivity.mTransactionData.mBaseAmount.equalsIgnoreCase("0.00"))
            mTxtAmt.setText(mMcardSaleActivity.mTransactionData.mBaseAmount);

    }

    public void initViews(View view) {
        // TODO Auto-generated method stub

        mTxtAmt = (EditText) view.findViewById(R.id.mcard_saleinputdetailsview_TXT_amount);
        mImageButtonSubmit = (ImageButton) view.findViewById(R.id.mcard_saleinputdetailsview_BTN_next);


        mImageButtonSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(mMcardSaleActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                double miAmountDisplay = 0;
                try {
                    if (mTxtAmt.getText().toString().length() > 0)
                        miAmountDisplay = Double.parseDouble(removeChar(mTxtAmt.getText().toString(), ','));


                } catch (Exception ex) {
                    miAmountDisplay = 0;
                }


                if (miAmountDisplay < 1) {
                    Constants.showDialog(mMcardSaleActivity, getString(R.string.void_sale),
                            String.format(getString(R.string.voidsaleamountfragment_you_need_to_enter_a_minimum_amount_of_at_least_inr_001_to_proceed), applicationData.mCurrency));

                    return;
                }
                else {

                    mMcardSaleActivity.mTransactionData.mBaseAmount = removeChar(mTxtAmt.getText().toString(),',');
                    mMcardSaleActivity.mTransactionData.mTotAmount = removeChar(mTxtAmt.getText().toString(),',');

                    mMcardSaleActivity.showScreen(McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_INPUT_CARD_SELECTION);

                }
            }
        });
    }

    public String removeChar(String s, char c) {

        String r = "";

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c)
                r += s.charAt(i);
        }

        return r;
    }

}
