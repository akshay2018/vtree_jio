package in.bigtree.vtree.view.historyMonthWise;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.view.BaseTitleActivity;


public class HistoryMenu extends BaseTitleActivity {
	
	String mOptionType = "";
	SharedVariable applicationData =null;
	/** Called when the activity is first created. */
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.view_menu);

		mOptionType = getIntent().getStringExtra("OptionsType");
		applicationData = (SharedVariable)getApplicationContext();
		initViews();
	
    }
	
		
	private void initViews() 
	{
		ListView listView = (ListView) findViewById(R.id.menuview_LST_options);
		String[] values =null;
		values= new String[] {"Card sale history","QR sale history","Link sale history","Cash sale history", "Bank sale history"};

		TextView txtHeading = (TextView) findViewById(R.id.topbar_LBL_heading);
		 txtHeading.setTypeface(applicationData.font);
		txtHeading.setText("History menu");

		txtHeading.setTypeface(applicationData.font);

	     MenuViewAdapter adapter = new MenuViewAdapter(this,values);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{

				if (arg2 == 0){
					Intent intent = new Intent(HistoryMenu.this, MonthWiseHistoryList.class);
					intent.putExtra("saletype","cardsale");
					startActivity(intent);
					return;
				}else if (arg2 == 1){

					Intent intent = new Intent(HistoryMenu.this, MonthWiseHistoryList.class);
					intent.putExtra("saletype","qrsale");
					startActivity(intent);
					return;
				}
				else if (arg2 == 2){

					Intent intent = new Intent(HistoryMenu.this, MonthWiseHistoryList.class);
					intent.putExtra("saletype","linksale");
					startActivity(intent);
					return;
				}else if (arg2 == 3){

					Intent intent = new Intent(HistoryMenu.this, MonthWiseHistoryList.class);
					intent.putExtra("saletype","cashsale");
					startActivity(intent);
					return;
				}else if (arg2 == 4){

					Intent intent = new Intent(HistoryMenu.this, MonthWiseHistoryList.class);
					intent.putExtra("saletype","banksale");
					startActivity(intent);
					return;
				}
			}
		});
	}
	
	public class MenuViewAdapter extends BaseAdapter
	{
		String[] listData = null;
		Context context;

		public MenuViewAdapter(Context context, String[] listData)
		{
			this.listData = listData;
			this.context=context;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listData.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			// TODO Auto-generated method stub
			if(convertView == null)
			{
				LayoutInflater inflater= LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.view_menulstitem, null);
			}
			TextView txtItem= (TextView)convertView.findViewById(R.id.menuview_lsttext);
			txtItem.setText(listData[position]);
			txtItem.setTypeface(applicationData.font);
			return convertView;
	    }
	}	
}