package in.bigtree.vtree.view.voidsale.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.TransactionDetailsResponseData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.Calendar;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.voidsale.VoidSaleActivity;
import in.bigtree.vtree.view.voidsale.VoidSaleEnum;

public class VoidSaleAmountFragment extends Fragment {

    Context mContext;
    VoidSaleActivity mVoidsaleInputDetailsView = null;
    SharedVariable applicationData = null;
    View viewamount;

    //fields for card sale screen
    private EditText mTxtVoidSaleAmt = null;
    private EditText mTxtLast4Digits = null;


    private ImageButton mImageButtonSubmit = null;

    private CustomProgressDialog mProgressActivity;

    // this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
    private long lastRequestTime = 0;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        mContext = activity;
        mVoidsaleInputDetailsView = ((VoidSaleActivity) mContext);
        applicationData = SharedVariable.getApplicationDataSharedInstance();
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        viewamount = inflater.inflate(R.layout.voidsale_amountview, container, false);

        initViews(viewamount);
        setVoidSaleData();

        return viewamount;
    }


    private void setVoidSaleData() {

        if (!mVoidsaleInputDetailsView.mTransactionData.mBaseAmount.equalsIgnoreCase("0.00"))
            mTxtVoidSaleAmt.setText(mVoidsaleInputDetailsView.mTransactionData.mBaseAmount);

        if (mVoidsaleInputDetailsView.mTransactionData.mLast4Digits.length() > 0)
            mTxtLast4Digits.setText(mVoidsaleInputDetailsView.mTransactionData.mLast4Digits);


    }

    public void initViews(View view) {
        // TODO Auto-generated method stub

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        mVoidsaleInputDetailsView.mTransactionData.mSelectedDate = year + "-" + (month + 1) + "-" + day;

        mTxtVoidSaleAmt = (EditText) view.findViewById(R.id.voidsaleinputdetailsview_TXT_amount);
        mTxtLast4Digits = (EditText) view.findViewById(R.id.voidsaleinputdetailsview_TXT_last4digits);

        mImageButtonSubmit = (ImageButton) view.findViewById(R.id.voidsaleinputdetailsview_BTN_next);


        mImageButtonSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
               /* InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(mVoidsaleInputDetailsView.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
*/
                double miAmountDisplay = 0;
                try {
                    if (mTxtVoidSaleAmt.getText().toString().length() > 0)
                        miAmountDisplay = Double.parseDouble(removeChar(mTxtVoidSaleAmt.getText().toString(), ','));


                } catch (Exception ex) {
                    miAmountDisplay = 0;
                }


                if (miAmountDisplay < 1) {
                    Constants.showDialog(mVoidsaleInputDetailsView, getString(R.string.void_sale),
                            String.format(getString(R.string.voidsaleamountfragment_you_need_to_enter_a_minimum_amount_of_at_least_inr_001_to_proceed), applicationData.mCurrency));

                    return;
                }
                else if (mTxtLast4Digits.getText().toString().trim().length() < 3) {

                    Constants.showDialog(mVoidsaleInputDetailsView, getString(R.string.void_sale),
                            getString(R.string.voidsaleamountfragment_invalid_last_4_digits));
                    mTxtLast4Digits.requestFocus();
                    return;
                }
                else {

                    mVoidsaleInputDetailsView.mTransactionData.mBaseAmount = removeChar(mTxtVoidSaleAmt.getText().toString(),',');
                    mVoidsaleInputDetailsView.mTransactionData.mLast4Digits = mTxtLast4Digits.getText().toString();

                    try {

                        if (applicationData.IS_DEBUGGING_ON)
                            Logs.v(SharedVariable.packName, "The base amout " + mTxtVoidSaleAmt.getText().toString() + " Tot " + mTxtVoidSaleAmt.getText().toString(), false, true);

                        final MSWisepadController wisepadController = MSWisepadController.
                                getSharedMSWisepadController(mVoidsaleInputDetailsView,
                                        AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                                        AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

                        MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());


                        if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

                            if(mVoidsaleInputDetailsView.mIsVoidWithOTp){

                                wisepadController.getCardSaleTrxDetailsForOTP(
                                        AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                                        AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                                        mVoidsaleInputDetailsView.mTransactionData.mSelectedDate,
                                        mVoidsaleInputDetailsView.mTransactionData.mBaseAmount,
                                        mVoidsaleInputDetailsView.mTransactionData.mLast4Digits,
                                        new MSWisepadControllerResponseListenerObserver());

                            }
                            else {

                                wisepadController.getCardSaleTrxDetails(
                                        AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                                        AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                                        mVoidsaleInputDetailsView.mTransactionData.mSelectedDate,
                                        mVoidsaleInputDetailsView.mTransactionData.mBaseAmount,
                                        mVoidsaleInputDetailsView.mTransactionData.mLast4Digits,
                                        new MSWisepadControllerResponseListenerObserver());
                            }


                            mProgressActivity = new CustomProgressDialog(mVoidsaleInputDetailsView, getString(R.string.void_sale));
                            mProgressActivity.show();
                        }

                        lastRequestTime = System.currentTimeMillis();




                    } catch (Exception ex) {

                        if (applicationData.IS_DEBUGGING_ON)
                            Logs.v(SharedVariable.packName, "the void sale exception " + ex.toString(), false, true);


                        Constants.showDialog(mVoidsaleInputDetailsView, getString(R.string.void_transaction),
                                getString(R.string.voidsaleamountfragment_the_void_sale_transaction_was_not_processed_successfully_please_try_again));


                    }
                }
            }
        });
    }


    /**
     * MswipeWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

     */
    class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            TransactionDetailsResponseData transactionDetailsResponseData = (TransactionDetailsResponseData) aMSDataStore;

            if(transactionDetailsResponseData.getResponseStatus())
            {

                mVoidsaleInputDetailsView.mTransactionData.mDate = transactionDetailsResponseData.getTrxDate();
                mVoidsaleInputDetailsView.mTransactionData.mStandId = transactionDetailsResponseData.getStanNo();
                mVoidsaleInputDetailsView.mTransactionData.mVoucherNo = transactionDetailsResponseData.getVoucherNo();
                mVoidsaleInputDetailsView.mTransactionData.mLast4Digits = transactionDetailsResponseData.getCardLastFourDigits();
                mVoidsaleInputDetailsView.mTransactionData.mBaseAmount = transactionDetailsResponseData.getTrxAmount();
                mVoidsaleInputDetailsView.mTransactionData.mAuthCode = transactionDetailsResponseData.getAuthNo();
                mVoidsaleInputDetailsView.mTransactionData.mRRNo = transactionDetailsResponseData.getRRNo();
                mVoidsaleInputDetailsView.mTransactionData.mInvoiceNo = transactionDetailsResponseData.getInvoiceNo();

                mVoidsaleInputDetailsView.showScreen(VoidSaleEnum.VoidSaleScreens.VoidSaleScreens_TXT_DETAILS);

            }
            else{


                final Dialog dialog = Constants.showDialog(mVoidsaleInputDetailsView,
                        getString(R.string.void_sale),
                        transactionDetailsResponseData.getResponseFailureReason(),
                        Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);

                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }


        }
    }

    public String removeChar(String s, char c) {

        String r = "";

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c)
                r += s.charAt(i);
        }

        return r;
    }

}
