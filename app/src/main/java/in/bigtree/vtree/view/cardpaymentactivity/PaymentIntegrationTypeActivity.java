package in.bigtree.vtree.view.cardpaymentactivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.BaseTitleActivity;
import in.bigtree.vtree.view.cardpaymentactivity.cardsaleactivityintegration.MswipePaymentView;
import in.bigtree.vtree.view.cardpaymentactivity.cardsalearrintegration.ARRActivityPaymentView;
import in.bigtree.vtree.view.cardpaymentactivity.cardsalesdkintegration.PaymentSDKTypeIntegration;

/**
 * Created by mswipe on 5/11/17.
 */

public class PaymentIntegrationTypeActivity extends BaseTitleActivity
{

    boolean isSaleWithCash = false;
    boolean isPreAuth = false;
    boolean isEmiSale = false;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.payment_integration_type_view);

        isSaleWithCash = getIntent().getBooleanExtra("salewithcash", false);
        isEmiSale = getIntent().getBooleanExtra("emisale", false);
        isPreAuth = getIntent().getBooleanExtra("preauthsale", false);

        initViews();

        verifyDeviceTime();
    }

    /**
     * Initializes the UI elements for the login screen
     * @param
     *
     * @return
     *
     */
    private void initViews()
    {

        ((TextView) findViewById(R.id.topbar_LBL_heading)).setText("Integration Method");

        ((Button) findViewById(R.id.integration_type_BTN_sdk)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId().length() !=0
                    && AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId().length() !=0)
                {
                    showWisePadConnectionDlg(2);
                }
                else{
                    Constants.showDialog(PaymentIntegrationTypeActivity.this, "SDK List", "you have to login first.");

                }
            }
        });


        ((Button) findViewById(R.id.integration_type_BTN_activity)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent intent = null;
                //Intent intent = new Intent(PaymentIntegrationTypeActivity.this, MswipePaymentView.class);

                if(isPreAuth){
                    intent = new Intent(PaymentIntegrationTypeActivity.this, MswipePaymentView.class);
                    intent.putExtra("preauthsale", true);
                }
                else if (isSaleWithCash){

                    intent = new Intent(PaymentIntegrationTypeActivity.this, MswipePaymentView.class);
                    intent.putExtra("salewithcash", true);
                }
                else if (isEmiSale){

                    intent = new Intent(PaymentIntegrationTypeActivity.this, MswipePaymentView.class);
                    intent.putExtra("emisale", true);
                }
                else {

                    intent = new Intent(PaymentIntegrationTypeActivity.this, MswipePaymentView.class);
                    intent.putExtra("cardsale", true);
                }

                startActivity(intent);
                finish();
            }
        });


        ((Button) findViewById(R.id.integration_type_BTN_arr)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                // Intent intent = new Intent(PaymentIntegrationTypeActivity.this, ARRActivityPaymentView.class);
                Intent intent = null;
                if(isPreAuth){
                    intent = new Intent(PaymentIntegrationTypeActivity.this, ARRActivityPaymentView.class);
                    intent.putExtra("preauthsale", true);
                }
                else if (isSaleWithCash){

                    intent = new Intent(PaymentIntegrationTypeActivity.this, ARRActivityPaymentView.class);
                    intent.putExtra("salewithcash", true);
                }
                else if (isEmiSale){

                    intent = new Intent(PaymentIntegrationTypeActivity.this, ARRActivityPaymentView.class);
                    intent.putExtra("emisale", true);
                }
                else {

                    intent = new Intent(PaymentIntegrationTypeActivity.this, ARRActivityPaymentView.class);
                    intent.putExtra("cardsale", true);
                }

                startActivity(intent);
                finish();

            }
        });

    }


    public void showWisePadConnectionDlg(final int selectedOption)
    {
        final Dialog dialog = Constants.showWisepadConnectionDialog(PaymentIntegrationTypeActivity.this, "Wisepad Settings");

        final CheckBox aCBAutoConnect = (CheckBox) dialog.findViewById(R.id.customwisepadconnection_CHK_autoconnect);
        final CheckBox aCBCheckCard = (CheckBox) dialog.findViewById(R.id.customwisepadconnection_CHK_checkcard);

        Button ok = (Button) dialog.findViewById(R.id.customwisepadconnection_BTN_ok);
        ok.setOnClickListener(new View.OnClickListener()
        {

            public void onClick(View v)
            {

                dialog.dismiss();


                Intent intent = null;

                if(selectedOption == 1){

                  /*  if(isPreAuth){

                        intent = new Intent(PaymentIntegrationTypeActivity.this, PaymentActivityTypeIntegration.class);
                        intent.putExtra("preauthsale", true);
                    }
                    else if (isEmiSale){

                        intent = new Intent(PaymentIntegrationTypeActivity.this, PaymentActivityTypeIntegration.class);
                        intent.putExtra("emisale", true);
                    }
                    else if (isSaleWithCash){

                        intent = new Intent(PaymentIntegrationTypeActivity.this, PaymentActivityTypeIntegration.class);
                        intent.putExtra("salewithcash", true);
                    }
                    else {

                        intent = new Intent(PaymentIntegrationTypeActivity.this, PaymentActivityTypeIntegration.class);
                        intent.putExtra("cardsale", true);
                    }*/

                }
                else{

                    if(isPreAuth){

                        intent = new Intent(PaymentIntegrationTypeActivity.this, PaymentSDKTypeIntegration.class);
                        intent.putExtra("preauthsale", true);
                    }
                    else if (isEmiSale){

                        intent = new Intent(PaymentIntegrationTypeActivity.this, PaymentSDKTypeIntegration.class);
                        intent.putExtra("emisale", true);
                    }
                    else if (isSaleWithCash){

                        intent = new Intent(PaymentIntegrationTypeActivity.this, PaymentSDKTypeIntegration.class);
                        intent.putExtra("salewithcash", true);
                    }
                    else {

                        intent = new Intent(PaymentIntegrationTypeActivity.this, PaymentSDKTypeIntegration.class);
                        intent.putExtra("cardsale", true);
                    }

                }

                intent.putExtra("autoconnect", aCBAutoConnect.isChecked());
                intent.putExtra("checkcardAfterConnection", aCBCheckCard.isChecked());

                startActivity(intent);
                finish();
            }
        });

        dialog.show();

    }

    private void verifyDeviceTime(){

        Logs.v(SharedVariable.packName, "", true, true);

        new LongOperation().execute();
    }

    private String mDateTime = "";
    private boolean mTryAgain = true;

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            getTIme();
            return "";
        }

        @Override
        protected void onPostExecute(String result) {

            Logs.v(SharedVariable.packName, "", true, true);


            if(mDateTime != null && mDateTime.length()>0)
            {

                SimpleDateFormat inputFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
                inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                SimpleDateFormat outputFormat = new SimpleDateFormat("yyMMddHHmm");

                Date date = null;

                try {
                    date = inputFormat.parse(mDateTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String outputText = outputFormat.format(date);

                // Get Current Date Time
                Calendar c = Calendar.getInstance();
                String getCurrentDateTime = outputFormat.format(c.getTime());

                Logs.v(SharedVariable.packName, "outputText: " + outputText, true, true);
                Logs.v(SharedVariable.packName, "getCurrentDateTime: " + getCurrentDateTime, true, true);
                Logs.v(SharedVariable.packName, "compareTo: " + getCurrentDateTime.compareTo(outputText) , true, true);

                if (getCurrentDateTime.compareTo(outputText) != 0)
                {

                    final Dialog dialog = Constants.showDialog(PaymentIntegrationTypeActivity.this, "", "please set proper date and time to perform the transaction.", Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                    Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                    yes.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    dialog.show();
                }
            }
            else{

                if(mTryAgain) {
                    verifyDeviceTime();
                    mTryAgain = false;
                }
            }
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private String mTimeUrl = "https://google.com/";

    private void getTIme()
    {

        try{

            Logs.v(SharedVariable.packName, "mTimeUrl: " + mTimeUrl, true, true);

            HttpClient httpclient = new DefaultHttpClient();

            HttpResponse response = httpclient.execute(new HttpGet(mTimeUrl));
            StatusLine statusLine = response.getStatusLine();

            if(statusLine.getStatusCode() == HttpStatus.SC_OK)
            {
                String dateStr = response.getFirstHeader("Date").getValue();

                Logs.v(SharedVariable.packName, "dateStr: " + dateStr, true, true);

                mDateTime = dateStr;

            } else{

                Logs.v(SharedVariable.packName, "getReasonPhrase" + statusLine.getReasonPhrase(), true, true);

                //Closes the connection.
                response.getEntity().getContent().close();
                throw new Exception(statusLine.getReasonPhrase());
            }

        }catch (Exception e) {

            Logs.e(SharedVariable.packName, e, true, true);

        }
    }
}