package in.bigtree.vtree.view.wiseposfeatures;


import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.device.MswipeWiseposBarcodeController;
import com.mswipetech.wisepad.sdk.device.MswipeWiseposBarcodeListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.BaseTitleActivity;

import static com.mswipetech.wisepad.sdk.device.MswipeWiseposBarcodeController.getSharedWiseposBarcodeController;


public class BarcodeReaderView extends BaseTitleActivity {
 
	
	SharedVariable applicationData;
    
	//The wisepos device barcode reader objects initializers 
	
  	private MswipeWiseposBarcodeController mMswipeWiseposBarcodeController;
  	private MswipeWiseposBarcodeObserver mMswipeWiseposDeviceObserver;
  	
  	private TextView mTXTTitle = null;
  	private TextView mTXTStatus = null;
  	private Button mBTNScan = null;
  	
  	private boolean isBarcodeConnected = false;

	
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.barcode_reader_activity);
        
		applicationData = (SharedVariable) getApplicationContext();
        mMswipeWiseposDeviceObserver = new MswipeWiseposBarcodeObserver();    
        mMswipeWiseposBarcodeController = getSharedWiseposBarcodeController(BarcodeReaderView.this, mMswipeWiseposDeviceObserver);
        
        initViews();

    }

    private void initViews() {

    	mTXTTitle = (TextView) findViewById(R.id.topbar_LBL_heading);
    	mTXTStatus = (TextView) findViewById(R.id.barcode_reader_LBL_status);

    	mBTNScan = (Button) findViewById(R.id.barcode_reader_BTN_scan);

    	mTXTTitle.setText("Barcode Reader");
    	
    	mTXTTitle.setTypeface(applicationData.font);
    	mTXTStatus.setTypeface(applicationData.font);
    	mBTNScan.setTypeface(applicationData.font);

    	mBTNScan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
            	            	
            	if(mBTNScan.getText().toString().equalsIgnoreCase("Scan"))
            	{

            		if(SharedVariable.IS_DEBUGGING_ON)
						Logs.v(SharedVariable.packName,"isBarcodeConnected" + isBarcodeConnected,true,true);

            		if (!isBarcodeConnected) {
						
            			if(mMswipeWiseposBarcodeController != null)
            				mMswipeWiseposBarcodeController.startBarcodeReader();
					}
            		else{
						
						if(mMswipeWiseposBarcodeController != null)
							mMswipeWiseposBarcodeController.getBarcode();
						
						mTXTStatus.setText("Scan Barcode");
						
					}
            		
            		mBTNScan.setText("Stop");
            	
            	}
            	else if(mBTNScan.getText().toString().equalsIgnoreCase("Stop"))
            	{
					if(SharedVariable.IS_DEBUGGING_ON)
						Logs.v(SharedVariable.packName,"isBarcodeConnected" + isBarcodeConnected,true,true);

					if(mMswipeWiseposBarcodeController != null)
            			mMswipeWiseposBarcodeController.stopBarcodeReader();
        			
            	}           	
            }
        });
    }
        
    
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    	
    	if(mMswipeWiseposBarcodeController != null)
    		mMswipeWiseposBarcodeController.stopBarcodeReader();
    }

    
    class MswipeWiseposBarcodeObserver implements MswipeWiseposBarcodeListener {

		@Override
		public void onBarcodeReaderConnected() {
			// TODO Auto-generated method stub

			if(SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,"",true,true);

			isBarcodeConnected = true;
			
			if(mMswipeWiseposBarcodeController != null){
        		mMswipeWiseposBarcodeController.getBarcode();   
        		mTXTStatus.setText("Scan Barcode");
			}
		}

		@Override
		public void onBarcodeReaderDisconnected() {
			// TODO Auto-generated method stub

			if(SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,"",true,true);

			isBarcodeConnected = false;
			mTXTStatus.setText("Press Scan");
			mBTNScan.setText("Scan");
		}

		@Override
		public void onReturnBarcode(String barCode) {
			// TODO Auto-generated method stub

			if(SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,"barCode " + barCode,true,true);

			isBarcodeConnected = true;
			mTXTStatus.setText(barCode);
			mBTNScan.setText("Scan");
			
		}
		@Override
		public void onError(Error error, String s) {
			// TODO Auto-generated method stub

			if(SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,"error " + s,true,true);

			isBarcodeConnected = false;
			mTXTStatus.setText(s);
			mBTNScan.setText("Scan");
		}
    }   
}