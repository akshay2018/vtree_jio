package in.bigtree.vtree.view.paybylink.fragment.bank;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.view.paybylink.PayByLinkActivity;
import in.bigtree.vtree.view.paybylink.PayByLinkEnum;


/**
 * Created by Mswipe on 5/10/2017.
 */

public class PayByLinkBankSelectionFragment extends Fragment
{

    PayByLinkActivity mPaybylinkActivity = null;

    private TextView mTXTSelectBank = null;

    public void onAttach(Activity activity) {
        mPaybylinkActivity = (PayByLinkActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view=inflater.inflate(R.layout.paybylink_bank_selection_fragment, container, false);
        initViews(view);
        return view;
    }

    public void initViews(View view)
    {

        ListView listView = (ListView) view.findViewById(R.id.payby_link_bank_view_LST_options);

        String[] values = new String[]
                {
                        "Select Bank A/C"
                };

        PayByLinkBankViewAdapter adapter = new PayByLinkBankViewAdapter(mPaybylinkActivity, values);
        int[] colors = {0, 0xFF0000FF, 0};
        listView.setDivider(new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors));
        listView.setDividerHeight(1);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                int menuoption = 0;

                if (arg2 == menuoption) {
                    mPaybylinkActivity.showScreen(PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_BANK_Details);
                }

            }
        });
    }

    public class PayByLinkBankViewAdapter extends BaseAdapter {
        String[] listData = null;
        Context context;

        public PayByLinkBankViewAdapter(Context context, String[] listData) {
            this.listData = listData;
            this.context = context;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return listData.length;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.view_menulstitem, null);
            }
            TextView txtItem = (TextView) convertView
                    .findViewById(R.id.menuview_lsttext);
            txtItem.setText(listData[position]);

            return convertView;
        }
    }

}