package in.bigtree.vtree.view.mcardsale.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.component.McardSaleReceiptView;
import com.mswipetech.wisepad.sdk.data.CardSaleReceiptResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.mcardsale.McardSaleActivity;

public class McardSaleTxtApprovalFragment extends Fragment {
	Context mContext;
	McardSaleActivity mMcardSaleActivity = null;
	SharedVariable applicationData = null;

	private McardSaleReceiptView mCardSaleReceiptView = null;

	private CustomProgressDialog mProgressActivity  =null;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		
		mContext = activity;
		mMcardSaleActivity = ((McardSaleActivity) mContext);
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		super.onAttach(activity);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.mcardsale_transactionapprovalview,container, false);

		initViews(view);
		return view;
	}

	public void initViews(View view) {
		// TODO Auto-generated method stub

		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		TextView lblTxAmtCurrency= (TextView)view. findViewById(R.id.mcard_sale_LBL_Amtprefix);
		lblTxAmtCurrency.setText(applicationData.mCurrency);
		TextView lblTxAmt = (TextView)view. findViewById(R.id.mcard_saletxtdetails_LBL_AmtRs);
		lblTxAmt.setText(mMcardSaleActivity.mTransactionData.mBaseAmount);

		TextView lblAuthNo = (TextView) view.findViewById(R.id.mcard_saletxtdetails_LBL_AuthNo);
		lblAuthNo.setText(mMcardSaleActivity.mTransactionData.mAuthCode);

		TextView lblRRNO = (TextView)view.findViewById(R.id.mcard_saletxtdetails_LBL_RRNo);
		lblRRNO.setText(mMcardSaleActivity.mTransactionData.mRRNo);

		String mStrCardNum = "xxxx xxxx xxxx " + mMcardSaleActivity.mTransactionData.mLast4Digits;

		TextView lblCardNo = (TextView)view.findViewById(R.id.mcard_saletxtdetails_LBL_cardNo);
		lblCardNo.setText(mStrCardNum);



		mCardSaleReceiptView = (McardSaleReceiptView)view.findViewById(R.id.mcard_salereceiptview_CSRV_receipt);

		ImageButton btnClear = (ImageButton) view.findViewById(R.id.mcard_sale_BTN_clear);
		btnClear.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{

				mCardSaleReceiptView.clearSignature();

			}
		});

		ImageButton btnTxNext = (ImageButton) view.findViewById(R.id.mcard_saletxtdetails_BTN_next);
		btnTxNext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if(mCardSaleReceiptView.isSignatureDrawn()) {

					final MSWisepadController wisepadController = MSWisepadController.
							getSharedMSWisepadController(mMcardSaleActivity,
									AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
									AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);


					MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

					wisepadController.processMcardSaleReceipt(
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getUserId(),
							mMcardSaleActivity.mTransactionData.mTotAmount,
							mMcardSaleActivity.mTransactionData.mLast4Digits,
							mMcardSaleActivity.mTransactionData.mRRNo,
							mMcardSaleActivity.mTransactionData.mStandId,
							mMcardSaleActivity.mTransactionData.mVoucherNo,
							mCardSaleReceiptView.getSignature(),
							new MSWisepadControllerSignatureListener());

					mProgressActivity = null;
					mProgressActivity = new CustomProgressDialog(mMcardSaleActivity, getString(R.string.processing));
					mProgressActivity.show();

				}
				else{

					Constants.showDialog(mMcardSaleActivity,"mcard sale",getString(R.string.creditsalesignatureactivity_please_sign_the_receipt_to_proceed));
				}


			}
		});
	}

	class MSWisepadControllerSignatureListener implements MSWisepadControllerResponseListener
	{

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
		 * need be type cast back to LastTransactionResponseData to access the  response data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{
			if (mProgressActivity != null)
				mProgressActivity.dismiss();

			CardSaleReceiptResponseData cardSaleReceiptResponseData = (CardSaleReceiptResponseData) aMSDataStore;

			if(cardSaleReceiptResponseData.getResponseStatus())
			{

				final Dialog dialog = Constants.showDialog(mMcardSaleActivity, getString(R.string.mcard_sale), "signature saved successfully", Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						dialog.dismiss();

						Intent intent = new Intent(mMcardSaleActivity, MenuView.class);
						startActivity(intent);
						mMcardSaleActivity.finish();
					}
				});
				dialog.show();

			}
			else{

				final Dialog dialog = Constants.showDialog(mMcardSaleActivity, getString(R.string.mcard_sale), cardSaleReceiptResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						dialog.dismiss();

					}
				});
				dialog.show();

			}
		}
	}

}
