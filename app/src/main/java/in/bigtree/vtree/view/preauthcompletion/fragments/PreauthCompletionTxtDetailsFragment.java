package in.bigtree.vtree.view.preauthcompletion.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.PreAuthData;
import com.mswipetech.wisepad.sdk.data.ReceiptData;
import com.mswipetech.wisepad.sdk.data.VoidTransactionResponseData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.preauthcompletion.PreauthCompletionActivity;
import in.bigtree.vtree.view.preauthcompletion.PreauthCompletionEnum;

public class PreauthCompletionTxtDetailsFragment extends Fragment {

	Context mContext;
	PreauthCompletionActivity mPreauthCompletionDetailsView = null;
	SharedVariable applicationData = null;
	CustomProgressDialog mProgressActivity = null;
	ReceiptData mReceiptData = null;

	EditText mETPreauthAmount = null;
	ImageButton mBtnCancel = null;

	//for the Preauthcompletion trx details
	public String mTrxAmount = "";
	public int mIsSelectedIndex = 0;

	// this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
	private long lastRequestTime = 0;


	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		
		mContext = activity;
		mPreauthCompletionDetailsView = ((PreauthCompletionActivity) mContext);
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		super.onAttach(activity);
	}

	public PreauthCompletionTxtDetailsFragment(int selectedindex){

		mIsSelectedIndex = selectedindex;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.preauthcompletion_txtdetailsview,container, false);

		initViews(view);
		return view;
	}


	public void initViews(View view) {
		// TODO Auto-generated method stub

		PreAuthData preAuthData = mPreauthCompletionDetailsView.getPreauthCompletionDataList().get(mIsSelectedIndex);
		mPreauthCompletionDetailsView.mTransactionData.mBaseAmount = preAuthData.trxAmount;
		mPreauthCompletionDetailsView.mTransactionData.mDate = preAuthData.trxDate;
		mPreauthCompletionDetailsView.mTransactionData.mStandId = preAuthData.stanNo;
		//mStandIdPrint = mStandId;
		mPreauthCompletionDetailsView.mTransactionData.mVoucherNo = preAuthData.voucherNo;
		mPreauthCompletionDetailsView.mTransactionData.mLast4Digits = preAuthData.cardLastFourDigits;
		mPreauthCompletionDetailsView.mTransactionData.mPrismInvoiceNo= preAuthData.prismInvoiceNo;
		mPreauthCompletionDetailsView.mTransactionData.mAuthCode = preAuthData.authNo;
		mPreauthCompletionDetailsView.mTransactionData.mRRNo = preAuthData.rrNo;


		TextView lblTxDateTime = (TextView)view. findViewById(R.id.preauthcompletiontxtdetails_LBL_TxDateTime);
		lblTxDateTime.setText(preAuthData.trxDate.toLowerCase());

		TextView lblTxcardno = (TextView)view. findViewById(R.id.preauthcompletiontxtdetails_LBL_last4digits);
		lblTxcardno.setText(preAuthData.cardLastFourDigits.toLowerCase());

		mBtnCancel = (ImageButton)view.findViewById(R.id.preauthcompletiontxtdetails_cancel_BTN_amount);
		mBtnCancel.setVisibility(View.VISIBLE);

		mETPreauthAmount = (EditText)view. findViewById(R.id.preauthcompletiontxtdetails_TXT_amount);
		mETPreauthAmount.requestFocus();
		mETPreauthAmount.setCursorVisible(true);
		mETPreauthAmount.setTypeface(applicationData.font);
		mETPreauthAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, SharedVariable.font_size_amount_normal);

		String preauthamount = preAuthData.trxAmount.toLowerCase();

		if (preauthamount.toString().length() >= 7)
			mETPreauthAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, SharedVariable.font_size_amount_small);
		else
			mETPreauthAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, SharedVariable.font_size_amount_normal);

		mETPreauthAmount.setText(preauthamount);

		mETPreauthAmount.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub


				if (mETPreauthAmount.getText().toString().length() > 0) {

					if (mETPreauthAmount.getText().toString().length() >= 7)
						mETPreauthAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, SharedVariable.font_size_amount_small);
					else
						mETPreauthAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, SharedVariable.font_size_amount_normal);

					mBtnCancel.setVisibility(View.VISIBLE);


				} else {

					mBtnCancel.setVisibility(View.GONE);
				}

				calculateTotalAmt();
			}
		});


		//clears the text in edittext field
		mBtnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				mETPreauthAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, SharedVariable.font_size_amount_normal);
				mETPreauthAmount.setText("");

			}
		});


		TextView lblAuthNo = (TextView) view.findViewById(R.id.preauthcompletiontxtdetails_LBL_AuthNo);
		lblAuthNo.setText(preAuthData.authNo.toLowerCase());

		TextView lblRRNO = (TextView)view. findViewById(R.id.preauthcompletiontxtdetails_LBL_RRNo);
		lblRRNO.setText(preAuthData.rrNo.toLowerCase());

		TextView lblInvoiceNO = (TextView)view. findViewById(R.id.preauthcompletiontxtdetails_LBL_invoicenum);
		lblInvoiceNO.setText(preAuthData.prismInvoiceNo.toLowerCase());


		ImageButton btnTxNext = (ImageButton) view.findViewById(R.id.preauthcompletiontxtdetails_BTN_next);
		btnTxNext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				double miPreautAmountDisplay = 0;
				try {
					if (mETPreauthAmount.length() > 0)
						miPreautAmountDisplay = Double.parseDouble(removeChar(mETPreauthAmount.getText().toString(), ','));


				} catch (Exception ex) {
					miPreautAmountDisplay = 0;
				}

				double miAmountDisplay = 0;
				mTrxAmount = removeChar(mETPreauthAmount.getText().toString(), ',');

				try {
					miAmountDisplay = Double.parseDouble(removeChar(mPreauthCompletionDetailsView.mTransactionData.mBaseAmount, ','));
				} catch (Exception ex) {
					miAmountDisplay = 0;
				}


				if (miPreautAmountDisplay < 1) {
					Constants.showDialog(mPreauthCompletionDetailsView, getString(R.string.preauth_completion), String.format(getString(R.string.preauthcompletiontxtdetailsfragment_you_need_to_enter_a_minimum_amount_of_at_least_inr_001_to_proceed), applicationData.mCurrency));

					return;
				} else if (miPreautAmountDisplay > miAmountDisplay) {
					Constants.showDialog(mPreauthCompletionDetailsView, getString(R.string.preauth_completion), getString(R.string.preauthcompletiontxtdetailsfragment_please_enter_a_valid_amount_the_preauth_amount_cannot_exceed_the_preauth_sale_amount));

					return;
				}


				if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

					processPreauthSaleVoid();
				}

				lastRequestTime = System.currentTimeMillis();




			}
		});
	}


	/**
	 * calculateTotalAmt
	 *
	 * as we are using the custom edit text, if we delete text using the defalut keypad in the amount filed 0.0 remain.
	 * to as empty with hint text we need to set empty to the text view
	 *
	 */

	public void calculateTotalAmt() {


		String strAmount = mETPreauthAmount.getText().toString().trim();
		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "strAmount " + strAmount, true, true);


		if(strAmount.equalsIgnoreCase("0.0")){

			mETPreauthAmount.setText("");
			mETPreauthAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, SharedVariable.font_size_amount_normal);
		}
	}


	public boolean processPreauthSaleVoid() {

		try {

			MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

			MSWisepadController.
					getSharedMSWisepadController(mPreauthCompletionDetailsView,
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(), null).processPreauthVoidTransaction(
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
					mPreauthCompletionDetailsView.mTransactionData.mDate,
					mTrxAmount,
					mPreauthCompletionDetailsView.mTransactionData.mLast4Digits,
					mPreauthCompletionDetailsView.mTransactionData.mStandId,
					mPreauthCompletionDetailsView.mTransactionData.mVoucherNo,
					mPreauthCompletionDetailsView.mTransactionData.mPrismInvoiceNo,
					new MSWisepadControllerResponseListenerObserver());

			mProgressActivity = null;
			mProgressActivity = new CustomProgressDialog(mPreauthCompletionDetailsView, getString(R.string.preauth_completion));
			mProgressActivity.show();

		} catch (Exception ex) {

			Constants.showDialog(mPreauthCompletionDetailsView, getString(R.string.preauth_completion),
					getString(R.string.preauthcompletionactivity_the_preauth_sale_was_not_processed_successfully_please_try_again));

		}

		return true;
	}


	/**
	 * MswipeWisepadControllerResponseListenerObserver
	 * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

	 */
	class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener
	{

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
		 * need be type cast back to LastTransactionResponseData to access the  response data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{
			if (mProgressActivity != null)
				mProgressActivity.dismiss();

			VoidTransactionResponseData preauthSaleVoidResponseData = (VoidTransactionResponseData) aMSDataStore;

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, "voidTransactionResponseData.getResponseStatus() " + preauthSaleVoidResponseData.getResponseStatus() , true, true);

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, "voidTransactionResponseData.getResponseFailureReason() " + preauthSaleVoidResponseData.getResponseFailureReason() , true, true);

			if(preauthSaleVoidResponseData.getResponseStatus())
			{

				mPreauthCompletionDetailsView.mTransactionData.mDate = preauthSaleVoidResponseData.getTrxDate();
				mPreauthCompletionDetailsView.showScreenApproval(preauthSaleVoidResponseData.getReceiptData());

				mPreauthCompletionDetailsView.showScreen(PreauthCompletionEnum.PreauthCompletionScreens.PreauthCompletion_TXT_APPROVALVIEW);

			}
			else{

				final Dialog dialog = Constants.showDialog(mPreauthCompletionDetailsView, getString(R.string.preauth_completion), preauthSaleVoidResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						dialog.dismiss();
						mPreauthCompletionDetailsView.doneWithCreditSale();

					}
				});
				dialog.show();

			}
		}
	}


	/**
	 * @description
	 *        We are removing specific character form original string.
	 * @param s original string
	 * @param c specific character.
	 * @return
	 */
	public String removeChar(String s, char c) {

		String r = "";

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c)
				r += s.charAt(i);
		}

		return r;
	}

}
