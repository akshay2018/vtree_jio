package in.bigtree.vtree.view.preauthcompletion;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.PreAuthData;
import com.mswipetech.wisepad.sdk.data.PreauthTransactionDetailsResponsedata;
import com.mswipetech.wisepad.sdk.data.ReceiptData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.ArrayList;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.data.TransactionData;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.preauthcompletion.PreauthCompletionEnum.PreauthCompletionScreens;
import in.bigtree.vtree.view.preauthcompletion.fragments.PreauthCompletionInputDetailsFragment;
import in.bigtree.vtree.view.preauthcompletion.fragments.PreauthCompletionListDataFragment;
import in.bigtree.vtree.view.preauthcompletion.fragments.PreauthCompletionTxtApprovalFragment;
import in.bigtree.vtree.view.preauthcompletion.fragments.PreauthCompletionTxtDetailsFragment;


public class PreauthCompletionActivity extends FragmentActivity {

    private SharedVariable applicationData = null;
    CustomProgressDialog mProgressActivity = null;

    public ArrayList<PreAuthData> mArrPreauthCompletionData = null;

    public String mstrEMVProcessTaskType = "";
    public EMVProcessTask mEMVProcessTask = null;

    public TransactionData mTransactionData;

    protected PreauthCompletionScreens mPreauthScreens;



    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.preauthcompletion_fragment);
        
        applicationData = SharedVariable.getApplicationDataSharedInstance();
        mTransactionData = new TransactionData();
        showScreen(PreauthCompletionScreens.PreauthCompletion_AMOUNT);

        initViews();

    }

    private void initViews(){

        ((TextView)findViewById(R.id.topbar_LBL_heading)).setTypeface(applicationData.fontMedium);
        ((TextView)findViewById(R.id.topbar_LBL_heading)).setText(getResources().getString(R.string.preauth_completion));

    }

    public void setChangeTopbarColor(){

        ((RelativeLayout)findViewById(R.id.top_bar_REL_content)).setBackgroundColor(getResources().getColor(R.color.green));
    }

    
    @Override
    protected void onStop() {
    	// TODO Auto-generated method stub
    	super.onStop();

    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)

    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {

            if (mPreauthScreens == PreauthCompletionScreens.PreauthCompletion_AMOUNT || mPreauthScreens == PreauthCompletionScreens.PreauthCompletion_TXT_APPROVALVIEW) {
                finish();
                Intent intent = new Intent(PreauthCompletionActivity.this, MenuView.class);
                startActivity(intent);

            } else {

               moveToPrevious();
            }

            return true;

        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void moveToPrevious(){

        if (mPreauthScreens == PreauthCompletionScreens.PreauthCompletion_TXT_DETAILS)
            showScreen(PreauthCompletionScreens.PreauthCompletion_AMOUNT);

    }

    public ArrayList<PreAuthData> getPreauthCompletionDataList(){

        return mArrPreauthCompletionData;
    }

    public void showPreauthTxDetails() {

        showScreenTxtDetails(0);
    }


    public void  getPreauthCompletionDetails(){

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(PreauthCompletionActivity.this,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());


            wisepadController.getPreauthSaleTrxDetails(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    mTransactionData.mSelectedDate,
                    mTransactionData.mBaseAmount,
                    mTransactionData.mLast4Digits,
                    new MSWisepadControllerResponseListenerObserver());


            mProgressActivity = new CustomProgressDialog(PreauthCompletionActivity.this, getString(R.string.preauth_completion));
            mProgressActivity.show();

        }
        catch (Exception ex)
        {

            Constants.showDialog(PreauthCompletionActivity.this, getString(R.string.preauth_completion), getString(R.string.preauthcompletionactivity_the_preauth_sale_was_not_processed_successfully_please_try_again));

        }

    }


    /**
     * MswipeWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

     */
    class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {



            PreauthTransactionDetailsResponsedata preauthTransactionDetailsResponsedata = (PreauthTransactionDetailsResponsedata) aMSDataStore;
            mProgressActivity.dismiss();


            if (preauthTransactionDetailsResponsedata.getResponseStatus())
            {
                try {

                    mArrPreauthCompletionData = preauthTransactionDetailsResponsedata.getPreauthTxtResultData();

                    if (mArrPreauthCompletionData.size() > 0) {

                        if(mArrPreauthCompletionData.size() >1)
                        {
                            showScreen(PreauthCompletionScreens.PreauthCompletion_LISTDATA_VIEW);

                        }
                        else{

                            showPreauthTxDetails();
                        }
                    }
                    else {

                        final Dialog dialog = Constants.showDialog(PreauthCompletionActivity.this, getString(R.string.preauth_completion), "Unable to show the preuth data, please contact support.",
                                Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                        Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                        yes.setOnClickListener(new View.OnClickListener() {

                            public void onClick(View v) {
                                dialog.dismiss();

                               finish();

                            }
                        });
                        dialog.show();

                    }

                }
                catch(Exception e){

                }

            }
            else {

                Constants.showDialog(PreauthCompletionActivity.this, getString(R.string.preauth_completion), preauthTransactionDetailsResponsedata.getResponseFailureReason());
            }
        }
    }



    public class EMVProcessTask extends AsyncTask<Void, Integer, Void> {

        @Override
        protected void onCancelled() {

            if (SharedVariable.IS_DEBUGGING_ON)
                Logs.v(getPackageName(),  " EmvOnlinePorcessTask onCancelled Task", true, true);


            //when the back tab is pressed
            if (mstrEMVProcessTaskType.equalsIgnoreCase("backbutton")) {

            } else if (mstrEMVProcessTaskType.equalsIgnoreCase("onlinesubmit")) {
                //dismissProgressDialog();
            } else if (mstrEMVProcessTaskType.equalsIgnoreCase("stopbluetooth")) {

                try {
                    this.notify();
                } catch (Exception ex) {
                }
                finish();
            }

            mstrEMVProcessTaskType = "";

        }

        @Override
        protected Void doInBackground(Void... unused) {

            //calling after this statement and canceling task will no meaning if you do some update database kind of operation
            //so be wise to choose correct place to put this condition
            //you can also put this condition in for loop, if you are doing iterative task
            //you should only check this condition in doInBackground() method, otherwise there is no logical meaning

            // if the task is not cancelled by calling LoginTask.clear(true), then make the thread wait for 10 sec and then
            //quit it self
            if (applicationData.IS_DEBUGGING_ON)
                Logs.v(getPackageName(), "EmvOnlinePorcessTask start doInBackground", true, true);

            int isec = 15;

            if (mstrEMVProcessTaskType.equalsIgnoreCase("stopbluetooth"))
                isec = 6;

            int ictr = 0;
            //it will wait for 15 sec or till the task is cancelled by the mSwiper routines.
            while (!isCancelled() & ictr < isec) {
                try {
                    Thread.sleep(500);
                } catch (Exception ex) {
                }
                ictr++;
            }
            if (applicationData.IS_DEBUGGING_ON)
                Logs.v(getPackageName(), "EmvOnlinePorcessTask  end doInBackground", true, true);
            return null;
        }


        @Override
        protected void onPostExecute(Void unused) {

            if (applicationData.IS_DEBUGGING_ON)
                Logs.v(getPackageName(),  "EmvOnlinePorcessTask onPostExecute", true, true);

            //when the back tab is pressed
            if (mstrEMVProcessTaskType.equalsIgnoreCase("backbutton")) {

            }
            else if (mstrEMVProcessTaskType.equalsIgnoreCase("onlinesubmit")) {

            }
            else if (mstrEMVProcessTaskType.equalsIgnoreCase("stopbluetooth")) {
                finish();
            }

            mstrEMVProcessTaskType = "";

        }
    }


    public void doneWithCreditSale() {

        finish();
        Intent intent = new Intent(PreauthCompletionActivity.this, MenuView.class);
        startActivity(intent);

    }

    public void showScreenApproval(ReceiptData reciptDataModel)
    {

        // TODO Auto-generated method stub
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;

        fragment = new PreauthCompletionTxtApprovalFragment(reciptDataModel);
        fragmentTransaction.replace(R.id.preauthcompletion_fragmentcontainer, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        mPreauthScreens = PreauthCompletionScreens.PreauthCompletion_LISTDATA_VIEW;

    }


    public void showScreenTxtDetails(int selectedindex)
    {

        // TODO Auto-generated method stub
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;

        if(mPreauthScreens == PreauthCompletionScreens.PreauthCompletion_AMOUNT || mPreauthScreens == PreauthCompletionScreens.PreauthCompletion_LISTDATA_VIEW)
        {
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        }

        fragment = new PreauthCompletionTxtDetailsFragment(selectedindex);
        fragmentTransaction.replace(R.id.preauthcompletion_fragmentcontainer, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        mPreauthScreens = PreauthCompletionScreens.PreauthCompletion_TXT_DETAILS;

    }


    /**
     * description
     *       Handling fragment replacement.
     */
    public void showScreen(PreauthCompletionScreens preauthscreens)
    {

        // TODO Auto-generated method stub
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;

        switch (preauthscreens)
        {

            case PreauthCompletion_AMOUNT:

                if(mPreauthScreens == PreauthCompletionScreens.PreauthCompletion_LISTDATA_VIEW)
                {
                    fragmentTransaction.setCustomAnimations( R.anim.enter_from_left, R.anim.exit_to_right);
                }

                fragment = new PreauthCompletionInputDetailsFragment();
                fragmentTransaction.replace(R.id.preauthcompletion_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case PreauthCompletion_LISTDATA_VIEW:

                if(mPreauthScreens == PreauthCompletionScreens.PreauthCompletion_AMOUNT)
                {
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                }
                else if(mPreauthScreens == PreauthCompletionScreens.PreauthCompletion_TXT_DETAILS)
                {
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                }

                fragment = new PreauthCompletionListDataFragment();
                fragmentTransaction.replace(R.id.preauthcompletion_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

          /*  case PreauthCompletion_TXT_APPROVALVIEW:

                if(mPreauthScreens == PreauthCompletionScreens.PreauthCompletion_TXT_DETAILS)
                {
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                }

                fragment = new PreauthCompletionTxtApprovalFragment();
                fragmentTransaction.replace(R.id.preauthcompletion_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;*/

            default:
                break;
        }
        mPreauthScreens = preauthscreens;
    }

}
