package in.bigtree.vtree.view.preauthcompletion;

public  class PreauthCompletionEnum {

	public static enum PreauthCompletionScreens
	{
		PreauthCompletion_AMOUNT,
		PreauthCompletion_LISTDATA_VIEW,
		PreauthCompletion_TXT_DETAILS,
		PreauthCompletion_TXT_APPROVALVIEW,
	}


}
