package in.bigtree.vtree.view.voidsale;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.view.BaseTitleActivity;

public class VoidMenuView extends BaseTitleActivity {
    public final static String log_tab = "VoidMenuView=>";


    CustomProgressDialog mProgressActivity = null;

    SharedVariable applicationData = null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.view_menu);
        applicationData = (SharedVariable) getApplicationContext();
        initViews();

    }


    private void initViews() {
    	
    	
    	ListView listView = (ListView) findViewById(R.id.menuview_LST_options);

        TextView txtHeading = (TextView) findViewById(R.id.topbar_LBL_heading);
        txtHeading.setText("Void  Menu");
        txtHeading.setTypeface(applicationData.font);
        String[] values =new String[]{"Card Sale Void", "Cash Sale Void"};
        MenuViewAdapter adapter = new MenuViewAdapter(this, values);
        int[] colors = {0, 0xFF0000FF, 0};
        listView.setDivider(new GradientDrawable(Orientation.LEFT_RIGHT, colors));
        listView.setDividerHeight(1);

        // Assign adapter to ListView
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Intent intent = null;
                
                if(arg2 ==0)
                {
                	intent= new Intent(VoidMenuView.this, VoidSaleActivity.class);
                    intent.putExtra("isCardVoidSale", true);


                }else if(arg2 == 1){
                	
                	intent= new Intent(VoidMenuView.this, VoidSaleActivity.class);
                    intent.putExtra("isCashVoidSale", false);
                	
                }

                startActivity(intent);
            }
        });
    }




    public class MenuViewAdapter extends BaseAdapter {
        String[] listData = null;
        Context context;

        public MenuViewAdapter(Context context, String[] listData) {
            this.listData = listData;
            this.context = context;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return listData.length;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.view_menulstitem, null);
            }

            TextView txtItem = (TextView) convertView.findViewById(R.id.menuview_lsttext);
            txtItem.setText(listData[position]);
            txtItem.setTypeface(applicationData.font);

            return convertView;
        }
    }
}
