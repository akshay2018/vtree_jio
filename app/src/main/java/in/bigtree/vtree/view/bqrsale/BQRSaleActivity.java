package in.bigtree.vtree.view.bqrsale;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.view.bqrsale.fragment.CheckStatusListFragment;
import in.bigtree.vtree.view.bqrsale.fragment.EnterAmountFragment;
import in.bigtree.vtree.view.bqrsale.fragment.GenerateQRFragment;
import in.bigtree.vtree.view.bqrsale.fragment.MapBQRIdFragment;
import in.bigtree.vtree.view.bqrsale.fragment.SelectionListFragment;

import static in.bigtree.vtree.view.bqrsale.BQRSaleEnum.BQRSaleScreens.BQR_SALE_GETBQRIDS;

public class BQRSaleActivity extends FragmentActivity {

    public BQRSaleEnum.BQRSaleScreens mBQRScreens;

    public Bitmap mBQRBitmap = null;
    public String mBQRAmount = "";
    public String mBQRReceipt = "";
    public String mBQRMobileNo = "";
    public String mBQRTrxId = "";
    public String mTrnxDate = "";
    public String mAuthcode = "";
    public String mRRNo = "";
    public String mVocherNo = "";
    public String mTrnxType = "";
    public String mErrorMsg = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.voidsale_fragment);

        if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getVirtualPaymentId().trim().length() == 0)
        {
            showScreen(BQR_SALE_GETBQRIDS);
        }
        else {
            showScreen(BQRSaleEnum.BQRSaleScreens.BQR_SELECTION_SCREEN);
        }

        initViews();
    }

    private void initViews(){

        ((TextView)findViewById(R.id.topbar_LBL_heading)).setText("bqr sale");
    }

    public void showScreen(BQRSaleEnum.BQRSaleScreens bqrSaleScreens)
    {

        // TODO Auto-generated method stub
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;

        switch (bqrSaleScreens)
        {

            case BQR_SELECTION_SCREEN:
                fragment = new SelectionListFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case BQR_SALE_GETBQRIDS:
                fragment = new MapBQRIdFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case BQR_SALE_AMOUNT:
                fragment = new EnterAmountFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case BQR_SALE_GENERATE_QR:
                fragment = new GenerateQRFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case BQR_SALE_CHECK_STATUS:

                fragment = new CheckStatusListFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

        }

        mBQRScreens = bqrSaleScreens;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mBQRScreens == BQRSaleEnum.BQRSaleScreens.BQR_SELECTION_SCREEN ||
                    mBQRScreens == BQRSaleEnum.BQRSaleScreens.BQR_SALE_GETBQRIDS)
            {
                finish();

            } else {

                moveToPrevious();
            }

            return true;

        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void moveToPrevious(){

        if(mBQRScreens == BQRSaleEnum.BQRSaleScreens.BQR_SALE_GENERATE_QR){

            showScreen(BQRSaleEnum.BQRSaleScreens.BQR_SALE_AMOUNT);

        }else if (mBQRScreens == BQRSaleEnum.BQRSaleScreens.BQR_SALE_AMOUNT||mBQRScreens == BQRSaleEnum.BQRSaleScreens.BQR_SALE_CHECK_STATUS)
        {
            showScreen(BQRSaleEnum.BQRSaleScreens.BQR_SELECTION_SCREEN);
        }
    }
}
