package in.bigtree.vtree.view.cardpaymentactivity.NFC;

import android.app.Dialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mswipetech.wisepad.sdk.device.WisePadConnection;
import com.mswipetech.wisepad.sdk.device.WisePadTransactionState;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.util.Utils;

public class NFCCardActivity extends NFCCardBaseActivity
{

	public LinearLayout mLNRTopbarCancel;

	/**
	 * the wise pad has to be disconnected if its not required and also when the application closes
	 * the wise pad has to be disconnected,
	 * this will see to that the dis-connecting to the wisepad does has execute multiple times
	 */
	public boolean onDoneWithCreditSaleCalled = false;
	public boolean mIsIgnoreBackDevicekeyOnCardProcess = false;

	/**
	 * in certain scenarios the wise pad where in a likely case becomes non responsive, at that precise
	 * moment a task is called and with in the elapsed time if their no communication back to the app this task will
	 * un-initiate the communication link from the wisepad
	 */

	public EMVProcessTask mEMVProcessTask = null;

	public enum EMVPPROCESSTASTTYPE{
		NO_TASK,
		STOP_BLUETOOTH,
		BACK_BUTTON,
		ONLINE_SUBMIT,
		SHOW_SIGNATURE}

	public EMVPPROCESSTASTTYPE mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;



	private long mBackPressed=0;

	TextView lblAmtMsg = null;
	TextView txtProgMsg = null;
	EditText mTxtStatusMsg = null;

	Button mBtnConnect = null;
	//Button mBtnDisConnect = null;
	Button mBtnClear = null;


	Button mBtnNFCConnect = null;
	Button mBtnNFCDisConnect = null;


	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState
	 */

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.nfccard_screen);
		mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;


		initViews();

	}

	@Override
	protected void onStop()
	{
		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, " ", true, true);

		/*stops the gateway connection */

		if (!onDoneWithCreditSaleCalled)
		{

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,  "onDoneWithCreditSaleCalled " + onDoneWithCreditSaleCalled, true, true);

			doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);
			super.onStop();

		}
		else {
			super.onStop();
		}
	}

	/**
	 * @description:
	 * 	 Initializes the UI elements for the Creditsale activity
	 * 	 @param
	 * 	 @return
	 *
	 */
	private void initViews()
	{

		((TextView) findViewById(R.id.topbar_LBL_heading)).setText("NFC");

		mLNRTopbarCancel = (LinearLayout)findViewById(R.id.topbar_LNR_topbar_cancel);
		mLNRTopbarCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);
			}
		});

		mLNRTopbarCancel.setVisibility(View.VISIBLE);

		lblAmtMsg = (TextView) findViewById(R.id.creditsale_totalamountview_LBL_totalamount);
		txtProgMsg = (TextView) findViewById(R.id.nfccard_swiperview_EDT_swipe_progmsg);
		mTxtStatusMsg = (EditText) findViewById(R.id.nfccard_swiperview_EDT_swipe_statusmsg);

		mBtnConnect = (Button) findViewById(R.id.nfccard_swiperview_BTN_connect);
		//mBtnDisConnect = (Button) findViewById(R.id.nfccard_swiperview_BTN_disconnect);
		mBtnClear = (Button) findViewById(R.id.nfccard_swiperview_BTN_clear);

		mBtnNFCConnect = (Button) findViewById(R.id.nfccard_swiperview_BTN_nfcconnect);
		mBtnNFCDisConnect = (Button) findViewById(R.id.nfccard_swiperview_BTN_nfcdisconnect);

		mBtnReadFirst = (Button) findViewById(R.id.nfccard_swiperview_BTN_read);
		mBtnReadNext = (Button) findViewById(R.id.nfccard_swiperview_BTN_readnext);
		mBtnWrite = (Button) findViewById(R.id.nfccard_swiperview_BTN_write);
		mBtnAuthenticate = (Button) findViewById(R.id.nfccard_swiperview_BTN_authenticate);

		mBtnCardType = (Button) findViewById(R.id.nfccard_swiperview_BTN_catdtype);
		mBtnCardType.setText("MIFARE Classic 1K/4K");
		mBtnCardType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				showNFCCardTypeOptionsDialog();
			}
		});

		mBtnConnect.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if(getWisePadConnectionState() == WisePadConnection.WisePadConnection_DIS_CONNECTED) {
					setWisepadStatusMsg(getString(R.string.connecting));
					initiateWisepadConnection();
				}
				else {
					disConnect();
				}
			}
		});

		mBtnClear.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				mTxtStatusMsg.setText("");
			}
		});

		mBtnAuthenticate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				promptForNfcDataAuthenticate();
			}
		});

		mBtnNFCConnect.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				promptForStartNfcDetection();
			}
		});

		mBtnNFCDisConnect.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				promptForStopNfcDetection();
			}
		});

		mBtnReadFirst.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				promptForNfcDataExchangeRead1St();
			}
		});

		mBtnReadNext.setEnabled(false);
		mBtnReadNext.setTextColor(Color.GRAY);
		mBtnReadNext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				promptForNfcDataExchangeReadNext();
			}
		});

		mBtnWrite.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				promptForNfcDataExchangeWrite();
			}
		});

	}

	/**
	 * @description
	 *     We are handling backbutton manually based on screen position.
	 * @param keyCode
	 * @param event
	 * @return
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,  "onKeyDown  keyCode " + keyCode, true, true);

			// if the state of the devcie connection is no
			// DEVCIE_NO_PAIRED_DEVICES then from the amount screen it will take
			// to steps screen,
			// from here the below will restrict moving back.

			if(getWisePadConnectionState() == WisePadConnection.WisePadConnection_CONNECTING){

				Toast.makeText(NFCCardActivity.this, "please wait connection in process", Toast.LENGTH_LONG).show();
			}
			else {

				if(isIgnoreBackDevicekeyOnCardProcess())
				{
					if (mBackPressed + 2000 > System.currentTimeMillis()) {

						doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);

					}
					else {
						Toast.makeText(this, getResources().getString(R.string.processing_card_in_progress_press_back_key_twice_in_succession_to_terminate_the_transaction), Toast.LENGTH_SHORT).show();
						mBackPressed = System.currentTimeMillis();
					}
				}
				else{
					doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);
				}
			}

			return true;
		}
		else {
			return super.onKeyDown(keyCode, event);
		}
	}

	public boolean isIgnoreBackDevicekeyOnCardProcess()
	{
		return mIsIgnoreBackDevicekeyOnCardProcess;
	}

	public void setIgnoreBackDevicekeyOnCardProcess(boolean ignoreBackDevicekeyOnCardProcess)
	{
		this.mIsIgnoreBackDevicekeyOnCardProcess = ignoreBackDevicekeyOnCardProcess;
	}

	/**setWisePadConnectionState
	 *	set the views to the connection state
	 * @param
	 * wisePadConnection
	 * 		The state of the wise pad connection
	 * @return
	 */
	@Override
	public void setWisePadConnectionStateResult(WisePadConnection wisePadConnection)
	{

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, " wisePadConnection " + wisePadConnection, true, true);

		if(wisePadConnection == WisePadConnection.WisePadConnection_CONNECTING)
		{
			setWisepadStatusMsg(getString(R.string.connecting_device));


			//mBtnConnect.setEnabled(false);
			//mBtnConnect.setBackgroundResource(R.drawable.button_next_inactive);
			mBtnConnect.setText("disconnect device");

			/*mBtnDisConnect.setEnabled(false);
			mBtnDisConnect.setBackgroundResource(R.drawable.button_cancel_grey);
			*/
			setIgnoreBackDevicekeyOnCardProcess(false);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_NOT_CONNECTED){

			if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.BACK_BUTTON
					|| mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.ONLINE_SUBMIT) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}

			/*mBtnConnect.setEnabled(true);
			mBtnConnect.setBackgroundResource(R.drawable.button_next_active);
			*/
			mBtnConnect.setText("connect device");


			/*mBtnDisConnect.setEnabled(false);
			mBtnDisConnect.setBackgroundResource(R.drawable.button_cancel_grey);
			*/
			setIgnoreBackDevicekeyOnCardProcess(false);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_CONNECTED){

			/*mBtnConnect.setEnabled(false);
			mBtnConnect.setBackgroundResource(R.drawable.button_next_inactive);
			*/
			mBtnConnect.setText("disconnect device");

			/*mBtnDisConnect.setEnabled(true);
			mBtnDisConnect.setBackgroundResource(R.drawable.button_cancel_red);
			*/


			setWisepadStatusMsg(getString(R.string.device_connected));

			setIgnoreBackDevicekeyOnCardProcess( false);


		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_DEVICE_NOTFOUND){

			setWisepadStatusMsg(getString(R.string.device_not_found));

			if (mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.BACK_BUTTON
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.ONLINE_SUBMIT) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}



			setIgnoreBackDevicekeyOnCardProcess( false);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_DIS_CONNECTED){

			/*mBtnConnect.setEnabled(true);
			mBtnConnect.setBackgroundResource(R.drawable.button_next_active);
			*/
			mBtnConnect.setText("connect device");

			/*mBtnDisConnect.setEnabled(false);
			mBtnDisConnect.setBackgroundResource(R.drawable.button_cancel_grey);
			*/
			setWisepadStatusMsg(getString(R.string.device_disconnected));


			if (mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.BACK_BUTTON
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.ONLINE_SUBMIT
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.STOP_BLUETOOTH) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}


			setIgnoreBackDevicekeyOnCardProcess( false);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_NO_PAIRED_DEVICES_FOUND){

			if (mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.BACK_BUTTON
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.ONLINE_SUBMIT
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.STOP_BLUETOOTH) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}

			setIgnoreBackDevicekeyOnCardProcess( false);

			final Dialog dialog = Constants.showDialog(this, mCardSaleDlgTitle,
					getResources().getString(R.string.no_paired_wisePad_found_please_pair_the_wisePad_from_your_phones_bluetooth_settings_and_try_again), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO, getString(R.string.ok),"");
			Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
			yes.setOnClickListener(new View.OnClickListener()
			{

				public void onClick(View v) {

					doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);
					//dialog.dismiss();

				}
			});

			dialog.show();

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_DEVICE_DETECTED){

			setWisepadStatusMsg(getString(R.string.device_detected));
		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_FAIL_TO_START_BT){

			setWisepadStatusMsg(getString(R.string.fail_to_start_bluetooth_v2));
		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_BLUETOOTH_DISABLED){

			setWisepadStatusMsg(getString(R.string.creditsaleswiperfragment_enable_bluetooth));
		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_MULTIPLE_PAIRED_DEVCIES_FOUND)
		{
			TaskShowMultiplePairedDevices pairedtask = new TaskShowMultiplePairedDevices();
			pairedtask.execute();
		}

	}

	@Override
	public void setWisePadStateInfo(String message){


		if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_WaitingForCard) {

			setWisepadStatusMsg(message);
		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Pin_Entry_Request) {


			if(message.equalsIgnoreCase(getString(R.string.bypass)))
				setWisepadStatusMsg(message);
			else
				setWisepadStatusMsg(getString(R.string.creditsale_swiperview_lable_enter_pin));

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Pin_Entry_Results) {


			setWisepadStatusMsg(message);

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_ICC_SetAmount) {

			setWisepadStatusMsg(message);

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_CheckCard) {

			setWisepadStatusMsg(message);

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_CancelCheckCard) {

			setWisepadStatusMsg(message);

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Ready) {

			setWisepadStatusMsg(message);
		}
		else if(getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Pin_Entry_Asterisk){

			txtProgMsg.setVisibility(View.VISIBLE);

			int astricIndex = 0;
			try {
				astricIndex = Integer.parseInt(message);
			}catch (Exception e){

			}

			if(astricIndex == 1)
				setWisepadStatusMsg("*");
			else if(astricIndex == 2)
				setWisepadStatusMsg("* *");
			else if(astricIndex == 3)
				setWisepadStatusMsg("* * *");
			else if(astricIndex == 4)
				setWisepadStatusMsg("* * * *");
		}
		else {

			txtProgMsg.setVisibility(View.VISIBLE);
			setWisepadStatusMsg(message);
		}
	}

	@Override
	public void setWisePadStateErrorInfo(String message)
	{

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName,  "message " + message, true, true);

		if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Pin_Entry_Results) {

			setWisepadStatusMsg(message);
		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Ready) {

			if(message.equalsIgnoreCase(getString(R.string.device_busy)))
				setWisepadStatusMsg(getString(R.string.creditsaleswiperfragment_status)+": "+ message);
			else
				setWisepadStatusMsg(message);
		}
		else {

			setWisepadStatusMsg(message);

			if(message.equalsIgnoreCase(getString(R.string.device_no_response))) {
				setNFCDeviceStatusInfo(message);
			}
		}
	}

	@Override
	public void setWisepadWaitingForCardResult() {

		setIgnoreBackDevicekeyOnCardProcess(true);
	}

	private void setWisepadStatusMsg(String msg)
	{
		txtProgMsg.setText(msg);
	}

	public void setNFCDeviceStatusInfo(String aTxtStatusMsg)
	{
		mTxtStatusMsg.setText( aTxtStatusMsg + "\n"  + mTxtStatusMsg.getText() );

	}

	public void setNFCReadResult(String data)
	{

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "data " + data, true, true);

		String text = "nfc_data_exchange_success ";
		text += "\n" + "ndef_record "  + data;

		if(data != null && data.length() >0)
		{
			String readeDataHEXSting = Utils.dencodeNdefFormat(data);
			text += "\n" + "plain data " + Utils.hexStringToString(readeDataHEXSting);
		}

		mTxtStatusMsg.setText( text + "\n"  + mTxtStatusMsg.getText());
	}



	/**
	 * We are finishing all running processes.
	 * @param processTaskType
	 * @return
	 */
	public void doneWithCreditSale(EMVPPROCESSTASTTYPE processTaskType)
	{
		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName,  "doneWithCreditSale  processTaskType " + processTaskType, true, true);

		if (!onDoneWithCreditSaleCalled)
		{

			if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
				mEMVProcessTask.cancel(true);

			mEMVPPROCESSTASTTYPE = processTaskType;
			mEMVProcessTask = new EMVProcessTask(); //every time create new object, as AsynTask will only be executed one time.
			mEMVProcessTask.execute();

			onDoneWithCreditSaleCalled = true;
		}
	}

	class EMVProcessTask extends AsyncTask<Void, Integer, Void>
	{
		@Override
		protected void onCancelled()
		{

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,  "onCancelled mEMVPPROCESSTASTTYPE " + mEMVPPROCESSTASTTYPE, true, true);

			//when the back tab is pressed
			if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.BACK_BUTTON)
			{
				finish();
			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.ONLINE_SUBMIT)
			{

			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.STOP_BLUETOOTH)
			{
				finish();
			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.SHOW_SIGNATURE)
			{
				finish();
			}

			mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;

		}

		@Override
		protected Void doInBackground(Void... unused) {

			//calling after this statement and canceling task will no meaning if you do some update database kind of operation
			//so be wise to choose correct place to put this condition
			//you can also put this condition in for loop, if you are doing iterative task
			//you should only check this condition in doInBackground() method, otherwise there is no logical meaning

			// if the task is not cancelled by calling LoginTask.cancel(true), then make the thread wait for 10 sec and then
			//quit it self


			int isec = 4;

			if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.STOP_BLUETOOTH)
				isec = 2;

			if (getWisePadConnectionState() == WisePadConnection.WisePadConnection_NO_PAIRED_DEVICES_FOUND)
				isec = 0;

			int ictr = 0;


			//it will wait for 15 sec or till the task is cancelled by the mSwiper routines.
			while (!isCancelled() & ictr < isec) {
				try {
					Thread.sleep(500);
				} catch (Exception ex) {
				}
				ictr++;
			}

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,  "EmvOnlinePorcessTask  end doInBackground", true, true);
			return null;
		}

		@Override
		protected void onPostExecute(Void unused)
		{
			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,  "onPostExecute mEMVPPROCESSTASTTYPE " + mEMVPPROCESSTASTTYPE, true, true);

			//when the back tab is pressed
			if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.BACK_BUTTON)
			{

				finish();
			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.ONLINE_SUBMIT)
			{

			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.STOP_BLUETOOTH)
			{
				finish();
			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.SHOW_SIGNATURE)
			{
				finish();
			}

			mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;
		}

	}


}