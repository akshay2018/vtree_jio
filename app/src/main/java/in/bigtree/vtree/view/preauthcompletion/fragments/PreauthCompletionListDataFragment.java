package in.bigtree.vtree.view.preauthcompletion.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.data.PreAuthData;

import java.util.ArrayList;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.view.preauthcompletion.PreauthCompletionActivity;

public class PreauthCompletionListDataFragment extends Fragment {

	Context mContext;
	PreauthCompletionActivity mPreauthCompletionListDataView = null;
	SharedVariable applicationData = null;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		
		mContext = activity;
		mPreauthCompletionListDataView = ((PreauthCompletionActivity) mContext);
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.preauthcompletion_listview,container, false);

		initViews(view);
		return view;
	}

	public void initViews(View view) {
		// TODO Auto-generated method stub

		ListView listview = (ListView)view.findViewById(R.id.preauthcompletion_listview_LST_preauthhistory);
		listview.setAdapter(new CardHistoryAdapter(mPreauthCompletionListDataView, mPreauthCompletionListDataView.getPreauthCompletionDataList()));
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
				// TODO Auto-generated method stub

				mPreauthCompletionListDataView.showScreenTxtDetails(position);

			}
		});
	}

	public class CardHistoryAdapter extends BaseAdapter {
		ArrayList<PreAuthData> listData = null;
		Context context;

		public CardHistoryAdapter(Context context, ArrayList<PreAuthData> listData) {
			this.listData = listData;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listData.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {

				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.preauthcompletion_custom_listdaata, null);
			}

			//ArrayList<String> arrData = Constants.splitStringByTwoChars(listData.get(position), '|');

			PreAuthData preAuthData = listData.get(position);

			TextView lblamt = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_lblamount);
			lblamt.setText("");
			lblamt.setVisibility(View.GONE);
			lblamt.setTypeface(applicationData.font);


			TextView amt = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_amount);
			amt.setText(preAuthData.trxAmount);
			amt.setTypeface(applicationData.font);


			TextView date = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_date);
			date.setText(preAuthData.trxDate);
			date.setTypeface(applicationData.font);


			TextView type = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_cardtype);
			type.setText(preAuthData.merchantInvoce);
			type.setTypeface(applicationData.font);

			return convertView;
		}

	}


}
