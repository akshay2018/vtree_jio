package in.bigtree.vtree.view.bqrsale.fragment;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.view.bqrsale.BQRSaleActivity;
import in.bigtree.vtree.view.bqrsale.BQRSaleEnum;


/**
 * MenuView
 * MenuView activity lists all the Mswipe api's available, each entry will demonstrate the integration process
 * of the api
 *
 */

public class SelectionListFragment extends android.support.v4.app.Fragment {


	Context mContext;
	BQRSaleActivity mBQRSaleActivity = null;
	SharedVariable applicationData = null;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub

		mContext = activity;
		mBQRSaleActivity = ((BQRSaleActivity) mContext);
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.view_payby_link, container, false);

		initViews(view);
		return view;
	}

	private void initViews(View view) {

		mBQRSaleActivity.mBQRBitmap = null;
		mBQRSaleActivity. mBQRAmount = "";
		mBQRSaleActivity. mBQRReceipt = "";
		mBQRSaleActivity. mBQRMobileNo = "";
		mBQRSaleActivity. mBQRTrxId = "";
		mBQRSaleActivity. mTrnxDate = "";
		mBQRSaleActivity. mAuthcode = "";
		mBQRSaleActivity. mRRNo = "";
		mBQRSaleActivity. mVocherNo = "";
		mBQRSaleActivity. mTrnxType = "";

		ListView listView = (ListView) view.findViewById(R.id.payby_link_view_LST_options);

		String[] values = new String[]
				{
						"QR Sale",
						"Check Status"
				};

		((RelativeLayout) view.findViewById(R.id.top_bar)).setVisibility(View.GONE);

		TextView txtHeading = (TextView) view.findViewById(R.id.topbar_LBL_heading);
		txtHeading.setText("SDK List");

		PayByLinkViewAdapter adapter = new PayByLinkViewAdapter(mBQRSaleActivity, values);
		int[] colors = {0, 0xFF0000FF, 0};
		listView.setDivider(new GradientDrawable(Orientation.LEFT_RIGHT, colors));
		listView.setDividerHeight(1);

		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				int menuoption = 0;

				if (arg2 == menuoption) {
					mBQRSaleActivity.showScreen(BQRSaleEnum.BQRSaleScreens.BQR_SALE_AMOUNT);

				}
				menuoption++;
				if (arg2 == menuoption) {
					mBQRSaleActivity.showScreen(BQRSaleEnum.BQRSaleScreens.BQR_SALE_CHECK_STATUS);
				}

			}
		});

	}

	public class PayByLinkViewAdapter extends BaseAdapter {
		String[] listData = null;
		Context context;

		public PayByLinkViewAdapter(Context context, String[] listData) {
			this.listData = listData;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listData.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.view_menulstitem, null);
			}
			TextView txtItem = (TextView) convertView
					.findViewById(R.id.menuview_lsttext);
			txtItem.setText(listData[position]);

			return convertView;
		}
	}
}
