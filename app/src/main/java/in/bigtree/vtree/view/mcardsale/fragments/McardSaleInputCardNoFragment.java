package in.bigtree.vtree.view.mcardsale.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.view.mcardsale.McardSaleActivity;
import in.bigtree.vtree.view.mcardsale.McardSaleEnum;

import static android.view.View.GONE;

/**
 * Created by mSwipe on 2/1/2016.
 */
public class McardSaleInputCardNoFragment extends Fragment {

    private Context mContext;
    private McardSaleActivity mMCardActivity = null;
    private TextView mETTopupAmount;
    private EditText mETCardNo;
    private TextView mTXTCardNo;
    private ImageButton mBTnNext;


    String amt = "";

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        mContext = activity;
        mMCardActivity = ((McardSaleActivity) mContext);

        super.onAttach(activity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.mcard_topup_fragment,container, false);
        initViews(view);

        return view;
    }

       public void initViews(View view) {

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        mETCardNo = (EditText) view.findViewById(R.id.mcard_card_transaction_ETXT_cardno);
        mBTnNext = (ImageButton)view.findViewById(R.id.mcard_card_transaction_IMG_addcard_next);
        mTXTCardNo = (TextView)view.findViewById(R.id.mcard_card_transaction_TXT_cardno);


           mETTopupAmount = (TextView)view.findViewById(R.id.mcard_topup_amountview_TXT_amount) ;
           mETTopupAmount.setText(mMCardActivity.mTransactionData.mTotAmount);
           mBTnNext.setEnabled(false);

           mETCardNo.requestFocus();

        if (mMCardActivity.mCardInputType != McardSaleEnum.CardInputType.ENTER)
        {

            ((TextView)view.findViewById(R.id.customTextViewMedium)).setText("card no.");
            mETCardNo.setText(mMCardActivity.mTopUpScanCardNum);
            mETCardNo.setEnabled(false);
            mETCardNo.setFocusable(false);
            mBTnNext.setEnabled(true);
            mTXTCardNo.setVisibility(View.VISIBLE);
            mETCardNo.setVisibility(GONE);
            mTXTCardNo.setText(mMCardActivity.mTopUpScanCardNum);

        }

        mETCardNo.addTextChangedListener(new TextWatcher()
        {
            int prevL = 0;

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

                prevL = mETCardNo.getText().toString().length();


            }
            @Override
            public void afterTextChanged(Editable editable)
            {


                int length = editable.length();
                if ((prevL < length) && length == 4 || (prevL < length) && length == 9 ||
                        (prevL < length) && length == 14){
                    String data = mETCardNo.getText().toString();
                    mETCardNo.setText(data + "-");
                    mETCardNo.setSelection(length + 1);
                }


                if (mETCardNo.getText().toString().length() >= 19)
                {
                    mBTnNext.setEnabled(true);
                }
                else {

                    mBTnNext.setEnabled(false);
                }
            }



            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }


        });


        mBTnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mMCardActivity.mTransactionData.mCardNo = removeHypen(mETCardNo.getText().toString(),'-') ;

                if(mMCardActivity.mTransactionData.mCardNo.startsWith("903001"))
                {
                    mMCardActivity.processMcardSale();
                }
                else
                {
                    mMCardActivity.showScreen(McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_OTP);
                }


            }
        });
    }




    public String removeHypen(String s, char c) {

        String r = "";

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c)
                r += s.charAt(i);
        }

        return r;
    }


}