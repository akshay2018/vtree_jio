package in.bigtree.vtree.view.paybylink.fragment.bank;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;

import in.bigtree.vtree.R;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.paybylink.PayByLinkActivity;
import in.bigtree.vtree.view.paybylink.PayByLinkEnum;


public class PayByLinkTermsandCondFragment extends Fragment
{

	private Context mContext;
	private PayByLinkActivity mPayByLinkActivity = null;
	private ImageButton mBtnNxt =  null;
	private CheckBox mCHKAgree = null;


	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub

		mContext = activity;
		mPayByLinkActivity = ((PayByLinkActivity) mContext);

		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.paybylink_bank_terms_and_conditions,container, false);

		initViews(view);
		return view;
	}

	public void initViews(View view)
	{
		mCHKAgree =(CheckBox)view.findViewById(R.id.paybylink_terms_and_conditions_CBK_read_me);
		mBtnNxt = (ImageButton)view.findViewById(R.id.paybylink_terms_IMGBtn_next);
		mBtnNxt.setEnabled(false);

		mCHKAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

				if(isChecked)
				{
					mBtnNxt.setEnabled(true);
				}else
				{
					mBtnNxt.setEnabled(false);
				}
			}
		});

		mBtnNxt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mCHKAgree.isChecked())
				{
					mPayByLinkActivity.showScreen(PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_OTP);
				}
				else
				{
					Constants.showDialog(mPayByLinkActivity, "", "accept terms and conditions");
				}
			}
		});
	}
}
