package in.bigtree.vtree.view.paybylink;

public  class PayByLinkEnum {

	public enum PayByLinkScreens
	{
		PayByLinkScreens_BANK_Selection,
		PayByLinkScreens_BANK_Details,
		PayByLinkScreens_Terms_Conditions,
		PayByLinkScreens_OTP,

		PayByLink_Screen_Selection,
        PayByLink_Screen_Mobile_Number,
		PayByLink_Screen_Aproved_Transaction,
		PayByLink_Screen_PaymentSatus
	}
}
