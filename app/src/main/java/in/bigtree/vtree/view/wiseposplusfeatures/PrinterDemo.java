package in.bigtree.vtree.view.wiseposplusfeatures;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import in.bigtree.vtree.R;

public class PrinterDemo extends Activity {

    private Button mBTNPrintText = null;
    private Button mBTNPrintImage = null;
    private Button mBTNPrintEnterText = null;
    private EditText mETPrintEnterText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_demo);
        /*
        mBTNPrintText = (Button) findViewById(R.id.printdemo_BTN_printtext);
        mBTNPrintImage = (Button) findViewById(R.id.printdemo_BTN_printimage);
        mBTNPrintEnterText = (Button) findViewById(R.id.printdemo_BTN_printentered);
        mETPrintEnterText = (EditText) findViewById(R.id.printdemo_TXT_entertext);

        mBTNPrintText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Print.StartPrinting("TITLE <br>",FontLattice.FORTY_EIGHT, true, Align.CENTER, true);
                Print.StartPrinting("LAST NAME<br>" ,FontLattice.THIRTY_TWO, true, Align.LEFT, true);
                Print.StartPrinting("ENTRY TIME<br>" ,FontLattice.THIRTY_TWO, true, Align.LEFT, true);
                Print.StartPrinting("TICKET NO<br>" ,FontLattice.THIRTY_TWO, true, Align.LEFT, true);

                Print.StartPrinting();
                Print.StartPrinting();
                Print.StartPrinting();
            }
        });

        mBTNPrintImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.mswipe);
                Print.StartPrintingImage(icon, Align.CENTER);
                Print.StartPrinting();
                Print.StartPrinting();
                Print.StartPrinting();

            }
        });

        mBTNPrintEnterText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Print.StartPrinting(mETPrintEnterText.getText().toString() ,FontLattice.THIRTY_TWO, true, Align.LEFT, true);
                Print.StartPrinting();
                Print.StartPrinting();

            }
        });
        */
    }
}
