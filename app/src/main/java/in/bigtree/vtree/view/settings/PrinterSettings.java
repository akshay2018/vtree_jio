package in.bigtree.vtree.view.settings;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.BaseTitleActivity;


public class PrinterSettings extends BaseTitleActivity {

    public final static String log_tab = "PrinterSettings=>";

	CheckBox mChkPrinterSupport = null;
	CheckBox mChkPrintSignature = null;
	CheckBox mChkSignatureRequired = null;
    SharedVariable applicationData = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        applicationData = (SharedVariable) getApplicationContext();
		requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.printer_settings);
        initViews();
        

    }

    private void initViews() {
    	
    	 TextView txtHeading = (TextView) findViewById(R.id.topbar_LBL_heading);
         txtHeading.setText("Printer Settings");
         txtHeading.setTypeface(applicationData.font);
    	
    	mChkPrinterSupport = (CheckBox) findViewById(R.id.settings_CHK_printer_support);
    	mChkPrinterSupport.setTypeface(applicationData.font);
    	mChkPrintSignature = (CheckBox) findViewById(R.id.settings_CHK_print_signature);
    	mChkPrintSignature.setTypeface(applicationData.font);
    	mChkSignatureRequired = (CheckBox)findViewById(R.id.settings_CHK_signature);
    	mChkSignatureRequired.setTypeface(applicationData.font);

    	if(AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrinterSupportRequired()){
    		mChkPrinterSupport.setChecked(true);
    	}else{
    		mChkPrinterSupport.setChecked(false);
    		mChkPrintSignature.setVisibility(View.GONE);
    	}

    	if (AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired())
		mChkSignatureRequired.setChecked(true);
    	else
    	mChkSignatureRequired.setChecked(false);

    	
    	mChkPrinterSupport.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View view) {
    			
    			if (SharedVariable.IS_DEBUGGING_ON)
    				Logs.v(getPackageName(), " mChkPrinterSupport " , true, true);
    			
    			mChkPrintSignature.setChecked(false);
    			
    			if (mChkPrinterSupport.isChecked()) {
					AppSharedPrefrences.getAppSharedPrefrencesInstace().setPrinterSupportRequired(true);
    				mChkPrintSignature.setVisibility(View.VISIBLE);
    			} else {
					AppSharedPrefrences.getAppSharedPrefrencesInstace().setPrinterSupportRequired(false);
    				mChkPrintSignature.setVisibility(View.GONE);
    				
    			}

				AppSharedPrefrences.getAppSharedPrefrencesInstace().setPrintSignatureRequired(false);
    			
    		}
    	});
    	
    	
    	if(AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrintSignatureRequired())
    		mChkPrintSignature.setChecked(true);
    	else
    		mChkPrintSignature.setChecked(false);
    	
    	mChkPrintSignature.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View view) {
    			
    			if (SharedVariable.IS_DEBUGGING_ON)
    				Logs.v(getPackageName(), " mChkPrintSignature " , true, true);
    			
    			if (mChkPrintSignature.isChecked()) {
					AppSharedPrefrences.getAppSharedPrefrencesInstace().setPrintSignatureRequired(true);
    			} else {
					AppSharedPrefrences.getAppSharedPrefrencesInstace().setPrintSignatureRequired(false);
    				
    			}
    		}
    	});

		mChkSignatureRequired.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

				if (isChecked)
					AppSharedPrefrences.getAppSharedPrefrencesInstace().setSignatureRequired(true);
				else
					AppSharedPrefrences.getAppSharedPrefrencesInstace().setSignatureRequired(false);

			}
		});


    }
}
