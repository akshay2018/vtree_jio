package in.bigtree.vtree.view.cardpaymentactivity.cardsalesdkintegration;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mswipetech.wisepad.sdk.device.MSWisepadDeviceController;
import com.mswipetech.wisepad.sdk.device.MSWisepadDeviceControllerResponseListener;
import com.mswipetech.wisepad.sdk.device.WisePadConnection;
import com.mswipetech.wisepad.sdk.device.WisePadTransactionState;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.data.TransactionData;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.cardpaymentactivity.MSWisepadView;

public class CreditSaleActivity extends MSWisepadView
{



	/*images to show the bluetooth and  websocket connection status i.e wheteher topbar_img_host_active or not*/
	public ImageView imgBlueToothStatus, imgHostConnectionStatus;
	public LinearLayout mLNRTopbarCancel;

	/*using these animation for connection status*/
	public Animation alphaAnim;

	/**
	 * the wise pad has to be disconnected if its not required and also when the application closes
	 * the wise pad has to be disconnected,
	 * this will see to that the dis-connecting to the wisepad does has execute multiple times
	 */

	public boolean onDoneWithCreditSaleCalled = false;
	public boolean mIsIgnoreBackDevicekeyOnCardProcess = false;

	/**
	 * in certain scenarios the wise pad where in a likely case becomes non responsive, at that precise
	 * moment a task is called and with in the elapsed time if their no communication back to the app this task will
	 * un-initiate the communication link from the wisepad
	 */

	public EMVProcessTask mEMVProcessTask = null;

	public enum EMVPPROCESSTASTTYPE{
		NO_TASK, STOP_BLUETOOTH, BACK_BUTTON,
		ONLINE_SUBMIT, SHOW_SIGNATURE}

	public EMVPPROCESSTASTTYPE mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;


	/* we are storing the cardsale datea */
	public TransactionData mTransactionData;

	private long mBackPressed=0;

	/*the preauth transaction if enabled to the user, since this is similar to the normal transaction they
	are handled here, and if this are configured for the user then a different set of api will be called */


	TextView lblAmtMsg = null;
	TextView txtProgMsg = null;
	Button mBtnSwipe = null;
	Button mBtnConnectBluetooth = null;


	boolean isPinBypassed = false;
	boolean mIsEmiSale = false;
	public boolean mIsPreAuth = false;
	public boolean mIsSaleWithCash = false;

	public String mEmiPeriod = "";
	public String mEmiBankCode = "";
	public String mEmiRate = "";
	public String mEmiAmount = "";

	Intent intent;

	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState
	 */

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.creditsale_screen);


		mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;
		mTransactionData = new TransactionData();

		mIsEmiSale = getIntent().getBooleanExtra("emisale", false);
		mIsPreAuth = getIntent().getBooleanExtra("preauthsale", false);
		mIsSaleWithCash = getIntent().getBooleanExtra("salewithcash", false);

		try {

			if(mIsEmiSale)
			{
				mEmiRate = getIntent().getStringExtra("emirate");
				mEmiAmount = getIntent().getStringExtra("emiamount");
				mEmiPeriod = getIntent().getStringExtra("emiperiod");
				mEmiBankCode = getIntent().getStringExtra("emibankcode");
				mTransactionData.mCardFirstSixDigit = getIntent().getStringExtra("cardfirstsixdigit");
				mTransactionData.mBaseAmount = getIntent().getStringExtra("baseamount");

			}
			else if(mIsSaleWithCash){

				mTransactionData.mSaleCashAmount = getIntent().getStringExtra("salecashamount");

			}else {

				mTransactionData.mBaseAmount = getIntent().getStringExtra("baseamount");
			}

			mTransactionData.mTotAmount = getIntent().getStringExtra("totalamount");
			mTransactionData.mTipAmount = getIntent().getStringExtra("tipamount");

			mTransactionData.mPhoneNo = getIntent().getStringExtra("mobileno");
			mTransactionData.mReceipt = getIntent().getStringExtra("receiptno");
			mTransactionData.mEmail = getIntent().getStringExtra("email");
			mTransactionData.mNotes = getIntent().getStringExtra("notes");
			mTransactionData.mExtraNote1 = getIntent().getStringExtra("extra1");
			mTransactionData.mExtraNote2 = getIntent().getStringExtra("extra2");
			mTransactionData.mExtraNote3 = getIntent().getStringExtra("extra3");
			mTransactionData.mExtraNote4 = getIntent().getStringExtra("extra4");
			mTransactionData.mExtraNote5 = getIntent().getStringExtra("extra5");
			mTransactionData.mExtraNote6 = getIntent().getStringExtra("extra6");
			mTransactionData.mExtraNote7 = getIntent().getStringExtra("extra7");
			mTransactionData.mExtraNote8 = getIntent().getStringExtra("extra8");
			mTransactionData.mExtraNote9 = getIntent().getStringExtra("extra9");
			mTransactionData.mExtraNote10 = getIntent().getStringExtra("extra9");

		}
		catch (Exception e){


		}

		initViews();

	}

	@Override
	protected void onStop()
	{
		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, " ", true, true);

		/*stops the gateway connection */

		if (!onDoneWithCreditSaleCalled)
		{

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,  "onDoneWithCreditSaleCalled " + onDoneWithCreditSaleCalled, true, true);

			doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);
			super.onStop();

		} else {
			super.onStop();
		}
	}

	/**
	 * @description:
	 * 	 Initializes the UI elements for the Creditsale activity
	 * 	 @param
	 * 	 @return
	 *
	 */
	private void initViews()
	{

		/*final Dialog dialog = Constants.showDialog(CreditSaleActivity.this, "MSWisepadView",
				"autoconnect : if set to true, the SDK will try to automatically connect to the available paired device\n" +
						"checkcardAfterConnection :  if set to true, once the SDK connects to the device it will immediately callup checkCard function",
				Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_SHOW_DLG_INFO);
		Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
		yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();


			}
		});

		dialog.show();*/


		imgBlueToothStatus = (ImageView) findViewById(R.id.topbar_IMG_position1);
		imgHostConnectionStatus = (ImageView) findViewById(R.id.topbar_IMG_position2);

		mLNRTopbarCancel = (LinearLayout)findViewById(R.id.topbar_LNR_topbar_cancel);
		mLNRTopbarCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(getWisePadConnectionState() == WisePadConnection.WisePadConnection_CONNECTING){

					Toast.makeText(CreditSaleActivity.this, "please wait connection in process", Toast.LENGTH_LONG).show();
				}
				else {

					doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);
				}

			}
		});
		mLNRTopbarCancel.setVisibility(View.VISIBLE);

		lblAmtMsg = (TextView) findViewById(R.id.creditsale_totalamountview_LBL_totalamount);

		if(mTransactionData.mTotAmount.toString().length() >= 7)
			lblAmtMsg.setTextSize(TypedValue.COMPLEX_UNIT_SP, SharedVariable.font_size_amount_small);
		else
			lblAmtMsg.setTextSize(TypedValue.COMPLEX_UNIT_SP, SharedVariable.font_size_amount_normal);

		lblAmtMsg.setText(mTransactionData.mTotAmount);
		mBtnConnectBluetooth = (Button) findViewById(R.id.creditsale_swiperview_BTN_connect_bluetooth);
		mBtnConnectBluetooth.setVisibility(View.GONE);

		txtProgMsg = (TextView) findViewById(R.id.creditsale_swiperview_EDT_swipe_progmsg);

		mBtnSwipe = (Button) findViewById(R.id.creditsale_swiperview_BTN_swipe);

		//imgBlueToothStatus.setVisibility(View.VISIBLE);
		imgHostConnectionStatus.setVisibility(View.VISIBLE);

		alphaAnim = AnimationUtils.loadAnimation(this, R.anim.alpha_anim);

		mBtnSwipe.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				setWisepadStatusMsg(getString(R.string.connecting));
				processDeviceConnetionWithAutoInitiation(AutoInitiationDevice.CHECKCARD);

			}
		});

		mBtnConnectBluetooth.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				setWisepadStatusMsg(getString(R.string.connecting));
				initiateWisepadConnection();
			}
		});
	}

	/**
	 * @description
	 *     We are handling backbutton manually based on screen position.
	 * @param keyCode
	 * @param event
	 * @return
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,  "onKeyDown  connection state " + getWisePadConnectionState(), true, true);

			// if the state of the devcie connection is no
			// DEVCIE_NO_PAIRED_DEVICES then from the amount screen it will take
			// to steps screen,
			// from here the below will restrict moving back.

			if(getWisePadConnectionState() == WisePadConnection.WisePadConnection_CONNECTING){

				Toast.makeText(CreditSaleActivity.this, "please wait connection in process", Toast.LENGTH_LONG).show();
			}
			else {

				if(isIgnoreBackDevicekeyOnCardProcess())
				{
					if (mBackPressed + 2000 > System.currentTimeMillis()) {

						doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);

					}
					else {
						Toast.makeText(this, getResources().getString(R.string.processing_card_in_progress_press_back_key_twice_in_succession_to_terminate_the_transaction), Toast.LENGTH_SHORT).show();
						mBackPressed = System.currentTimeMillis();
					}
				}
				else{
					doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);

				}
			}

			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	public boolean isIgnoreBackDevicekeyOnCardProcess() {
		return mIsIgnoreBackDevicekeyOnCardProcess;
	}

	public void setIgnoreBackDevicekeyOnCardProcess(boolean ignoreBackDevicekeyOnCardProcess) {
		this.mIsIgnoreBackDevicekeyOnCardProcess = ignoreBackDevicekeyOnCardProcess;
	}

	int pinCount = 0;

	/**setWisePadConnectionState
	 *	set the views to the connection state
	 * @param
	 * wisePadConnection
	 * 		The state of the wise pad connection
	 * @return
	 */
	@Override
	public void setWisePadConnectionStateResult(WisePadConnection wisePadConnection)
	{

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, " wisePadConnection " + wisePadConnection, true, true);

		if(MSWisepadDeviceController.getDeviceType() == MSWisepadDeviceController.DeviceType.WISEPOS){
            mBtnConnectBluetooth.setVisibility(View.GONE);
			imgBlueToothStatus.setVisibility(View.GONE);
		}
		else if(MSWisepadDeviceController.getDeviceType() == MSWisepadDeviceController.DeviceType.WISEPOS_PLUS)
		{
			mBtnConnectBluetooth.setVisibility(View.GONE);
			imgBlueToothStatus.setVisibility(View.GONE);
		}else
		{
			mBtnConnectBluetooth.setVisibility(View.VISIBLE);
			imgBlueToothStatus.setVisibility(View.VISIBLE);
		}
		if(wisePadConnection == WisePadConnection.WisePadConnection_CONNECTING)
		{

			setWisepadStatusMsg(getString(R.string.connecting_device));

			imgBlueToothStatus.startAnimation(alphaAnim);
			imgBlueToothStatus.setImageResource(R.drawable.topbar_img_bluetooth_inactive);

			setIgnoreBackDevicekeyOnCardProcess(false);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_NOT_CONNECTED){

			if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.BACK_BUTTON
					|| mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.ONLINE_SUBMIT) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}

			mBtnSwipe.setEnabled(false);
			mBtnSwipe.setBackgroundResource(R.drawable.button_next_inactive);
			setIgnoreBackDevicekeyOnCardProcess(false);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_CONNECTED){

			mBtnSwipe.setEnabled(false);
			mBtnSwipe.setBackgroundResource(R.drawable.button_next_inactive);
			setWisepadStatusMsg(getString(R.string.device_connected));

			setIgnoreBackDevicekeyOnCardProcess( false);

			imgBlueToothStatus.setAnimation(null);
			imgBlueToothStatus.setImageResource(R.drawable.topbar_img_bluetooth_active);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_DEVICE_NOTFOUND){

			setWisepadStatusMsg(getString(R.string.device_not_found));

			if (mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.BACK_BUTTON
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.ONLINE_SUBMIT) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}

			imgBlueToothStatus.setAnimation(null);
			imgBlueToothStatus.setImageResource(R.drawable.topbar_img_bluetooth_inactive);


			setIgnoreBackDevicekeyOnCardProcess( false);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_DIS_CONNECTED){

			setWisepadStatusMsg(getString(R.string.device_disconnected));


			if (mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.BACK_BUTTON
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.ONLINE_SUBMIT
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.STOP_BLUETOOTH) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}

			imgBlueToothStatus.setAnimation(null);
			imgBlueToothStatus.setImageResource(R.drawable.topbar_img_bluetooth_inactive);

			setIgnoreBackDevicekeyOnCardProcess( false);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_NO_PAIRED_DEVICES_FOUND){

			if (mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.BACK_BUTTON
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.ONLINE_SUBMIT
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.STOP_BLUETOOTH) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}

			imgBlueToothStatus.setAnimation(null);
			imgBlueToothStatus.setImageResource(R.drawable.topbar_img_bluetooth_inactive);

			setIgnoreBackDevicekeyOnCardProcess( false);

			final Dialog dialog = Constants.showDialog(this, mCardSaleDlgTitle,
					getResources().getString(R.string.no_paired_wisePad_found_please_pair_the_wisePad_from_your_phones_bluetooth_settings_and_try_again), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO, getString(R.string.ok),"");
			Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
			yes.setOnClickListener(new View.OnClickListener()
			{

				public void onClick(View v) {

					doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);
					//dialog.dismiss();

				}
			});

			dialog.show();

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_DEVICE_DETECTED){

			setWisepadStatusMsg(getString(R.string.device_detected));
		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_FAIL_TO_START_BT){

			setWisepadStatusMsg(getString(R.string.fail_to_start_bluetooth_v2));
		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_BLUETOOTH_DISABLED){

			setWisepadStatusMsg(getString(R.string.creditsaleswiperfragment_enable_bluetooth));
		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_MULTIPLE_PAIRED_DEVCIES_FOUND)
		{
			TaskShowMultiplePairedDevices pairedtask = new TaskShowMultiplePairedDevices();
			pairedtask.execute();
		}

	}

	@Override
	public void setWisePadStateInfo(String message){


		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName,  "message: " + message, true, true);


		if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_WaitingForCard) {

			isPinBypassed = false;
			setWisepadStatusMsg(message);
			mBtnSwipe.setEnabled(false);
			mBtnSwipe.setBackgroundResource(R.drawable.button_next_inactive);

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Pin_Entry_Request) {

			isPinBypassed = false;

			if(message.equalsIgnoreCase(getString(R.string.bypass)))
				setWisepadStatusMsg(message);
			else
				setWisepadStatusMsg(getString(R.string.creditsale_swiperview_lable_enter_pin));

			showPinpad();


		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Pin_Entry_Results) {

			isPinBypassed = false;
            pinCount = 0;
			setWisepadStatusMsg(message);

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_ICC_SetAmount) {

			setWisepadStatusMsg(message);

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_CheckCard) {

			isPinBypassed = false;
			setWisepadStatusMsg(message);

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_CancelCheckCard) {

			mBtnSwipe.setEnabled(true);
			mBtnSwipe.setBackgroundResource(R.drawable.button_next_active);
			setWisepadStatusMsg(message);

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Ready) {

			mBtnSwipe.setEnabled(true);
			mBtnSwipe.setBackgroundResource(R.drawable.button_next_active);
			setWisepadStatusMsg(message);
		}
		else if(getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Pin_Entry_Asterisk){

			txtProgMsg.setVisibility(View.VISIBLE);

			int astricIndex = 0;
			try {
				astricIndex = Integer.parseInt(message);
			}catch (Exception e){

			}

			if(astricIndex == 0){

				pinCount = 0;
			}
			else {

				pinCount = pinCount+1;
			}

			if(pinCount == 0) {
				setWisepadStatusMsg(getString(R.string.creditsale_swiperview_lable_enter_pin));
			}
			else {

				String strPin = "";
				String delimiter = "";
				for (int i = 1; i <= pinCount && i <= 6; i++) {

					strPin = strPin + delimiter + "*";
					delimiter = " ";
				}

				setWisepadStatusMsg(strPin);
			}
		}
		else {

			txtProgMsg.setVisibility(View.VISIBLE);
			setWisepadStatusMsg(message);
		}
	}

	@Override
	public void setWisePadStateErrorInfo(String message)
	{

	    pinCount = 0;
		mBtnSwipe.setEnabled(true);
		mBtnSwipe.setBackgroundResource(R.drawable.button_next_active);

		if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Pin_Entry_Results) {

			isPinBypassed = false;
			setWisepadStatusMsg(message);

		}
		else if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_Ready) {

			if(message.equalsIgnoreCase(getString(R.string.device_busy)))
				setWisepadStatusMsg(getString(R.string.creditsaleswiperfragment_status)+": "+ message);
			else
				setWisepadStatusMsg(message);
		}
		else {

			setWisepadStatusMsg(message);

		}
	}

	/**
	 * this will get called only in the total amount and when the total amount will bot be shown i.w in the amount screen
	 */
	@Override
	public void setWisepadAmount()
	{

		/* To start the gateway connection*/

		if (mMSWisepadDeviceController != null)
			mMSWisepadDeviceController.setAmount(mTransactionData.mTotAmount, MSWisepadDeviceControllerResponseListener.TransactionType.GOODS);

	}

	@Override
	public void setWisepadWaitingForCardResult() {

		setIgnoreBackDevicekeyOnCardProcess(true);
	}

	private void setWisepadStatusMsg(String msg)
	{

		if(isPinBypassed && !AppSharedPrefrences.getAppSharedPrefrencesInstace().isPinBypassEnabled()) {

			msg = getString(R.string.creditsale_swiperview_pin_bypass_disabled);

		}

		txtProgMsg.setText(msg);

	}

	/**
	 * description
	 *       it will so amex pin pin entry fragment.
	 */
	@Override
	public void showAmexPinEntry(){


		final Dialog dialog = Constants.showDialogPin(CreditSaleActivity.this, mCardSaleDlgTitle, getString(R.string.creditsale_amexcardpinentryview_enter_pin_of_amex), "ok", "cancel","");

		final EditText txtPin = (EditText)dialog.findViewById(R.id.custompindlg_TXT_pin);

		((Button)dialog.findViewById(R.id.custompindlg_BTN_accept)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if(txtPin.getText().toString().trim().length() != 4){

					final Dialog dialog = Constants.showDialog(CreditSaleActivity.this, mCardSaleDlgTitle,
							getResources().getString(R.string.invalid_pin_input_should_be_4_digits_in_length), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);

					Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
					yes.setOnClickListener(new View.OnClickListener() {

						public void onClick(View v) {
							dialog.dismiss();
							txtPin.requestFocus();
						}
					});

					dialog.show();

				}
				else {

					dialog.dismiss();
					mTransactionData.mAmexSecurityCode = txtPin.getText().toString().trim();
					showCardDetails();
				}

			}
		});

		((Button)dialog.findViewById(R.id.custompindlg_BTN_bypass)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				dialog.dismiss();
			}
		});


		dialog.show();
	}



	/**
	 *  where we are showing details screen to the merchant
	 *  @param
	 *  @return
	 */
	@Override
	public void showCardDetails()
	{
		// TODO Auto-generated method stub

		if (getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_ICC_Online_Process
				|| getWisePadTranscationState() == WisePadTransactionState.WisePadTransactionState_MAG_Online_Process) {

			// TODO Auto-generated method stub

			SharedPreferences preferences;
			preferences = PreferenceManager.getDefaultSharedPreferences(CreditSaleActivity.this);
			preferences.edit().putString(MSWisepadDeviceController.LAST_CONNECTED_DEVCIECLASSNAME, mBlueToothMcId).commit();


			int ilen = mCardData.getCreditCardNo().length();

			String tempFirst2Digits;
			if (ilen >= 2)
				tempFirst2Digits = mCardData.getCreditCardNo().substring(0, 2);
			else
				tempFirst2Digits = mCardData.getCreditCardNo();

			if(mIsEmiSale)
			{
				if (ilen >= 6) {

					String cardFirstSixDigit = mCardData.getCreditCardNo().substring(0, 6);

					if (!mTransactionData.mCardFirstSixDigit.equalsIgnoreCase(cardFirstSixDigit))
					{
						final Dialog dialog = Constants.showDialog(CreditSaleActivity.this, "", "invalid card, card first six digits not matched.", Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
						Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
						yes.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {

								dialog.dismiss();

								if(mCardData.getCardSchemeResults() == MSWisepadDeviceControllerResponseListener.CARDSCHEMERRESULTS.ICC_CARD)
									requestCancelOnlineProcess();
								else
									requestCheckCard();

							}
						});

						dialog.show();

						return;
					}
				}
			}

			processCardSaleOnline();

		}
	}

	public void processCardSaleOnline(){}


	/**
	 * We are finishing all running processes.
	 * @param processTaskType
	 * @return
	 */
	public void doneWithCreditSale(EMVPPROCESSTASTTYPE processTaskType)
	{
		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName,  "doneWithCreditSale  processTaskType " + processTaskType, true, true);

		if (!onDoneWithCreditSaleCalled)
		{

			if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
				mEMVProcessTask.cancel(true);

			mEMVPPROCESSTASTTYPE = processTaskType;
			mEMVProcessTask = new EMVProcessTask(); //every time create new object, as AsynTask will only be executed one time.
			mEMVProcessTask.execute();

			onDoneWithCreditSaleCalled = true;
		}
	}

	class EMVProcessTask extends AsyncTask<Void, Integer, Void>
	{
		@Override
		protected void onCancelled()
		{

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,  "onCancelled mEMVPPROCESSTASTTYPE " + mEMVPPROCESSTASTTYPE, true, true);

			//when the back tab is pressed
			if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.BACK_BUTTON)
			{
				Intent intent = new Intent(CreditSaleActivity.this, MenuView.class);
				startActivity(intent);
				finish();
			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.ONLINE_SUBMIT)
			{

			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.STOP_BLUETOOTH)
			{
				finish();
			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.SHOW_SIGNATURE)
			{
				finish();
				startActivity(intent);
			}

			mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;

		}

		@Override
		protected Void doInBackground(Void... unused) {

			//calling after this statement and canceling task will no meaning if you do some update database kind of operation
			//so be wise to choose correct place to put this condition
			//you can also put this condition in for loop, if you are doing iterative task
			//you should only check this condition in doInBackground() method, otherwise there is no logical meaning

			// if the task is not cancelled by calling LoginTask.cancel(true), then make the thread wait for 10 sec and then
			//quit it self


			int isec = 4;

			if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.STOP_BLUETOOTH)
				isec = 2;

			if (getWisePadConnectionState() == WisePadConnection.WisePadConnection_NO_PAIRED_DEVICES_FOUND)
				isec = 0;

			int ictr = 0;


			//it will wait for 15 sec or till the task is cancelled by the mSwiper routines.
			while (!isCancelled() & ictr < isec) {
				try {
					Thread.sleep(500);
				} catch (Exception ex) {
				}
				ictr++;
			}

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,  "EmvOnlinePorcessTask  end doInBackground", true, true);
			return null;
		}

		@Override
		protected void onPostExecute(Void unused)
		{
			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,  "onPostExecute mEMVPPROCESSTASTTYPE " + mEMVPPROCESSTASTTYPE, true, true);

			//when the back tab is pressed
			if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.BACK_BUTTON)
			{
				Intent intent = new Intent(CreditSaleActivity.this, MenuView.class);
				startActivity(intent);
				finish();
			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.ONLINE_SUBMIT)
			{

			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.STOP_BLUETOOTH)
			{
				finish();
			}
			else if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.SHOW_SIGNATURE)
			{
				finish();
				startActivity(intent);
			}

			mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;
		}
	}

	public void showPinpad()
	{
		if(mMSWisepadDeviceController != null)
		{
			//if (mMSWisepadDeviceController.getDeviceType() == MSWisepadDeviceController.DeviceType.WISEPOS_PLUS) {
				mMSWisepadDeviceController.showPinPad(MSWisepadDeviceController.Orientation.POTRAIT);
			//}
		}
	}


}