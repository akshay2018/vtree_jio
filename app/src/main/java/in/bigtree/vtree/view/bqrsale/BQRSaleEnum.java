package in.bigtree.vtree.view.bqrsale;

public  class BQRSaleEnum {

	public enum BQRSaleScreens
	{
		BQR_SALE_GETBQRIDS,
		BQR_SALE_AMOUNT,
		BQR_SALE_GENERATE_QR,
		BQR_SALE_CHECK_STATUS,
		BQR_SELECTION_SCREEN

	}
}
