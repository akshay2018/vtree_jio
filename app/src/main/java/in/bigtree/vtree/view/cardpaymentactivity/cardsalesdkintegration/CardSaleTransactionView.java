package in.bigtree.vtree.view.cardpaymentactivity.cardsalesdkintegration;

import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mswipetech.sdk.network.MSGatewayConnectionListener;
import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.CardSaleResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.cardpaymentactivity.CreditSaleDeclineActivity;
import in.bigtree.vtree.view.cardpaymentactivity.CreditSaleSignatureActivity;


public class CardSaleTransactionView extends CreditSaleActivity {


	/* the mswipe gateway server objects */

	protected WisePadGatewayConncetionListener mWisePadGatewayConncetionListener;

	/* the store the copy of the the date */

	public CardSaleResponseData mCardSaleResponseData = null;

	/**
	 * Called when the activity is first created.
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mWisePadGatewayConncetionListener = new WisePadGatewayConncetionListener();

		MSWisepadController.getSharedMSWisepadController(CardSaleTransactionView.this,
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
				mWisePadGatewayConncetionListener);

		MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());



		mCardSaleDlgTitle = getResources().getString(R.string.card_sale);

		TextView txtHeading = ((TextView) findViewById(R.id.topbar_LBL_heading));
		txtHeading.setText(mCardSaleDlgTitle);

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "", true, true);
	}

	/**
	 * processTransactionsOnline
	 * call up the API request which further processes the secured card data collected from the device by posting the data
	 * to online network and the responses are handled through the call back functions and are appropriately presented to the user
	 */

	@Override
	public void processCardSaleOnline()
	{

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "ReferenceId " + AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(), true, true);

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "SessionToken " +  AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(), true, true);

		mProgressActivity = new CustomProgressDialog(CardSaleTransactionView.this, "Processing Card Tx...");
		mProgressActivity.show();

		MSWisepadController.getSharedMSWisepadController(this,
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
				mWisePadGatewayConncetionListener).processCardSaleOnline(
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
				removeChar(mTransactionData.mBaseAmount,','),
				removeChar(mTransactionData.mTipAmount,','),
				SharedVariable.smsCode + mTransactionData.mPhoneNo,
				mTransactionData.mReceipt,
				mTransactionData.mEmail,
				mTransactionData.mNotes,
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getTipEnabled(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().isReceiptEnabled(),
				mTransactionData.mAmexSecurityCode,
				0,
				0,
				"",
				mTransactionData.mExtraNote1,
				mTransactionData.mExtraNote2,
				mTransactionData.mExtraNote3,
				mTransactionData.mExtraNote4,
				mTransactionData.mExtraNote5,
				mTransactionData.mExtraNote6,
				mTransactionData.mExtraNote7,
				mTransactionData.mExtraNote8,
				mTransactionData.mExtraNote9,
				mTransactionData.mExtraNote10,
				new MSWisepadControllerResponseListenerObserver());
	}


	/**
	 * MSWisepadControllerResponseListenerObserver
	 * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests
	 */
	class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener {

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 *
		 * @param aMSDataStore the generic mswipe data store, this instance is refers to InvoiceTrxData or CardSaleResponseData, so this
		 *                     need be converted back to access the relavant data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{

			if(mProgressActivity != null){

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "not null " , true, true);

				mProgressActivity.dismiss();
			}


			if (aMSDataStore instanceof CardSaleResponseData) {

				mCardSaleResponseData = (CardSaleResponseData) aMSDataStore;

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "Status: " + mCardSaleResponseData.getResponseStatus() + " FailureReason: " + mCardSaleResponseData.getResponseFailureReason() , true, true);

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "ExceptoinStackTrace: " + mCardSaleResponseData.getExceptoinStackTrace(), true, true);

				if(!mCardSaleResponseData.getResponseStatus())
				{
					showSignature();
				}
				else {

					if(AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired())
					{
						playSound(100, R.raw.approved);
						showSignature();
					}else
					{
						showapproveDialog(mCardSaleResponseData.getResponseStatus().toString() , mCardSaleResponseData.getAuthCode(),
								mCardSaleResponseData.getRRNO(),mCardSaleResponseData.getCardSaleApprovedMessage());
					}
				}

			}
		}
	}

	/**
	 * @param
	 * @return
	 * @description After successful transaction, we are showing signature screen.For that we sending details to signature screen.
	 */
	public void showSignature() {

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "", true, true);

		if(mCardSaleResponseData.getResponseStatus())
		{
			intent = new Intent(CardSaleTransactionView.this, CreditSaleSignatureActivity.class);
		}
		else {
			intent = new Intent(CardSaleTransactionView.this, CreditSaleDeclineActivity.class);
		}

		intent.putExtra("Title", mCardSaleDlgTitle);
		intent.putExtra("cardSaleResponseData", mCardSaleResponseData);

		doneWithCreditSale(EMVPPROCESSTASTTYPE.SHOW_SIGNATURE);

	}

	public void showapproveDialog(String status, String authcode, String rrno, String reason)
	{

		final Dialog dialog = new Dialog(CardSaleTransactionView.this, R.style.styleCustDlg);
		dialog.setContentView(R.layout.cardsale_status_customdlg);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(true);

		TextView txtstatusmsg = (TextView) dialog.findViewById(R.id.customdlg_Txt_status);
		txtstatusmsg.setText(status);

		TextView txtauthcode = (TextView) dialog.findViewById(R.id.customdlg_Txt_authcode);
		txtauthcode.setText(authcode);

		TextView txtrrno = (TextView) dialog.findViewById(R.id.customdlg_Txt_rrno);
		txtrrno.setText(rrno);

		TextView txtreason = (TextView) dialog.findViewById(R.id.customdlg_Txt_reason);
		txtreason.setText(reason);

		Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
		yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				finish();
				Intent intent = new Intent(CardSaleTransactionView.this, MenuView.class);
				startActivity(intent);
			}
		});

		dialog.show();
	}

	class WisePadGatewayConncetionListener implements MSGatewayConnectionListener {

		@Override
		public void Connected(String msg) {

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, " msg " + msg, true, true);

			imgHostConnectionStatus.setAnimation(null);
			imgHostConnectionStatus.setImageResource(R.drawable.topbar_img_host_active);
		}

		@Override
		public void Connecting(String msg) {


			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, " msg " + msg, true, true);

			imgHostConnectionStatus.startAnimation(alphaAnim);
			imgHostConnectionStatus.setImageResource(R.drawable.topbar_img_host_inactive);
		}

		@Override
		public void disConnect(String msg) {

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, " msg " + msg, true, true);

			imgHostConnectionStatus.setAnimation(null);
			imgHostConnectionStatus.setImageResource(R.drawable.topbar_img_host_inactive);
		}
	}


	private void playSound(long delay, int soundfile) {

		new playBeepTask(soundfile).execute(delay);
		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "", true, true);
	}

	private class playBeepTask extends AsyncTask<Long, Void, Void> {

		int soundfile;

		playBeepTask(int soundfile) {
			this.soundfile = soundfile;
		}

		@Override
		protected Void doInBackground(Long... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(params[0]);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			MediaPlayer mp;
			mp = MediaPlayer.create(CardSaleTransactionView.this, soundfile);
			mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					// TODO Auto-generated method stub
					mp.reset();
					mp.release();
					mp = null;
				}
			});
			mp.start();
			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, "", true, true);
		}

	}

	/**
	 * @description
	 *        We are removing specific character form original string.
	 * @param s original string
	 * @param c specific character.
	 * @return
	 */
	public String removeChar(String s, char c) {

		String r = "";

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c)
				r += s.charAt(i);
		}

		return r;
	}

}