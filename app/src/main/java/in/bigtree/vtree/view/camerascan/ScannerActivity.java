package in.bigtree.vtree.view.camerascan;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.util.Logs;


/**
 * Created by mswipe on 18-Apr-17.
 */

public class ScannerActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scannerlayout);

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setPrompt("Scan a barcode or QRcode");
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(true);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
        integrator.initiateScan();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (SharedVariable.IS_DEBUGGING_ON)
            Logs.v(SharedVariable.packName, "onActivityResult Scanner Activity", true, true);

        if (result != null) {
            if (result.getContents() == null) {

                finish();

            } else {

              if (SharedVariable.IS_DEBUGGING_ON)
                    Logs.v(SharedVariable.packName, "Result "+result.getContents(), true, true);

                String resData = result.getContents();
                data.setData(Uri.parse(resData));
                setResult(RESULT_OK, data);
                finish();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
