package in.bigtree.vtree.view.history;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.CashHistoryDetailsResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.data.HistoryDetails;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.BaseTitleActivity;

public class CashHistoryList extends BaseTitleActivity {
    public final static String log_tab = "CashHistoryList=>";

    CustomProgressDialog mProgressActivity = null;
    ListView lstHistory = null;
    TextView lblRefresh = null;
    LinearLayout mLnrCashHistoryContent = null;

    Button mBtnRefresh = null;
    Button mBtnRefreshDown = null;

    LinearLayout lnrLayoutList = null;
    ArrayList<Object> listData = new ArrayList<Object>();
    int totalRecord = 0;
    int ihistoryrecords = 0;

    String randomKey = "";
    SharedVariable applicationData = null;

    // this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
    private long lastRequestTime = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.cardhistorylist);
        applicationData = (SharedVariable) getApplicationContext();
        
        initViews();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void initViews() {
    	
        ((TextView) findViewById(R.id.topbar_LBL_heading)).setText("History");
        ((TextView) findViewById(R.id.topbar_LBL_heading)).setTypeface(applicationData.font);
        
        mLnrCashHistoryContent = (LinearLayout) findViewById(R.id.cardhistory_LNR_cardhistorycontent);

        lblRefresh = (TextView) findViewById(R.id.cardhistory_LBL_refresh);
        lblRefresh.setTypeface(applicationData.font);


        lstHistory = (ListView) findViewById(R.id.cardhistory_LST_cardhistory);
        lstHistory.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(CashHistoryList.this, CashHistory.class);
                intent.putExtra("curRecord", position + 1);
                intent.putExtra("totalRecord", listData.size());
                intent.putExtra("listData", listData);  
                startActivity(intent);


            }
        });


        mBtnRefresh = (Button) findViewById(R.id.cardhistorylist_BTN_refresh);
        mBtnRefresh.setTypeface(applicationData.font);
        mBtnRefresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = Constants.showDialog(CashHistoryList.this, Constants.CARDHISTORYLIST_DIALOG_MSG,
                        Constants.CARDHISTORYLIST_GET_HISTROYDATA, Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_CONFIRMATION);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setTypeface(applicationData.font);
                yes.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        dialog.dismiss();

                        if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

                            getCashHistory();
                        }

                        lastRequestTime = System.currentTimeMillis();


                    }
                });

                Button no = (Button) dialog.findViewById(R.id.customdlg_BTN_no);
                no.setTypeface(applicationData.font);
                no.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });
                dialog.show();


            }
        });

        mBtnRefreshDown = (Button) findViewById(R.id.cardhistorylist_BTN_refreshdown);
        mBtnRefreshDown.setTypeface(applicationData.font);
        mBtnRefreshDown.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = Constants.showDialog(CashHistoryList.this, Constants.CARDHISTORYLIST_DIALOG_MSG,
                        Constants.CARDHISTORYLIST_GET_HISTROYDATA, Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_CONFIRMATION);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setTypeface(applicationData.font);
                yes.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        dialog.dismiss();

                        if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

                            getCashHistory();
                        }

                        lastRequestTime = System.currentTimeMillis();


                    }
                });

                Button no = (Button) dialog.findViewById(R.id.customdlg_BTN_no);
                no.setTypeface(applicationData.font);
                no.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });
                dialog.show();


            }
        });

        lnrLayoutList = (LinearLayout) findViewById(R.id.cardhistory_LNR_historylist);

    }

    public void ShowHistoryData(String xmlHistoryData) {

    	ihistoryrecords = 0;
        totalRecord = xmlHistoryData.length();

        mBtnRefreshDown.setVisibility(View.VISIBLE);
        
        if (totalRecord == 0) {
        	
            lblRefresh.setVisibility(View.VISIBLE);
            mBtnRefresh.setVisibility(View.VISIBLE);
            lnrLayoutList.setVisibility(View.INVISIBLE);

        } else {
        	
            lblRefresh.setVisibility(View.GONE);
            mBtnRefresh.setVisibility(View.GONE);
            lnrLayoutList.setVisibility(View.VISIBLE);
            listData.clear();
            String errMsg= "";
            
            try{
             errMsg= getHistoryListFromXml(xmlHistoryData);
            }catch(Exception ex){ errMsg="Error";}
            
            
            if (errMsg.length() == 0 && listData.size()>0) {
                lstHistory.setAdapter(new CardHistoryAdapter(this, listData));
            } else {
                final Dialog dialog = Constants.showDialog(CashHistoryList.this, Constants.CARDHISTORYLIST_DIALOG_MSG, "Unable to show the history data, please contact support.", Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                 Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                 yes.setOnClickListener(new OnClickListener() {

                     public void onClick(View v) {
                         dialog.dismiss();

                         finish();

                     }
                 });
                 dialog.show();
            }
        }
    }

    public String getHistoryListFromXml(String xmlHistoryData) throws Exception
    {
    	XmlPullParser parser = Xml.newPullParser();
        String errMsg = "";
        boolean isErrElementExists = false;
        HashMap<String, String> hashMember = new HashMap<String, String>();
        
        String strDataTags[] = new String[]{
        		"TrxDate", 
        		"TrxAmount", 
        		"TrxType", 
        		"MerchantInvoice", 
        		"VoucherNo"};
        
        HistoryDetails historyData = null;
        try {
           parser.setInput(new StringReader(xmlHistoryData));
           int eventType = XmlPullParser.START_TAG;
           boolean leave = false;
           boolean isDocElementExists = false;

           boolean isRowTagFound = false;
           
           int iDataTagIndex = 0;

           while (!leave && eventType != XmlPullParser.END_DOCUMENT) {
               eventType = parser.next();
               switch (eventType) {
                   case XmlPullParser.START_TAG: {
                       if ("ResultElement".equalsIgnoreCase(parser.getName().trim())) {
                           //String xmlText = parser.Text(); // there is no text for this
                           hashMember.put("ResultElement", "");// store the key
                           isDocElementExists = true;
                       
                       } else if ("data".equalsIgnoreCase(parser.getName().trim())) {
                           isRowTagFound = true;
                           historyData = new HistoryDetails();
                           
                       } else if (isDocElementExists && isRowTagFound && !isErrElementExists) {
                           String xmlTag = parser.getName().toString();
                           if (iDataTagIndex < strDataTags.length && strDataTags[iDataTagIndex].equals(xmlTag) ) {
                               
                               eventType = parser.next();
                               String xmlText = "";
                               if (eventType == XmlPullParser.TEXT) {
                                   xmlText = ((parser.getText() == null)? "":parser.getText());
                               } else if (eventType == XmlPullParser.END_TAG) {

                               }
                               
                               if(iDataTagIndex ==0) 	historyData.TrxDate =xmlText;
                               else if(iDataTagIndex ==1) historyData.TrxAmount=xmlText;
                               else if(iDataTagIndex ==2) historyData.TrxType=xmlText;
                               else if(iDataTagIndex ==3) historyData.MerchantInvoice=xmlText;
                               else if(iDataTagIndex ==4) historyData.VoucherNo=xmlText;
                               iDataTagIndex++;
                               
                               if (SharedVariable.IS_DEBUGGING_ON) {
                                   Logs.v(SharedVariable.packName, log_tab + " The data is  " + xmlText, true, true);
                               }
                           }
                       }
                       break;
                   }
                   case XmlPullParser.END_TAG: {
                       if (!isErrElementExists && isRowTagFound && "data".equalsIgnoreCase(parser.getName().trim())) {
                           isRowTagFound = false;
                           ihistoryrecords++;

                           if (SharedVariable.IS_DEBUGGING_ON) {
                               Logs.v(SharedVariable.packName, log_tab + " The rows " + ihistoryrecords + " data is " + historyData.StanNo, true, true);
                           }
                           if (ihistoryrecords == 1) {
                                
                           }
                           try{
                            //saveValueToHistoryDB(Constants.compressRespData(stHistoryRowData,context), ihistoryrecords);
                           	listData.add(historyData);
                           }
                           catch(Exception ex)              {
                               ex.printStackTrace();
                               errMsg = "Unable to get the History data, please contact support.";
                               leave =true;
                           }
                           //stHistoryRowData = "";
                           iDataTagIndex = 0;
                       }
                       break;

                   }
               }
           }

       } catch (IOException e) {
           e.printStackTrace();
           throw e;

       } catch (Exception e) {
           e.printStackTrace();
           throw e;
       } finally {
           if (parser != null) {
               parser = null;
           }
       }
        
       return errMsg;
    }


    public void getCashHistory() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(CashHistoryList.this,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
                            null);

            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

            wisepadController.getCashHistory(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    new MSWisepadControllerResponseListenerObserver());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(CashHistoryList.this, "Fetching History...");
            mProgressActivity.show();

        }
        catch (Exception e)
        {
            Constants.showDialog(this, "History", "unable to process cash history.");
            e.printStackTrace();
        }

    }



    /**
     * MswipeWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

     */
    class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {

            CashHistoryDetailsResponseData cashHistoryDetailsResponseData = (CashHistoryDetailsResponseData) aMSDataStore;
            mProgressActivity.dismiss();

            if (cashHistoryDetailsResponseData.getResponseStatus())
            {
                mLnrCashHistoryContent.setVisibility(View.VISIBLE);
                ShowHistoryData(cashHistoryDetailsResponseData.getCashHistoryResultData());
            }
            else {

                Constants.showDialog(CashHistoryList.this, "History", cashHistoryDetailsResponseData.getResponseFailureReason());
            }
        }
    }

    public class CardHistoryAdapter extends BaseAdapter {
    	ArrayList<Object> listData = null;
        Context context;

        public CardHistoryAdapter(Context context, ArrayList<Object> listData) {
            this.listData = listData;
            this.context = context;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.cardhistorylistdata, null);
            }
            HistoryDetails cardSaleDetails = (HistoryDetails) listData.get(position);

              //TextView lstFourDigits= (TextView)convertView.findViewById(R.id.cardhistorylist_LBL_lastfourdigits);
            //lstFourDigits.setText(cardSaleDetails.CardFourDigits);

            TextView lblamt = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_lblamount);
            lblamt.setText(SharedVariable.Currency_Code);
            lblamt.setTypeface(applicationData.font);


            TextView amt = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_amount);
            amt.setText(cardSaleDetails.TrxAmount.toString());
            amt.setTypeface(applicationData.font);


            TextView date = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_date);
            date.setText(cardSaleDetails.TrxDate);
            date.setTypeface(applicationData.font);


            TextView type = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_cardtype);
            type.setText(cardSaleDetails.TrxType);
            type.setTypeface(applicationData.font);

            if (cardSaleDetails.TrxType.toString().equalsIgnoreCase("card sale")) {
                type.setTextColor(Color.rgb(0, 176, 80));

            } else {
                type.setTextColor(Color.rgb(255, 0, 0));
            }
            
            return convertView;
        }

    }
}
