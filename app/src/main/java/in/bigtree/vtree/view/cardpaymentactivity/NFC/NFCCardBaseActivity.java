package in.bigtree.vtree.view.cardpaymentactivity.NFC;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.device.MSWisepadDeviceControllerResponseListener;

import java.util.ArrayList;
import java.util.Hashtable;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.util.Utils;
import in.bigtree.vtree.view.cardpaymentactivity.MSWisepadView;

public class NFCCardBaseActivity extends MSWisepadView
{


	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState
	 */
	ArrayList<Object> mArrCardTypes = new ArrayList<>();
	int miSelectedCardType = 0;

	Button mBtnCardType = null;
	Dialog mDlgNFC = null;

	public enum NFCCardType{
		MIFARE_CLASSIC,
		MIFARE_ULTRA_LIGHT ,
		MIFARE_DESFIRE,
		NTAG21X}

	public NFCCardType mENUMNFCCardType = NFCCardType.MIFARE_CLASSIC;

	Button mBtnReadFirst = null;
	Button mBtnReadNext = null;
	Button mBtnWrite = null;
	Button mBtnAuthenticate = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mCallCheckCardAfterConnection = false;

		mArrCardTypes.add("MIFARE Classic 1K/4K");
		mArrCardTypes.add("MIFARE UltraLight");
		mArrCardTypes.add("DESFire");
		mArrCardTypes.add("NTag21x (Type A)");
	}

	public void showNFCCardTypeOptionsDialog()
	{

		final Dialog dialog = new Dialog(this, R.style.styleCustDlg);
		dialog.setContentView(R.layout.nfccardtype);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(true);
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		ListView listView = (ListView)dialog.findViewById(R.id.nfccardtype_list_selection);

		listView.setAdapter(new BaseAdapter() {
			@Override
			public int getCount() {
				return mArrCardTypes.size();
			}

			@Override
			public Object getItem(int i) {
				return null;
			}

			@Override
			public long getItemId(int i) {
				return 0;
			}

			@Override
			public View getView(int i, View convertView, ViewGroup viewGroup)
			{
				if (convertView == null) {
					LayoutInflater inflater = LayoutInflater.from(NFCCardBaseActivity.this);
					convertView = inflater.inflate(R.layout.nfccardtype_list_row, null);
				}

				TextView txt = (TextView) convertView.findViewById(R.id.nfccardtype_text);
				txt.setText((String) mArrCardTypes.get(i));


				return convertView;

			}
		});

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
			{

				miSelectedCardType = position;
				mENUMNFCCardType = NFCCardType.values()[position];
				mBtnCardType.setText( (String) mArrCardTypes.get(position));

				if(mENUMNFCCardType == NFCCardType.NTAG21X)
				{
					mBtnReadNext.setEnabled(true);
					mBtnReadNext.setTextColor(Color.WHITE);

				}
				else{
					mBtnReadNext.setEnabled(false);
					mBtnReadNext.setTextColor(Color.GRAY);

				}
				dialog.dismiss();
			}
		});
		dialog.show();

	}


	public void promptForStartNfcDetection()
	{
		//mDlgNFC.dismiss();;

		mDlgNFC = new Dialog(this, R.style.styleCustDlg);
		mDlgNFC.setContentView(R.layout.nfccard_string_input_dialog);
		mDlgNFC.setTitle("Start NFC detection");
		mDlgNFC.setCanceledOnTouchOutside(false);

		((TextView)(mDlgNFC.findViewById(R.id.nfccard_txt_title))).setText("Start NFC detection");

		((TextView)(mDlgNFC.findViewById(R.id.general1TextView))).setText("NFC card detection timeout");
		((TextView)(mDlgNFC.findViewById(R.id.general1TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general1EditText))).setText("15");
		((EditText) (mDlgNFC.findViewById(R.id.general1EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).getText().length());


		((TextView)(mDlgNFC.findViewById(R.id.general2TextView))).setText("NFC Operation Mode(0 read, 1 read and write");
		((TextView)(mDlgNFC.findViewById(R.id.general2TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general2EditText))).setText("1");
		((EditText) (mDlgNFC.findViewById(R.id.general2EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).getText().length());


		((TextView)(mDlgNFC.findViewById(R.id.general3TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general3EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general4TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general4EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general5TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general5EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general6EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general7TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general7EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general8TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general8EditText))).setVisibility(View.GONE);

		((LinearLayout)(mDlgNFC.findViewById(R.id.general1LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general2LinearLayout))).setOrientation(LinearLayout.VERTICAL);

		mDlgNFC.findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Hashtable<String, Object> data = new Hashtable<String, Object>();
				String nfcCardDetectionTimeout = ((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).getText().toString();
				if ((nfcCardDetectionTimeout != null) && (!nfcCardDetectionTimeout.equalsIgnoreCase(""))) {
					data.put("nfcCardDetectionTimeout", nfcCardDetectionTimeout);
				}
				String nfcOperationMode = ((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).getText().toString();
				if ((nfcOperationMode != null) && (!nfcOperationMode.equalsIgnoreCase(""))) {
					data.put("nfcOperationMode", nfcOperationMode);
				}

				startNfcDetection(data);
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v) {
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.show();
	}

	public void promptForStopNfcDetection()
	{
		//mDlgNFC.dismiss();;

		mDlgNFC = new Dialog(this, R.style.styleCustDlg);
		mDlgNFC.setContentView(R.layout.nfccard_string_input_dialog);
		mDlgNFC.setTitle("Stop NFC detection");
		mDlgNFC.setCanceledOnTouchOutside(false);

		((TextView)(mDlgNFC.findViewById(R.id.nfccard_txt_title))).setText("Stop NFC detection");

		((TextView)(mDlgNFC.findViewById(R.id.general1TextView))).setText("NFC card detection timeout");
		((TextView)(mDlgNFC.findViewById(R.id.general1TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general1EditText))).setText("15");
		((EditText) (mDlgNFC.findViewById(R.id.general1EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).getText().length());

		((TextView)(mDlgNFC.findViewById(R.id.general2TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general2EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general3TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general3EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general4TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general4EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general5TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general5EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general6EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general7TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general7EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general8TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general8EditText))).setVisibility(View.GONE);

		((LinearLayout)(mDlgNFC.findViewById(R.id.general1LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general2LinearLayout))).setOrientation(LinearLayout.VERTICAL);

		mDlgNFC.findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Hashtable<String, Object> data = new Hashtable<String, Object>();
				String nfcCardDetectionTimeout = ((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).getText().toString();
				if ((nfcCardDetectionTimeout != null) && (!nfcCardDetectionTimeout.equalsIgnoreCase(""))) {
					data.put("nfcCardRemovalTimeout", nfcCardDetectionTimeout);
				}

				stopNfcDetection(data);
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v) {
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.show();
	}

	public void promptForNfcDataAuthenticate()
	{
		mDlgNFC = new Dialog(this, R.style.styleCustDlg);
		mDlgNFC.setContentView(R.layout.nfccard_string_input_dialog);
		mDlgNFC.setTitle("NFC data exchange read1st");
		mDlgNFC.setCanceledOnTouchOutside(false);

		((TextView)(mDlgNFC.findViewById(R.id.nfccard_txt_title))).setText("NFC data exchange read1st");

		/*data to be written is hidden*/
		((TextView) (mDlgNFC.findViewById(R.id.general1TextView))).setVisibility(View.GONE);
		((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).setVisibility(View.GONE);


		((TextView)(mDlgNFC.findViewById(R.id.general2TextView))).setText("Mifare Card Auth Type");
		((TextView)(mDlgNFC.findViewById(R.id.general2TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general2EditText))).setText("");

		if(mENUMNFCCardType != NFCCardType.NTAG21X
			&& mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT
			&& mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC
			&& mENUMNFCCardType != NFCCardType.MIFARE_DESFIRE) {

			((TextView) (mDlgNFC.findViewById(R.id.general2TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general2TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general3TextView))).setText("Mifare Card Key(new card, FFFFFFFFFFFF)");
		((TextView)(mDlgNFC.findViewById(R.id.general3TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general3EditText))).setText("FFFFFFFFFFFF");
		((EditText) (mDlgNFC.findViewById(R.id.general3EditText)))
			.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.NTAG21X && mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT) {
			((TextView) (mDlgNFC.findViewById(R.id.general3TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general3TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general4TextView))).setText("Mifare Card Key Number(new card, 00)");
		((TextView)(mDlgNFC.findViewById(R.id.general4TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general4EditText))).setText("00");
		((EditText) (mDlgNFC.findViewById(R.id.general4EditText)))
			.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.NTAG21X && mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT) {
			((TextView) (mDlgNFC.findViewById(R.id.general4TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general4TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general5TextView))).setText("Mifare Card Block Number(new card, 02)");
		((TextView)(mDlgNFC.findViewById(R.id.general5TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general5EditText))).setText("02");
		((EditText) (mDlgNFC.findViewById(R.id.general5EditText)))
			.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.NTAG21X) {
			((TextView) (mDlgNFC.findViewById(R.id.general5TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general5TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).setVisibility(View.GONE);

		}


		if(mENUMNFCCardType == NFCCardType.MIFARE_CLASSIC){

			((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).setText("Read Data Length");
			((EditText)(mDlgNFC.findViewById(R.id.general6EditText))).setText("00");

		}
		else{
			((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).setText("Mifare Desfire Card AID(new card, 000001)");
			((EditText)(mDlgNFC.findViewById(R.id.general6EditText))).setText("000001");

		}

		((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).getLayoutParams().width = 500;
		((EditText) (mDlgNFC.findViewById(R.id.general6EditText)))
			.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT
			&& mENUMNFCCardType != NFCCardType.NTAG21X
			&& mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general6TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general6TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general7TextView))).setText("Mifare Desfire Card FID(new card, 00)");
		((TextView)(mDlgNFC.findViewById(R.id.general7TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general7EditText))).setText("00");
		((EditText) (mDlgNFC.findViewById(R.id.general7EditText)))
			.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).getText().length());

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general7TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general7TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general8TextView))).setText("Mifare Desfire Card File Offset(new card, 000000)");
		((TextView)(mDlgNFC.findViewById(R.id.general8TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general8EditText))).setText("000000");
		((EditText) (mDlgNFC.findViewById(R.id.general8EditText)))
			.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).getText().length());

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general8TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general8TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general9TextView))).setText("Mifare Desfire Card Read Data Length(new card, 01)");
		((TextView)(mDlgNFC.findViewById(R.id.general9TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general9EditText))).setText("01");
		((EditText) (mDlgNFC.findViewById(R.id.general9EditText)))
			.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).getText().length());

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general9TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general9TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general10TextView))).setText("Mifare Desfire Card Special Operation Command");
		((TextView)(mDlgNFC.findViewById(R.id.general10TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general10EditText))).setText("");

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general10TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general10EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general10TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general10EditText))).setVisibility(View.GONE);
		}

		((TextView)(mDlgNFC.findViewById(R.id.general11TextView))).setText("Mifare Desfire Card Special Operation Parameter");
		((TextView)(mDlgNFC.findViewById(R.id.general11TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general11EditText))).setText("");

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general11TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general11EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general11TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general11EditText))).setVisibility(View.GONE);
		}


		((LinearLayout)(mDlgNFC.findViewById(R.id.general1LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general2LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general3LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general4LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general5LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general6LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general7LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general8LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general9LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general10LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general11LinearLayout))).setOrientation(LinearLayout.VERTICAL);


		mDlgNFC.findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				Hashtable<String, Object> data = new Hashtable<String, Object>();
				//data.put("readNdefRecord", MSWisepadDeviceControllerResponseListener.ReadNdefRecord.READ_1ST);

				String mifareCardAuthType = ((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).getText().toString();


				if(mENUMNFCCardType == NFCCardType.MIFARE_ULTRA_LIGHT)
				{
					String mifareCardKey = ((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().toString();
					if ((mifareCardKey != null) && (!mifareCardKey.equalsIgnoreCase(""))) {
						data.put("mifareCardKey", mifareCardKey);
					}

					String mifareCardKeyNumber = ((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().toString();
					if ((mifareCardKeyNumber != null) && (!mifareCardKeyNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardKeyNumber", mifareCardKeyNumber);
					}

					String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();
					if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {

						data.put("mifareCardBlockNumber", mifareCardBlockNumber);
					}

				}
				else if(mENUMNFCCardType == NFCCardType.MIFARE_CLASSIC)
				{

					String mifareCardKey = ((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().toString();

					if ((mifareCardKey != null) && (!mifareCardKey.equalsIgnoreCase(""))) {
						data.put("mifareCardKey", mifareCardKey);
					}

					String mifareCardKeyNumber = ((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().toString();

					if ((mifareCardKeyNumber != null) && (!mifareCardKeyNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardKeyNumber", mifareCardKeyNumber);
					}

					String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();

					if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardBlockNumber", mifareCardBlockNumber);
					}

					data.put("mifareDesFireCardCommand", "FD");


				}
				else if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE)
				{
					String mifareCardKey = ((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().toString();
					if ((mifareCardKey != null) && (!mifareCardKey.equalsIgnoreCase(""))) {
						data.put("mifareCardKey", mifareCardKey);
					}

					String mifareCardKeyNumber = ((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().toString();
					if ((mifareCardKeyNumber != null) && (!mifareCardKeyNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardKeyNumber", mifareCardKeyNumber);
					}

					String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();
					if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardBlockNumber", mifareCardBlockNumber);
					}

				}



				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "data " + data, true, true);

				nfcDataExchange(data);
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v) {
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.show();
	}


	public void promptForNfcDataExchangeRead1St()
	{
		mDlgNFC = new Dialog(this, R.style.styleCustDlg);
		mDlgNFC.setContentView(R.layout.nfccard_string_input_dialog);
		mDlgNFC.setTitle("NFC data exchange read1st");
		mDlgNFC.setCanceledOnTouchOutside(false);

		((TextView)(mDlgNFC.findViewById(R.id.nfccard_txt_title))).setText("NFC data exchange read1st");

		/*data to be written is hidden*/
		((TextView) (mDlgNFC.findViewById(R.id.general1TextView))).setVisibility(View.GONE);
		((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).setVisibility(View.GONE);


		((TextView)(mDlgNFC.findViewById(R.id.general2TextView))).setText("Mifare Card Auth Type");
		((TextView)(mDlgNFC.findViewById(R.id.general2TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general2EditText))).setText("");

		if(mENUMNFCCardType != NFCCardType.NTAG21X && mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT
				&& mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC && mENUMNFCCardType != NFCCardType.MIFARE_DESFIRE) {

			((TextView) (mDlgNFC.findViewById(R.id.general2TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general2TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general3TextView))).setText("Mifare Card Key(new card, FFFFFFFFFFFF)");
		((TextView)(mDlgNFC.findViewById(R.id.general3TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general3EditText))).setText("FFFFFFFFFFFF");
		((EditText) (mDlgNFC.findViewById(R.id.general3EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.NTAG21X
			&& mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT
			&& mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general3TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general3TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general4TextView))).setText("Mifare Card Key Number(new card, 00)");
		((TextView)(mDlgNFC.findViewById(R.id.general4TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general4EditText))).setText("00");
		((EditText) (mDlgNFC.findViewById(R.id.general4EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.NTAG21X
			&& mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT
			&& mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general4TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general4TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general5TextView))).setText("Mifare Card Block Number(new card, 02)");
		((TextView)(mDlgNFC.findViewById(R.id.general5TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general5EditText))).setText("02");
		((EditText) (mDlgNFC.findViewById(R.id.general5EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.NTAG21X) {
			((TextView) (mDlgNFC.findViewById(R.id.general5TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general5TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).setVisibility(View.GONE);

		}


		if(mENUMNFCCardType == NFCCardType.MIFARE_CLASSIC){

			((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).setText("Read Data Length");
			((EditText)(mDlgNFC.findViewById(R.id.general6EditText))).setText("00");

		}
		else{
			((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).setText("Mifare Desfire Card AID(new card, 000001)");
			((EditText)(mDlgNFC.findViewById(R.id.general6EditText))).setText("000001");

		}

		((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).getLayoutParams().width = 500;
		((EditText) (mDlgNFC.findViewById(R.id.general6EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT && mENUMNFCCardType != NFCCardType.NTAG21X) {
			((TextView) (mDlgNFC.findViewById(R.id.general6TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general6TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general7TextView))).setText("Mifare Desfire Card FID(new card, 00)");
		((TextView)(mDlgNFC.findViewById(R.id.general7TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general7EditText))).setText("00");
		((EditText) (mDlgNFC.findViewById(R.id.general7EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).getText().length());

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general7TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general7TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general8TextView))).setText("Mifare Desfire Card File Offset(new card, 000000)");
		((TextView)(mDlgNFC.findViewById(R.id.general8TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general8EditText))).setText("000000");
		((EditText) (mDlgNFC.findViewById(R.id.general8EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).getText().length());

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general8TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general8TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general9TextView))).setText("Mifare Desfire Card Read Data Length(new card, 01)");
		((TextView)(mDlgNFC.findViewById(R.id.general9TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general9EditText))).setText("01");
		((EditText) (mDlgNFC.findViewById(R.id.general9EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).getText().length());

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general9TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general9TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general10TextView))).setText("Mifare Desfire Card Special Operation Command");
		((TextView)(mDlgNFC.findViewById(R.id.general10TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general10EditText))).setText("");

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general10TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general10EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general10TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general10EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general11TextView))).setText("Mifare Desfire Card Special Operation Parameter");
		((TextView)(mDlgNFC.findViewById(R.id.general11TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general11EditText))).setText("");

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general11TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general11EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general11TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general11EditText))).setVisibility(View.GONE);

		}


		((LinearLayout)(mDlgNFC.findViewById(R.id.general1LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general2LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general3LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general4LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general5LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general6LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general7LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general8LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general9LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general10LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general11LinearLayout))).setOrientation(LinearLayout.VERTICAL);



		mDlgNFC.findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				Hashtable<String, Object> data = new Hashtable<String, Object>();
				data.put("readNdefRecord", MSWisepadDeviceControllerResponseListener.ReadNdefRecord.READ_1ST);

				String mifareCardAuthType = ((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).getText().toString();


				if(mENUMNFCCardType == NFCCardType.MIFARE_ULTRA_LIGHT)
				{
					String mifareCardKey = ((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().toString();
					if ((mifareCardKey != null) && (!mifareCardKey.equalsIgnoreCase(""))) {
						data.put("mifareCardKey", mifareCardKey);
					}

					String mifareCardKeyNumber = ((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().toString();
					if ((mifareCardKeyNumber != null) && (!mifareCardKeyNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardKeyNumber", mifareCardKeyNumber);
					}

					String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();
					if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {

						data.put("mifareCardBlockNumber", mifareCardBlockNumber);
					}

				}
				else if(mENUMNFCCardType == NFCCardType.MIFARE_CLASSIC)
				{

					String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();

					if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardBlockNumber", mifareCardBlockNumber);
					}

					String mifareCardReadLength = ((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).getText().toString();

					if ((mifareCardReadLength != null) && (!mifareCardReadLength.equalsIgnoreCase(""))) {
						data.put("mifareDesFireCardDataLength", mifareCardReadLength);
					}

				}
				else if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE)
				{
					String mifareCardKey = ((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().toString();
					if ((mifareCardKey != null) && (!mifareCardKey.equalsIgnoreCase(""))) {
						data.put("mifareCardKey", mifareCardKey);
					}

					String mifareCardKeyNumber = ((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().toString();
					if ((mifareCardKeyNumber != null) && (!mifareCardKeyNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardKeyNumber", mifareCardKeyNumber);
					}

					String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();
					if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardBlockNumber", mifareCardBlockNumber);
					}

				}



				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "data " + data, true, true);

				nfcDataExchange(data);
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v) {
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.show();
	}

	public void promptForNfcDataExchangeReadNext()
	{
		//mDlgNFC.dismiss();;

		mDlgNFC = new Dialog(this, R.style.styleCustDlg);
		mDlgNFC.setContentView(R.layout.nfccard_string_input_dialog);
		mDlgNFC.setTitle("NFC data exchange read next");
		mDlgNFC.setCanceledOnTouchOutside(false);

		((TextView)(mDlgNFC.findViewById(R.id.nfccard_txt_title))).setText("NFC data exchange read next");

		if(mENUMNFCCardType != NFCCardType.NTAG21X) {
			((TextView) (mDlgNFC.findViewById(R.id.general1TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).setVisibility(View.GONE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general1TextView))).setVisibility(View.INVISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).setVisibility(View.INVISIBLE);

		}
		((TextView)(mDlgNFC.findViewById(R.id.general2TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general2EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general3TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general3EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general4TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general4EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general5TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general5EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general6EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general7TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general7EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general8TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general8EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general9TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general9EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general10TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general10EditText))).setVisibility(View.GONE);

		((TextView)(mDlgNFC.findViewById(R.id.general11TextView))).setVisibility(View.GONE);
		((EditText)(mDlgNFC.findViewById(R.id.general11EditText))).setVisibility(View.GONE);

		((LinearLayout)(mDlgNFC.findViewById(R.id.general1LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general2LinearLayout))).setOrientation(LinearLayout.VERTICAL);


		mDlgNFC.findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				Hashtable<String, Object> data = new Hashtable<String, Object>();
				data.put("readNdefRecord", MSWisepadDeviceControllerResponseListener.ReadNdefRecord.READ_NEXT);


				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "data " + data, true, true);


				nfcDataExchange(data);
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v) {
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.show();
	}

	public void promptForNfcDataExchangeWrite()
	{
		mDlgNFC = new Dialog(this, R.style.styleCustDlg);
		mDlgNFC.setContentView(R.layout.nfccard_string_input_dialog);
		mDlgNFC.setTitle("NFC data exchange write");
		mDlgNFC.setCanceledOnTouchOutside(false);

		((TextView)(mDlgNFC.findViewById(R.id.nfccard_txt_title))).setText("NFC data exchange write");

		((TextView) (mDlgNFC.findViewById(R.id.general1TextView))).setVisibility(View.VISIBLE);
		((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).setVisibility(View.VISIBLE);
		((TextView) (mDlgNFC.findViewById(R.id.general1TextView))).setText("Write NDEF Record");
		((TextView) (mDlgNFC.findViewById(R.id.general1TextView))).getLayoutParams().width = 500;
		((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).setText("");
		((EditText) (mDlgNFC.findViewById(R.id.general1EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).getText().length());

		((TextView)(mDlgNFC.findViewById(R.id.general2TextView))).setText("Mifare Card Auth Type");
		((TextView)(mDlgNFC.findViewById(R.id.general2TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general2EditText))).setText("0");
		((EditText) (mDlgNFC.findViewById(R.id.general2EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.NTAG21X && mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT
				&& mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general2TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general2TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).setVisibility(View.GONE);
		}

		((TextView)(mDlgNFC.findViewById(R.id.general3TextView))).setText("Mifare Card Key(new card, FFFFFFFFFFFF)");
		((TextView)(mDlgNFC.findViewById(R.id.general3TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general3EditText))).setText("FFFFFFFFFFFF");
		((EditText) (mDlgNFC.findViewById(R.id.general3EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.NTAG21X
			&& mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT
			&& mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC ) {
			((TextView) (mDlgNFC.findViewById(R.id.general3TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general3TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).setVisibility(View.GONE);
		}


		((TextView)(mDlgNFC.findViewById(R.id.general4TextView))).setText("Mifare Card Key Number(new card, 00)");
		((TextView)(mDlgNFC.findViewById(R.id.general4TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general4EditText))).setText("00");
		((EditText) (mDlgNFC.findViewById(R.id.general4EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.NTAG21X
			&& mENUMNFCCardType != NFCCardType.MIFARE_ULTRA_LIGHT
			&& mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC )
		{
			((TextView) (mDlgNFC.findViewById(R.id.general4TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general4TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).setVisibility(View.GONE);
		}

		((TextView)(mDlgNFC.findViewById(R.id.general5TextView))).setText("Mifare Card Block Number(new card, 02)");
		((TextView)(mDlgNFC.findViewById(R.id.general5TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general5EditText))).setText("02");
		((EditText) (mDlgNFC.findViewById(R.id.general5EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().length());

		if(mENUMNFCCardType != NFCCardType.NTAG21X) {
			((TextView) (mDlgNFC.findViewById(R.id.general5TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general5TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).setVisibility(View.GONE);
		}

		((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).setText("Mifare Desfire Card AID(new card, 000001)");
		((TextView)(mDlgNFC.findViewById(R.id.general6TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general6EditText))).setText("000001");
		((EditText) (mDlgNFC.findViewById(R.id.general6EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).getText().length());

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE && mENUMNFCCardType != NFCCardType.MIFARE_CLASSIC) {
			((TextView) (mDlgNFC.findViewById(R.id.general6TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general6TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general7TextView))).setText("Mifare Desfire Card FID(new card, 00)");
		((TextView)(mDlgNFC.findViewById(R.id.general7TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general7EditText))).setText("00");
		((EditText) (mDlgNFC.findViewById(R.id.general7EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).getText().length());

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE) {
			((TextView) (mDlgNFC.findViewById(R.id.general7TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general7TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general8TextView))).setText("Mifare Desfire Card File Offset(new card, 000000)");
		((TextView)(mDlgNFC.findViewById(R.id.general8TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general8EditText))).setText("000000");
		((EditText) (mDlgNFC.findViewById(R.id.general8EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).getText().length());

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE) {
			((TextView) (mDlgNFC.findViewById(R.id.general8TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general8TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general9TextView))).setText("Mifare Desfire Card Read Data Length(new card, 01)");
		((TextView)(mDlgNFC.findViewById(R.id.general9TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general9EditText))).setText("01");
		((EditText) (mDlgNFC.findViewById(R.id.general9EditText)))
				.setSelection(((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).getText().length());

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE) {
			((TextView) (mDlgNFC.findViewById(R.id.general9TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general9TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general10TextView))).setText("Mifare Desfire Card Special Operation Command(new card, FFFFFFFFFFFF)");
		((TextView)(mDlgNFC.findViewById(R.id.general10TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general10EditText))).setText("");

		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE) {
			((TextView) (mDlgNFC.findViewById(R.id.general10TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general10EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general10TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general10EditText))).setVisibility(View.GONE);

		}


		((TextView)(mDlgNFC.findViewById(R.id.general11TextView))).setText("Mifare Desfire Card Special Operation Parameter");
		((TextView)(mDlgNFC.findViewById(R.id.general11TextView))).getLayoutParams().width = 500;
		((EditText)(mDlgNFC.findViewById(R.id.general11EditText))).setText("");
		if(mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE) {
			((TextView) (mDlgNFC.findViewById(R.id.general11TextView))).setVisibility(View.VISIBLE);
			((EditText) (mDlgNFC.findViewById(R.id.general11EditText))).setVisibility(View.VISIBLE);
		}
		else{
			((TextView) (mDlgNFC.findViewById(R.id.general11TextView))).setVisibility(View.GONE);
			((EditText) (mDlgNFC.findViewById(R.id.general11EditText))).setVisibility(View.GONE);

		}

		((LinearLayout)(mDlgNFC.findViewById(R.id.general1LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general2LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general3LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general4LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general5LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general6LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general7LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general8LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general9LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general10LinearLayout))).setOrientation(LinearLayout.VERTICAL);
		((LinearLayout)(mDlgNFC.findViewById(R.id.general11LinearLayout))).setOrientation(LinearLayout.VERTICAL);


		mDlgNFC.findViewById(R.id.confirmButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				Hashtable<String, Object> data = new Hashtable<String, Object>();

				String writeDataSting = ((EditText) (mDlgNFC.findViewById(R.id.general1EditText))).getText().toString();

				String writeDataHEXSting = Utils.stringToHexString(writeDataSting.getBytes());

				if ((writeDataHEXSting != null) && (!writeDataHEXSting.equalsIgnoreCase(""))) {

					data.put("writeNdefRecord", Utils.encodeNdefFormat(writeDataHEXSting));
				}
				else{
					Constants.showDialog(NFCCardBaseActivity.this, "", "please enter valid data to write");
					return;
				}

				if( mENUMNFCCardType == NFCCardType.MIFARE_ULTRA_LIGHT){

					String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();
					if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardBlockNumber", mifareCardBlockNumber);
					}

				}
				else if( mENUMNFCCardType == NFCCardType.MIFARE_CLASSIC){

					String mifareCardKey = ((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().toString();

					String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();
					if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardBlockNumber", mifareCardBlockNumber);
					}
				}
				else if( mENUMNFCCardType == NFCCardType.MIFARE_DESFIRE){

					String mifareCardKey = ((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().toString();
					if ((mifareCardKey != null) && (!mifareCardKey.equalsIgnoreCase(""))) {
						data.put("mifareCardKey", mifareCardKey);
					}
					String mifareCardKeyNumber = ((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().toString();
					if ((mifareCardKeyNumber != null) && (!mifareCardKeyNumber.equalsIgnoreCase(""))) {
						data.put("mifareCardKeyNumber", mifareCardKeyNumber);
					}
					String mifareDesFireCardAID = ((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).getText().toString();
					if ((mifareDesFireCardAID != null) && (!mifareDesFireCardAID.equalsIgnoreCase(""))) {
						data.put("mifareDesFireCardAID", mifareDesFireCardAID);
					}
					String mifareDesFireCardFID = ((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).getText().toString();
					if ((mifareDesFireCardFID != null) && (!mifareDesFireCardFID.equalsIgnoreCase(""))) {
						data.put("mifareDesFireCardFID", mifareDesFireCardFID);
					}
					String mifareDesFireCardFileOffset = ((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).getText().toString();
					if ((mifareDesFireCardFileOffset != null) && (!mifareDesFireCardFileOffset.equalsIgnoreCase(""))) {
						data.put("mifareDesFireCardFileOffset", mifareDesFireCardFileOffset);
					}
					String mifareDesFireCardDataLength = ((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).getText().toString();
					if ((mifareDesFireCardDataLength != null) && (!mifareDesFireCardDataLength.equalsIgnoreCase(""))) {
						data.put("mifareDesFireCardDataLength", mifareDesFireCardDataLength);
					}
				}

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "data " + data, true, true);

				nfcDataExchange(data);
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v) {
				mDlgNFC.dismiss();
			}

		});

		mDlgNFC.show();

	}
}







				/*if ((mifareCardAuthType != null) && (!mifareCardAuthType.equalsIgnoreCase(""))) {
					data.put("mifareCardAuthType", mifareCardAuthType);
				}
				String mifareCardKey = ((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().toString();
				if ((mifareCardKey != null) && (!mifareCardKey.equalsIgnoreCase(""))) {
					data.put("mifareCardKey", mifareCardKey);
				}
				String mifareCardKeyNumber = ((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().toString();
				if ((mifareCardKeyNumber != null) && (!mifareCardKeyNumber.equalsIgnoreCase(""))) {
					data.put("mifareCardKeyNumber", mifareCardKeyNumber);
				}
				String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();
				if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {
					data.put("mifareCardBlockNumber", mifareCardBlockNumber);
				}
				String mifareDesFireCardAID = ((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).getText().toString();
				if ((mifareDesFireCardAID != null) && (!mifareDesFireCardAID.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardAID", mifareDesFireCardAID);
				}
				String mifareDesFireCardFID = ((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).getText().toString();
				if ((mifareDesFireCardFID != null) && (!mifareDesFireCardFID.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardFID", mifareDesFireCardFID);
				}
				String mifareDesFireCardFileOffset = ((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).getText().toString();
				if ((mifareDesFireCardFileOffset != null) && (!mifareDesFireCardFileOffset.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardFileOffset", mifareDesFireCardFileOffset);
				}
				String mifareDesFireCardDataLength = ((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).getText().toString();
				if ((mifareDesFireCardDataLength != null) && (!mifareDesFireCardDataLength.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardDataLength", mifareDesFireCardDataLength);
				}
				String mifareDesFireCardCommand = ((EditText) (mDlgNFC.findViewById(R.id.general10EditText))).getText().toString();
				if ((mifareDesFireCardCommand != null) && (!mifareDesFireCardCommand.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardCommand", mifareDesFireCardCommand);
				}
				String mifareDesFireCardCommandData = ((EditText) (mDlgNFC.findViewById(R.id.general11EditText))).getText().toString();
				if ((mifareDesFireCardCommandData != null) && (!mifareDesFireCardCommandData.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardCommandData", mifareDesFireCardCommandData);
				}*/


				/*String mifareCardAuthType = ((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).getText().toString();

				if ((mifareCardAuthType != null) && (!mifareCardAuthType.equalsIgnoreCase(""))) {
					data.put("mifareCardAuthType", mifareCardAuthType);
				}
				String mifareCardKey = ((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().toString();
				if ((mifareCardKey != null) && (!mifareCardKey.equalsIgnoreCase(""))) {
					data.put("mifareCardKey", mifareCardKey);
				}
				String mifareCardKeyNumber = ((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().toString();
				if ((mifareCardKeyNumber != null) && (!mifareCardKeyNumber.equalsIgnoreCase(""))) {
					data.put("mifareCardKeyNumber", mifareCardKeyNumber);
				}
				String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();
				if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {
					data.put("mifareCardBlockNumber", mifareCardBlockNumber);
				}
				String mifareDesFireCardAID = ((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).getText().toString();
				if ((mifareDesFireCardAID != null) && (!mifareDesFireCardAID.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardAID", mifareDesFireCardAID);
				}
				String mifareDesFireCardFID = ((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).getText().toString();
				if ((mifareDesFireCardFID != null) && (!mifareDesFireCardFID.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardFID", mifareDesFireCardFID);
				}
				String mifareDesFireCardFileOffset = ((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).getText().toString();
				if ((mifareDesFireCardFileOffset != null) && (!mifareDesFireCardFileOffset.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardFileOffset", mifareDesFireCardFileOffset);
				}
				String mifareDesFireCardDataLength = ((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).getText().toString();
				if ((mifareDesFireCardDataLength != null) && (!mifareDesFireCardDataLength.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardDataLength", mifareDesFireCardDataLength);
				}
				String mifareDesFireCardCommand = ((EditText) (mDlgNFC.findViewById(R.id.general10EditText))).getText().toString();
				if ((mifareDesFireCardCommand != null) && (!mifareDesFireCardCommand.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardCommand", mifareDesFireCardCommand);
				}
				String mifareDesFireCardCommandData = ((EditText) (mDlgNFC.findViewById(R.id.general11EditText))).getText().toString();
				if ((mifareDesFireCardCommandData != null) && (!mifareDesFireCardCommandData.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardCommandData", mifareDesFireCardCommandData);
				}*/




				/*String mifareCardAuthType = ((EditText) (mDlgNFC.findViewById(R.id.general2EditText))).getText().toString();
				if ((mifareCardAuthType != null) && (!mifareCardAuthType.equalsIgnoreCase(""))) {
					data.put("mifareCardAuthType", mifareCardAuthType);
				}
				String mifareCardKey = ((EditText) (mDlgNFC.findViewById(R.id.general3EditText))).getText().toString();
				if ((mifareCardKey != null) && (!mifareCardKey.equalsIgnoreCase(""))) {
					data.put("mifareCardKey", mifareCardKey);
				}
				String mifareCardKeyNumber = ((EditText) (mDlgNFC.findViewById(R.id.general4EditText))).getText().toString();
				if ((mifareCardKeyNumber != null) && (!mifareCardKeyNumber.equalsIgnoreCase(""))) {
					data.put("mifareCardKeyNumber", mifareCardKeyNumber);
				}
				String mifareCardBlockNumber = ((EditText) (mDlgNFC.findViewById(R.id.general5EditText))).getText().toString();
				if ((mifareCardBlockNumber != null) && (!mifareCardBlockNumber.equalsIgnoreCase(""))) {
					data.put("mifareCardBlockNumber", mifareCardBlockNumber);
				}
				String mifareDesFireCardAID = ((EditText) (mDlgNFC.findViewById(R.id.general6EditText))).getText().toString();
				if ((mifareDesFireCardAID != null) && (!mifareDesFireCardAID.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardAID", mifareDesFireCardAID);
				}
				String mifareDesFireCardFID = ((EditText) (mDlgNFC.findViewById(R.id.general7EditText))).getText().toString();
				if ((mifareDesFireCardFID != null) && (!mifareDesFireCardFID.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardFID", mifareDesFireCardFID);
				}
				String mifareDesFireCardFileOffset = ((EditText) (mDlgNFC.findViewById(R.id.general8EditText))).getText().toString();
				if ((mifareDesFireCardFileOffset != null) && (!mifareDesFireCardFileOffset.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardFileOffset", mifareDesFireCardFileOffset);
				}
				String mifareDesFireCardDataLength = ((EditText) (mDlgNFC.findViewById(R.id.general9EditText))).getText().toString();
				if ((mifareDesFireCardDataLength != null) && (!mifareDesFireCardDataLength.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardDataLength", mifareDesFireCardDataLength);
				}
				String mifareDesFireCardCommand = ((EditText) (mDlgNFC.findViewById(R.id.general10EditText))).getText().toString();
				if ((mifareDesFireCardCommand != null) && (!mifareDesFireCardCommand.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardCommand", mifareDesFireCardCommand);
				}
				String mifareDesFireCardCommandData = ((EditText) (mDlgNFC.findViewById(R.id.general11EditText))).getText().toString();
				if ((mifareDesFireCardCommandData != null) && (!mifareDesFireCardCommandData.equalsIgnoreCase(""))) {
					data.put("mifareDesFireCardCommandData", mifareDesFireCardCommandData);
				}*/
