package in.bigtree.vtree.view.cardpaymentactivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.CardTypeResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;


public class GetCardSchemeActivity extends Activity {


	private EditText mETFirstSixDigit = null;
	private Button mBTNGetCardScheme = null;

	private CustomProgressDialog mProgressActivity = null;
	private long lastRequestTime = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_cardscheme);

		mETFirstSixDigit = (EditText) findViewById(R.id.cardscheme_ET_firstsixdigit);

		mBTNGetCardScheme = (Button) findViewById(R.id.cardscheme_BTN_cardtype);

		mBTNGetCardScheme.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				getCardScheme();
			}
		});

	}

	private void getCardScheme() {


		try
		{
			if (mETFirstSixDigit.getText().toString().trim().length() != 6) {
				Constants.showDialog(GetCardSchemeActivity.this, "card scheme", "please enter valid card first six digits");
				mETFirstSixDigit.requestFocus();
				return;
			}

			final MSWisepadController wisepadController = MSWisepadController.
					getSharedMSWisepadController(GetCardSchemeActivity.this ,
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource()
							,null);

			MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());


			if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

				wisepadController.getCardScheme(
						AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
						AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
						mETFirstSixDigit.getText().toString().trim(),
						new MSWisepadControllerResponseListenerObserver());

				mProgressActivity = new CustomProgressDialog(GetCardSchemeActivity.this, "processing...");
				mProgressActivity.show();
			}

			lastRequestTime = System.currentTimeMillis();


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * MSWisepadControllerResponseListenerObserver
	 * The mswipe overridden class  observer which listens to the responses for the mswipe sdk function requests
	 */
	class MSWisepadControllerResponseListenerObserver implements  MSWisepadControllerResponseListener
	{
		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to ChangePasswordResponseData, so this
		 * need be type cast back to ChangePasswordResponseData to access response details for the requested password change
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{
			if(mProgressActivity != null)
			{
				mProgressActivity.dismiss();
				mProgressActivity = null;
			}

			CardTypeResponseData responseData = (CardTypeResponseData) aMSDataStore;
			boolean responseStatus = responseData.getResponseStatus();

			if (!responseStatus)
			{
				Constants.showDialog(GetCardSchemeActivity.this, "card scheme", responseData.getResponseFailureReason());

			}
			else if (responseStatus){

				String cardType = responseData.getCardType();

				if(responseData.getCardScheme().trim().equalsIgnoreCase("C")){

					cardType = cardType+" Credit Card";
				}
				else {
					cardType = cardType+" Debit Card";
				}

				final Dialog dlg = Constants.showDialog(GetCardSchemeActivity.this, "card scheme",
						"Card Type: "+cardType,
						Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button btnOk = (Button) dlg.findViewById(R.id.customdlg_BTN_yes);
				btnOk.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dlg.dismiss();
					}
				});
				dlg.show();

			}
		}
	}
}

