package in.bigtree.vtree.view;

import android.app.Activity;
import android.os.Bundle;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.MSWisepadController.NETWORK_SOURCE;

import in.bigtree.vtree.R;
import in.bigtree.vtree.data.AppSharedPrefrences;

public class BaseTitleActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		if (AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment() == MSWisepadController.GATEWAY_ENVIRONMENT.PRODUCTION)
		{
			this.setTitle(false);
		}
		else{
			this.setTitle(true);
		}
	}

	protected void setTitle(boolean aIsGatewayVertical)
	{
		if(aIsGatewayVertical)
		{
			if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource() == NETWORK_SOURCE.WIFI)					
				this.setTitle(getString(R.string.app_name) + "- (UAT)-W");
			else if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource() == NETWORK_SOURCE.SIM)
				this.setTitle(getString(R.string.app_name) + "- (UAT)-S");
			else if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource() == NETWORK_SOURCE.EHTERNET)
				this.setTitle(getString(R.string.app_name) + "- (UAT)-E");
			
		}
		else{
			
			if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource() == NETWORK_SOURCE.WIFI)					
				this.setTitle(getString(R.string.app_name) + "- (Live)-W");
			else if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource() == NETWORK_SOURCE.SIM)
				this.setTitle(getString(R.string.app_name) + "- (Live)-S");
			else if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource() == NETWORK_SOURCE.EHTERNET)
				this.setTitle(getString(R.string.app_name) + "- (Live)-E");

		}
	}
}
