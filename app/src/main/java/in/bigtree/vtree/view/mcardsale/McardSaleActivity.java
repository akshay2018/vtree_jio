package in.bigtree.vtree.view.mcardsale;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.CardSaleResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.data.TransactionData;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.mcardsale.fragments.MCardSaleOTPFragment;
import in.bigtree.vtree.view.mcardsale.fragments.McardSaleAmountFragment;
import in.bigtree.vtree.view.mcardsale.fragments.McardSaleInputCardNoFragment;
import in.bigtree.vtree.view.mcardsale.fragments.McardSaleInputCradSelectionFragment;
import in.bigtree.vtree.view.mcardsale.fragments.McardSaleTxtApprovalFragment;

public class McardSaleActivity extends FragmentActivity {

    public McardSaleEnum.McardSaleScreens mMCArdScreens;

    public TransactionData mTransactionData;

    public StringBuilder mTopUpScanCardNum ;

    public McardSaleEnum.CardInputType mCardInputType = McardSaleEnum.CardInputType.SWIPE;

    public boolean IsScan = false;
    private CustomProgressDialog mProgressActivity = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.voidsale_fragment);

        mTransactionData = new TransactionData();

        showScreen(McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_AMOUNT);
        initViews();
    }

    private void initViews(){

        ((TextView)findViewById(R.id.topbar_LBL_heading)).setText("mcard sale");

    }

    public void showScreen(McardSaleEnum.McardSaleScreens mcardsalescreens)
    {

        // TODO Auto-generated method stub
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;

        switch (mcardsalescreens)
        {

            case MCARD_CARDSALE_AMOUNT:
                fragment = new McardSaleAmountFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case MCARD_CARDSALE_INPUT_CARD_SELECTION:
                fragment = new McardSaleInputCradSelectionFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case MCARD_CARDSALE_INPUT_CARD_NUM:
                fragment = new McardSaleInputCardNoFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case MCARD_CARDSALE_OTP:

                fragment = new MCardSaleOTPFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                break;

            case MCARD_CARDSALE_APPROVED:

                fragment = new McardSaleTxtApprovalFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                break;


        }
        mMCArdScreens = mcardsalescreens;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mMCArdScreens == McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_AMOUNT)
            {
                finish();

            } else {

                moveToPrevious();
            }

            return true;

        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void moveToPrevious(){

        if(mMCArdScreens == McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_INPUT_CARD_SELECTION){
            showScreen(McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_AMOUNT);
        }
        else if(mMCArdScreens == McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_INPUT_CARD_NUM){
            showScreen(McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_INPUT_CARD_SELECTION);
        }
        else if (mMCArdScreens == McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_OTP) {

            showScreen(McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_INPUT_CARD_NUM);
        }
    }

    public boolean processMcardSale() {

        try {

            String amt = removeChar(mTransactionData.mTotAmount,',');
            String trxType = "";

            if(mCardInputType != McardSaleEnum.CardInputType.ENTER){

                trxType = "Scan";
            }
            else {
                trxType = "Enter";
            }

            String lastFourDigits = "";
            int ilen = mTransactionData.mCardNo.length();

            if (ilen > 4)
            {
                lastFourDigits = mTransactionData.mCardNo.substring(ilen - 4, ilen);
            }
            else {
                lastFourDigits = mTransactionData.mCardNo;
            }

            mTransactionData.mLast4Digits = lastFourDigits;

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(McardSaleActivity.this,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);


            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

            wisepadController.processMCardSaleOnline(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getMID(),
                    amt,
                    trxType,
                    mTransactionData.mCardNo,
                    mTransactionData.mCardExpDate,
                    mTransactionData.mCardHolderName,
                    mTransactionData.mOTPToken,
                    mTransactionData.mPhoneNo,
                    mTransactionData.mEmail,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    new MSWisepadControllerOTPListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(McardSaleActivity.this, getString(R.string.mcard_sale));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(McardSaleActivity.this, getString(R.string.mcard_sale),
                    getString(R.string.voidsaleactivity_the_otp_was_not_processed_successfully_please_try_again));

        }

        return true;
    }


    /**
     * MswipeWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

     */
    class MSWisepadControllerOTPListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            CardSaleResponseData cardSaleResponseData = (CardSaleResponseData) aMSDataStore;

            if(cardSaleResponseData.getResponseStatus())
            {

                mTransactionData.mAuthCode = cardSaleResponseData.getAuthCode();
                mTransactionData.mRRNo = cardSaleResponseData.getRRNO();
                mTransactionData.mStandId = cardSaleResponseData.getStandId();
                mTransactionData.mVoucherNo = cardSaleResponseData.getVoucherNo();

                showScreen(McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_APPROVED);

               /* final Dialog dialog = Constants.showDialog(mCardActivity, getString(R.string.void_sale), String.format(getResources().getString(R.string.otp_sent_message), maskedMobileNum.toString()), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        dialog.dismiss();

                    }
                });
                dialog.show();
*/
            }
            else{

                final Dialog dialog = Constants.showDialog(McardSaleActivity.this, getString(R.string.mcard_sale), cardSaleResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();

                        Intent intent = new Intent(McardSaleActivity.this, MenuView.class);
                        startActivity(intent);
                        finish();

                    }
                });
                dialog.show();

            }
        }
    }


    public String removeChar(String s, char c) {

        String r = "";

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c)
                r += s.charAt(i);
        }

        return r;
    }
}
