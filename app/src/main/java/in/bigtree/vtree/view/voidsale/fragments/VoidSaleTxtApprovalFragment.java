package in.bigtree.vtree.view.voidsale.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.view.voidsale.VoidSaleActivity;

public class VoidSaleTxtApprovalFragment extends Fragment {
	Context mContext;
	VoidSaleActivity mVoidsaleInputDetailsView = null;
	SharedVariable applicationData = null;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		
		mContext = activity;
		mVoidsaleInputDetailsView = ((VoidSaleActivity) mContext);
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		super.onAttach(activity);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.voidsale_transactionapprovalview,container, false);

		initViews(view);
		return view;
	}

	public void initViews(View view) {
		// TODO Auto-generated method stub

		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		TextView lblTxDateTime = (TextView)view. findViewById(R.id.voidsaletxtdetails_LBL_TxDateTime);



		lblTxDateTime.setText((mVoidsaleInputDetailsView.mTransactionData.mDate).toLowerCase());

		TextView lblTxAmtCurrency= (TextView)view. findViewById(R.id.creditsale_LBL_Amtprefix);
		lblTxAmtCurrency.setText(applicationData.mCurrency);
		TextView lblTxAmt = (TextView)view. findViewById(R.id.voidsaletxtdetails_LBL_AmtRs);
		lblTxAmt.setText(" - "+mVoidsaleInputDetailsView.mTransactionData.mBaseAmount);

		((LinearLayout)view.findViewById(R.id.voidsaletxtdetails_LNR_lbllast4digits)).setVisibility(View.VISIBLE);
		((LinearLayout)view.findViewById(R.id.voidsaletxtdetails_LNR_lblAuthNo)).setVisibility(View.VISIBLE);
		((LinearLayout)view.findViewById(R.id.voidsaletxtdetails_LNR_lblRRNo)).setVisibility(View.VISIBLE);

		TextView lblTxcardno = (TextView)view. findViewById(R.id.voidsaletxtdetails_LBL_last4digits);
		lblTxcardno.setText(mVoidsaleInputDetailsView.mTransactionData.mLast4Digits);

		TextView lblAuthNo = (TextView) view.findViewById(R.id.voidsaletxtdetails_LBL_AuthNo);
		lblAuthNo.setText(mVoidsaleInputDetailsView.mTransactionData.mAuthCode);

		TextView lblRRNO = (TextView)view. findViewById(R.id.voidsaletxtdetails_LBL_RRNo);
		lblRRNO.setText(mVoidsaleInputDetailsView.mTransactionData.mRRNo);


		ImageButton btnTxNext = (ImageButton) view.findViewById(R.id.voidsaletxtdetails_BTN_next);
		btnTxNext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				mVoidsaleInputDetailsView.doneWithCreditSale("approved");
			}
		});
	}
}
