package in.bigtree.vtree.view.voidsale;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.VoidTransactionResponseData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.data.TransactionData;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.voidsale.VoidSaleEnum.VoidSaleScreens;
import in.bigtree.vtree.view.voidsale.fragments.VoidOTPFragment;
import in.bigtree.vtree.view.voidsale.fragments.VoidSaleAmountFragment;
import in.bigtree.vtree.view.voidsale.fragments.VoidSaleTxtApprovalFragment;
import in.bigtree.vtree.view.voidsale.fragments.VoidSaleTxtDetailsFragment;


public class VoidSaleActivity extends FragmentActivity {

    private CustomProgressDialog mProgressActivity = null;

    public String displayMsg = "";

    //public boolean mIsCardVoidSale = false;

    protected VoidSaleScreens mVoidSaleScreens;

    /*images to show the websocket connection status i.e wheteher topbar_img_host_active or not*/
    public ImageView mIMGHostConnectionStatus;

    /*using these animation for connection status*/
    public Animation alphaAnim;

    public TransactionData mTransactionData;

     MSWisepadController mWisepadController = null;

     public boolean mIsVoidWithOTp = false;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.voidsale_fragment);

        mIsVoidWithOTp = getIntent().getBooleanExtra("isVoidWithOTp", false);

        mTransactionData = new TransactionData();
        showScreen(VoidSaleScreens.VoidSaleScreens_AMOUNT);

       // mIsCardVoidSale = getIntent().getBooleanExtra("isCardVoidSale", false);

        mWisepadController = MSWisepadController.
                getSharedMSWisepadController(this,
                        AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                        AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(), null);

        alphaAnim = AnimationUtils.loadAnimation(this, R.anim.alpha_anim);
        registerStatusBroadCast();
        initViews();

    }

    private void initViews(){

        ((TextView)findViewById(R.id.topbar_LBL_heading)).setText(getResources().getString(R.string.void_transaction));

        mIMGHostConnectionStatus =(ImageView)findViewById(R.id.topbar_IMG_position2);
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStart() {
        super.onStart();

    }
    
    @Override
    protected void onStop()
    {
    	// TODO Auto-generated method stub
    	super.onStop();
        mWisepadController.stopMSGatewayConnection();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    	unRegisterStatusBroadCast();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mVoidSaleScreens == VoidSaleScreens.VoidSaleScreens_AMOUNT)
            {
                finish();
                Intent intent = new Intent(VoidSaleActivity.this, MenuView.class);
                startActivity(intent);

            } else {

               moveToPrevious();
            }

            return true;

        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void moveToPrevious(){

        if(mVoidSaleScreens == VoidSaleScreens.VoidSaleScreens_TXT_DETAILS){
            showScreen(VoidSaleScreens.VoidSaleScreens_AMOUNT);
        }
        if(mVoidSaleScreens == VoidSaleScreens.VoidSaleScreens_TXT_OTP){
            showScreen(VoidSaleScreens.VoidSaleScreens_TXT_DETAILS);
        }else{

        }
    }


    public boolean processCardSaleVoid(String otp) {

        try {

            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

            mWisepadController.processVoidTransactionWithOTP(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    mTransactionData.mSelectedDate,
                    mTransactionData.mBaseAmount,
                    mTransactionData.mLast4Digits,
                    mTransactionData.mStandId,
                    mTransactionData.mVoucherNo,
                    "",
                    otp,
                    new MSWisepadControllerResponseListenerObserver());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(VoidSaleActivity.this, getString(R.string.void_sale));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(VoidSaleActivity.this, getString(R.string.void_sale),
            		getString(R.string.voidsaleactivity_the_void_sale_transaction_was_not_processed_successfully_please_try_again));

        }

        return true;
    }


    public boolean processCashSaleVoid() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(this,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

            wisepadController.processCashVoidTransaction(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    mTransactionData.mSelectedDate,
                    mTransactionData.mBaseAmount,
                    mTransactionData.mVoucherNo,
                    new MSWisepadControllerResponseListenerObserver());


            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(VoidSaleActivity.this, getString(R.string.void_sale));
            mProgressActivity.show();

        } catch (Exception ex) {

            if (SharedVariable.IS_DEBUGGING_ON)
                Logs.v(SharedVariable.packName, "ex " + ex.toString() , true, true);


            Constants.showDialog(VoidSaleActivity.this, getString(R.string.void_sale),
                    getString(R.string.voidsaleactivity_the_void_sale_transaction_was_not_processed_successfully_please_try_again));

        }

        return true;
    }


    /**
     * MswipeWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

     */
    class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            VoidTransactionResponseData voidTransactionResponseData = (VoidTransactionResponseData) aMSDataStore;

            if (SharedVariable.IS_DEBUGGING_ON)
                Logs.v(SharedVariable.packName, "voidTransactionResponseData.getResponseStatus() " + voidTransactionResponseData.getResponseStatus() , true, true);

            if (SharedVariable.IS_DEBUGGING_ON)
                Logs.v(SharedVariable.packName, "voidTransactionResponseData.getResponseFailureReason() " + voidTransactionResponseData.getResponseFailureReason() , true, true);

            if(voidTransactionResponseData.getResponseStatus())
            {

                mTransactionData.mDate = voidTransactionResponseData.getTrxDate();
                showScreen(VoidSaleScreens.VoidSaleScreens_TXT_APPROVAL_DETAILS);

            }
            else{

                final Dialog dialog = Constants.showDialog(VoidSaleActivity.this, getString(R.string.void_sale), voidTransactionResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {

                        dialog.dismiss();

                    }
                });
                dialog.show();

            }
        }
    }


    public void doneWithCreditSale(String errorMsg) {

        creditSaleViewDestory();
        finish();
        Intent intent = new Intent(VoidSaleActivity.this, MenuView.class);
        startActivity(intent);

    }

    public void creditSaleViewDestory() {


    }


    /**
     * description
     *       Handling fragment replacement.
     */
    public void showScreen(VoidSaleScreens voidsalescreens)
    {

        // TODO Auto-generated method stub
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;

        switch (voidsalescreens)
        {

            case VoidSaleScreens_AMOUNT:

                if(mVoidSaleScreens == VoidSaleScreens.VoidSaleScreens_TXT_DETAILS)
                {
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                }

                fragment = new VoidSaleAmountFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;


            case VoidSaleScreens_TXT_DETAILS:

            if(mVoidSaleScreens == VoidSaleScreens.VoidSaleScreens_AMOUNT)
            {
                fragmentTransaction.setCustomAnimations( R.anim.enter_from_right ,R.anim.exit_to_left);
            }

            fragment = new VoidSaleTxtDetailsFragment();


            fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            break;

            case VoidSaleScreens_TXT_OTP:

                if(mVoidSaleScreens == VoidSaleScreens.VoidSaleScreens_TXT_DETAILS)
                {
                    fragmentTransaction.setCustomAnimations( R.anim.enter_from_right ,R.anim.exit_to_left);
                }

                fragment = new VoidOTPFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;



            case VoidSaleScreens_TXT_APPROVAL_DETAILS:

               // mLINTopBarMenu.setVisibility(View.GONE);
               // mLINTopBarCancel.setVisibility(View.GONE);

                if(mVoidSaleScreens == VoidSaleScreens.VoidSaleScreens_TXT_DETAILS)
                {
                    fragmentTransaction.setCustomAnimations( R.anim.enter_from_right, R.anim.exit_to_left);
                }

                fragment = new VoidSaleTxtApprovalFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            default:
                break;
        }
        mVoidSaleScreens = voidsalescreens;
    }

    BroadcastReceiver sentSmsBroadcastCome;
    private void registerStatusBroadCast(){
    	sentSmsBroadcastCome = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                String state = intent.getExtras().getString("state");
                if(state.equalsIgnoreCase(getString(R.string.host_connecting)))
                {
                    mIMGHostConnectionStatus.startAnimation(alphaAnim);
                    mIMGHostConnectionStatus.setImageResource(R.drawable.topbar_img_host_inactive);
                }
                else if(state.equalsIgnoreCase(getString(R.string.host_online))){
                    mIMGHostConnectionStatus.setAnimation(null);
                    mIMGHostConnectionStatus.setImageResource(R.drawable.topbar_img_host_active);
                }
                else if(state.equalsIgnoreCase(getString(R.string.host_offline))){
                    mIMGHostConnectionStatus.setAnimation(null);
                    mIMGHostConnectionStatus.setImageResource(R.drawable.topbar_img_host_inactive);
                }
            }
        };
        IntentFilter filterSend = new IntentFilter();
        filterSend.addAction("com.mswipeWisepad.connection.status");
        registerReceiver(sentSmsBroadcastCome, filterSend);
    }

    private void unRegisterStatusBroadCast(){
    	unregisterReceiver(sentSmsBroadcastCome);
    }

}
