package in.bigtree.vtree.view;

import android.Manifest;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Set;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.ListAdapter;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Constants.CUSTOM_DLG_TYPE;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.bqrsale.BQRSaleActivity;
import in.bigtree.vtree.view.cardpaymentactivity.GetCardSchemeActivity;
import in.bigtree.vtree.view.cardpaymentactivity.LastTransactionActivity;
import in.bigtree.vtree.view.cardpaymentactivity.NFC.NFCCardActivity;
import in.bigtree.vtree.view.cardpaymentactivity.PaymentIntegrationTypeActivity;
import in.bigtree.vtree.view.cardpaymentactivity.VerificationActivity;
import in.bigtree.vtree.view.cashorbanksale.BankSaleView;
import in.bigtree.vtree.view.cashorbanksale.CashSaleView;
import in.bigtree.vtree.view.deviceinfo.DeviceInfoActivity;
import in.bigtree.vtree.view.history.HistorySummaryView;
import in.bigtree.vtree.view.historyMonthWise.HistoryMenu;
import in.bigtree.vtree.view.login.ChangePasswordView;
import in.bigtree.vtree.view.login.LoginView;
import in.bigtree.vtree.view.mcardsale.McardSaleActivity;
import in.bigtree.vtree.view.offlinehistory.OfflineHistoryActivity;
import in.bigtree.vtree.view.offlinetransactions.OfflineTransactionMenu;
import in.bigtree.vtree.view.paybylink.PayByLinkActivity;
import in.bigtree.vtree.view.preauthcompletion.PreauthCompletionActivity;
import in.bigtree.vtree.view.settings.GatewayEnvView;
import in.bigtree.vtree.view.settings.PermissionsActivity;
import in.bigtree.vtree.view.settings.PrinterSettings;
import in.bigtree.vtree.view.voidsale.VoidSaleActivity;
import in.bigtree.vtree.view.wiseposfeatures.WiseposMenuActivity;
import in.bigtree.vtree.view.wiseposplusfeatures.WiseposPlusMenuActivity;


/**
 * MenuView
 * MenuView activity lists all the Mswipe api's available, each entry will demonstrate the integration process
 * of the api
 *
 */

public class MenuView extends BaseTitleActivity {

	private final int CHANGE_PASSWORD = 1011;
	Intent intent = null;

	private int REQUEST_CODE_PERMISSIONS = 2002;


	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_menu);

		initViews();

	}

	private void initViews() {

		requestPermissions();

		ListView listView = (ListView) findViewById(R.id.menuview_LST_options);

		String[] values = new String[]
				{
						"Printer Settings",
						"Gateway Preferences",
						"Login",
						"Card Sale Trx",
						"Preauth Sale Trx",
						"Preauth Completion",
						"Cash Sale Trx",
						"Bank Sale Trx",
						"Cash at pos",
						"EMI",
						"Void Trx",
						"Void Trx With OTP",
						"Verify Trx",
						"History",
						"offline Transactions",
						"offline History",
						"Summary",
						"Last Transaction Status",
						"Change Password",
						"Device Info",
						"Get Card Scheme",
						"Mcard Sale",
						"BQR Sale",
						"PayBy Link",
						"NFC",
						"Wisepos Feature",
						"Wisepos Plus Feature"
				};

		((RelativeLayout) findViewById(R.id.top_bar)).setVisibility(View.GONE);

		TextView txtHeading = (TextView) findViewById(R.id.topbar_LBL_heading);
		txtHeading.setText("SDK List");

		MenuViewAdapter adapter = new MenuViewAdapter(this, values);
		int[] colors = {0, 0xFF0000FF, 0};
		listView.setDivider(new GradientDrawable(Orientation.LEFT_RIGHT, colors));
		listView.setDividerHeight(1);

		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				int menuoption = 0;

				if (arg2 == menuoption) {
					intent = new Intent(MenuView.this, PrinterSettings.class);
					startActivityForResult(intent, 0);
				}

				menuoption++;
				if (arg2 == menuoption) {
					intent = new Intent(MenuView.this, GatewayEnvView.class);
					startActivityForResult(intent, 0);
				}

				menuoption++;
				if (arg2 == menuoption) {

					if (!isMerchantAuthenticated()) {

						final Dialog dialog = Constants.showDialog(MenuView.this, "SDK List",
								getString(R.string.loginactivity_disclaimer_message),
								CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO, "ok", "no");

						Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
						yes.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {

								dialog.dismiss();
								intent = new Intent(MenuView.this, LoginView.class);
								startActivity(intent);
								return;
							}
						});

						dialog.show();

					} else {
						intent = new Intent(MenuView.this, LoginView.class);
						promptMerchantRelogin();

					}
				}

				menuoption++;
				if (arg2 == menuoption) {
					intent = new Intent(MenuView.this, PaymentIntegrationTypeActivity.class);
					promptMerchantRelogin();

					return;
				}

				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, PaymentIntegrationTypeActivity.class);
						intent.putExtra("preauthsale", true);
						promptMerchantRelogin();
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, PreauthCompletionActivity.class);
						promptMerchantRelogin();
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, CashSaleView.class);
						startActivity(intent);

					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}


				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, BankSaleView.class);
						startActivity(intent);
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}


				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {
						intent = new Intent(MenuView.this, PaymentIntegrationTypeActivity.class);
						intent.putExtra("salewithcash", true);
						promptMerchantRelogin();
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, PaymentIntegrationTypeActivity.class);
						intent.putExtra("emisale", true);
						promptMerchantRelogin();
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, VoidSaleActivity.class);
						startActivity(intent);
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, VoidSaleActivity.class);
						intent.putExtra("isVoidWithOTp", true);
						startActivity(intent);
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, VerificationActivity.class);
						startActivity(intent);
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, HistoryMenu.class);
						intent.putExtra("OptionsType", "History");
						startActivity(intent);
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}


				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, OfflineTransactionMenu.class);
						intent.putExtra("OptionsType", "offlinetransactions");
						startActivity(intent);
						//finish();
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;

                if (arg2 == menuoption)
                {
                    if(isMerchantAuthenticated())
                    {

                        intent = new Intent(MenuView.this, OfflineHistoryActivity.class);
                        intent.putExtra("OptionsType","offline History");
                        startActivity(intent);
                        finish();
                    }
                    else{
                        Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

                    }
                    return;
                }

                menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {
						intent = new Intent(MenuView.this, HistorySummaryView.class);
						intent.putExtra("OptionsType", "Summary");
						startActivity(intent);
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;
				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, LastTransactionActivity.class);
						startActivity(intent);
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first. .");

					}
					return;
				}

				menuoption++;


				if (arg2 == menuoption) {
					if (isMerchantAuthenticated()) {
						Intent intent = new Intent(MenuView.this, ChangePasswordView.class);
						startActivityForResult(intent, CHANGE_PASSWORD);
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
				}

				menuoption++;
				if (arg2 == menuoption) {

					Intent intent = new Intent(MenuView.this, DeviceInfoActivity.class);
					startActivity(intent);

					return;
				}

				menuoption++;
				if (arg2 == menuoption) {

					Intent intent = new Intent(MenuView.this, GetCardSchemeActivity.class);
					startActivity(intent);

					return;
				}

				menuoption++;
				if (arg2 == menuoption) {

					if (isMerchantAuthenticated()) {

						intent = new Intent(MenuView.this, McardSaleActivity.class);
						startActivity(intent);
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;
				if (arg2 == menuoption) {

					if (isMerchantAuthenticated()) {

						if(AppSharedPrefrences.getAppSharedPrefrencesInstace().isBQREnabled()) {

							intent = new Intent(MenuView.this, BQRSaleActivity.class);
							startActivity(intent);

						}else {

							Constants.showDialog(MenuView.this, "SDK List", "brq sale not enable for this user");
						}
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;

				if (arg2 == menuoption) {

					if (isMerchantAuthenticated()) {

						/*if(AppSharedPrefrences.getAppSharedPrefrencesInstace().isPayByLink()) {

							intent = new Intent(MenuView.this, PayByLinkActivity.class);
							startActivity(intent);

						}else {

							Constants.showDialog(MenuView.this, "SDK List", "payby link not enable for this user");
						}*/

						intent = new Intent(MenuView.this, PayByLinkActivity.class);
						startActivity(intent);
					} else {
						Constants.showDialog(MenuView.this, "SDK List", "you have to login first.");

					}
					return;
				}

				menuoption++;
				if (arg2 == menuoption) {
					intent = new Intent(MenuView.this, NFCCardActivity.class);
					startActivity(intent);
				}

				menuoption++;
				if (arg2 == menuoption) {
                    intent = new Intent(MenuView.this, WiseposMenuActivity.class);
                    startActivity(intent);
				}

				menuoption++;
				if (arg2 == menuoption) {
                    intent = new Intent(MenuView.this, WiseposPlusMenuActivity.class);
                    startActivity(intent);
				}
			}
		});

	}

	public static boolean hasPermissions(Context context, String... permissions) {
		if (context != null && permissions != null) {
			for (String permission : permissions) {
				if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
					return false;
				}
			}
		}
		return true;
	}


	public boolean isMerchantAuthenticated() {

		if (AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId().length() != 0
				&& AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId().length() != 0) {
			return true;

		}
		return false;

	}

	public void promptMerchantRelogin()
	{

		final Dialog dialog = Constants.showDialog(MenuView.this, "SDK List",
				"Merchant authenticated, please select \"ok\" to continue or \"no\" to relogin again.",
				CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_CONFIRMATION, "ok", "no");
		Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
		yes.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();

				if (!intent.getComponent().getClassName().equals(LoginView.class.getName())) {

					MenuView.this.startActivity(intent);
				}
			}
		});

		Button no = (Button) dialog.findViewById(R.id.customdlg_BTN_no);
		no.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				clearDeviceCache();
				AppSharedPrefrences.getAppSharedPrefrencesInstace().setReferenceId("");
				AppSharedPrefrences.getAppSharedPrefrencesInstace().setSessionToken("");

				dialog.dismiss();

			}
		});
		dialog.show();

	}

	CheckBox chkLastKnownDevice;
	CheckBox chkSelectDevice;
	CheckBox chkAutoConnect;
	CheckBox chkCheckCard;

	ArrayList<String> pairedDevcieNameList;

	public void showWisePadConnectionDlg() {
		final Dialog dialog = Constants.showWisepadConnectionDialog(MenuView.this, "Wisepos Settings");
		chkLastKnownDevice = (CheckBox) dialog.findViewById(R.id.customwisepadconnection_CHK_remembereddevcie);
		chkSelectDevice = (CheckBox) dialog.findViewById(R.id.customwisepadconnection_CHK_selectdevice);
		chkAutoConnect = (CheckBox) dialog.findViewById(R.id.customwisepadconnection_CHK_autoconnect);
		chkCheckCard = (CheckBox) dialog.findViewById(R.id.customwisepadconnection_CHK_checkcard);

		chkAutoConnect.setChecked(true);
		chkCheckCard.setChecked(true);

		SharedPreferences preferences;
		preferences = PreferenceManager.getDefaultSharedPreferences(MenuView.this);
		String lastConnectedDevice = preferences.getString("mswipeLastConnectedDeviceClassName", "");

		if (lastConnectedDevice.length() > 0) {
			chkLastKnownDevice.setChecked(true);
			((TextView) dialog.findViewById(R.id.customwisepadconnection_LBL_remembereddevcie)).setText(lastConnectedDevice);
		} else {
			((TextView) dialog.findViewById(R.id.customwisepadconnection_LBL_remembereddevcie)).setText("No device selected");

		}

		((TextView) dialog.findViewById(R.id.customwisepadconnection_LBL_selecteddevice)).setText(("No device selected"));

		chkLastKnownDevice.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
				// TODO Auto-generated method stub

				if (isChecked) {
					chkSelectDevice.setChecked(false);
				} else {
					chkSelectDevice.setChecked(true);
				}
			}
		});

		chkSelectDevice.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
				// TODO Auto-generated method stub

				if (isChecked) {
					chkLastKnownDevice.setChecked(false);
				} else {
					chkLastKnownDevice.setChecked(true);
				}
			}
		});

		Button ok = (Button) dialog.findViewById(R.id.customwisepadconnection_BTN_ok);
		ok.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				dialog.dismiss();
				SharedPreferences preferences;
				preferences = PreferenceManager.getDefaultSharedPreferences(MenuView.this);


				if (!chkLastKnownDevice.isChecked() && !chkSelectDevice.isChecked()) {
					preferences.edit().putString("mswipeLastConnectedDeviceClassName", "").commit();

				} else if (chkLastKnownDevice.isChecked()) {

					String stDevice = ((TextView) dialog.findViewById(R.id.customwisepadconnection_LBL_remembereddevcie)).getText().toString();
					if (stDevice.startsWith("No device selected")) {
						preferences.edit().putString("mswipeLastConnectedDeviceClassName", "").commit();
					} else {
						preferences.edit().putString("mswipeLastConnectedDeviceClassName", stDevice).commit();

					}
				} else if (chkSelectDevice.isChecked()) {

					String stDevice = ((TextView) dialog.findViewById(R.id.customwisepadconnection_LBL_selecteddevice)).getText().toString();

					if (stDevice.startsWith("No device selected")) {
						preferences.edit().putString("mswipeLastConnectedDeviceClassName", "").commit();

					} else {
						preferences.edit().putString("mswipeLastConnectedDeviceClassName", stDevice).commit();


					}
				}
				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "********************************************** " + preferences.getString("mswipeLastConnectedDeviceClassName", "") + " ******************************************************", true, true);

				intent.putExtra("autoconnect", chkAutoConnect.isChecked());
				intent.putExtra("checkcardAfterConnection", chkCheckCard.isChecked());
				MenuView.this.startActivity(intent);

			}
		});

		Button selectDevice = (Button) dialog.findViewById(R.id.customwisepadconnection_BTN_selectdevice);
		selectDevice.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				final Dialog devcieDialog = Constants.showAppCustomDialog(MenuView.this, "Select paired devices");

				Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
				pairedDevcieNameList = new ArrayList<String>();
				for (BluetoothDevice device : pairedDevices) {
					pairedDevcieNameList.add(device.getName());
				}

				ListView deviceListView = (ListView) devcieDialog.findViewById(R.id.customapplicationdlg_LST_applications);
				deviceListView.setAdapter(new ListAdapter(MenuView.this, pairedDevcieNameList));
				deviceListView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

						((TextView) dialog.findViewById(R.id.customwisepadconnection_LBL_selecteddevice)).setText(pairedDevcieNameList.get(position));
						chkLastKnownDevice.setChecked(false);
						chkSelectDevice.setChecked(true);
						devcieDialog.dismiss();

					}

				});

				devcieDialog.findViewById(R.id.customapplicationdlg_BTN_cancel).setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {


						devcieDialog.dismiss();
					}
				});
				devcieDialog.show();


			}
		});

		dialog.show();

	}


	public void clearDeviceCache() {
		AppSharedPrefrences.getAppSharedPrefrencesInstace().setReferenceId("");
		AppSharedPrefrences.getAppSharedPrefrencesInstace().setSessionToken("");

		deleteCache(this);
	}

	public static void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			if (dir != null && dir.isDirectory()) {
				deleteDir(dir);
			}
		} catch (Exception e) {
		}
	}

	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		}
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.loginview, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

			case R.id.loginview_exit: {
				finish();
				break;
			}
			case R.id.loginview_Help: {
				new HelpView(this).show();
				break;
			}

		}
		return super.onOptionsItemSelected(item);

	}


	public class MenuViewAdapter extends BaseAdapter {
		String[] listData = null;
		Context context;

		public MenuViewAdapter(Context context, String[] listData) {
			this.listData = listData;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listData.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.view_menulstitem, null);
			}
			TextView txtItem = (TextView) convertView
					.findViewById(R.id.menuview_lsttext);
			txtItem.setText(listData[position]);

			return convertView;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		Logs.v(SharedVariable.packName, "requestCode: " + requestCode, true, true);
		Logs.v(SharedVariable.packName, "resultCode: " + resultCode, true, true);

		if (requestCode == REQUEST_CODE_PERMISSIONS) {


		} else if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				this.finish();
				startActivity(this.getIntent());
			}
		} else if (requestCode == CHANGE_PASSWORD) {
			if (resultCode == RESULT_OK) {

				clearDeviceCache();
				Intent intent = new Intent(MenuView.this, LoginView.class);
				startActivity(intent);
			}
		}
	}

	private void requestPermissions() {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		{

			if ((ContextCompat.checkSelfPermission(this, "android.permission.BBPOS") != PackageManager.PERMISSION_GRANTED)
					|| (ContextCompat.checkSelfPermission(this, Manifest.permission.VIBRATE) != PackageManager.PERMISSION_GRANTED)
					|| (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
					|| (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
					|| (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
					|| (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
					|| (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
					|| (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
				// Permission is not granted

				Intent intent = new Intent(this, PermissionsActivity.class);
				startActivityForResult(intent, REQUEST_CODE_PERMISSIONS);

			}
		}
	}
}
