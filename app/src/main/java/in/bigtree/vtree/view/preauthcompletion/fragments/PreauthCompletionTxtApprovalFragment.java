package in.bigtree.vtree.view.preauthcompletion.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bbpos.simplyprint.SimplyPrintController.BatteryStatus;
import com.bbpos.simplyprint.SimplyPrintController.Error;
import com.bbpos.simplyprint.SimplyPrintController.PrinterResult;
import com.mswipetech.wisepad.sdk.data.ReceiptData;
import com.mswipetech.wisepad.sdk.device.MSWisepadDeviceController;
import com.socsi.smartposapi.printer.Printer2;

import java.util.ArrayList;
import java.util.Hashtable;

import in.bigtree.vtree.Manager.services.MswipePrinterListner;
import in.bigtree.vtree.Manager.services.MswipePrinterService;
import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.ListAdapter;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Constants.CUSTOM_DLG_TYPE;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.util.ReceiptUtility;
import in.bigtree.vtree.util.ReceiptUtilityWPP;
import in.bigtree.vtree.view.preauthcompletion.PreauthCompletionActivity;

public class PreauthCompletionTxtApprovalFragment extends Fragment {
	Context mContext;
	PreauthCompletionActivity mPreauthCompletionApprovalView = null;
	SharedVariable applicationData = null;
	private static final int REQUEST_ENABLE_BT = 0;

	RelativeLayout mRLTPrint = null;
	String title = "";

	boolean isCustomerCopy = false;

	MswipePrinterService mPrinterConnectionService;
	MswipePrinterListner printerListner;
	boolean isBound = false;

	ArrayList<Printer2> wppreceipts;
	private boolean isPrintingProcess = false;

	boolean wisePadDeviceConnecting = false;
	boolean isonPrinterOperationEnd = false;
	ArrayList<BluetoothDevice> pairedDevicesFound = new ArrayList<BluetoothDevice>();
	private ArrayList<byte[]> receipts;
	ReceiptData mReceiptDataModel = null;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		
		mContext = activity;
		mPreauthCompletionApprovalView = ((PreauthCompletionActivity) mContext);
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		super.onAttach(activity);
	}

	public PreauthCompletionTxtApprovalFragment(ReceiptData recieptdatamodel){

		mReceiptDataModel = recieptdatamodel;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.preauthcompletion_txtapprovalview,container, false);

		initViews(view);
		printerListner = new PrinterListner();
		title = getString(R.string.preauth_completion);
		return view;
	}

	public void initViews(View view) {
		// TODO Auto-generated method stub

		try {
			mPreauthCompletionApprovalView.getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		}catch (Exception e){}

		//for changing the topbar color
		mPreauthCompletionApprovalView.setChangeTopbarColor();

		ImageButton btnTxNext = (ImageButton) view.findViewById(R.id.preauthcompletion_approvalview_BTN_next);
		btnTxNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				mPreauthCompletionApprovalView.doneWithCreditSale();

			}
		});

		mRLTPrint = (RelativeLayout)view.findViewById(R.id.preauthcompletion_approvalview_RLT_print);

		if(AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrinterSupportRequired()){
			mRLTPrint.setVisibility(View.VISIBLE);
		}else{
			mRLTPrint.setVisibility(View.GONE);
		}

		mRLTPrint.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				onPrintButtonclick();
			}
		});

	}

	@Override
	public void onStop() {
		super.onStop();
		doUnbindService();
	}

	void doBindService() {
		mPreauthCompletionApprovalView.bindService(new Intent(mPreauthCompletionApprovalView, MswipePrinterService.class), mConnection, Context.BIND_AUTO_CREATE);
		isBound = true;
	}


	void doUnbindService() {
		if (isBound) {
			// If we have received the service, and hence registered with it,
			// then now is the time to unregister.
			// Detach our existing connection.
			mPreauthCompletionApprovalView.unbindService(mConnection);
			isBound = false;
		}
	}

	private ServiceConnection mConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder service) {

			try {

				MswipePrinterService.LocalBinder localBinder = (MswipePrinterService.LocalBinder) service;
				mPrinterConnectionService =  localBinder.getService();
				mPrinterConnectionService.setPrinterListner(printerListner);

			} catch (Exception e) {
				// In this case the service has crashed before we could even do
				// anything with it
			}

		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected - process crashed.
			mPrinterConnectionService = null;
		}
	};

	public void printReceipt()
	{
		if(isBound){
			ReceiptUtility receiptUtility = new ReceiptUtility(mPreauthCompletionApprovalView);
			receipts = new ArrayList<byte[]>();
			receipts.add(receiptUtility.printReceipt(mReceiptDataModel, null, false, ReceiptUtility.TYPE.CARD));

			isPrintRequestInQue = true;

			if(receipts.size() > 0)
			{
				mPrinterConnectionService.printReceipt(receipts.get(0));
			}

		}else{
			isPrintRequestInQue = true;
			doBindService();
		}


	}

	public void printWppReceipt() {

		if(isBound){

			ReceiptUtilityWPP receiptUtility = new ReceiptUtilityWPP(mPreauthCompletionApprovalView);
			wppreceipts = new ArrayList<>();
			wppreceipts.add(receiptUtility.printReceipt(mReceiptDataModel,
					null,
					false,
					ReceiptUtilityWPP.TYPE.CARD, isCustomerCopy));

			isPrintRequestInQue = true;

			if(wppreceipts.size() > 0)
			{
				mPrinterConnectionService.printReceipt(wppreceipts.get(0));
			}

		}else{
			isPrintRequestInQue = true;
			doBindService();
		}
	}


	class PrinterListner extends MswipePrinterListner {


		@Override
		public void onRegisterd() {
			// TODO Auto-generated method stub
			super.onRegisterd();

		}

		@Override
		public void onUnRegisterd() {
			// TODO Auto-generated method stub
			super.onUnRegisterd();
		}


		@Override
		public void onPrinterStateChanged(PRINTER_STATE state) {
			// TODO Auto-generated method stub
			super.onPrinterStateChanged(state);
			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, "printer state " + state, true, true);

			if (state == PRINTER_STATE.WAITINGFORCONNECTION) {

				mRLTPrint.setVisibility(View.VISIBLE);
				mRLTPrint.setEnabled(true);

				final Dialog dialog = Constants.showDialog(mPreauthCompletionApprovalView, title,
						getString(R.string.connecting_to_printer_if_its_taking_longer_than_usual_please_restart_the_printer_and_try_reconnecting), CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);

				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						dialog.dismiss();

					}
				});

				dialog.show();

			}else if (state == PRINTER_STATE.CONNECTED) {

				mRLTPrint.setVisibility(View.VISIBLE);
				mRLTPrint.setEnabled(true);

				if (applicationData.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName,  "onBTv2Connected", true, true);

				if (mPreauthCompletionApprovalView.mstrEMVProcessTaskType.equalsIgnoreCase("backbutton") || mPreauthCompletionApprovalView.mstrEMVProcessTaskType.equalsIgnoreCase("onlinesubmit")) {

					if (mPreauthCompletionApprovalView.mEMVProcessTask != null && mPreauthCompletionApprovalView.mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
						mPreauthCompletionApprovalView.mEMVProcessTask.cancel(true);
				}

				if(isPrintRequestInQue){
					isPrintRequestInQue = false;
					new Thread(){
						public void run() {
							try {
								sleep(1000);

								onPrintButtonclick();

							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						};
					}.start();
				}
			}else if (state == PRINTER_STATE.DISCONNECTED) {

				mRLTPrint.setVisibility(View.VISIBLE);
				mRLTPrint.setEnabled(true);

				wisePadDeviceConnecting = false;
				//bluetoothConnectionState = 0;

				if (applicationData.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName,  "onBTv2Disconnected  ", true, true);

				if (mPreauthCompletionApprovalView.mstrEMVProcessTaskType.equalsIgnoreCase("backbutton") ||
						mPreauthCompletionApprovalView.mstrEMVProcessTaskType.equalsIgnoreCase("onlinesubmit") ||
						mPreauthCompletionApprovalView.mstrEMVProcessTaskType.equalsIgnoreCase("stopbluetooth")) {

					if (mPreauthCompletionApprovalView.mEMVProcessTask != null && mPreauthCompletionApprovalView.mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
						mPreauthCompletionApprovalView.mEMVProcessTask.cancel(true);
				}

			}else if (state == PRINTER_STATE.PRINTING) {


			}
		}


		@Override
		public void onPrinterConnectionError(PRINTER_CONNECTION_ERROR state, ArrayList<BluetoothDevice> devices) {
			// TODO Auto-generated method stub
			super.onPrinterConnectionError(state, devices);

			String msg = "";
			mRLTPrint.setVisibility(View.VISIBLE);
			mRLTPrint.setEnabled(true);

			wisePadDeviceConnecting = false;

			if(state == PRINTER_CONNECTION_ERROR.NO_PAIRED_DEVICE_FOUND){

				msg = getString(R.string.no_paired_printer_found_please_pair_the_printer_from_your_phones_bluetooth_settings_and_try_again);

			}else if(state == PRINTER_CONNECTION_ERROR.MULTIPLE_PAIRED_DEVICE){

				pairedDevicesFound = devices;

				TaskShowMultiplePairedDevices pairedtask = new TaskShowMultiplePairedDevices();
				pairedtask.execute();

				mRLTPrint.setEnabled(false);

				return;

			}else if (state == PRINTER_CONNECTION_ERROR.BLUETOOTH_OFF) {

				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

			}else if (state == PRINTER_CONNECTION_ERROR.UNKNOWN) {

				msg = getString(R.string.unable_to_connect_to_the_bluetooth_printer);

			}else if (state == PRINTER_CONNECTION_ERROR.CONNECTING){
				msg = getString(R.string.connecting_to_printer_if_its_taking_longer_than_usual_please_restart_the_printer_and_try_reconnecting);
			}else if (state == PRINTER_CONNECTION_ERROR.WISEPAD_SWITCHED_OFF){
				final Dialog dialog = Constants.showDialog(mPreauthCompletionApprovalView, title,
						getString(R.string.printer_not_connected_please_make_sure_that_the_printer_is_switched_on),CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO,
						getResources().getString(R.string.connect),
						getResources().getString(R.string.close));

				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				((Button) dialog.findViewById(R.id.customdlg_BTN_yes)).setText(getResources().getString(R.string.ok));
				yes.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						dialog.dismiss();
						mPrinterConnectionService.reconnectToDevice();
					}
				});


				dialog.show();

				return;
			}else if(state == PRINTER_CONNECTION_ERROR.PRINTING_IN_PROGRESS){
				new Handler(Looper.getMainLooper()).post(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(mPreauthCompletionApprovalView, getString(R.string.printing_in_process), Toast.LENGTH_SHORT).show();
					}
				});

			}

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, "makeConnnection msg "+ msg,true, true);

			if (msg.length()>0) {
				Constants.showDialog(mPreauthCompletionApprovalView, title, msg);
			}
		}



		@Override
		public void onReturnDeviceInfo(Hashtable<String, String> deviceInfoTable) {

			String msg = "";

			String productId = deviceInfoTable.get("productId");
			String firmwareVersion = deviceInfoTable.get("firmwareVersion");
			String bootloaderVersion = deviceInfoTable.get("bootloaderVersion");
			String hardwareVersion = deviceInfoTable.get("hardwareVersion");
			String isUsbConnected = deviceInfoTable.get("isUsbConnected");
			String isCharging = deviceInfoTable.get("isCharging");
			String batteryLevel = deviceInfoTable.get("batteryLevel");

			String content = "";
			content += getString(R.string.product_id) + productId + "\n";
			content += getString(R.string.firmware_version) + firmwareVersion + "\n";
			content += getString(R.string.bootloader_version) + bootloaderVersion + "\n";
			content += getString(R.string.hardware_version) + hardwareVersion + "\n";
			content += getString(R.string.usb) + isUsbConnected + "\n";
			content += getString(R.string.charge) + isCharging + "\n";
			content += getString(R.string.battery_level) + batteryLevel + "\n";

			msg =  content;

			if (applicationData.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,   "onReturnDeviceInfo =>  msg " + msg, true, true);
		}

		@Override
		public void onReturnPrinterResult(PrinterResult printerResult) {

			String msg = "";

			mRLTPrint.setVisibility(View.VISIBLE);
			mRLTPrint.setEnabled(true);

			if(printerResult == PrinterResult.SUCCESS) {
				msg =  getString(R.string.printer_command_success);
			} else if(printerResult == PrinterResult.NO_PAPER) {
				msg =  getString(R.string.no_paper);
			} else if(printerResult == PrinterResult.WRONG_CMD) {
				msg =  getString(R.string.wrong_printer_cmd);
			} else if(printerResult == PrinterResult.OVERHEAT) {
				msg =  getString(R.string.printer_overheat);
			}

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,    " onReturnPrinterResult PrinterResult "+ msg, true, true);

			Constants.showDialog(mPreauthCompletionApprovalView, title, msg);

		}

		@Override
		public void onPrinterOperationEnd() {

			isonPrinterOperationEnd = true;

		}

		@Override
		public void onBatteryLow(BatteryStatus batteryStatus) {

			String msg = "";

			if(batteryStatus == BatteryStatus.LOW) {
				msg =  getString(R.string.printer_battery_low);
			} else if(batteryStatus == BatteryStatus.CRITICALLY_LOW) {
				msg =  getString(R.string.printer_battery_critically_low);
			}

			try{

				Constants.showDialog(mPreauthCompletionApprovalView, title, msg);

			}catch(Exception e){
				e.printStackTrace();
			}

		}

		@Override
		public void onError(Error errorState) {

			String msg = "";

			if (mPreauthCompletionApprovalView.mstrEMVProcessTaskType.equalsIgnoreCase("backbutton") ||
					mPreauthCompletionApprovalView.mstrEMVProcessTaskType.equalsIgnoreCase("onlinesubmit")) {

				if (mPreauthCompletionApprovalView.mEMVProcessTask != null && mPreauthCompletionApprovalView.mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mPreauthCompletionApprovalView.mEMVProcessTask.cancel(true);

			}

			if(errorState == Error.UNKNOWN) {
				msg =  getString(R.string.printer_unknown_error);
			} else if(errorState == Error.CMD_NOT_AVAILABLE) {
				msg =  getString(R.string.printer_command_not_available);
			} else if(errorState == Error.TIMEOUT) {
				msg =  getString(R.string.printer_device_no_response);
			} else if(errorState == Error.DEVICE_BUSY) {
				msg =  getString(R.string.printer_device_busy);
			} else if(errorState == Error.INPUT_OUT_OF_RANGE) {
				msg =  getString(R.string.printer_out_of_range);
			} else if(errorState == Error.INPUT_INVALID) {
				msg =  getString(R.string.printer_input_invalid);
			} else if(errorState == Error.CRC_ERROR) {
				msg =  getString(R.string.printer_crc_error);
			} else if(errorState == Error.FAIL_TO_START_BTV2) {
				msg =  getString(R.string.printer_fail_to_start_bluetooth);
			} else if(errorState == Error.COMM_LINK_UNINITIALIZED) {
				msg =  getString(R.string.printer_comm_link_uninitialized);
			} else if(errorState == Error.BTV2_ALREADY_STARTED) {
				msg =  getString(R.string.printer_bluetooth_already_started);
			}


			if (applicationData.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName,    "onError  the error state is " + errorState, true, true);

			try{
				Constants.showDialog(mPreauthCompletionApprovalView, title, msg);

			}catch(Exception e){
				e.printStackTrace();
			}

			mRLTPrint.setVisibility(View.VISIBLE);
			mRLTPrint.setEnabled(true);

		}
	}

	boolean isPrintRequestInQue = false;
	private void onPrintButtonclick(){

		if(MSWisepadDeviceController.getDeviceType() == MSWisepadDeviceController.DeviceType.WISEPOS_PLUS)
		{
			printWppReceipt();
		}else
		{
			printReceipt();
		}
	}

	private class TaskShowMultiplePairedDevices extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {
			return "";
		}

		@Override
		protected void onPostExecute(String result) {

			final Dialog dlgPairedDevices = Constants.showAppCustomDialog(mPreauthCompletionApprovalView, getResources().getString(R.string.mswisepadview_select_wisepad));
			dlgPairedDevices.setCancelable(true);

			final ArrayList<String> deviceNameList = new ArrayList<String>();

			for (int i = 0; i < pairedDevicesFound.size(); ++i) {
				deviceNameList.add(pairedDevicesFound.get(i).getName());
			}

			ListView appListView = (ListView) dlgPairedDevices.findViewById(R.id.customapplicationdlg_LST_applications);
			final ArrayList<String> arrSelectedItem = new ArrayList<String>();

			for(int ictr =0 ; ictr< deviceNameList.size(); ictr++) {

				if(ictr == 0){
					arrSelectedItem.add("yes");
				}
				else {
					arrSelectedItem.add("no");
				}
			}
			appListView.setAdapter(new ListAdapter(mPreauthCompletionApprovalView, deviceNameList));
			appListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
			{

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


					if (mPrinterConnectionService != null)
						mPrinterConnectionService.connectToWisePad(pairedDevicesFound.get(position));


					dlgPairedDevices.dismiss();
				}

			});

			dlgPairedDevices.findViewById(R.id.customapplicationdlg_BTN_cancel).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					dlgPairedDevices.dismiss();
				}
			});

			dlgPairedDevices.show();
		}
	}

}