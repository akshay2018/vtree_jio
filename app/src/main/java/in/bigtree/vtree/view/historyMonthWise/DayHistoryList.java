package in.bigtree.vtree.view.historyMonthWise;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.HistoryData;
import com.mswipetech.wisepad.sdk.data.HistoryResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.ArrayList;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.BaseTitleActivity;

public class DayHistoryList extends BaseTitleActivity {
    public final static String log_tab = "DayHistoryList=>";

    CustomProgressDialog mProgressActivity = null;
    ListView lstHistory = null;


    ArrayList<HistoryData> listData = new ArrayList<>();
    int totalRecord = 0;


    String mSaleType = "";
    String mDate = "";
    String mTrnxdate ="";
    SharedVariable applicationData = null;

    // this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
    private long lastRequestTime = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.card_details_historylist);
        applicationData = (SharedVariable) getApplicationContext();
        mSaleType  = getIntent().getStringExtra("trnxtype");
        mDate = getIntent().getStringExtra("date");

        mTrnxdate = Constants.getDateWithFormate(mDate, "dd-MMM-yyyy", "yyyy-MM-dd");

        if (SharedVariable.IS_DEBUGGING_ON)
            Logs.v(SharedVariable.packName, "dates  " + mDate+" "+mTrnxdate, true, true);

        initViews();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void initViews() {
        ((TextView) findViewById(R.id.topbar_LBL_heading)).setText("History");
        ((TextView) findViewById(R.id.topbar_LBL_heading)).setTypeface(applicationData.font);



        lstHistory = (ListView) findViewById(R.id.cardhistory_LST_cardhistory);
        lstHistory.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                // TODO Auto-generated method stub

                if (mSaleType.equalsIgnoreCase("banksale")||mSaleType.equalsIgnoreCase("cashsale"))
                {
                    Intent intent = new Intent(DayHistoryList.this, CashHistoryDetailView.class);
                    intent.putExtra("curRecord", position + 1);
                    intent.putExtra("totalRecord", listData.size());
                    intent.putExtra("listData", listData);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(DayHistoryList.this, CardHistoryDetailView.class);
                    intent.putExtra("curRecord", position + 1);
                    intent.putExtra("totalRecord", listData.size());
                    intent.putExtra("listData", listData);
                    startActivity(intent);
                }


            }
        });

        getCardHistory();


    }

    public void ShowHistoryData(ArrayList<HistoryData> historyData) {
    	

        totalRecord = historyData.size();



        if (totalRecord != 0) {
        	

            if (listData.size()>0) {
                lstHistory.setAdapter(new CardHistoryAdapter(this, listData));
            } else {
                final Dialog dialog = Constants.showDialog(DayHistoryList.this, Constants.CARDHISTORYLIST_DIALOG_MSG, "Unable to show the history data, please contact support.",
                        Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                 Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                 yes.setOnClickListener(new OnClickListener() {

                     public void onClick(View v) {
                         dialog.dismiss();

                         finish();

                     }
                 });
                 dialog.show();

            }
        }
    }




    public void getCardHistory() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(DayHistoryList.this,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
                            null);

            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

            MSWisepadController.SaleType mTrnxType = null;
            if (mSaleType.equalsIgnoreCase("cardsale"))
            {
                mTrnxType = MSWisepadController.SaleType.card;
            }
            else if (mSaleType.equalsIgnoreCase("qrsale"))
            {
                mTrnxType = MSWisepadController.SaleType.qrsale;

            }else if (mSaleType.equalsIgnoreCase("linksale"))
            {
                mTrnxType =  MSWisepadController.SaleType.linksale;
            }
            else if (mSaleType.equalsIgnoreCase("banksale"))
            {
                mTrnxType = MSWisepadController.SaleType.bank;

            }else if (mSaleType.equalsIgnoreCase("cashsale"))
            {
                mTrnxType =  MSWisepadController.SaleType.cash;
            }

            wisepadController.getHistoryDetails(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    mTrnxType,
                    mTrnxdate,
                    new MSWisepadControllerResponseListenerObserver());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(DayHistoryList.this, "Fetching History...");
            mProgressActivity.show();

        }
        catch (Exception e)
        {
            Constants.showDialog(this, "History", "unable to process card history.");
            e.printStackTrace();
        }

     }



    /**
     * MswipeWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

     */
    class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {

            HistoryResponseData cardHistoryDetailsResponseData = (HistoryResponseData) aMSDataStore;
            mProgressActivity.dismiss();

            if (cardHistoryDetailsResponseData.getResponseStatus())
            {
                listData = cardHistoryDetailsResponseData.getHistoryData();
                ShowHistoryData(listData);
            }
            else {

                Constants.showDialog(DayHistoryList.this, "History", cardHistoryDetailsResponseData.getResponseFailureReason());
            }
        }
    }




    public class CardHistoryAdapter extends BaseAdapter {
    	ArrayList<HistoryData> listData = null;
        Context context;

        public CardHistoryAdapter(Context context, ArrayList<HistoryData> listData) {
            this.listData = listData;
            this.context = context;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.cardhistorylistdata, null);
            }
            HistoryData cardSaleDetails = (HistoryData) listData.get(position);

              //TextView lstFourDigits= (TextView)convertView.findViewById(R.id.cardhistorylist_LBL_lastfourdigits);
            //lstFourDigits.setText(cardSaleDetails.CardFourDigits);

            TextView lblamt = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_lblamount);
            lblamt.setText(SharedVariable.Currency_Code);
            lblamt.setTypeface(applicationData.font);


            TextView amt = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_amount);
            amt.setText(cardSaleDetails.TrxAmount.toString());
            amt.setTypeface(applicationData.font);


            TextView date = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_date);
            date.setText(cardSaleDetails.trxDate);
            date.setTypeface(applicationData.font);


            TextView type = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_cardtype);
            type.setText(cardSaleDetails.trxType);
            type.setTypeface(applicationData.font);

            if (!cardSaleDetails.trxType.toString().equalsIgnoreCase("void")) {
                type.setTextColor(Color.rgb(0, 176, 80));

            } else {
                type.setTextColor(Color.rgb(255, 0, 0));
            }
            
            return convertView;
        }

    }
}
