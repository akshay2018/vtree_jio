package in.bigtree.vtree.view.cardpaymentactivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.LastTransactionResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.ResendReceiptResponsedata;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.MenuView;


public class LastTransactionActivity extends Activity
{

	private LastTransactionResponseData mLastTransactionResponseData = new LastTransactionResponseData();
	CustomProgressDialog mProgressActivity = null;

	/**
	 * Called when the activity is first created.
	 */
	SharedVariable applicationData = null;


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.lasttransaction_view);

		//mDrawerRequired = true;

		applicationData = SharedVariable.getApplicationDataSharedInstance();

		((TextView)findViewById(R.id.topbar_LBL_heading)).setText(getResources().getString(R.string.lasttransactionactivity_last_tx));
		((ImageView)findViewById(R.id.topbar_IMG_position1)).setVisibility(View.GONE);

		getLastTransaction();

	}


	@Override
	public void onStart() {
		super.onStart();

		/*mLINTopBarCancel.setVisibility(View.VISIBLE);
		mLINTopBarMenu.setVisibility(View.GONE);*/

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	private void initViews()
	{

		try
		{

			TextView lblStatus = (TextView) findViewById(R.id.lasttxstatus_TXT_status);
			String statusMsg = mLastTransactionResponseData.getTrxStatus().toLowerCase();
			lblStatus.setText(statusMsg); //TrxStatus

			if(statusMsg.equalsIgnoreCase("approved"))
				lblStatus.setTextColor(getResources().getColor(R.color.green));
			else
				lblStatus.setTextColor(getResources().getColor(R.color.red));


			TextView lblamt = (TextView) findViewById(R.id.lasttxstatus_TXT_totalamount);
			lblamt.setText(SharedVariable.mCurrency+" "+mLastTransactionResponseData.getTrxAmount());//TrxAmount

			TextView lblTxtDateTime = (TextView) findViewById(R.id.lasttxstatus_TXT_date);
			lblTxtDateTime.setText(mLastTransactionResponseData.getTrxDate().toLowerCase()); //TrxDate

			TextView lblType = (TextView) findViewById(R.id.lasttxstatus_TXT_type);
			String mTrxType = mLastTransactionResponseData.getTrxType().toLowerCase();
			lblType.setText(mTrxType.toLowerCase());//TrxType

			TextView lblName = (TextView) findViewById(R.id.lasttxstatus_TXT_cardholder);
			lblName.setText(mLastTransactionResponseData.getCardHolderName().toLowerCase());//CardHolderName

			TextView lblNum = (TextView) findViewById(R.id.lasttxstatus_TXT_last4digits);
			lblNum.setText(mLastTransactionResponseData.getCardLastFourDigits().toLowerCase());//CardLastFourDigit

			TextView lblAuthNo = (TextView) findViewById(R.id.lasttxstatus_TXT_authorisingcode);
			lblAuthNo.setText(mLastTransactionResponseData.getAuthNo().toLowerCase());//AuthNo

			TextView lblRRNo = (TextView) findViewById(R.id.lasttxstatus_TXT_rrno);
			lblRRNo.setText(mLastTransactionResponseData.getRRNo().toLowerCase());//RRNo

			TextView lblStan = (TextView) findViewById(R.id.lasttxstatus_TXT_standid);
			lblStan.setText(mLastTransactionResponseData.getStanNo().toLowerCase());//StanNo

			TextView lblVoucher = (TextView) findViewById(R.id.lasttxstatus_TXT_voucher);
			lblVoucher.setText(mLastTransactionResponseData.getVoucherNo().toLowerCase());//VoucherNo

			TextView lbltmsg = (TextView) findViewById(R.id.lasttxstatus_TXT_txmsg);
			lbltmsg.setText(mLastTransactionResponseData.getTerminalMessage().toLowerCase());//TerminalMessage


			TextView lblnotes = (TextView) findViewById(R.id.lasttxstatus_TXT_notes);
			lblnotes.setText(mLastTransactionResponseData.getTrxNotes().toLowerCase());//txtnotes

			RelativeLayout Resend = (RelativeLayout) findViewById(R.id.lasttxstatus_RLT_resendrecipt);
			Resend.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					callResendReceipt();
				}
			});


		} catch (Exception ex) {
			ex.printStackTrace();

		}
	}

	private void callResendReceipt(){

		final Dialog dialog = Constants.showDialog(LastTransactionActivity.this, getString(R.string.lasttransaction_view_status),
				getString(R.string.lasttransactionactivity_would_you_like_to_resend_the_receipt_for_the_selected_transaction),
				Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_CONFIRMATION);

		Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
		yes.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				dialog.dismiss();
				resendReceipt();

			}
		});

		Button no = (Button) dialog.findViewById(R.id.customdlg_BTN_no);
		no.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();

			}
		});

		dialog.show();

	}


	/**
	 * @description
	 *     We are handling backbutton manually based on screen position.
	 * @param keyCode
	 * @param event
	 * @return
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{

			finish();
			Intent intent = new Intent(LastTransactionActivity.this, MenuView.class);
			startActivity(intent);

			return true;

		} else {

			return super.onKeyDown(keyCode, event);
		}
	}


	private void getLastTransaction(){

		try {

			final MSWisepadController wisepadController = MSWisepadController.
					getSharedMSWisepadController(LastTransactionActivity.this,
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
							null);

			MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

			wisepadController.getLastTransactionDetails(
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
					new MSWisepadControllerResponseListenerObserver());

			mProgressActivity = new CustomProgressDialog(LastTransactionActivity.this, getString(R.string.processing));
			mProgressActivity.show();

		}
		catch (Exception e)
		{
			Constants.showDialog(this,getResources().getString(R.string.lasttransaction_view_status),getResources().getString(R.string.lasttransactionactivity_unable_to_process_last_transaction));
			e.printStackTrace();
		}
	}


	/**
	 * MswipeWisepadControllerResponseListenerObserver
	 * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

	 */
	class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener
	{

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
		 * need be type cast back to LastTransactionResponseData to access the  response data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{


			mLastTransactionResponseData = (LastTransactionResponseData) aMSDataStore;

			boolean responseStatus = mLastTransactionResponseData.getResponseStatus();
			mProgressActivity.dismiss();

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, " The lastTransactionResponseData is " + mLastTransactionResponseData.getTrxAmount(), true, true);


			if (!responseStatus)
			{


				final Dialog dialog = Constants.showDialog(LastTransactionActivity.this, getString(R.string.lasttransactionactivity_last_tx), mLastTransactionResponseData.getResponseFailureReason()
						, Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						dialog.dismiss();
						doneWithLastTrx();

					}
				});
				dialog.show();
			}
			else if (responseStatus)
			{

				initViews();

			}
		}
	}


	private void resendReceipt(){

		try {

			final MSWisepadController wisepadController = MSWisepadController.
					getSharedMSWisepadController(LastTransactionActivity.this,
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
							null);

			MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

			wisepadController.processResendReceipt(
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
					mLastTransactionResponseData.getTrxAmount(),
					mLastTransactionResponseData.getCardLastFourDigits(),
					mLastTransactionResponseData.getStanNo(),
					new MSWisepadControllerResendReceipResponseListenerObserver());

			mProgressActivity = new CustomProgressDialog(LastTransactionActivity.this, getString(R.string.processing));
			mProgressActivity.show();

		}
		catch (Exception e)
		{
			Constants.showDialog(this,getResources().getString(R.string.lasttransaction_view_status),getResources().getString(R.string.lasttransactionactivity_unable_to_process_last_transaction));
			e.printStackTrace();
		}
	}

	/**
	 * MSWisepadControllerResendReceipResponseListenerObserver
	 * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

	 */

	class MSWisepadControllerResendReceipResponseListenerObserver implements MSWisepadControllerResponseListener
	{

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
		 * need be type cast back to LastTransactionResponseData to access the  response data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{


			ResendReceiptResponsedata resendReceiptResponsedata = (ResendReceiptResponsedata) aMSDataStore;

			boolean responseStatus = resendReceiptResponsedata.getResponseStatus();
			mProgressActivity.dismiss();

			if (!responseStatus)
			{


				final Dialog dialog = Constants.showDialog(LastTransactionActivity.this, getString(R.string.lasttransactionactivity_last_tx), resendReceiptResponsedata.getResponseFailureReason()
						, Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						dialog.dismiss();
						doneWithLastTrx();

					}
				});
				dialog.show();
			}
			else if (responseStatus)
			{

				final Dialog dialog = Constants.showDialog(LastTransactionActivity.this, getString(R.string.lasttransactionactivity_last_tx), resendReceiptResponsedata.getResponseSuccessMessage()
						, Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						dialog.dismiss();
						doneWithLastTrx();

					}
				});
				dialog.show();
			}
		}
	}


	public void doneWithLastTrx() {

		finish();

	}
}
