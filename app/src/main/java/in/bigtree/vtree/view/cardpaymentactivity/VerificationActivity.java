package in.bigtree.vtree.view.cardpaymentactivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.InvoiceTransactionDetailsResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;


public class VerificationActivity extends Activity {


	private EditText mETOrderNo = null;
	private Button mBTNVerifyTrx = null;

	private CustomProgressDialog mProgressActivity = null;
	private long lastRequestTime = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_verification);

		mETOrderNo = (EditText) findViewById(R.id.verify_ET_receipt_no);

		mBTNVerifyTrx = (Button) findViewById(R.id.verify_BTN_verity_trx);

		mBTNVerifyTrx.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				verifyTrxByOrderId(mETOrderNo.getText().toString());
			}
		});

	}

	private void verifyTrxByOrderId(String aInvoiceNo) {

		try
		{

			final MSWisepadController wisepadController = MSWisepadController.
					getSharedMSWisepadController(VerificationActivity.this ,
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

			MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());


			if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

				String clientCode = "";
				String userId = "";
				String password = "";

				if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment()
						== MSWisepadController.GATEWAY_ENVIRONMENT.LABS)
				{

					/*clientCode = "LNTFINANCIAL";
					userId = "FINANCIAL@msD";
					password = "FINANCIAL~msD@17";*/


					clientCode = "FLIPKART";
					userId = "FLIPKART@SOL";
					password = "FLIPKART~DsD@250418";

				}
				else{

					clientCode = "";
					userId = "";
					password = "";
				}

				wisepadController.verifyTransactionStatus(clientCode, userId, password, aInvoiceNo,
						new MSWisepadControllerResponseListenerObserver());

				mProgressActivity = new CustomProgressDialog(VerificationActivity.this, "Processing...");
				mProgressActivity.show();
			}

			lastRequestTime = System.currentTimeMillis();


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * MSWisepadControllerResponseListenerObserver
	 * The mswipe overridden class  observer which listens to the responses for the mswipe sdk function requests
	 */
	class MSWisepadControllerResponseListenerObserver implements  MSWisepadControllerResponseListener
	{
		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to ChangePasswordResponseData, so this
		 * need be type cast back to ChangePasswordResponseData to access response details for the requested password change
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{
			if(mProgressActivity != null)
			{
				mProgressActivity.dismiss();
				mProgressActivity = null;
			}

			InvoiceTransactionDetailsResponseData responseData = (InvoiceTransactionDetailsResponseData) aMSDataStore;
			boolean responseStatus = responseData.getResponseStatus();

			if (!responseStatus)
			{

				Constants.showDialog(VerificationActivity.this, "verify trx", responseData.getResponseFailureReason());

			}
			else if (responseStatus){

				final Dialog dlg = Constants.showDialog(VerificationActivity.this, "verify trx",
						responseData.getResponseSuccessMessage()+"\n"+"Invoice No: "+responseData.getInvoiceNO(),
						Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button btnOk = (Button) dlg.findViewById(R.id.customdlg_BTN_yes);
				btnOk.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dlg.dismiss();

						setResult(RESULT_OK);
						finish();

					}
				});
				dlg.show();

			}
			else{

				Constants.showDialog(VerificationActivity.this, "verify trx", "Invalid response from Mswipe server, please contact support.");
			}
		}
	}
}

