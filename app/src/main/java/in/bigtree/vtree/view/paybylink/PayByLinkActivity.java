package in.bigtree.vtree.view.paybylink;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.data.TransactionData;
import in.bigtree.vtree.view.paybylink.fragment.ApprovedTransactionFragment;
import in.bigtree.vtree.view.paybylink.fragment.MobileNoFragment;
import in.bigtree.vtree.view.paybylink.fragment.PaymentStatusListFragment;
import in.bigtree.vtree.view.paybylink.fragment.SelectionListFragment;
import in.bigtree.vtree.view.paybylink.fragment.bank.PayByLinkBankSelectionFragment;
import in.bigtree.vtree.view.paybylink.fragment.bank.PayByLinkBanksDetailsFragment;
import in.bigtree.vtree.view.paybylink.fragment.bank.PayByLinkOTPFragment;
import in.bigtree.vtree.view.paybylink.fragment.bank.PayByLinkTermsandCondFragment;

public class PayByLinkActivity extends android.support.v4.app.FragmentActivity {

    public PayByLinkEnum.PayByLinkScreens mPayByLinkScreens;

    public TransactionData mTransactionData;

    public String mTrnxId = "";

    public String mBankCode = "";
    public String mLocationCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.voidsale_fragment);

        mTransactionData = new TransactionData();

        if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getPayByLinkBank().length() > 0)
        {
            showScreen(PayByLinkEnum.PayByLinkScreens.PayByLink_Screen_Selection);

        }else
        {
            showScreen(PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_BANK_Selection);
        }

        initViews();
    }

    private void initViews(){

        ((TextView)findViewById(R.id.topbar_LBL_heading)).setText("payby link");
    }

    public void showScreen(PayByLinkEnum.PayByLinkScreens payByLinkScreens)
    {

        // TODO Auto-generated method stub
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        android.support.v4.app.Fragment fragment = null;

        switch (payByLinkScreens)
        {

            case PayByLinkScreens_BANK_Selection:

                fragment = new PayByLinkBankSelectionFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                break;

            case PayByLinkScreens_BANK_Details:

                fragment = new PayByLinkBanksDetailsFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                break;

            case PayByLinkScreens_Terms_Conditions:

                fragment = new PayByLinkTermsandCondFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                break;

            case PayByLinkScreens_OTP:

                fragment = new PayByLinkOTPFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                break;

            case PayByLink_Screen_Selection:

                fragment = new SelectionListFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                break;

            case PayByLink_Screen_Mobile_Number:

                fragment = new MobileNoFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                break;

            case PayByLink_Screen_Aproved_Transaction:

                fragment = new ApprovedTransactionFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case PayByLink_Screen_PaymentSatus:

                fragment = new PaymentStatusListFragment();
                fragmentTransaction.replace(R.id.voidsale_fragmentcontainer,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                break;

        }
        mPayByLinkScreens = payByLinkScreens;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mPayByLinkScreens == PayByLinkEnum.PayByLinkScreens.PayByLink_Screen_Selection
                    ||mPayByLinkScreens == PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_BANK_Selection)
            {
                finish();

            }else
            {
                moveToPrevious();
            }
            return true;

        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void moveToPrevious() {

        if (mPayByLinkScreens == PayByLinkEnum.PayByLinkScreens.PayByLink_Screen_Mobile_Number || mPayByLinkScreens == PayByLinkEnum.PayByLinkScreens.PayByLink_Screen_PaymentSatus) {

            showScreen(PayByLinkEnum.PayByLinkScreens.PayByLink_Screen_Selection);
        }
        else if (mPayByLinkScreens == PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_BANK_Details) {

            showScreen(PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_BANK_Selection);
        }
        else if (mPayByLinkScreens == PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_Terms_Conditions) {

            showScreen(PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_BANK_Details);
        }
        else if (mPayByLinkScreens == PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_OTP) {

            showScreen(PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_Terms_Conditions);
        }

    }

}
