package in.bigtree.vtree.view.cardpaymentactivity.cardsalearrintegration;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.view.MSAARHandlerActivity;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.MenuView;

public class ARRActivityPaymentView extends FragmentActivity
{
	//fields for card sale amount details screen,
	EditText mTxtUserName = null;
	EditText mTxtPassword = null;
	EditText mTxtCreditAmount = null;
	EditText mTxtFirstSixDigits = null;
	EditText mTxtPhoneNum = null;
	EditText mTxtReceipt = null;
	EditText mTxtNotes = null;

	SharedVariable applicationData = null;

	boolean isSaleWithCash = false;
	boolean isPreAuth = false;
	boolean isEmiSale = false;

	private static final String MS_CARDSALE_ACTIVITY_INTENT_ACTION = "mswipe.wisepad.sdk.CardSaleAction";
	private static final String MS_CASHATPOS_ACTIVITY_INTENT_ACTION = "mswipe.wisepad.sdk.CashAtPosAction";
	private static final String MS_EMISALE_ACTIVITY_INTENT_ACTION = "mswipe.wisepad.sdk.EmiAction";
	private static final String MS_PREAUTH_ACTIVITY_INTENT_ACTION = "mswipe.wisepad.sdk.PreauthAction";
	public static final int MS_CARDSALE_ACTIVITY_REQUEST_CODE = 1003;
	public static final int MS_CASHATPOS_ACTIVITY_REQUEST_CODE = 1004;
	public static final int MS_EMISALE_ACTIVITY_REQUEST_CODE = 1005;
	public static final int MS_PREAUTH_ACTIVITY_REQUEST_CODE = 1006;

	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState
	 */

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mswipe_payment_view);

		((RelativeLayout) findViewById(R.id.top_bar_REL_content)).setBackgroundColor(getResources().getColor(R.color.topbar_activity_background));


		applicationData = SharedVariable.getApplicationDataSharedInstance();

		initViews();
	}

	/**
	 *@description
	 *      All fields intilising here.
	 *
	 */
	private void initViews()
	{
		isSaleWithCash = getIntent().getBooleanExtra("salewithcash", false);
		isPreAuth = getIntent().getBooleanExtra("preauthsale", false);
		isEmiSale = getIntent().getBooleanExtra("emisale", false);

		TextView txtHeading = ((TextView) findViewById(R.id.topbar_LBL_heading));

	    if(isSaleWithCash){
			txtHeading.setText(getResources().getString(R.string.cash_at_pos));
		}
		else if(isPreAuth){
			txtHeading.setText(getResources().getString(R.string.preauth));
		}
		else if(isEmiSale){
			txtHeading.setText(getResources().getString(R.string.emi));
			((LinearLayout) findViewById(R.id.payment_LNR_firstsidigits)).setVisibility(View.VISIBLE);

		}
		else {
			txtHeading.setText(getResources().getString(R.string.card_sale));
		}

		//The screen are for the amount

		mTxtUserName = (EditText) findViewById(R.id.payment_TXT_username);
		mTxtPassword = (EditText) findViewById(R.id.payment_TXT_password);
		mTxtCreditAmount = (EditText) findViewById(R.id.payment_TXT_amount);
		mTxtFirstSixDigits = (EditText) findViewById(R.id.payment_TXT_firstsidigits);
		mTxtPhoneNum = (EditText) findViewById(R.id.payment_TXT_mobileno);
		mTxtReceipt = (EditText) findViewById(R.id.payment_TXT_receipt);
		mTxtNotes = (EditText) findViewById(R.id.payment_TXT_notes);

		mTxtUserName.requestFocus();


		Button btnAmtNext = (Button) findViewById(R.id.payment_BTN_amt_next);
		btnAmtNext.setBackgroundColor(getResources().getColor(R.color.topbar_activity_background));
		btnAmtNext.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				if(imm != null)
					imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

				double amount = 0;

				try
				{
					if (mTxtCreditAmount.length() > 0)
						amount = Double.parseDouble(removeChar(mTxtCreditAmount.getText().toString(),','));
				}
				catch (Exception ex) {
					amount = 0;
				}

				if (mTxtUserName.getText().toString().trim().startsWith("0")) {


					showDialog("The userid cannot start with 0.");
					mTxtUserName.requestFocus();
					return;
				}
				else if(mTxtUserName.getText().toString().length() < 10)
				{
					showDialog("enter valid userid");
					mTxtUserName.requestFocus();
					return;
				}
				else if(mTxtPassword.getText().toString().length() == 0)
				{
					showDialog("enter a valid password");
					mTxtPassword.requestFocus();
					return;
				}
				else if (amount < 1)
				{
					showDialog("invalid amount! minimum amount should be inr 1.00 to proceed.");
					return;
				}
				else if (mTxtPhoneNum.getText().toString().trim().length() != 10) {

					showDialog("required length of the mobile number is 10 digits.");
					mTxtPhoneNum.requestFocus();
					return;
				}
				else if (mTxtPhoneNum.getText().toString().trim().startsWith("0")) {

					showDialog("the mobile number cannot start with 0.");
					mTxtPhoneNum.requestFocus();
					return;

				}
				else if (isEmiSale && mTxtFirstSixDigits.getText().toString().trim().length() == 0) {

					showDialog("enter first six digits");
					mTxtFirstSixDigits.requestFocus();
					return;

				}
				else if (isEmiSale && mTxtFirstSixDigits.getText().toString().trim().length() < 6) {

					showDialog("enter valid first six digits");
					mTxtFirstSixDigits.requestFocus();
					return;

				}

				if(isSaleWithCash)
				{
					processCashAtPOS();
				}
				else if(isEmiSale)
				{
					processEMISale();
				}
				else if(isPreAuth){
					processPreauthSale();
				}else
				{
					processCardSale();
				}

			}
		});
	}

	private void processCardSale(){

		Intent intent = new Intent(ARRActivityPaymentView.this, MSAARHandlerActivity.class);
		intent.setType(MS_CARDSALE_ACTIVITY_INTENT_ACTION);

        //Intent intent = new Intent();
		//intent.setAction(MS_CARDSALE_ACTIVITY_INTENT_ACTION);
		intent.putExtra("username", mTxtUserName.getText().toString().trim());
		intent.putExtra("password", mTxtPassword.getText().toString().trim());

		if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment() == MSWisepadController.GATEWAY_ENVIRONMENT.LABS)
			intent.putExtra("production", false);
		else
			intent.putExtra("production", true);

		double amt = Double.parseDouble(mTxtCreditAmount.getText().toString().trim());
        String totalAmt = String.format("%.2f",amt);

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName,  "totalAmt" + totalAmt, true, true);


		intent.putExtra("Amount", totalAmt);
		intent.putExtra("MobileNumber", mTxtPhoneNum.getText().toString().trim());
		intent.putExtra("Reciept", mTxtReceipt.getText().toString().trim());
		intent.putExtra("Notes", mTxtNotes.getText().toString().trim());
		intent.putExtra("MailId", "");
		intent.putExtra("extra1", "");
		intent.putExtra("extra2", "");
		intent.putExtra("extra3", "");
		intent.putExtra("extra2", "");
		intent.putExtra("orientation", "auto");
		intent.putExtra("isSignatureRequired", AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired());
		intent.putExtra("isPrinterSupported", AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrinterSupportRequired());
		intent.putExtra("isPrintSignatureOnReceipt", AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrintSignatureRequired());
		startActivityForResult(intent, MS_CARDSALE_ACTIVITY_REQUEST_CODE);
	}

	private void processCashAtPOS(){

		//Intent intent = new Intent();
		//intent.setAction(MS_CASHATPOS_ACTIVITY_INTENT_ACTION);
		Intent intent = new Intent(ARRActivityPaymentView.this, MSAARHandlerActivity.class);
		intent.setType(MS_CASHATPOS_ACTIVITY_INTENT_ACTION);
		intent.putExtra("username", mTxtUserName.getText().toString().trim());
		intent.putExtra("password", mTxtPassword.getText().toString().trim());

		if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment() == MSWisepadController.GATEWAY_ENVIRONMENT.LABS)
			intent.putExtra("production", false);
		else
			intent.putExtra("production", true);

		double amt = Double.parseDouble(mTxtCreditAmount.getText().toString().trim());
		String totalAmt = String.format("%.2f",amt);

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName,  "totalAmt" + totalAmt, true, true);


		intent.putExtra("cashAmount", totalAmt);
		intent.putExtra("MobileNumber", mTxtPhoneNum.getText().toString().trim());
		intent.putExtra("Reciept", mTxtReceipt.getText().toString().trim());
		intent.putExtra("Notes", mTxtNotes.getText().toString().trim());
		intent.putExtra("MailId", "");
		intent.putExtra("extra1", "");
		intent.putExtra("extra2", "");
		intent.putExtra("extra3", "");
		intent.putExtra("extra2", "");
		intent.putExtra("orientation", "auto");
		intent.putExtra("isSignatureRequired", AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired());
		intent.putExtra("isPrinterSupported", AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrinterSupportRequired());
		intent.putExtra("isPrintSignatureOnReceipt", AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrintSignatureRequired());
		startActivityForResult(intent, MS_CASHATPOS_ACTIVITY_REQUEST_CODE);
	}

	private void processEMISale(){

		//Intent intent = new Intent();
		//intent.setAction(MS_EMISALE_ACTIVITY_INTENT_ACTION);
		Intent intent = new Intent(ARRActivityPaymentView.this, MSAARHandlerActivity.class);
		intent.setType(MS_EMISALE_ACTIVITY_INTENT_ACTION);
		intent.putExtra("username", mTxtUserName.getText().toString().trim());
		intent.putExtra("password", mTxtPassword.getText().toString().trim());

		if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment() == MSWisepadController.GATEWAY_ENVIRONMENT.LABS)
			intent.putExtra("production", false);
		else
			intent.putExtra("production", true);

		double amt = Double.parseDouble(mTxtCreditAmount.getText().toString().trim());
		String totalAmt = String.format("%.2f",amt);

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName,  "totalAmt" + totalAmt, true, true);


		intent.putExtra("CardFirstSixDigit", mTxtFirstSixDigits.getText().toString());
		intent.putExtra("EmiAmount", totalAmt);
		intent.putExtra("MobileNumber", mTxtPhoneNum.getText().toString().trim());
		intent.putExtra("Reciept", mTxtReceipt.getText().toString().trim());
		intent.putExtra("Notes", mTxtNotes.getText().toString().trim());
		intent.putExtra("MailId", "");
		intent.putExtra("extra1", "");
		intent.putExtra("extra2", "");
		intent.putExtra("extra3", "");
		intent.putExtra("extra2", "");
		intent.putExtra("orientation", "auto");
		intent.putExtra("isSignatureRequired", AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired());
		intent.putExtra("isPrinterSupported", AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrinterSupportRequired());
		intent.putExtra("isPrintSignatureOnReceipt", AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrintSignatureRequired());
		startActivityForResult(intent, MS_EMISALE_ACTIVITY_REQUEST_CODE);
	}

	private void processPreauthSale(){

		//Intent intent = new Intent();
		//intent.setAction(MS_PREAUTH_ACTIVITY_INTENT_ACTION);
		Intent intent = new Intent(ARRActivityPaymentView.this, MSAARHandlerActivity.class);
		intent.setType(MS_PREAUTH_ACTIVITY_INTENT_ACTION);
		intent.putExtra("username", mTxtUserName.getText().toString().trim());
		intent.putExtra("password", mTxtPassword.getText().toString().trim());

		if(AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment() == MSWisepadController.GATEWAY_ENVIRONMENT.LABS)
			intent.putExtra("production", false);
		else
			intent.putExtra("production", true);

		double amt = Double.parseDouble(mTxtCreditAmount.getText().toString().trim());
		String totalAmt = String.format("%.2f",amt);

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName,  "totalAmt" + totalAmt, true, true);


		intent.putExtra("Amount", totalAmt);
		intent.putExtra("MobileNumber", mTxtPhoneNum.getText().toString().trim());
		intent.putExtra("Reciept", mTxtReceipt.getText().toString().trim());
		intent.putExtra("Notes", mTxtNotes.getText().toString().trim());
		intent.putExtra("MailId", "");
		intent.putExtra("extra1", "");
		intent.putExtra("extra2", "");
		intent.putExtra("extra3", "");
		intent.putExtra("extra2", "");
		intent.putExtra("orientation", "auto");
		intent.putExtra("isSignatureRequired", AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired());
		intent.putExtra("isPrinterSupported", AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrinterSupportRequired());
		intent.putExtra("isPrintSignatureOnReceipt", AppSharedPrefrences.getAppSharedPrefrencesInstace().isPrintSignatureRequired());
		startActivityForResult(intent, MS_PREAUTH_ACTIVITY_REQUEST_CODE);
	}


	private void showDialog(String message){

		Constants.showActivityDialog(this, "", message);

		/*AlertDialog.Builder builder1 = new AlertDialog.Builder(this, R.style.MswipeDialogTheme);
		builder1.setMessage(message);
		builder1.setCancelable(false);

		builder1.setPositiveButton(
			"Ok",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();

				}
			});

		AlertDialog alert11 = builder1.create();
		alert11.show();*/

	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == MS_CARDSALE_ACTIVITY_REQUEST_CODE)
		{

			boolean status = data.getBooleanExtra("status", false);
			String statusMessage = data.getStringExtra("statusMessage");

			if(resultCode == RESULT_OK)
			{
				if(AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired()){
					finish();
					Intent intent = new Intent(ARRActivityPaymentView.this, MenuView.class);
					startActivity(intent);
				}else
				{
					showapproveDialog(Boolean.toString(status),data.getExtras().getString("AuthCode"),
							data.getExtras().getString("RRNo"),statusMessage);
				}
			}
			else {

				/*showapproveDialog(Boolean.toString(status),"",
						"",statusMessage);*/

				Constants.showActivityDialog(this, "", statusMessage);
			}
		}else if(requestCode == MS_CASHATPOS_ACTIVITY_REQUEST_CODE )
        {

            boolean status = data.getBooleanExtra("status", false);
            String statusMessage = data.getStringExtra("statusMessage");

            if(resultCode == RESULT_OK)
            {
				if(AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired()){
					finish();
					Intent intent = new Intent(ARRActivityPaymentView.this, MenuView.class);
					startActivity(intent);
				}else
				{
					showapproveDialog(Boolean.toString(status),data.getExtras().getString("AuthCode"),
							data.getExtras().getString("RRNo"),statusMessage);
				}

            }
            else {

               /* showapproveDialog(Boolean.toString(status),"",
                        "",statusMessage);*/

				Constants.showActivityDialog(this, "", statusMessage);

            }
        }else if(requestCode == MS_EMISALE_ACTIVITY_REQUEST_CODE)
        {

            boolean status = data.getBooleanExtra("status", false);
            String statusMessage = data.getStringExtra("statusMessage");

            if(resultCode == RESULT_OK)
            {

				if(AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired()){
					finish();
					Intent intent = new Intent(ARRActivityPaymentView.this, MenuView.class);
					startActivity(intent);
				}else
				{
					showapproveDialog(Boolean.toString(status),data.getExtras().getString("AuthCode"),
							data.getExtras().getString("RRNo"),statusMessage);
				}

            }
            else {

               /* showapproveDialog(Boolean.toString(status),"",
                        "",statusMessage);*/

				Constants.showActivityDialog(this, "", statusMessage);

            }
        }
		else if(requestCode == MS_PREAUTH_ACTIVITY_REQUEST_CODE)
		{

			boolean status = data.getBooleanExtra("status", false);
			String statusMessage = data.getStringExtra("statusMessage");

			if(resultCode == RESULT_OK)
			{

				if(AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired()){
					finish();
					Intent intent = new Intent(ARRActivityPaymentView.this, MenuView.class);
					startActivity(intent);
				}else
				{
					showapproveDialog(Boolean.toString(status),data.getExtras().getString("AuthCode"),
							data.getExtras().getString("RRNo"),statusMessage);
				}
			}
			else {

				Constants.showActivityDialog(this, "", statusMessage);

				/*showapproveDialog(Boolean.toString(status),"",
						"",statusMessage);*/

			}
		}
	}

	/**
	 * @description
	 *        We are removing specific character form original string.
	 * @param s original string
	 * @param c specific character.
	 * @return
	 */
	public String removeChar(String s, char c) {

		String r = "";

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c)
				r += s.charAt(i);
		}

		return r;
	}
	public void showapproveDialog(String status, String authcode, String rrno, String reason)
	{

		final Dialog dialog = new Dialog(ARRActivityPaymentView.this, R.style.styleCustDlg);
		dialog.setContentView(R.layout.cardsale_status_customdlg);
		dialog.setCanceledOnTouchOutside(false);

		dialog.setCancelable(true);


		TextView txtstatusmsg = (TextView) dialog.findViewById(R.id.customdlg_Txt_status);
		txtstatusmsg.setText(status);

		TextView txtauthcode = (TextView) dialog.findViewById(R.id.customdlg_Txt_authcode);
		txtauthcode.setText(authcode);

		TextView txtrrno = (TextView) dialog.findViewById(R.id.customdlg_Txt_rrno);
		txtrrno.setText(rrno);

		TextView txtreason = (TextView) dialog.findViewById(R.id.customdlg_Txt_reason);
		txtreason.setText(reason);

		Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
		yes.setBackgroundColor(getResources().getColor(R.color.topbar_activity_background));
		yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				finish();
				Intent intent = new Intent(ARRActivityPaymentView.this, MenuView.class);
				startActivity(intent);
			}
		});

		dialog.show();
	}

}