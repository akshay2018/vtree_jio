package in.bigtree.vtree.view.voidsale.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.OTPResponseData;
import com.mswipetech.wisepad.sdk.data.VoidTransactionResponseData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.voidsale.VoidSaleActivity;
import in.bigtree.vtree.view.voidsale.VoidSaleEnum;


public class VoidSaleTxtDetailsFragment extends Fragment {
	Context mContext;
	VoidSaleActivity mVoidsaleTxttDetailsView = null;
	SharedVariable applicationData = null;

	// this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
	private long lastRequestTime = 0;

	private CustomProgressDialog mProgressActivity;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub

		mContext = activity;
		mVoidsaleTxttDetailsView = ((VoidSaleActivity) mContext);
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		super.onAttach(activity);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.voidsale_transactiondetailsview,container, false);

		initViews(view);
		return view;
	}


	public void initViews(View view) {
		// TODO Auto-generated method stub

		TextView lblTxDateTime = (TextView)view. findViewById(R.id.voidsaletxtdetails_LBL_TxDateTime);
		lblTxDateTime.setText((mVoidsaleTxttDetailsView.mTransactionData.mDate).toLowerCase());

		TextView lblTxcardno = (TextView)view. findViewById(R.id.voidsaletxtdetails_LBL_last4digits);
		lblTxcardno.setText(mVoidsaleTxttDetailsView.mTransactionData.mLast4Digits);

		TextView lblTxAmtCurrency= (TextView)view. findViewById(R.id.creditsale_LBL_Amtprefix);
		lblTxAmtCurrency.setText(applicationData.mCurrency);
		TextView lblTxAmt = (TextView)view. findViewById(R.id.voidsaletxtdetails_LBL_AmtRs);
		lblTxAmt.setText(mVoidsaleTxttDetailsView.mTransactionData.mBaseAmount);

		TextView lblAuthNo = (TextView) view.findViewById(R.id.voidsaletxtdetails_LBL_AuthNo);
		lblAuthNo.setText(mVoidsaleTxttDetailsView.mTransactionData.mAuthCode);

		TextView lblRRNO = (TextView)view. findViewById(R.id.voidsaletxtdetails_LBL_RRNo);
		lblRRNO.setText(mVoidsaleTxttDetailsView.mTransactionData.mRRNo);


		ImageButton btnTxNext = (ImageButton) view.findViewById(R.id.voidsaletxtdetails_BTN_next);
		btnTxNext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

					if(mVoidsaleTxttDetailsView.mIsVoidWithOTp){

						sendOTP();
					}
					else {

						processCardSaleVoid();
					}

				}

				lastRequestTime = System.currentTimeMillis();


			}
		});
	}

	public boolean processCardSaleVoid() {

		try {

			final MSWisepadController wisepadController = MSWisepadController.
					getSharedMSWisepadController(mVoidsaleTxttDetailsView,
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);


			MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

			wisepadController.processVoidTransaction(
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
					mVoidsaleTxttDetailsView.mTransactionData.mSelectedDate,
					mVoidsaleTxttDetailsView.mTransactionData.mBaseAmount,
					mVoidsaleTxttDetailsView.mTransactionData.mLast4Digits,
					mVoidsaleTxttDetailsView.mTransactionData.mStandId,
					mVoidsaleTxttDetailsView.mTransactionData.mVoucherNo,
					"",
					new MSWisepadControllerResponseListenerObserver());

			mProgressActivity = null;
			mProgressActivity = new CustomProgressDialog(mVoidsaleTxttDetailsView, getString(R.string.void_sale));
			mProgressActivity.show();

		} catch (Exception ex) {

			Constants.showDialog(mVoidsaleTxttDetailsView, getString(R.string.void_sale),
					getString(R.string.voidsaleactivity_the_void_sale_transaction_was_not_processed_successfully_please_try_again));

		}

		return true;
	}

	/**
	 * MswipeWisepadControllerResponseListenerObserver
	 * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

	 */
	class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener
	{

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
		 * need be type cast back to LastTransactionResponseData to access the  response data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{
			if (mProgressActivity != null)
				mProgressActivity.dismiss();

			VoidTransactionResponseData voidTransactionResponseData = (VoidTransactionResponseData) aMSDataStore;

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, "voidTransactionResponseData.getResponseStatus() " + voidTransactionResponseData.getResponseStatus() , true, true);

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, "voidTransactionResponseData.getResponseFailureReason() " + voidTransactionResponseData.getResponseFailureReason() , true, true);

			if(voidTransactionResponseData.getResponseStatus())
			{

				mVoidsaleTxttDetailsView.mTransactionData.mDate = voidTransactionResponseData.getTrxDate();
				mVoidsaleTxttDetailsView.showScreen(VoidSaleEnum.VoidSaleScreens.VoidSaleScreens_TXT_APPROVAL_DETAILS);

			}
			else{

				final Dialog dialog = Constants.showDialog(mVoidsaleTxttDetailsView, getString(R.string.void_sale), voidTransactionResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {

						dialog.dismiss();

					}
				});
				dialog.show();

			}
		}
	}



	public boolean sendOTP() {

		try {


			final MSWisepadController wisepadController = MSWisepadController.
					getSharedMSWisepadController(mVoidsaleTxttDetailsView,
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);


			MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

			wisepadController.initiateVoidTransactionWithOTP(
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
					new MSWisepadControllerOTPListener());

			mProgressActivity = null;
			mProgressActivity = new CustomProgressDialog(mVoidsaleTxttDetailsView, getString(R.string.void_sale));
			mProgressActivity.show();

		} catch (Exception ex) {

			Constants.showDialog(mVoidsaleTxttDetailsView, getString(R.string.void_sale),
					getString(R.string.voidsaleactivity_the_otp_was_not_processed_successfully_please_try_again));

		}

		return true;
	}


	/**
	 * MswipeWisepadControllerResponseListenerObserver
	 * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

	 */
	class MSWisepadControllerOTPListener implements MSWisepadControllerResponseListener
	{

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
		 * need be type cast back to LastTransactionResponseData to access the  response data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{
			if (mProgressActivity != null)
				mProgressActivity.dismiss();

			OTPResponseData voidTransactionResponseData = (OTPResponseData) aMSDataStore;

			if(voidTransactionResponseData.getResponseStatus())
			{

				String mobileNum = AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId();

				StringBuilder maskedMobileNum = new StringBuilder(mobileNum);

				maskedMobileNum.setCharAt(3, 'x');
				maskedMobileNum.setCharAt(4, 'x');
				maskedMobileNum.setCharAt(6, 'x');
				maskedMobileNum.setCharAt(7, 'x');
				maskedMobileNum.setCharAt(8, 'x');

				final Dialog dialog = Constants.showDialog(mVoidsaleTxttDetailsView, getString(R.string.void_sale), String.format(getResources().getString(R.string.otp_sent_message), maskedMobileNum.toString()), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {

						dialog.dismiss();
						mVoidsaleTxttDetailsView.showScreen(VoidSaleEnum.VoidSaleScreens.VoidSaleScreens_TXT_OTP);

					}
				});
				dialog.show();

			}
			else{

				final Dialog dialog = Constants.showDialog(mVoidsaleTxttDetailsView, getString(R.string.void_sale), voidTransactionResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						dialog.dismiss();

					}
				});
				dialog.show();

			}
		}
	}
}
