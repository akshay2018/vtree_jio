package in.bigtree.vtree.view.summary;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.BaseTitleActivity;

public class LinkSaleSummaryDetails extends BaseTitleActivity
{
	SharedVariable applicationData = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.linksummarydetails);
		applicationData = (SharedVariable)getApplicationContext();

        initViews();
    }


	private void initViews() 
	{
		((TextView)findViewById(R.id.topbar_LBL_heading)).setText("link sale summary");
		((TextView)findViewById(R.id.topbar_LBL_heading)).setTypeface(applicationData.font);
		
	
		Intent intent = getIntent();
		
		String CardAmount = intent.getStringExtra("CardSaleAmount");
		String VoidAmt = intent.getStringExtra("VoidSaleAmount");
		String NoofSwipes = intent.getStringExtra("CardSaleCount");
		String NoofVoids = intent.getStringExtra("VoidSaleCount");
		String TotalAmt = intent.getStringExtra("TotalAmount");
		String NoOfswipesVoids = intent.getStringExtra("TotalSwipesAndVoids");
		String SummaryDate = intent.getStringExtra("SummaryDate");
		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "CardSaleCount" +intent.getStringExtra("CardSaleCount") , true, true);


		((LinearLayout)findViewById(R.id.card_summary_LNR_no_of_swipes)).setVisibility(View.VISIBLE);

		TextView lblSummaryDate = (TextView)  findViewById(R.id.cardsummarydetails_LBL_TxDateTime);
		lblSummaryDate.setTypeface(applicationData.font);
		((TextView)  findViewById(R.id.cardsummarydetails_LBL_lblTxDateTime)).setTypeface(applicationData.font);
		
		if(SummaryDate!=null)
			lblSummaryDate.setText(SummaryDate);

		/*TextView txtCardAmt = (TextView) findViewById(R.id.cardsummarydetails_LBL_cardamount);
		txtCardAmt.setTypeface(applicationData.font);
		((TextView)  findViewById(R.id.cardsummarydetails_LBL_lblcardamount)).setTypeface(applicationData.font);

		if(CardAmount!=null)
			txtCardAmt.setText(CardAmount);*/
		//((TextView) findViewById(R.id.cardsummarydetails_LBL_Amtprefixcardamount)).setTypeface(applicationData.font);
		//((TextView) findViewById(R.id.cardsummarydetails_LBL_Amtprefixcardamount)).setText(SharedVariable.mCurrency);


    	//TextView txtVoidAmt = (TextView) findViewById(R.id.cardsummarydetails_LBL_VoidAmt);
    	//txtVoidAmt.setTypeface(applicationData.font);
		//((TextView)  findViewById(R.id.cardsummarydetails_LBL_lblVoidAmt)).setTypeface(applicationData.font);

		//((TextView) findViewById(R.id.cardsummarydetails_LBL_Amtprefixvoid)).setTypeface(applicationData.font);
		//((TextView) findViewById(R.id.cardsummarydetails_LBL_Amtprefixvoid)).setText(SharedVariable.mCurrency);

		//if(VoidAmt!=null)
		//	txtVoidAmt.setText(VoidAmt);


		TextView txtAmt = (TextView) findViewById(R.id.cardsummarydetails_LBL_TotalAmt);
		txtAmt.setTypeface(applicationData.font);
		((TextView)  findViewById(R.id.cardsummarydetails_LBL_lblTotalAmt)).setTypeface(applicationData.font);

		if(TotalAmt!=null)
			txtAmt.setText(TotalAmt);
		((TextView) findViewById(R.id.cardsummarydetails_LBL_Amtprefixtotal)).setTypeface(applicationData.font);
		((TextView) findViewById(R.id.cardsummarydetails_LBL_Amtprefixtotal)).setText(SharedVariable.mCurrency);


		TextView txtNoOfSwipe = (TextView) findViewById(R.id.cardsummarydetails_LBL_NoofSwipes);
    	txtNoOfSwipe.setTypeface(applicationData.font);
    	((TextView)  findViewById(R.id.cardsummarydetails_LBL_lblNoofSwipes)).setTypeface(applicationData.font);

    	if(NoofSwipes!=null)
			txtNoOfSwipe.setText(NoofSwipes);
    	

				
	}	
}