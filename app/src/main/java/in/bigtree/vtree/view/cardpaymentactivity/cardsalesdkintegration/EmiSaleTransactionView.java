package in.bigtree.vtree.view.cardpaymentactivity.cardsalesdkintegration;

import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mswipetech.sdk.network.MSGatewayConnectionListener;
import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.CardSaleResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.ReceiptData;
import com.mswipetech.wisepad.sdk.device.MSWisepadDeviceControllerResponseListener;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.cardpaymentactivity.CreditSaleDeclineActivity;
import in.bigtree.vtree.view.cardpaymentactivity.CreditSaleSignatureActivity;


public class EmiSaleTransactionView extends CreditSaleActivity {


	/* the mswipe gateway server objects */

	protected WisePadGatewayConncetionListener mWisePadGatewayConncetionListener;

	/* the store the copy of the the date */

	public CardSaleResponseData mCardSaleResponseData = null;

	/**
	 * Called when the activity is first created.
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mWisePadGatewayConncetionListener = new WisePadGatewayConncetionListener();

		MSWisepadController.getSharedMSWisepadController(EmiSaleTransactionView.this,
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
				mWisePadGatewayConncetionListener);

		MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());


		mCardSaleDlgTitle = getResources().getString(R.string.emi);

		TextView txtHeading = ((TextView) findViewById(R.id.topbar_LBL_heading));
		txtHeading.setText(mCardSaleDlgTitle);


		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "", true, true);

	}

	/**
	 * processTransactionsOnline
	 * call up the API request which further processes the secured card data collected from the device by posting the data
	 * to online network and the responses are handled through the call back functions and are appropriately presented to the user
	 */

	@Override
	public void processCardSaleOnline()
	{

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "ReferenceId " + AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(), true, true);

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "SessionToken " +  AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(), true, true);

		mProgressActivity = new CustomProgressDialog(EmiSaleTransactionView.this, "Processing Emi Tx...");
		mProgressActivity.show();

		MSWisepadController.getSharedMSWisepadController(EmiSaleTransactionView.this,
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
				mWisePadGatewayConncetionListener).processEMISaleOnline(
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
				removeChar(mTransactionData.mBaseAmount,','),
				removeChar(mTransactionData.mTipAmount,','),
				SharedVariable.smsCode + mTransactionData.mPhoneNo,
				mTransactionData.mReceipt,
				mTransactionData.mEmail,
				mTransactionData.mNotes,
				AppSharedPrefrences.getAppSharedPrefrencesInstace().getTipEnabled(),
				AppSharedPrefrences.getAppSharedPrefrencesInstace().isReceiptEnabled(),
				mTransactionData.mAmexSecurityCode,
				mEmiPeriod,
				mEmiBankCode,
				"",
				mTransactionData.mExtraNote1,
				mTransactionData.mExtraNote2,
				mTransactionData.mExtraNote3,
				mTransactionData.mExtraNote4,
				mTransactionData.mExtraNote5,
				mTransactionData.mExtraNote6,
				mTransactionData.mExtraNote7,
				mTransactionData.mExtraNote8,
				mTransactionData.mExtraNote9,
				mTransactionData.mExtraNote10,
				new MSWisepadControllerResponseListenerObserver());
	}


	/**
	 * MSWisepadControllerResponseListenerObserver
	 * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests
	 */
	class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener {

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 *
		 * @param aMSDataStore the generic mswipe data store, this instance is refers to InvoiceTrxData or CardSaleResponseData, so this
		 *                     need be converted back to access the relavant data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{

			if(mProgressActivity != null)
				mProgressActivity.dismiss();

			if (aMSDataStore instanceof CardSaleResponseData) {

				mCardSaleResponseData = (CardSaleResponseData) aMSDataStore;

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "Status: " + mCardSaleResponseData.getResponseStatus() + " FailureReason: " + mCardSaleResponseData.getResponseFailureReason() , true, true);

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "ExceptoinStackTrace: " + mCardSaleResponseData.getExceptoinStackTrace(), true, true);

				if(!mCardSaleResponseData.getResponseStatus())
				{

					showSignature();
				}else if(mIsEmiSale) {

					showTermsConditionsDialog();
				}
				else {

					playSound(100, R.raw.approved);
					showSignature();
				}

			}
		}
	}

	/**
	 * @param
	 * @return
	 * @description After successful transaction, we are showing signature screen.For that we sending details to signature screen.
	 */
	public void showSignature() {

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "", true, true);

		if(mCardSaleResponseData.getResponseStatus())
		{
			intent = new Intent(EmiSaleTransactionView.this, CreditSaleSignatureActivity.class);
		}
		else {
			intent = new Intent(EmiSaleTransactionView.this, CreditSaleDeclineActivity.class);
		}

		intent.putExtra("Title", mCardSaleDlgTitle);
		intent.putExtra("cardSaleResponseData", mCardSaleResponseData);
		doneWithCreditSale(EMVPPROCESSTASTTYPE.SHOW_SIGNATURE);

	}

	class WisePadGatewayConncetionListener implements MSGatewayConnectionListener {

		@Override
		public void Connected(String msg) {

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, " msg " + msg, true, true);

			imgHostConnectionStatus.setAnimation(null);
			imgHostConnectionStatus.setImageResource(R.drawable.topbar_img_host_active);
		}

		@Override
		public void Connecting(String msg) {


			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, " msg " + msg, true, true);

			imgHostConnectionStatus.startAnimation(alphaAnim);
			imgHostConnectionStatus.setImageResource(R.drawable.topbar_img_host_inactive);
		}

		@Override
		public void disConnect(String msg) {

			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, " msg " + msg, true, true);

			imgHostConnectionStatus.setAnimation(null);
			imgHostConnectionStatus.setImageResource(R.drawable.topbar_img_host_inactive);
		}
	}


	private void playSound(long delay, int soundfile) {

		new playBeepTask(soundfile).execute(delay);
		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "", true, true);
	}

	private class playBeepTask extends AsyncTask<Long, Void, Void> {

		int soundfile;

		playBeepTask(int soundfile) {
			this.soundfile = soundfile;
		}

		@Override
		protected Void doInBackground(Long... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(params[0]);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			MediaPlayer mp;
			mp = MediaPlayer.create(EmiSaleTransactionView.this, soundfile);
			mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					// TODO Auto-generated method stub
					mp.reset();
					mp.release();
					mp = null;
				}
			});
			mp.start();
			if (SharedVariable.IS_DEBUGGING_ON)
				Logs.v(SharedVariable.packName, "", true, true);
		}

	}

	private void  showTermsConditionsDialog(){

		Dialog dialog = Constants.showEmiTermsAndConditionDialog(EmiSaleTransactionView.this);

		boolean isEmvSwiper = false;
		if(mCardSaleResponseData.getCardSchemeResults() == MSWisepadDeviceControllerResponseListener.CARDSCHEMERRESULTS.ICC_CARD)
			isEmvSwiper = true;

		String title = mCardSaleDlgTitle;

		if(title == null)
			title = getResources().getString(R.string.emi_termconditionview_emi);

		ReceiptData mReceiptData = new ReceiptData();

		if(mCardSaleResponseData.getReceiptData() != null){

			mReceiptData = mCardSaleResponseData.getReceiptData();
		}

		String mAmt= mCardSaleResponseData.getTrxAmount();
		String authCode = mCardSaleResponseData.getAuthCode();
		String rrno = mCardSaleResponseData.getRRNO();
		String date = mCardSaleResponseData.getDate();
		String ExpiryDate = mCardSaleResponseData.getExpiryDate();
		String EmvCardExpdate = mCardSaleResponseData.getEmvCardExpdate();
		String AppIdentifier = mCardSaleResponseData.getAppIdentifier();
		String ApplicationName = mCardSaleResponseData.getApplicationName();
		String SwitchCardType = mReceiptData.cardType;

		if(SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, " The text is " +mAmt+ " "+authCode + " "+rrno , true, true);

		String TVR = mCardSaleResponseData.getTVR();
		if(TVR == null) TVR = "";

		String TSI = mCardSaleResponseData.getTSI();
		if(TSI == null) TSI = "";

		if(EmvCardExpdate==null)
			EmvCardExpdate = "";

		String txtID= "TXN ID: "+mCardSaleResponseData.getStandId();

		String merchantDetails= "";
		String cardIssuer= "";
		String sponsorBankName= "";
		String emiTxnID= "";
		String merchantOtherDetails= "";


		String emiTenure= "";
		String emiRate= "";
		String emiTotalPayableAmount= "";
		String emiPerMonthEmi= "";

		StringBuilder cardNumMask = new StringBuilder();

		if(mReceiptData != null){

			merchantDetails = mReceiptData.merchantName+"\n"+ Html.fromHtml(mReceiptData.merchantAdd);
			cardIssuer = "CARD ISSUER: "+mReceiptData.cardIssuer;
			sponsorBankName = mReceiptData.bankName;
			emiTxnID = "EMI TXN ID: "+mReceiptData.billNo;

			String[] dateTime = date.split(",");
			merchantOtherDetails = "Date: "+dateTime[0]+"\n"+"Time: "+dateTime[1].trim()+"\n"+"MID: "+mReceiptData.mId+"\n"+"TID: "+mReceiptData.tId+"\n"+"Batch No.: "+mReceiptData.batchNo+"\nInvoice No.: "+mReceiptData.refNo+"\n"+"Bill No.:"+mReceiptData.billNo;


			if (mReceiptData.firstDigitsOfCard.length() >= 6) {

				cardNumMask.append(mReceiptData.firstDigitsOfCard.substring(0,4)+" "+mReceiptData.firstDigitsOfCard.substring(4,6));
			}

			if (mReceiptData.cardNo.length() >= 8) {

				cardNumMask.append(mReceiptData.cardNo.substring(8,mReceiptData.cardNo.length()));
			}
		}

		emiTenure = "TENURE: "+mEmiPeriod+" Months";
		emiRate = "INTEREST RATE(P.A): "+mEmiRate+"%";
		emiTotalPayableAmount = "TOTAL PAYABLE AMT(Incl. Interest): "+SharedVariable.mCurrency+" "+mEmiAmount;
		emiPerMonthEmi = "EMI AMT: "+SharedVariable.mCurrency+" "+ String.format("%.2f", (Double.parseDouble(mEmiAmount)/ Double.parseDouble(mEmiPeriod)));


		String mStrAuthCodeReceipt= "";
		String mStrDate= "";
		String mStrCardNum= "";
		String mStrExpDate= "";
		String mStrAmt= "";
		String mStrCardType= "";
		String mStrApplication= "";
		String mStrTVR= "";

		if(isEmvSwiper)
		{
			String tempString=EmvCardExpdate.trim();
			if(tempString.length()==5)
			{
				EmvCardExpdate=tempString.substring(3,5);
				EmvCardExpdate=EmvCardExpdate+"/" +tempString.substring(0,2);

			}else if(tempString.length()==4){
				EmvCardExpdate=tempString.substring(2,4);
				EmvCardExpdate=EmvCardExpdate+  "/" + tempString.substring(0,2);
			}else{
				EmvCardExpdate=tempString;
			}

			mStrAuthCodeReceipt = "APPR CD: " + authCode;
			mStrDate = "DATE/TIME: " + date;
			//mStrCardNum = "CARD NUM: " + mReceiptData.cardNo;
			mStrCardNum = "CARD NUM: " + cardNumMask.toString().replaceAll("\\s","");
			mStrExpDate = "EXP DT: " + EmvCardExpdate;
			mStrAmt = "BASE AMT: " + SharedVariable.mCurrency + " "  + mAmt ;
			mStrCardType =  SwitchCardType +"-Chip";
			mStrApplication = "APP ID: " + AppIdentifier + " APP NAME: " + ApplicationName;
			mStrTVR = "TVR: " + TVR + " TSI: " + TSI;

		}
		else{
			mStrAuthCodeReceipt = "APPR CD: " + authCode;
			mStrDate = "DATE: " + date ;
			mStrCardNum = "CARD NUM: " + cardNumMask.toString().replaceAll("\\s","");
			mStrExpDate = "EXP DT: " +( ExpiryDate.length() >= 3 ? ExpiryDate.substring(0, 2)+"/"+ExpiryDate.substring(2) : ExpiryDate);
			mStrAmt = "BASE AMT: " + SharedVariable.mCurrency + " "  + mAmt ;
			mStrCardType = SwitchCardType +"-Swipe";

		}


		((TextView)dialog. findViewById(R.id.topbar_LBL_heading)).setText(title);
		((TextView)dialog. findViewById(R.id.sponsorbankname)).setText(sponsorBankName);
		((TextView)dialog. findViewById(R.id.mearchant_details)).setText(merchantDetails.trim());
		((TextView) dialog.findViewById(R.id.mearchant_other_details)).setText(merchantOtherDetails);

		TextView txtTransactionDetails = (TextView) dialog.findViewById(R.id.transaction_details);
		String swipeChip = "";
		if(isEmvSwiper)
			swipeChip = "Chip";
		else
			swipeChip ="Swipe";
		txtTransactionDetails.setText(mStrCardNum+" "+swipeChip+"\n"+mStrExpDate+"\n"+"CARD TYPE: "+mStrCardType+"\n"+txtID+"\n"+mStrAuthCodeReceipt
				+"\nRRNO: "+rrno+"\n"+mStrTVR);

		TextView txtEmiDetials = (TextView)dialog. findViewById(R.id.emi_details);
		txtEmiDetials.setText(emiTxnID+"\n"+emiTenure+"\n"+cardIssuer+"\n"+mStrAmt+"\n"+emiRate+"\n"+emiPerMonthEmi+"\n"+emiTotalPayableAmount);
		((TextView)dialog. findViewById(R.id.final_transction_details)).setText("BASE AMT. :"+SharedVariable.mCurrency+" "+mAmt+"\n"+emiTotalPayableAmount);
		final ImageButton btn_next = (ImageButton) dialog.findViewById(R.id.emi_BTN_next);
		btn_next.setEnabled(false);
		btn_next.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				showSignature();
			}
		});

		CheckBox chkIagree = (CheckBox) dialog.findViewById(R.id.emi_CHK_i_agree);
		chkIagree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				btn_next.setEnabled(isChecked);
			}
		});

		dialog.show();

	}

	/**
	 * @description
	 *        We are removing specific character form original string.
	 * @param s original string
	 * @param c specific character.
	 * @return
	 */
	public String removeChar(String s, char c) {

		String r = "";

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c)
				r += s.charAt(i);
		}

		return r;
	}

}