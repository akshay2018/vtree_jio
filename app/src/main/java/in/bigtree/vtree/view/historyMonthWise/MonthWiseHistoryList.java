package in.bigtree.vtree.view.historyMonthWise;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.HistoryData;
import com.mswipetech.wisepad.sdk.data.HistoryResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.ArrayList;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.BaseTitleActivity;

public class MonthWiseHistoryList extends BaseTitleActivity {
    public final static String log_tab = "CardHistoryList=>";

    CustomProgressDialog mProgressActivity = null;
    ListView lstHistory = null;

    ArrayList<HistoryData> listData = new ArrayList<>();
    int totalRecord = 0;


    String mSaletype = "";
    SharedVariable applicationData = null;

    // this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
    private long lastRequestTime = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.cardmonthhistorylist);
        applicationData = (SharedVariable) getApplicationContext();
        mSaletype = getIntent().getStringExtra("saletype");
        initViews();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void initViews() {
        ((TextView) findViewById(R.id.topbar_LBL_heading)).setText("History");
        ((TextView) findViewById(R.id.topbar_LBL_heading)).setTypeface(applicationData.font);


        lstHistory = (ListView) findViewById(R.id.cardhistory_LST_cardhistory);


        lstHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MonthWiseHistoryList.this, DayWiseHistoryList.class);
                intent.putExtra("date",listData.get(position).getMonthAlias());
                intent.putExtra("trnxtype",mSaletype );

                startActivity(intent);


            }
        });


        getCardHistory();

    }

    public void ShowHistoryData(ArrayList<HistoryData> historyData) {


        totalRecord = historyData.size();


        if (totalRecord != 0) {


            if (listData.size()>0) {
                lstHistory.setAdapter(new CardHistoryAdapter(this, listData));
            } else {
                final Dialog dialog = Constants.showDialog(MonthWiseHistoryList.this, Constants.CARDHISTORYLIST_DIALOG_MSG, "Unable to show the history data, please contact support.",
                        Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                 Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                 yes.setOnClickListener(new OnClickListener() {

                     public void onClick(View v) {
                         dialog.dismiss();

                         finish();

                     }
                 });
                 dialog.show();

            }
        }
    }




    public void getCardHistory() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(MonthWiseHistoryList.this,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
                            null);

            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());


            MSWisepadController.SaleType mTrnxType = null;
            if (mSaletype.equalsIgnoreCase("cardsale"))
            {
                mTrnxType = MSWisepadController.SaleType.card;
            }
            else if (mSaletype.equalsIgnoreCase("qrsale"))
            {
                mTrnxType = MSWisepadController.SaleType.qrsale;

            }else if (mSaletype.equalsIgnoreCase("linksale"))
            {
                mTrnxType =  MSWisepadController.SaleType.linksale;
            }
            else if (mSaletype.equalsIgnoreCase("banksale"))
            {
                mTrnxType = MSWisepadController.SaleType.bank;

            }else if (mSaletype.equalsIgnoreCase("cashsale"))
            {
                mTrnxType =  MSWisepadController.SaleType.cash;
            }

            wisepadController.getMonthHistoryDetails(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    mTrnxType,
                    new MSWisepadControllerResponseListenerObserver());


            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(MonthWiseHistoryList.this, "Fetching History...");
            mProgressActivity.show();

        }
        catch (Exception e)
        {
            Constants.showDialog(this, "History", "unable to process card history.");
            e.printStackTrace();
        }

     }



    /**
     * MswipeWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

     */
    class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {

            HistoryResponseData cardHistoryDetailsResponseData = (HistoryResponseData) aMSDataStore;
            mProgressActivity.dismiss();

            if (cardHistoryDetailsResponseData.getResponseStatus())
            {

                listData = cardHistoryDetailsResponseData.getHistoryData();
                ShowHistoryData(listData);
            }
            else {

                Constants.showDialog(MonthWiseHistoryList.this, "History", cardHistoryDetailsResponseData.getResponseFailureReason());
            }
        }
    }



    public class CardHistoryAdapter extends BaseAdapter {
    	ArrayList<HistoryData> listData = null;
        Context context;

        public CardHistoryAdapter(Context context, ArrayList<HistoryData> listData) {
            this.listData = listData;
            this.context = context;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.cardhistorymonthlistdata, null);
            }
            HistoryData cardSaleDetails = (HistoryData) listData.get(position);

              //TextView lstFourDigits= (TextView)convertView.findViewById(R.id.cardhistorylist_LBL_lastfourdigits);
            //lstFourDigits.setText(cardSaleDetails.CardFourDigits);

            TextView lblamt = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_lblamount);
            lblamt.setText(SharedVariable.Currency_Code);
            lblamt.setTypeface(applicationData.font);


            TextView amt = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_amount);
            amt.setText(cardSaleDetails.getTrxAmount().toString());
            amt.setTypeface(applicationData.font);


            TextView trxcount = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_transactions_count);
            trxcount.setText(cardSaleDetails.getTrxCount()+" transactions");
            trxcount.setTypeface(applicationData.font);


            TextView date = (TextView) convertView.findViewById(R.id.cardhistorylist_LBL_date);
            date.setText(cardSaleDetails.getMonthAlias());
            date.setTypeface(applicationData.font);



            return convertView;
        }

    }
}
