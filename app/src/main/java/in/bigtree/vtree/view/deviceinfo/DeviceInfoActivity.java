package in.bigtree.vtree.view.deviceinfo;


import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.device.WisePadConnection;

import java.util.Hashtable;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.cardpaymentactivity.MSWisepadView;

/**
 * DeviceInfoView 
 * 		display the wisepad device information, this inherits the MSWisepadView which handled the communications
 * to the device.
 */
public class DeviceInfoActivity extends MSWisepadView
{

	private ImageView mIMGTopBarRefresh = null;

	private TextView mTxtEmvKsn = null;
	private TextView mTxtKsn = null;
	private TextView mTxtBatteryLevel = null;
	private TextView mTxtProgMsg = null;
	private TextView mTxtFirmwareVersion = null;

	
	//the wise pad has to be disconnected if its not required and also when the application closes
    //the wise pad has to be disconnected, this will see to that the dis-connecting to the wisepad does has execute multiple times 	
	private boolean onDoneWithCreditSaleCalled = false;
    
  //in certain scenarios the wise pad where in a likely case becomes non responsive, at that precise
  	//moment a task is called and with in the elapsed time if their no communication back to the app this task will
  	//un-initiate the communication link from the wisepad

	private EMVProcessTask mEMVProcessTask = null;
	private EMVPPROCESSTASTTYPE mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;
	private enum EMVPPROCESSTASTTYPE{ NO_TASK, STOP_BLUETOOTH, BACK_BUTTON, ONLINE_SUBMIT, SHOW_ALERTS}

	//The network connection listener observes all the network notification to the mswipe gatewat

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.deviceinfo_view);

		mAutoConnect = true;
		mCallCheckCardAfterConnection = false;
        
        initViews();

    }


	/**
     * Initiates the ui elements for the card sale, and based on the option selected like emi preaut or sale with cash
     * the ui behavior changes. 
     * @param  
     * @return     
     */
    private void initViews() 
    {

		((ImageView)findViewById(R.id.topbar_IMG_position1)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.topbar_LBL_heading)).setText(getResources().getString(R.string.deviceinfoactivity_info));
    		
    	mTxtProgMsg = (TextView)findViewById(R.id.deviceinfo_TXT_progress_msg);

    	mTxtEmvKsn = (TextView)findViewById(R.id.deviceinfo_TXT_emv_ksn);
    	mTxtKsn = (TextView)findViewById(R.id.deviceinfo_TXT_track_ksn);
    	mTxtBatteryLevel = (TextView)findViewById(R.id.deviceinfo_TXT_battery_level);
		mTxtFirmwareVersion = (TextView)findViewById(R.id.deviceinfo_TXT_firmware);

		mIMGTopBarRefresh = (ImageView)findViewById(R.id.topbar_IMG_position2);
		mIMGTopBarRefresh.setImageResource(R.drawable.refresh);
		mIMGTopBarRefresh.setVisibility(View.VISIBLE);
		mIMGTopBarRefresh.setEnabled(false);

		mIMGTopBarRefresh.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				requestDeviceInfo();
			}
		});
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {

			doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);

			return true;

		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

    /**setWisePadConnectionState
     *	set the views to the connection state 
     * @param  
     * wisePadConnection
     * 		The state of the wise pad connection
     * @return     
     */  
    @Override
    public void setWisePadConnectionStateResult(WisePadConnection wisePadConnection)
	{
    	if (SharedVariable.IS_DEBUGGING_ON)
    		Logs.v(SharedVariable.packName,  "wisePadConnection " + wisePadConnection, true, true);
	
		if(wisePadConnection == WisePadConnection.WisePadConnection_CONNECTING)
		{
			 
            setWisepadStateDisplayInfo(getString(R.string.connecting_device));

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_NOT_CONNECTED){

			if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.BACK_BUTTON
					|| mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.ONLINE_SUBMIT) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}

			mIMGTopBarRefresh.setEnabled(true);
			setWisepadStateDisplayInfo(getString(R.string.device_not_connected));

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_CONNECTED){

			setWisepadStateDisplayInfo(getString(R.string.device_connected));
			requestDeviceInfo();

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_DEVICE_NOTFOUND){

			if (mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.BACK_BUTTON 
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.ONLINE_SUBMIT) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}
			setWisepadStateDisplayInfo(getString(R.string.unable_to_detect_the_wisepad_restart_the_device_and_try_reconnecting));
			mIMGTopBarRefresh.setEnabled(true);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_DIS_CONNECTED){

			if (mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.BACK_BUTTON
					|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.ONLINE_SUBMIT ||
							mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.STOP_BLUETOOTH) {
				if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
					mEMVProcessTask.cancel(true);
			}
			setWisepadStateDisplayInfo(getString(R.string.device_disconnected_please_ensure_the_connecting_device_is__a_wisepad));
			mIMGTopBarRefresh.setEnabled(true);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_FAIL_TO_START_BT){
			
			setWisepadStateErrorInfo( DeviceInfoActivity.this.getString(R.string.fail_to_start_bluetooth_v2));
			mIMGTopBarRefresh.setEnabled(true);

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_MULTIPLE_PAIRED_DEVCIES_FOUND){

			showMultiplePairedDeviceDialog();

		}
		else if(wisePadConnection == WisePadConnection.WisePadConnection_NO_PAIRED_DEVICES_FOUND){

			showNoPairedDeviceDialog();
		}
	}
     
    public void processOnErrorWisePad()
	{
    	// TODO Auto-generated method stub		
		if (mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.BACK_BUTTON
				|| mEMVPPROCESSTASTTYPE == mEMVPPROCESSTASTTYPE.ONLINE_SUBMIT) {
			if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
				mEMVProcessTask.cancel(true);
			
		}
	}
	
	public void setWisepadStateDisplayInfo(String msg)
	{
    		mTxtProgMsg.setText(msg);
	}
	
	public void setWisepadStateErrorInfo(String msg)
	{
			mTxtProgMsg.setText(msg);
	}
	
	@Override
	public void setWisepadDeviceInfo(Hashtable<String, String> deviceInfoData){

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName,  ""+deviceInfoData, true, true);

		mIMGTopBarRefresh.setEnabled(true);

		if(deviceInfoData.get("emvKsn")!=null)
        {
          String emv = (String)deviceInfoData.get("emvKsn");
          if(emv.length()>=14)
          {

           mTxtEmvKsn.setText(emv.substring(0,14));

          }
          else{
             mTxtEmvKsn.setText(emv);
          }
        }

        if(deviceInfoData.get("trackKsn")!=null)
        {
          String emv = (String)deviceInfoData.get("trackKsn");
          if(emv.length()>=14)
          {
              mTxtKsn.setText(emv.substring(0,14));
          }
          else{
              mTxtKsn.setText(emv);
          }
        }

        if(deviceInfoData.get("batteryPercentage")!=null)
        {
        	String emv = (String)deviceInfoData.get("batteryPercentage") + "%";
        	mTxtBatteryLevel.setText(emv);

        }

		if(deviceInfoData.get("firmwareVersion")!=null)
		{
			String emv = (String)deviceInfoData.get("firmwareVersion");
			mTxtFirmwareVersion.setText(emv);

		}
    }


	@Override
	public void showMultiplePairedDeviceDialog(){

		TaskShowMultiplePairedDevices pairedtask = new TaskShowMultiplePairedDevices();
		pairedtask.execute();
	}

	public void showNoPairedDeviceDialog(){

		final Dialog dialog = Constants.showDialog(this, getResources().getString(R.string.deviceinfoactivity_device_info),
				getResources().getString(R.string.no_paired_wisePad_found_please_pair_the_wisePad_from_your_phones_bluetooth_settings_and_try_again), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO, getString(R.string.ok), "");
		Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
		yes.setOnClickListener(new View.OnClickListener()
		{

			public void onClick(View v) {

				doneWithCreditSale(EMVPPROCESSTASTTYPE.STOP_BLUETOOTH);
				dialog.dismiss();

			}
		});

		dialog.show();
	}


    class EMVProcessTask extends AsyncTask<Void, Integer, Void>
    {
    	@Override
    	protected void onCancelled()
    	{

    		if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.STOP_BLUETOOTH) {

				Intent intent = new Intent(DeviceInfoActivity.this, MenuView.class);
				startActivity(intent);
				finish();

    		}

    		mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;

    	}

    	@Override
    	protected Void doInBackground(Void... unused) {

    		//calling after this statement and canceling task will no meaning if you do some update database kind of operation
    		//so be wise to choose correct place to put this condition
    		//you can also put this condition in for loop, if you are doing iterative task
    		//you should only check this condition in doInBackground() method, otherwise there is no logical meaning

    		// if the task is not cancelled by calling LoginTask.cancel(true), then make the thread wait for 10 sec and then
    		//quit it self

    		int isec = 6;

    		if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.STOP_BLUETOOTH)
    			isec = 4;

			int ictr = 0;
    		//it will wait for 15 sec or till the task is cancelled by the mSwiper routines.
    		while (!isCancelled() & ictr < isec) {
    			try {
    				Thread.sleep(500);
    			} catch (Exception ex) {
    			}
    			ictr++;
    		}

    		if (SharedVariable.IS_DEBUGGING_ON)
    			Logs.v(SharedVariable.packName,  "EmvOnlinePorcessTask  end doInBackground", true, true);
    		return null;
    	}


    	@Override
    	protected void onPostExecute(Void unused)
    	{
    		if (SharedVariable.IS_DEBUGGING_ON)
    			Logs.v(SharedVariable.packName,  "EmvOnlinePorcessTask onPostExecute", true, true);


    		if (mEMVPPROCESSTASTTYPE == EMVPPROCESSTASTTYPE.STOP_BLUETOOTH)
			{
				Intent intent = new Intent(DeviceInfoActivity.this, MenuView.class);
				startActivity(intent);
				finish();
    		}

    		mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;

    	}
    }
    
    public void doneWithCreditSale(EMVPPROCESSTASTTYPE processTaskType)
    {   	
    	if (!onDoneWithCreditSaleCalled)
    	{
    		deviceInfoViewDestory();

			//in case if the task is still running cancel it and not execute any thing which are based on this data.
    		mEMVPPROCESSTASTTYPE = EMVPPROCESSTASTTYPE.NO_TASK;
    		
    		if (mEMVProcessTask != null && mEMVProcessTask.getStatus() != AsyncTask.Status.FINISHED)
    			mEMVProcessTask.cancel(true);
    		
    		mEMVPPROCESSTASTTYPE = processTaskType;
    		mEMVProcessTask = new EMVProcessTask(); //every time create new object, as AsynTask will only be executed one time.
    		mEMVProcessTask.execute();
    		
    		onDoneWithCreditSaleCalled = true;
    	}
    	
    }
     
    public void deviceInfoViewDestory() 
    {

    	if(mProgressActivity != null)
			mProgressActivity.dismiss();
    	
    }

}