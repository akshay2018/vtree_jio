package in.bigtree.vtree.view.paybylink.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.PayByLinkResponseData;
import com.mswipetech.wisepad.sdk.data.PayByLinkTrxData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.ArrayList;

import in.bigtree.vtree.R;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.paybylink.PayByLinkActivity;
import in.bigtree.vtree.view.paybylink.PayByLinkEnum;

/**
 * Created by ABC on 25/02/2017.
 */

public class PaymentStatusListFragment extends android.support.v4.app.Fragment {

    CustomProgressDialog mProgressActivity;
    PayByLinkActivity mPayByLinkActivity;
    ListView mLSTPaymentdetails;

    PayByLinkPaymentStatusListAdapter mPaymentSatusdapter = null;
    ArrayList<PayByLinkTrxData> mArrAllocatedTerminalsData = new ArrayList<>();

    public String mTxtMobileNo = "";

    View view;
    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        mPayByLinkActivity = ((PayByLinkActivity) activity);
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.paybylink_paymentstatus_list, container, false);
        initViews();

        return view;
    }

    private void initViews(){

        mLSTPaymentdetails = (ListView)view.findViewById(R.id.paybylink_payment_details_LV);

        getPaymentDetails();
    }

    public boolean getPaymentDetails() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mPayByLinkActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            wisepadController.getPayByTrxList(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getCustCode(),
                    new PayByLinkTrnxListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mPayByLinkActivity, getString(R.string.processing));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mPayByLinkActivity, "payby link",
                    "the payby link transaction was not processed successfully. please try again");

        }

        return true;
    }

    class PayByLinkTrnxListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            PayByLinkResponseData PayByLinkResponseData = (com.mswipetech.wisepad.sdk.data.PayByLinkResponseData) aMSDataStore;
            if(PayByLinkResponseData.getResponseStatus())
            {

                mArrAllocatedTerminalsData = PayByLinkResponseData.getmTransHistoryData();

                mPaymentSatusdapter = new PayByLinkPaymentStatusListAdapter(mPayByLinkActivity, mArrAllocatedTerminalsData);
                mLSTPaymentdetails.setAdapter(mPaymentSatusdapter);
            }
            else{

                final Dialog dialog = Constants.showDialog(mPayByLinkActivity, getString(R.string.payby_link), PayByLinkResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        mPayByLinkActivity.showScreen(PayByLinkEnum.PayByLinkScreens.PayByLink_Screen_Selection);
                        dialog.dismiss();

                    }
                });
                dialog.show();

            }
        }
    }

    public class PayByLinkPaymentStatusListAdapter extends BaseAdapter {

        ArrayList<PayByLinkTrxData> listData = null;
        Context context;

        public PayByLinkPaymentStatusListAdapter(Context context, ArrayList<PayByLinkTrxData> listData) {
            this.listData = listData;
            this.context = context;

        }

        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            // TODO Auto-generated method stub

            PayByLinkTrxData payByLinkTrxData = listData.get(position);

            LayoutInflater inflater = LayoutInflater.from(context);

            convertView = inflater.inflate(R.layout.paybylink_paymentstatus_detail_listrow, null);

            TextView TxtPaymentStatus = (TextView) convertView.findViewById(R.id.paybylink_TXT_payment_status);
            TextView TxtAmount = (TextView) convertView.findViewById(R.id.paybylink_TXT_amount);
            final TextView TxtMobileNo = (TextView) convertView.findViewById(R.id.paybylink_TXT_mobileno);
            final TextView TxtEmailID = (TextView) convertView.findViewById(R.id.paybylink_TXT_email_id);
            TextView Txtdate = (TextView) convertView.findViewById(R.id.paybylink_TXT_date);
            TextView TxtResendlink = (TextView) convertView.findViewById(R.id.paybylink_TXT_resendlink);

            LinearLayout mLNREmialId = (LinearLayout) convertView.findViewById(R.id.paybylink_LNR_email_id);

            String amount = String.format("%.2f", Double.parseDouble(payByLinkTrxData.getAmount()));
            TxtAmount.setText(Constants.getAmountWithComma(amount));

            TxtMobileNo.setText(payByLinkTrxData.getMobileNo());

            TxtEmailID.setText(payByLinkTrxData.getEmailId());

            if(TxtEmailID.getText().toString().length() > 0) {
                mLNREmialId.setVisibility(View.VISIBLE);
            }else {
                mLNREmialId.setVisibility(View.GONE);
            }

            final String invoiceId = payByLinkTrxData.getTrxId();
            TxtPaymentStatus.setText(payByLinkTrxData.getTrxStatus().toLowerCase());

            String date = Constants.getDateWithFormate(payByLinkTrxData.getTrxDate(), "MM/dd/yyyy hh:mm:ss a", "dd-MMM-yyyy hh:mma");
            Txtdate.setText(date.toLowerCase());

            TxtResendlink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mTxtMobileNo = TxtMobileNo.getText().toString();
                    resendLink(invoiceId);
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mTxtMobileNo = TxtMobileNo.getText().toString();

                }
            });

            return convertView;
        }
    }

    public boolean resendLink(String aInvoiceId) {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mPayByLinkActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            wisepadController.resendPayByLink(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getCustCode(),
                    aInvoiceId,
                    new PayByLinkResendLinkListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mPayByLinkActivity, getString(R.string.processing));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mPayByLinkActivity, "payby link",
                    "the payby link transaction was not processed successfully. please try again");

        }

        return true;
    }

    class PayByLinkResendLinkListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            PayByLinkResponseData PayByLinkResponseData = (com.mswipetech.wisepad.sdk.data.PayByLinkResponseData) aMSDataStore;
            if(PayByLinkResponseData.getResponseStatus())
            {

                final Dialog dialog = Constants.showDialog(mPayByLinkActivity, getString(R.string.payby_link),
                        "payment link has been resent on your referred mobile number "+ mTxtMobileNo, Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });
                dialog.show();

            }
            else{
                final Dialog dialog = Constants.showDialog(mPayByLinkActivity, getString(R.string.payby_link), PayByLinkResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });
                dialog.show();
            }
        }
    }
}
