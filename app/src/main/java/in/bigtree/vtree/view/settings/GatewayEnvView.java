package in.bigtree.vtree.view.settings;

import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.MSWisepadController.GATEWAY_ENVIRONMENT;
import com.mswipetech.wisepad.sdk.MSWisepadController.NETWORK_SOURCE;

import in.bigtree.vtree.R;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Utils;
import in.bigtree.vtree.view.BaseTitleActivity;

/**
 * GatewayEnvView
 * GatewayEnvView activity define the Mswipe gateway preferences this settings
 * should be set or passed to the Mswipe controller object so that the controller knows about the
 * this settings.
 */

public class GatewayEnvView extends BaseTitleActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_gatewayenvironment);
		initViews();
	}

	private void initViews()
	{
		((TextView) findViewById(R.id.topbar_LBL_heading))
				.setText("Gateway Preferences");

		final CheckBox chkBoxVertical = (CheckBox) findViewById(R.id.gatewayenvview_CHK_serververtical);
		RadioGroup radNetworkSource = (RadioGroup) findViewById(R.id.gatewayenvview_RG_networksource);

		if (AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment() == GATEWAY_ENVIRONMENT.PRODUCTION)
		{
			chkBoxVertical.setChecked(false);

		} else {
			chkBoxVertical.setChecked(true);
		}

		chkBoxVertical.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
				// TODO Auto-generated method stub

				if (isChecked)
				{
					AppSharedPrefrences.getAppSharedPrefrencesInstace().setGatewayEnvironment(GATEWAY_ENVIRONMENT.LABS);
				}
				else {

					AppSharedPrefrences.getAppSharedPrefrencesInstace().setGatewayEnvironment(GATEWAY_ENVIRONMENT.PRODUCTION);

				}

				MSWisepadController.getSharedMSWisepadController(GatewayEnvView.this,
						AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
						AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
						null).setGatewaySource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment());

				setTitle(isChecked);
			}
		});


		RadioButton radioButtonSim = (RadioButton)findViewById(R.id.gatewayenvview_RB_networksourcesim) ;

		radNetworkSource.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup rGroup, int checkedId)
			{
				// TODO Auto-generated method stub
				if (checkedId == R.id.gatewayenvview_RB_networksourcewifi) {
					AppSharedPrefrences.getAppSharedPrefrencesInstace()
							.setNetworkSource(NETWORK_SOURCE.WIFI);

				}
				else if (checkedId == R.id.gatewayenvview_RB_networksourcesim) {
					AppSharedPrefrences.getAppSharedPrefrencesInstace()
							.setNetworkSource(NETWORK_SOURCE.SIM);
				}
				else if (checkedId == R.id.gatewayenvview_RB_networksourceethernet) {
					AppSharedPrefrences.getAppSharedPrefrencesInstace()
							.setNetworkSource(NETWORK_SOURCE.EHTERNET);
				}

				getMacAddress();

				setTitle(chkBoxVertical.isChecked());
			}
		});

		if (AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource() == NETWORK_SOURCE.WIFI)
		{
			radNetworkSource.check(R.id.gatewayenvview_RB_networksourcewifi);
		}
		else if (AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource() == NETWORK_SOURCE.SIM)
		{
			radNetworkSource.check(R.id.gatewayenvview_RB_networksourcesim);
		}
		else if (AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource() == NETWORK_SOURCE.EHTERNET)
		{
			radNetworkSource.check(R.id.gatewayenvview_RB_networksourceethernet);
		}



		getMacAddress();

	}

	public void getMacAddress() {

		String networkaddress = "";
		try
		{

			if (AppSharedPrefrences.getAppSharedPrefrencesInstace()
					.getNetworkSource() == NETWORK_SOURCE.WIFI)
			{
				networkaddress = Utils.getWifiAddress(this);
			}
			else if (AppSharedPrefrences.getAppSharedPrefrencesInstace()
					.getNetworkSource() == NETWORK_SOURCE.SIM) {

				networkaddress = Utils.getSimIMEI(this);

			}
			else {
				networkaddress = Utils.getMACAddress("eth0");

			}

		} catch (Exception ex) {
			networkaddress = "";
		}

		if (networkaddress != null && networkaddress.length() == 0)
		{
			if (AppSharedPrefrences.getAppSharedPrefrencesInstace()
					.getNetworkSource() == NETWORK_SOURCE.WIFI)
			{
				networkaddress = "Wifi not supported.";

			}
			else if (AppSharedPrefrences.getAppSharedPrefrencesInstace()
					.getNetworkSource() == NETWORK_SOURCE.SIM) {

				networkaddress = "Sim not supported.";


			}
			else {
				networkaddress = "Ethernet not supported.";
			}
		}
		((TextView) findViewById(R.id.gatewayenvview_lbl_networksourceaddress)).setText(networkaddress);

	}

	@Override
	public void onBackPressed()
	{
		// TODO Auto-generated method stub
		this.setResult(RESULT_OK);
		finish();
	}

}