package in.bigtree.vtree.view.wiseposfeatures;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.view.BaseTitleActivity;


/**
 * MenuView
 * MenuView activity lists all the Mswipe api's available, each entry will demonstrate the integration process
 * of the api
 *
 */

public class WiseposMenuActivity extends BaseTitleActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_wisepos);

		initViews();

	}

	private void initViews() {

		ListView listView = (ListView) findViewById(R.id.wisepos_view_LST_options);

		String[] values = new String[]
				{
						"Barcode Reader",
				};

		((RelativeLayout) findViewById(R.id.top_bar)).setVisibility(View.GONE);

		TextView txtHeading = (TextView) findViewById(R.id.topbar_LBL_heading);
		txtHeading.setText("SDK List");

		MenuViewAdapter adapter = new MenuViewAdapter(this, values);
		int[] colors = {0, 0xFF0000FF, 0};
		listView.setDivider(new GradientDrawable(Orientation.LEFT_RIGHT, colors));
		listView.setDividerHeight(1);

		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				int menuoption = 0;

				if (arg2 == menuoption) {
					Intent intent = new Intent(WiseposMenuActivity.this, BarcodeReaderView.class);
					startActivity(intent);
				}


			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}

	public class MenuViewAdapter extends BaseAdapter {
		String[] listData = null;
		Context context;

		public MenuViewAdapter(Context context, String[] listData) {
			this.listData = listData;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listData.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.view_menulstitem, null);
			}
			TextView txtItem = (TextView) convertView.findViewById(R.id.menuview_lsttext);
			txtItem.setText(listData[position]);

			return convertView;
		}
	}

}
