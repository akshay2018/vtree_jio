package in.bigtree.vtree.view.cardpaymentactivity;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.view.BaseTitleActivity;
import in.bigtree.vtree.view.cardpaymentactivity.cardsalesdkintegration.PaymentSDKTypeIntegration;

/**
 * MenuView 
 * MenuView activity lists all the Mswipe api's available, each entry will demonstrate the integration process
 * of the api
 * 
 */

public class SelectPaymentActivity extends BaseTitleActivity {

	Intent intent = null;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_menu);

		initViews();

	}

	private void initViews() 
	{

		final boolean autoConnect = getIntent().getExtras().getBoolean("autoconnect");
		final boolean checkCard = getIntent().getExtras().getBoolean("checkcardAfterConnection");
		final String integrationType = getIntent().getStringExtra("integrationType");

		ListView listView = (ListView) findViewById(R.id.menuview_LST_options);

		String[] values = new String[]
				{
						"Credit/Debit Sale",
						"Preauth Sale",
						"Cash @ POS",
						"Emi Sale"
				};

		((RelativeLayout)findViewById(R.id.top_bar)).setVisibility(View.GONE);

        TextView txtHeading = (TextView) findViewById(R.id.topbar_LBL_heading);
        txtHeading.setText("SDK List");
        
        MenuViewAdapter adapter = new MenuViewAdapter(this, values);
        int[] colors = {0, 0xFF0000FF, 0};
        listView.setDivider(new GradientDrawable(Orientation.LEFT_RIGHT, colors));
        listView.setDividerHeight(1);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                int menuoption =0;

				if (arg2 == menuoption)
				{
					if(integrationType.equalsIgnoreCase("activityIntegration")){

						/*intent = new Intent(SelectPaymentActivity.this, PaymentActivityTypeIntegration.class);
						intent.putExtra("cardsale", true);
						intent.putExtra("autoconnect", autoConnect);
						intent.putExtra("checkcardAfterConnection", checkCard);
						startActivity(intent);*/
					}
					else {

						intent = new Intent(SelectPaymentActivity.this, PaymentSDKTypeIntegration.class);
						intent.putExtra("cardsale", true);
						intent.putExtra("autoconnect", autoConnect);
						intent.putExtra("checkcardAfterConnection", checkCard);
						startActivity(intent);
					}

				}

				menuoption++;
                if (arg2 == menuoption)
                {
					if(integrationType.equalsIgnoreCase("activityIntegration")){

						/*intent = new Intent(SelectPaymentActivity.this, PaymentActivityTypeIntegration.class);
						intent.putExtra("preauthsale", true);
						intent.putExtra("autoconnect", autoConnect);
						intent.putExtra("checkcardAfterConnection", checkCard);
						startActivity(intent);*/
					}
					else {

						intent = new Intent(SelectPaymentActivity.this, PaymentSDKTypeIntegration.class);
						intent.putExtra("preauthsale", true);
						intent.putExtra("autoconnect", autoConnect);
						intent.putExtra("checkcardAfterConnection", checkCard);
						startActivity(intent);
					}
                }

				menuoption++;
				if (arg2 == menuoption)
				{
					if(integrationType.equalsIgnoreCase("activityIntegration")){

						/*intent = new Intent(SelectPaymentActivity.this, PaymentActivityTypeIntegration.class);
						intent.putExtra("salewithcash", true);
						intent.putExtra("autoconnect", autoConnect);
						intent.putExtra("checkcardAfterConnection", checkCard);
						startActivity(intent);*/
					}
					else {

						intent = new Intent(SelectPaymentActivity.this, PaymentSDKTypeIntegration.class);
						intent.putExtra("salewithcash", true);
						intent.putExtra("autoconnect", autoConnect);
						intent.putExtra("checkcardAfterConnection", checkCard);
						startActivity(intent);
					}
				}

				menuoption++;
				if (arg2 == menuoption)
				{
					if(integrationType.equalsIgnoreCase("activityIntegration")){

						/*intent = new Intent(SelectPaymentActivity.this, PaymentActivityTypeIntegration.class);
						intent.putExtra("emisale", true);
						intent.putExtra("autoconnect", autoConnect);
						intent.putExtra("checkcardAfterConnection", checkCard);
						startActivity(intent);*/
					}
					else {

						intent = new Intent(SelectPaymentActivity.this, PaymentSDKTypeIntegration.class);
						intent.putExtra("emisale", true);
						intent.putExtra("autoconnect", autoConnect);
						intent.putExtra("checkcardAfterConnection", checkCard);
						startActivity(intent);
					}
				}

            }
        });

    }
	


	public class MenuViewAdapter extends BaseAdapter {
		String[] listData = null;
		Context context;

		public MenuViewAdapter(Context context, String[] listData) {
			this.listData = listData;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listData.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null)
			{
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.view_menulstitem, null);
			}
			TextView txtItem = (TextView) convertView
					.findViewById(R.id.menuview_lsttext);
			txtItem.setText(listData[position]);
			
			return convertView;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
	}


}
