package in.bigtree.vtree.view.mcardsale;

public  class McardSaleEnum {

	public static enum McardSaleScreens
	{
		MCARD_CARDSALE_AMOUNT,
		MCARD_CARDSALE_INPUT_CARD_SELECTION,
		MCARD_CARDSALE_INPUT_CARD_NUM,
		MCARD_CARDSALE_OTP,
		MCARD_CARDSALE_APPROVED
	}

	public enum CardInputType {SWIPE, ENTER, SCAN};
}
