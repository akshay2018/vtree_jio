package in.bigtree.vtree.view.paybylink.fragment.bank;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.IPGBankListData;
import com.mswipetech.wisepad.sdk.data.IPGBankResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.ArrayList;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.paybylink.PayByLinkActivity;
import in.bigtree.vtree.view.paybylink.PayByLinkEnum;

import static in.bigtree.vtree.view.paybylink.PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_Terms_Conditions;

/**
 * Created by ABC on 25/02/2017.
 */

public class PayByLinkBanksDetailsFragment extends Fragment {

    CustomProgressDialog mProgressActivity;
    PayByLinkActivity mPayByLinkActivity;
    private ListView mLSTPaymentdetails;

    private ImageButton mIMGNext = null;

    PayByLinkBankListAdapter mPayByLinkBankListadapter = null;
    ArrayList<IPGBankListData> mArrIPGBankListData = new ArrayList<IPGBankListData>();

    private int mSelectedPosition = -1;

    View view;
    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        mPayByLinkActivity = ((PayByLinkActivity) activity);
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.paybylink_bank_list, container, false);

        initViews();

        return view;
    }

    private void initViews(){

        mLSTPaymentdetails = (ListView)view.findViewById(R.id.paybylink_bank_details_LV);

        mIMGNext = (ImageButton) view.findViewById(R.id.paybylink_bank_IMGBtn_next);
        mIMGNext.setEnabled(false);

        mLSTPaymentdetails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                mSelectedPosition = position;
                mIMGNext.setEnabled(true);

                mPayByLinkBankListadapter.notifyDataSetChanged();

                mPayByLinkActivity.mBankCode = mArrIPGBankListData.get(position).getBankCode();
                mPayByLinkActivity.mLocationCode =  mArrIPGBankListData.get(position).getLocationCode();
            }
        });

        mIMGNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPayByLinkActivity.showScreen(PayByLinkScreens_Terms_Conditions);
            }
        });


        getIPGBankList();
    }

    public boolean getIPGBankList() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mPayByLinkActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

            wisepadController.getIPGBankList(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    new UpdateIPGListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mPayByLinkActivity, getString(R.string.processing));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mPayByLinkActivity, getString(R.string.mcard_sale),
                    getString(R.string.voidsaleactivity_the_otp_was_not_processed_successfully_please_try_again));

        }

        return true;
    }

    class UpdateIPGListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            IPGBankResponseData ipgBankResponseData = (IPGBankResponseData) aMSDataStore;

            if(ipgBankResponseData.getResponseStatus())
            {
                mArrIPGBankListData = ipgBankResponseData.getmIPGBankListData();

                mPayByLinkBankListadapter = new PayByLinkBankListAdapter(mPayByLinkActivity, mArrIPGBankListData);
                mLSTPaymentdetails.setAdapter(mPayByLinkBankListadapter);
            }
            else{

                final Dialog dialog = Constants.showDialog(mPayByLinkActivity, getString(R.string.payby_link), ipgBankResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        mPayByLinkActivity.showScreen(PayByLinkEnum.PayByLinkScreens.PayByLinkScreens_BANK_Selection);
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        }
    }

    public class PayByLinkBankListAdapter extends BaseAdapter {

        ArrayList<IPGBankListData> listData = null;
        Context context;

        public PayByLinkBankListAdapter(Context context, ArrayList<IPGBankListData> listData) {
            this.listData = listData;
            this.context = context;

        }

        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            // TODO Auto-generated method stub

            final IPGBankListData ipgBankListData = listData.get(position);

            LayoutInflater inflater = LayoutInflater.from(context);

            convertView = inflater.inflate(R.layout.paybylink_bank_detail_listrow, null);

            TextView mTXTBankCount = (TextView) convertView.findViewById(R.id.paybylink_LNR_bank_count);

            final ImageView mCHKBoxClick = (ImageView) convertView.findViewById(R.id.paybylink_bank_CHK_details);

            TextView mTXTBankName = (TextView) convertView.findViewById(R.id.paybylink_bank_TXT_bankName);
            TextView mTXTBranchName = (TextView) convertView.findViewById(R.id.paybylink_bank_TXT_branchName);
            TextView mTXTIfscCode = (TextView) convertView.findViewById(R.id.paybylink_bank_TXT_ifscCode);
            TextView mTXTAccountType = (TextView) convertView.findViewById(R.id.paybylink_bank_TXT_accountType);

            TextView mTXTLocation = (TextView) convertView.findViewById(R.id.paybylink_bank_TXT_location);

            mTXTBankCount.setText("bank 0" + (position+1));

            mTXTBankName.setText(ipgBankListData.getBankName().toLowerCase());
            mTXTBranchName.setText(ipgBankListData.getBankBranch().toLowerCase());
            mTXTIfscCode.setText(ipgBankListData.getIFSC().toLowerCase());
            mTXTAccountType.setText(ipgBankListData.getAccountType().toLowerCase());

            mTXTLocation.setText(ipgBankListData.getAddress() +", "+ ipgBankListData.getLocation() +", "+ ipgBankListData.getPincode());

            if(mSelectedPosition == position)
            {
                if (SharedVariable.IS_DEBUGGING_ON)
                    Logs.v(SharedVariable.packName,"mSelectedPosition  "+true,true,true);
                mCHKBoxClick.setBackgroundResource(R.drawable.radio_button_active);
                //mCHKBoxClick.setChecked(true);
            }
            else{
                if (SharedVariable.IS_DEBUGGING_ON)
                    Logs.v(SharedVariable.packName,"mSelectedPosition  "+false,true,true);
                //mCHKBoxClick.setChecked(false);
                mCHKBoxClick.setBackgroundResource(R.drawable.radio_button_inactive);
            }

            return convertView;
        }
    }
}
