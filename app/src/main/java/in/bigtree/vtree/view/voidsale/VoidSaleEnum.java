package in.bigtree.vtree.view.voidsale;

public  class VoidSaleEnum {

	public static enum VoidSaleScreens
	{
		VoidSaleScreens_AMOUNT,
		VoidSaleScreens_TXT_DETAILS,
		VoidSaleScreens_TXT_OTP,
		VoidSaleScreens_TXT_APPROVAL_DETAILS
	}
}
