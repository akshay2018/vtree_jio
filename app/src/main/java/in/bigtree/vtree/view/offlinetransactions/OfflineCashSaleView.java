package in.bigtree.vtree.view.offlinetransactions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mswipetech.sdk.network.MSGatewayConnectionListener;
import com.mswipetech.wisepad.sdk.MSWisepadOfflineController;
import com.mswipetech.wisepad.sdk.data.CashSaleResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.BaseTitleActivity;
import in.bigtree.vtree.view.cashorbanksale.CashSaleSignatureView;


public class OfflineCashSaleView extends BaseTitleActivity
{
    public final static String log_tab = "OfflineCashSaleView=>";
    //fields for cash sale screen
    EditText mTxtCreditAmountDollars = null;
    ProgressDialog mProgressDialog = null;
    //fields for Credit Notes
    /*EditText mTxtUsername = null;
    EditText mTxtPassword = null;*/
    EditText mTxtPhoneNum = null;
    EditText mTxtEmail = null;
    EditText mTxtReceipt = null;
    EditText mTxtNotes = null;

    EditText mTxtExtraOne = null;
    EditText mTxtExtraTwo = null;
    EditText mTxtExtraThree = null;
    EditText mTxtExtraFour = null;
    EditText mTxtExtraFive = null;
    EditText mTxtExtraSix = null;
    EditText mTxtExtraSeven = null;
    EditText mTxtExtraEight = null;
    EditText mTxtExtraNine = null;
    EditText mTxtExtraTen = null;

    String mNotes = "";
    String mEmail = "";
    String mReceipt = "";
    String mPhoneNum = "";

    CustomProgressDialog mProgressActivity = null;
    SharedVariable applicationData = null;

    /* the mswipe gateway server objects */

    private CashSaleResponseData mCashSaleResponseData;

    // this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
    private long lastRequestTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.offlinecashsale);

        applicationData = SharedVariable.getApplicationDataSharedInstance();

        if (SharedVariable.IS_DEBUGGING_ON)
            Logs.v(SharedVariable.packName, "", true, true);
                
        initViews();

    }

    private void initViews() {


        ((TextView) findViewById(R.id.topbar_LBL_heading)).setText("cash sale");

        //sms prefix from the setting
        ((TextView) findViewById(R.id.cashsale_LBL_countrycodeprefix)).setText(applicationData.smsCode);
        mTxtPhoneNum = (EditText) findViewById(R.id.cashsale_TXT_mobileno);
        mTxtEmail = (EditText) findViewById(R.id.cashsale_TXT_email);
        mTxtReceipt = (EditText) findViewById(R.id.cashsale_TXT_receipt);
        mTxtNotes = (EditText) findViewById(R.id.cashsale_TXT_notes);

        mTxtExtraOne = (EditText) findViewById(R.id.cashsale_TXT_extra_one);
        mTxtExtraTwo = (EditText) findViewById(R.id.cashsale_TXT_extra_two);
        mTxtExtraThree = (EditText) findViewById(R.id.cashsale_TXT_extra_three);
        mTxtExtraFour = (EditText) findViewById(R.id.cashsale_TXT_extra_four);
        mTxtExtraFive = (EditText) findViewById(R.id.cashsale_TXT_extra_five);
        mTxtExtraSix = (EditText) findViewById(R.id.cashsale_TXT_extra_six);
        mTxtExtraSeven = (EditText) findViewById(R.id.cashsale_TXT_extra_seven);
        mTxtExtraEight = (EditText) findViewById(R.id.cashsale_TXT_extra_eight);
        mTxtExtraNine = (EditText) findViewById(R.id.cashsale_TXT_extra_nine);
        mTxtExtraTen = (EditText) findViewById(R.id.cashsale_TXT_extra_ten);

        mTxtCreditAmountDollars = (EditText) findViewById(R.id.cashsale_TXT_amount);
        mTxtCreditAmountDollars.setTypeface(applicationData.font);

        mTxtCreditAmountDollars.setTag("1");
        mTxtCreditAmountDollars.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                // TODO Auto-generated method stub
                if (mTxtCreditAmountDollars.isFocused() && mTxtCreditAmountDollars.getText().length() != 0) {
                                    mTxtCreditAmountDollars.setSelection(mTxtCreditAmountDollars.getText().length());
                                    InputMethodManager imm = (InputMethodManager)
                                            getSystemService(Context.INPUT_METHOD_SERVICE);
                                    if (imm != null) {
                                        imm.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
                                    }
                                    return true;
                                }
                return false;

            }
        });
        mTxtCreditAmountDollars.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (v.getTag().toString().equals("1")) {
                    mTxtCreditAmountDollars.setSelection(mTxtCreditAmountDollars.getText().length());
                }

            }
        });

        mTxtCreditAmountDollars.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
                String oldString = mTxtCreditAmountDollars.getText().toString();
                String newString = removeChar(oldString, '.');
                int ilen = newString.length();
                if (ilen == 1 || ilen == 2) {
                    newString = "." + newString;
                } else if (ilen > 2) {
                    newString = newString.substring(0, ilen - 2) + "." + newString.substring(ilen - 2, ilen);
                }
                if (!newString.equals(oldString)) {
                    mTxtCreditAmountDollars.setText(newString);
                    mTxtCreditAmountDollars.setSelection(mTxtCreditAmountDollars.getText().length());

                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
            	
            }
        });


        Button btnSubmit = (Button) findViewById(R.id.cashsale_BTN_submit);
        btnSubmit.setTypeface(applicationData.font);
        btnSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                //remove the decimal since in j2me it does not exists
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(OfflineCashSaleView.this.getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);


                double miAmountDisplay = 0;
                try {
                	
                    if (mTxtCreditAmountDollars.length() > 0)
                        miAmountDisplay = Double.parseDouble(mTxtCreditAmountDollars.getText().toString());
                    
                } catch (Exception ex) {
                    miAmountDisplay = 0;
                }

                if (miAmountDisplay < 1) {

                    Constants.showDialog(OfflineCashSaleView.this ,Constants.CASHSALE_DIALOG_MSG, Constants.CARDSALE_ERROR_INVALIDAMT);

                    return;

                } else if (mTxtPhoneNum.getText().toString().trim().length() != applicationData.PhoneNoLength) {

                	String phoneLength = String.format(Constants.CARDSALE_ERROR_mobilenolen, applicationData.PhoneNoLength);
                    Constants.showDialog(OfflineCashSaleView.this ,Constants.CASHSALE_DIALOG_MSG, phoneLength);
                    mTxtPhoneNum.requestFocus();
                    return;
                    
                } else if (mTxtPhoneNum.getText().toString().trim().startsWith("0")) {

                    Constants.showDialog(OfflineCashSaleView.this ,Constants.CASHSALE_DIALOG_MSG, Constants.CARDSALE_ERROR_mobileformat);
                    mTxtPhoneNum.requestFocus();
                    return;

                }else if (mTxtReceipt.getText().toString().trim().length() == 0) {

                    Constants.showDialog(OfflineCashSaleView.this, Constants.CASHSALE_DIALOG_MSG, Constants.CARDSALE_ERROR_receiptvalidation);
                    return;
                }

                else {

                    if (mTxtEmail.getText().toString().trim().length() != 0) {

                        if (!Constants.isValidEmail(mTxtEmail.getText().toString().trim()))
                        {
                            Constants.showDialog(OfflineCashSaleView.this ,Constants.CASHSALE_DIALOG_MSG, Constants.CARDSALE_ERROR_email);
                            mTxtEmail.requestFocus();
                            return;
                        }

                    }


                    mPhoneNum = mTxtPhoneNum.getText().toString();
                    mEmail = mTxtEmail.getText().toString();
                    mNotes = mTxtNotes.getText().toString();
                    mReceipt = mTxtReceipt.getText().toString();


                    String dlgMsg = String.format(Constants.CASHSALE_ALERT_AMOUNTMSG, applicationData.Currency_Code);
                    final Dialog dialog = Constants.showDialog(OfflineCashSaleView.this, Constants.CASHSALE_DIALOG_MSG,
                            dlgMsg + mTxtCreditAmountDollars.getText().toString(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_CONFIRMATION);

                    Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);

                    yes.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            dialog.dismiss();
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                            try {



                                if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

                                    if (SharedVariable.IS_DEBUGGING_ON)
                                        Logs.v(SharedVariable.packName, "ReferenceId " + AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(), true, true);


                                    MSWisepadOfflineController.getSharedMSWisepadController(OfflineCashSaleView.this,
                                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
                                            new WisePadGatewayConncetionListener()).processCashSaleOffline(
                                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                                            mTxtCreditAmountDollars.getText().toString(),
                                            mTxtReceipt.getText().toString(),
                                            SharedVariable.smsCode + mTxtPhoneNum.getText().toString(),
                                            mTxtEmail.getText().toString(),
                                            mTxtNotes.getText().toString(),
                                            mTxtExtraOne.getText().toString(),
                                            mTxtExtraTwo.getText().toString(),
                                            mTxtExtraThree.getText().toString(),
                                            mTxtExtraFour.getText().toString(),
                                            mTxtExtraFive.getText().toString(),
                                            mTxtExtraSix.getText().toString(),
                                            mTxtExtraSeven.getText().toString(),
                                            mTxtExtraEight.getText().toString(),
                                            mTxtExtraNine.getText().toString(),
                                            mTxtExtraTen.getText().toString(),
                                            new MSWisepadControllerResponseListenerObserver());

                                  /*  mProgressActivity = new CustomProgressDialog(OfflineCashSaleView.this, "Processing Cash Tx...");
                                    mProgressActivity.show();*/
                                }

                                lastRequestTime = System.currentTimeMillis();

                            	
 
                            } catch (Exception ex) {

                                Constants.showDialog(OfflineCashSaleView.this, Constants.CASHSALE_DIALOG_MSG, Constants.CARDSALE_ERROR_PROCESSIING_DATA, Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_SHOW_DLG_INFO);
                            } finally {

                            }

                        }
                    });

                    Button no = (Button) dialog.findViewById(R.id.customdlg_BTN_no);

                    no.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            mTxtCreditAmountDollars.requestFocus();
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }

            }// end of the funcation
        });

    }


    /**
     * MSWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests
     */
    class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener {

        /**
         * onReponseData
         * The response data notified back to the call back function
         *
         * @param aMSDataStore the generic mswipe data store, this instance is refers to InvoiceTrxData or CardSaleResponseData, so this
         *                     need be converted back to access the relavant data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {

            if(mProgressActivity != null)
                mProgressActivity.dismiss();

            if (aMSDataStore instanceof CashSaleResponseData) {

                mCashSaleResponseData = (CashSaleResponseData) aMSDataStore;

                if (SharedVariable.IS_DEBUGGING_ON)
                    Logs.v(SharedVariable.packName, "Status: " + mCashSaleResponseData.getResponseStatus() + " FailureReason: " + mCashSaleResponseData.getResponseFailureReason() , true, true);

                if (SharedVariable.IS_DEBUGGING_ON)
                    Logs.v(SharedVariable.packName, "ExceptoinStackTrace: " + mCashSaleResponseData.getExceptoinStackTrace(), true, true);

                if(!mCashSaleResponseData.getResponseStatus()){


                    final Dialog dialog = Constants.showDialog(OfflineCashSaleView.this, Constants.BANKSALE_DIALOG_MSG, mCashSaleResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                    Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                    yes.setOnClickListener(new OnClickListener() {

                        public void onClick(View v) {
                            dialog.dismiss();
                            doneWithCashSale();

                        }
                    });
                    dialog.show();
                }
                else {

                    showDialog("transaction is pending for approval");

                }
            }
        }
    }

    public void showSignature() 
    {

        Intent intent = new Intent(this, CashSaleSignatureView.class);

        intent.putExtra("title", getString(R.string.cash_sale));
        intent.putExtra("mAmt", mTxtCreditAmountDollars.getText().toString());
        intent.putExtra("cashSaleResponseData", mCashSaleResponseData);

        startActivity(intent);

        doneWithCashSale();

    }

    public String removeChar(String s, char c)
    {

        String r = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c) r += s.charAt(i);
        }

        return r;
    }

  
    public void doneWithCashSale() {

        finish();

    }


    class WisePadGatewayConncetionListener implements MSGatewayConnectionListener{

		@Override
		public void Connected(String msg) {

			
		}

		@Override
		public void Connecting(String msg) {

			
		}

		@Override
		public void disConnect(String msg) {

			
		}
    }



    private void showDialog(String message){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(message);
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                        /*Intent intent = new Intent(OfflineCashSaleView.this, MenuView.class);
                        startActivity(intent);*/
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }




}
