package in.bigtree.vtree.view.bqrsale.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.BQRIdsListData;
import com.mswipetech.wisepad.sdk.data.BQRIdsResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.ArrayList;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.bqrsale.BQRSaleActivity;

public class MapBQRIdFragment extends Fragment {

	Context mContext;
	BQRSaleActivity mBQRSaleActivity = null;
	SharedVariable applicationData = null;
	private CustomProgressDialog mProgressActivity;

	private ListView mLSTMswipeIds;
	private ImageButton mBTNSubmit;

	private String mSelectedBQRId = "";
    private int mSeletedPosition = -1;

	private ArrayList<BQRIdsListData> mArrBQRIdsData = new ArrayList<BQRIdsListData>();
	SelectMVisaIdAdapter mSelectMVisaIdAdapter = null;

	/**
	 * @description
	 *    Attaching this fragment to Activity.
	 * @param activity
	 */
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub

		mContext = activity;
		mBQRSaleActivity = ((BQRSaleActivity)mContext);
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		super.onAttach(activity);
	}


	/**
	 * Here we are intilising all fields.
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.bqr_getmvisa_id_view, container,false);
		initViews(view);

		return view;
	}

	/**
	 *@description
	 *      All fields intilising here.
	 * @param view
	 */
	private void initViews(View view)
	{

		mLSTMswipeIds = (ListView)view.findViewById(R.id.selectmvisaid_LV);
		mBTNSubmit = (ImageButton)view.findViewById(R.id.selectmvisaid_BTN_next);
		mBTNSubmit.setEnabled(false);

		mLSTMswipeIds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				mSeletedPosition = position;
				mSelectMVisaIdAdapter.notifyDataSetChanged();
				mBTNSubmit.setEnabled(true);
			}
		});


		mBTNSubmit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				mSelectedBQRId = mArrBQRIdsData.get(mSeletedPosition).getSoftposId();
				mapSelectedBQRId(mSelectedBQRId);
			}
		});

		getBQRIdsList();

	}

	public boolean getBQRIdsList() {

		try {

			final MSWisepadController wisepadController = MSWisepadController.
					getSharedMSWisepadController(mBQRSaleActivity,
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

			wisepadController.getBQRIds(
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
					new BQRMEListener());

			mProgressActivity = null;
			mProgressActivity = new CustomProgressDialog(mBQRSaleActivity, getString(R.string.processing));
			mProgressActivity.show();

		} catch (Exception ex) {

			Constants.showDialog(mBQRSaleActivity, getString(R.string.mvisagetmvisaidfragment_mvisa),
					getString(R.string.settings_the_get_me_mvisaid_was_not_processed_successfully_please_try_again));
		}

		return true;
	}

	class BQRMEListener implements MSWisepadControllerResponseListener
	{

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
		 * need be type cast back to LastTransactionResponseData to access the  response data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{
			if (mProgressActivity != null)
				mProgressActivity.dismiss();

			BQRIdsResponseData responseData = (BQRIdsResponseData) aMSDataStore;
			if(responseData.getResponseStatus())
			{

				mArrBQRIdsData = responseData.getmBQRIdsList();
				showmBQRIds();
			}
			else{

				final Dialog dialog = Constants.showDialog(mBQRSaleActivity, getString(R.string.mcard_sale), responseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						dialog.dismiss();
						startActivity(new Intent(mBQRSaleActivity, MenuView.class));
						mBQRSaleActivity.finish();

					}
				});
				dialog.show();

			}
		}
	}

	private void showmBQRIds(){

		mSelectMVisaIdAdapter = new SelectMVisaIdAdapter(mBQRSaleActivity, mArrBQRIdsData);
		mLSTMswipeIds.setAdapter(mSelectMVisaIdAdapter);
		mSelectMVisaIdAdapter.notifyDataSetChanged();
	}

	public boolean mapSelectedBQRId(String mBQRId) {

		try {

			final MSWisepadController wisepadController = MSWisepadController.
					getSharedMSWisepadController(mBQRSaleActivity,
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
							AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

			wisepadController.mapBQRId(
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
					AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
					mBQRId,
					new BQRMapMvisaListener());

			mProgressActivity = null;
			mProgressActivity = new CustomProgressDialog(mBQRSaleActivity, getString(R.string.processing));
			mProgressActivity.show();

		} catch (Exception ex) {

			Constants.showDialog(mBQRSaleActivity, getString(R.string.mvisagetmvisaidfragment_mvisa),
					getString(R.string.settings_the_get_me_mvisaid_was_not_processed_successfully_please_try_again));
		}

		return true;
	}

	class BQRMapMvisaListener implements MSWisepadControllerResponseListener
	{

		/**
		 * onReponseData
		 * The response data notified back to the call back function
		 * @param
		 * aMSDataStore
		 * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
		 * need be type cast back to LastTransactionResponseData to access the  response data
		 * @return
		 */
		public void onReponseData(MSDataStore aMSDataStore)
		{
			if (mProgressActivity != null)
				mProgressActivity.dismiss();

			BQRIdsResponseData bqrResponseData = (BQRIdsResponseData) aMSDataStore;
			if(bqrResponseData.getResponseStatus())
			{
				AppSharedPrefrences.getAppSharedPrefrencesInstace().setVirtualPaymentId(mSelectedBQRId);

				final Dialog dialog = Constants.showDialog(mBQRSaleActivity, getString(R.string.mcard_sale), bqrResponseData.getResponseSuccessMessage(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						dialog.dismiss();
						startActivity(new Intent(mBQRSaleActivity, MenuView.class));
						mBQRSaleActivity.finish();

					}
				});
				dialog.show();
			}
			else{

				final Dialog dialog = Constants.showDialog(mBQRSaleActivity, getString(R.string.mcard_sale), bqrResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
				Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
				yes.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						dialog.dismiss();
						startActivity(new Intent(mBQRSaleActivity, MenuView.class));
						mBQRSaleActivity.finish();

					}
				});
				dialog.show();

			}
		}
	}

	public class SelectMVisaIdAdapter extends BaseAdapter
	{
		ArrayList<BQRIdsListData> listData = null;
		Context context;

		public SelectMVisaIdAdapter(Context context, ArrayList<BQRIdsListData> listData)
		{
			this.listData = listData;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listData.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(R.layout.bqr_getmvisaid_listitem, null);
			}

			TextView txtItem = (TextView) convertView.findViewById(R.id.selectmswipeid_TXT_id);
			if (listData.get(position) != null)
				txtItem.setText(listData.get(position).getVPA());

			ImageView img = (ImageView) convertView.findViewById(R.id.selectmswipeid_IMG_radiobutton);

            if(mSeletedPosition == position)
            {
				img.setBackgroundResource(R.drawable.radio_button_active);
            }
            else{
				img.setBackgroundResource(R.drawable.radio_button_inactive);
			}

			return convertView;
		}
	}
}
