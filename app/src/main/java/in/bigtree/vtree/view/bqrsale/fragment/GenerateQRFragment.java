package in.bigtree.vtree.view.bqrsale.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.BQRResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.bqrsale.BQRSaleActivity;


/**
 * Created by mSwipe on 2/1/2016.
 */
public class GenerateQRFragment extends Fragment {

    Context mContext;
    BQRSaleActivity mBQRSaleActivity = null;
    SharedVariable applicationData = null;

    private CustomProgressDialog mProgressActivity;


    TextView mTXTCheckStatus = null;
    View view;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        mContext = activity;
        mBQRSaleActivity = ((BQRSaleActivity) mContext);
        applicationData = SharedVariable.getApplicationDataSharedInstance();

        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.bqr_qr_view_fragament,container, false);

        initViews(view);
        return view;
    }

    protected void initViews(View view) {

        mTXTCheckStatus = (TextView) view.findViewById(R.id.bqr_LBL_check_status);

        mTXTCheckStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        checkBQRSaleStatus();
                    }
                }, 100);
            }
        });

        new TLVData(mBQRSaleActivity).execute();

    }

    class TLVData extends AsyncTask<Void, Void, Void> {

        private CustomProgressDialog mProgress  = null;
        Context context;

        TLVData(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(mProgress == null) {
                mProgress = new CustomProgressDialog(mBQRSaleActivity, getString(R.string.processing));
                mProgress.show();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            TextView lblAmount = (TextView) view.findViewById(R.id.bqr_TXT_amount);
            lblAmount.setTypeface(applicationData.font);
            lblAmount.setText(SharedVariable.mCurrency + " " + mBQRSaleActivity.mBQRAmount);
            ImageView mqrimage = (ImageView)view.findViewById(R.id.bqr_IMG_qrcode);
            mqrimage.setImageBitmap(mBQRSaleActivity.mBQRBitmap);

            TextView lblMerchantName = (TextView) view.findViewById(R.id.bqr_TXT_merchant_name);
            lblMerchantName.setTypeface(applicationData.font);
            //lblMerchantName.setText(AppSharedPrefrences.getAppSharedPrefrencesInstace().getMerchantName().toLowerCase());

            if(mProgress != null)
                mProgress.dismiss();
        }
    }

    public boolean checkBQRSaleStatus() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mBQRSaleActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            wisepadController.getBQRSaleStatus(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    mBQRSaleActivity.mBQRTrxId,
                    new BQRStatusListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mBQRSaleActivity, getString(R.string.processing));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mBQRSaleActivity, getString(R.string.mcard_sale),
                    getString(R.string.voidsaleactivity_the_otp_was_not_processed_successfully_please_try_again));

        }

        return true;
    }

    class BQRStatusListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            BQRResponseData bqrResponseData = (BQRResponseData) aMSDataStore;
            if(bqrResponseData.getResponseStatus())
            {
                mBQRSaleActivity.mTrnxDate = bqrResponseData.getTrnx_date();
                mBQRSaleActivity.mAuthcode = bqrResponseData.getAuthcode();
                mBQRSaleActivity.mRRNo = bqrResponseData.getRrno();
                mBQRSaleActivity.mVocherNo = bqrResponseData.getVocherno();
                mBQRSaleActivity.mTrnxType = bqrResponseData.getTrnx_type();

                showapproveDialog("approved",mBQRSaleActivity.mAuthcode ,mBQRSaleActivity.mRRNo, "" );

            }
            else{

                mBQRSaleActivity.mErrorMsg = bqrResponseData.getResponseFailureReason();

                showfailedDialog("failed" ,mBQRSaleActivity.mErrorMsg);
            }
        }
    }

    public void showapproveDialog(String status, String authcode, String rrno, String reason)
    {

        final Dialog dialog = new Dialog(mBQRSaleActivity, R.style.styleCustDlg);
        dialog.setContentView(R.layout.cardsale_status_customdlg);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        TextView txtstatusmsg = (TextView) dialog.findViewById(R.id.customdlg_Txt_status);
        txtstatusmsg.setText(status);

        TextView txtauthcode = (TextView) dialog.findViewById(R.id.customdlg_Txt_authcode);
        txtauthcode.setText(authcode);

        TextView txtrrno = (TextView) dialog.findViewById(R.id.customdlg_Txt_rrno);
        txtrrno.setText(rrno);

        TextView txtreason = (TextView) dialog.findViewById(R.id.customdlg_Txt_reason);
        txtreason.setText(reason);

        Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mBQRSaleActivity.finish();
                Intent intent = new Intent(mBQRSaleActivity, MenuView.class);
                startActivity(intent);
            }
        });

        dialog.show();
    }
    public void showfailedDialog(String status , String reason)
    {

        final Dialog dialog = new Dialog(mBQRSaleActivity, R.style.styleCustDlg);
        dialog.setContentView(R.layout.cardsale_status_customdlg);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        TextView txtstatusmsg = (TextView) dialog.findViewById(R.id.customdlg_Txt_status);
        txtstatusmsg.setText(status);

        ((LinearLayout) dialog.findViewById(R.id.customdlg_LNR_authcode)).setVisibility(View.GONE);
        ((LinearLayout) dialog.findViewById(R.id.customdlg_LNR_rrn)).setVisibility(View.GONE);

        TextView txtreason = (TextView) dialog.findViewById(R.id.customdlg_Txt_reason);
        txtreason.setText(reason);

        Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
