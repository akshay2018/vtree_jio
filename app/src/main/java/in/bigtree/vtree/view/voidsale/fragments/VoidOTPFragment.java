package in.bigtree.vtree.view.voidsale.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.OTPResponseData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.voidsale.VoidSaleActivity;
import in.bigtree.vtree.view.voidsale.VoidSaleEnum;


/**
 * Created by mSwipe on 2/1/2016.
 */
public class VoidOTPFragment extends Fragment {

    Context mContext;
    VoidSaleActivity mVoidActivity = null;
    SharedVariable applicationData = null;

    EditText mTXTotp = null;
    ImageButton mBTNext = null;
    TextView mLBLResendOtp;

    // this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
    private long lastRequestTime = 0;

    public static long mProcessedEndTime = 0;


    private CustomProgressDialog mProgressActivity = null;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        mContext = activity;
        mVoidActivity = ((VoidSaleActivity) mContext);
        applicationData = SharedVariable.getApplicationDataSharedInstance();

        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.void_otp_fragment,container, false);

        initViews(view);

        return view;
    }

    protected void initViews(View view) {

        mTXTotp =(EditText)view.findViewById(R.id.void_sale_otp_verification_TXT_otp);

        mLBLResendOtp =(TextView)view.findViewById(R.id.void_sale_LBL_resendotp);


        mBTNext =(ImageButton)view.findViewById(R.id.void_sale_otp_verification_BTN_next);
        mBTNext.setEnabled(false);

        mTXTotp.requestFocus();

        mLBLResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendOTP();

            }
        });

        mTXTotp.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                String searchString = s.toString();
                int textLength = searchString.length();
                mTXTotp.setSelection(textLength);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (mTXTotp.getText().toString().length() >= 4) {

                    mBTNext.setEnabled(true);

                    mLBLResendOtp.setTextColor(getResources().getColor(R.color.light_grey));
                    mLBLResendOtp.setClickable(false);

                } else {

                    mBTNext.setEnabled(false);
                    mLBLResendOtp.setTextColor(getResources().getColor(R.color.blue));
                    mLBLResendOtp.setClickable(true);
                }
            }
        });

        mBTNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

                    mVoidActivity.processCardSaleVoid(mTXTotp.getText().toString());
                }

                lastRequestTime = System.currentTimeMillis();

            }
        });
    }


    public boolean sendOTP() {

        try {


            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mVoidActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);


            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

            wisepadController.initiateVoidTransactionWithOTP(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    new MSWisepadControllerOTPListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mVoidActivity, getString(R.string.void_sale));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mVoidActivity, getString(R.string.void_sale),
                    getString(R.string.voidsaleactivity_the_otp_was_not_processed_successfully_please_try_again));

        }

        return true;
    }


    /**
     * MswipeWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

     */
    class MSWisepadControllerOTPListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            OTPResponseData voidTransactionResponseData = (OTPResponseData) aMSDataStore;

            if(voidTransactionResponseData.getResponseStatus())
            {

                String mobileNum = AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId();

                StringBuilder maskedMobileNum = new StringBuilder(mobileNum);

                maskedMobileNum.setCharAt(3, 'x');
                maskedMobileNum.setCharAt(4, 'x');
                maskedMobileNum.setCharAt(6, 'x');
                maskedMobileNum.setCharAt(7, 'x');
                maskedMobileNum.setCharAt(8, 'x');

                final Dialog dialog = Constants.showDialog(mVoidActivity, getString(R.string.void_sale), String.format(getResources().getString(R.string.otp_sent_message), maskedMobileNum.toString()), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        dialog.dismiss();
                        mVoidActivity.showScreen(VoidSaleEnum.VoidSaleScreens.VoidSaleScreens_TXT_OTP);

                    }
                });
                dialog.show();

            }
            else{

                final Dialog dialog = Constants.showDialog(mVoidActivity, getString(R.string.void_sale), voidTransactionResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });
                dialog.show();

            }
        }
    }


}
