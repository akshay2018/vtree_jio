package in.bigtree.vtree.view.bqrsale.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.BQRResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.bqrsale.BQRSaleActivity;
import in.bigtree.vtree.view.bqrsale.BQRSaleEnum;

public class EnterAmountFragment extends Fragment {

    Context mContext;
    BQRSaleActivity mBQRSaleActivity = null;
    SharedVariable applicationData = null;
    View viewamount;

    //fields for card sale screen
    private EditText mTxtAmt = null;
    EditText mTxtPhoneNum = null;
    EditText mTxtReceipt = null;
    EditText mTxtExtraNote1 = null;
    EditText mTxtExtraNote2 = null;
    EditText mTxtExtraNote3 = null;
    EditText mTxtExtraNote4 = null;
    EditText mTxtExtraNote5 = null;
    EditText mTxtExtraNote6 = null;
    EditText mTxtExtraNote7 = null;
    EditText mTxtExtraNote8 = null;
    EditText mTxtExtraNote9 = null;
    EditText mTxtExtraNote10 = null;


    private ImageButton mImageButtonSubmit = null;

    private CustomProgressDialog mProgressActivity;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub

        mContext = activity;
        mBQRSaleActivity = ((BQRSaleActivity) mContext);
        applicationData = SharedVariable.getApplicationDataSharedInstance();
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        viewamount = inflater.inflate(R.layout.bqrsale_amountview, container, false);

        initViews(viewamount);
        setDefaultData();

        return viewamount;
    }


    private void setDefaultData() {

        if (!mBQRSaleActivity.mBQRAmount.equalsIgnoreCase("0.00"))
            mTxtAmt.setText(mBQRSaleActivity.mBQRAmount);

    }

    public void initViews(View view) {
        // TODO Auto-generated method stub

        mTxtAmt = (EditText) view.findViewById(R.id.bqr_saleinputdetailsview_TXT_amount);
        mTxtPhoneNum = (EditText) view.findViewById(R.id.payment_TXT_mobileno);
        mTxtReceipt = (EditText) view.findViewById(R.id.payment_TXT_receipt);

        mTxtExtraNote1 = (EditText) view.findViewById(R.id.bqr_TXT_extra_one);
        mTxtExtraNote2 = (EditText) view.findViewById(R.id.bqr_TXT_extra_two);
        mTxtExtraNote3 = (EditText) view.findViewById(R.id.bqr_TXT_extra_three);
        mTxtExtraNote4 = (EditText) view.findViewById(R.id.bqr_TXT_extra_four);
        mTxtExtraNote5 = (EditText) view.findViewById(R.id.bqr_TXT_extra_five);
        mTxtExtraNote6 = (EditText) view.findViewById(R.id.bqr_TXT_extra_six);
        mTxtExtraNote7 = (EditText) view.findViewById(R.id.bqr_TXT_extra_seven);
        mTxtExtraNote8 = (EditText) view.findViewById(R.id.bqr_TXT_extra_eight);
        mTxtExtraNote9 = (EditText) view.findViewById(R.id.bqr_TXT_extra_nine);
        mTxtExtraNote10 = (EditText) view.findViewById(R.id.bqr_TXT_extra_ten);

        mImageButtonSubmit = (ImageButton) view.findViewById(R.id.bqr_saleinputdetailsview_BTN_next);

        mImageButtonSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);

                if (imm != null)
                    imm.hideSoftInputFromWindow(new View(mBQRSaleActivity).getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                double miAmountDisplay = 0;
                try {
                    if (mTxtAmt.getText().toString().length() > 0)
                        miAmountDisplay = Double.parseDouble(removeChar(mTxtAmt.getText().toString(), ','));


                } catch (Exception ex) {
                    miAmountDisplay = 0;
                }


                if (miAmountDisplay < 1) {
                    Constants.showDialog(mBQRSaleActivity, "bqr sale",
                            String.format(getString(R.string.voidsaleamountfragment_you_need_to_enter_a_minimum_amount_of_at_least_inr_001_to_proceed), applicationData.mCurrency));

                    return;
                }
                else if (mTxtPhoneNum.getText().toString().trim().length() > 0 && mTxtPhoneNum.getText().toString().trim().length() != 10) {

                    Constants.showDialog(mBQRSaleActivity, "bqr sale", "required length of the mobile number is 10 digits.");
                    mTxtPhoneNum.requestFocus();

                    return;
                }
                else if (mTxtPhoneNum.getText().toString().trim().startsWith("0")) {

                    Constants.showDialog(mBQRSaleActivity, "bqr sale", "the mobile number cannot start with 0.");
                    mTxtPhoneNum.requestFocus();

                    return;

                }
                else {

                    mBQRSaleActivity.mBQRAmount = removeChar(mTxtAmt.getText().toString(),',');
                    mBQRSaleActivity.mBQRReceipt = mTxtReceipt.getText().toString().trim();
                    mBQRSaleActivity.mBQRMobileNo = mTxtPhoneNum.getText().toString().trim();

                    getBQRData();

                }
            }
        });
    }

    public String removeChar(String s, char c) {

        String r = "";

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c)
                r += s.charAt(i);
        }

        return r;
    }

    public boolean getBQRData() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(mBQRSaleActivity,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),null);

            wisepadController.generateBQRCode(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    mBQRSaleActivity.mBQRAmount,
                    mBQRSaleActivity.mBQRReceipt,
                    mBQRSaleActivity.mBQRMobileNo,
                    mTxtExtraNote1.getText().toString(),
                    mTxtExtraNote2.getText().toString(),
                    mTxtExtraNote3.getText().toString(),
                    mTxtExtraNote4.getText().toString(),
                    mTxtExtraNote5.getText().toString(),
                    mTxtExtraNote6.getText().toString(),
                    mTxtExtraNote7.getText().toString(),
                    mTxtExtraNote8.getText().toString(),
                    mTxtExtraNote9.getText().toString(),
                    mTxtExtraNote10.getText().toString(),
                    500,
                    500,
                    new BQRListener());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(mBQRSaleActivity, getString(R.string.processing));
            mProgressActivity.show();

        } catch (Exception ex) {

            Constants.showDialog(mBQRSaleActivity, "bqr sale",
                    "the bqr sale transaction was not processed successfully. please try again");

        }

        return true;
    }

    class BQRListener implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {
            if (mProgressActivity != null)
                mProgressActivity.dismiss();

            BQRResponseData bqrResponseData = (BQRResponseData) aMSDataStore;
            if(bqrResponseData.getResponseStatus())
            {
                mBQRSaleActivity.mBQRBitmap = bqrResponseData.getmQrBitmap();
                mBQRSaleActivity.mBQRTrxId = bqrResponseData.mUPIId;

                mBQRSaleActivity.showScreen(BQRSaleEnum.BQRSaleScreens.BQR_SALE_GENERATE_QR);
            }
            else{

                final Dialog dialog = Constants.showDialog(mBQRSaleActivity, getString(R.string.mcard_sale), bqrResponseData.getResponseFailureReason(), Constants.CUSTOM_DLG_TYPE.CUSTOM_DLG_TYPE_RETURN_DLG_INFO);
                Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        Intent intent = new Intent(mBQRSaleActivity, MenuView.class);
                        startActivity(intent);
                        mBQRSaleActivity.finish();

                        dialog.dismiss();

                    }
                });
                dialog.show();

            }
        }
    }
}
