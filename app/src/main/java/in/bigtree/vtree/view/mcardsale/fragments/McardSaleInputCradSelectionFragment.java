package in.bigtree.vtree.view.mcardsale.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.camerascan.ScannerActivity;
import in.bigtree.vtree.view.mcardsale.McardSaleActivity;
import in.bigtree.vtree.view.mcardsale.McardSaleEnum;

public class McardSaleInputCradSelectionFragment extends Fragment
{
	LinearLayout mLNREnter;
	LinearLayout mLNRScan;

	ImageView mIMGSwipe;
	ImageView mIMGEnter;
	ImageView mIMGScan;

	TextView mLBLSwipe;
	TextView mLBLEnter;
	TextView mLBLScan;

	Context mContext;
	McardSaleActivity mCardActivity = null;
	SharedVariable applicationData = null;

	String mScanDetails;


	public static final int SWIPE_REQUEST = 1999;
	public static final int SCAN_REQUEST = 1003;


	/**
	 * @description
	 *    Attaching this fragment to Activity.
	 * @param activity
	 */
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub

		mContext = activity;
		mCardActivity = ((McardSaleActivity)mContext);
		applicationData = SharedVariable.getApplicationDataSharedInstance();
		super.onAttach(activity);
	}

	/**
	 * Here we are intilising all fields.
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.mcardsale_inputcard_selection, container,false);initViews(view);
		return view;
	}



	/**
	 *@description
	 *      All fields intilising here.
	 * @param view
	 */
	protected void initViews(View view)
	{

		mLNREnter = (LinearLayout) view.findViewById(R.id.mcardsale_selction_LIN_enter);
		mLNRScan = (LinearLayout) view.findViewById(R.id.mcardsale_selction_LIN_scan);

		mIMGSwipe = (ImageView) view.findViewById(R.id.mcardsale_selction_IMG_swipe);
		mIMGEnter = (ImageView) view.findViewById(R.id.mcardsale_selction_IMG_enter);
		mIMGScan = (ImageView) view.findViewById(R.id.mcardsale_selction_IMG_scan);

		mLBLSwipe = (TextView) view.findViewById(R.id.mcardsale_selction_LBL_swipe);
		mLBLEnter = (TextView) view.findViewById(R.id.mcardsale_selction_LBL_enter);
		mLBLScan = (TextView) view.findViewById(R.id.mcardsale_selction_LBL_scan);

		((TextView)view.findViewById(R.id.mcardsale_selction_LBL_totalamount)).setText(mCardActivity.mTransactionData.mTotAmount);

		mLNREnter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				mIMGEnter.setImageResource(R.drawable.tap_active);
				mLBLEnter.setTextColor(getResources().getColor(R.color.blue));

				mIMGSwipe.setImageResource(R.drawable.swipe_inactive);
				mLBLSwipe.setTextColor(getResources().getColor(R.color.dark_grey));

				mIMGScan.setImageResource(R.drawable.scan_inactive);
				mLBLScan.setTextColor(getResources().getColor(R.color.dark_grey));

				mCardActivity.mCardInputType = McardSaleEnum.CardInputType.ENTER;

				mCardActivity.mTopUpScanCardNum= new StringBuilder("");
				mCardActivity.showScreen(McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_INPUT_CARD_NUM);
			}
		});

		mLNRScan.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				mIMGScan.setImageResource(R.drawable.scan_active);
				mLBLScan.setTextColor(getResources().getColor(R.color.blue));

				mIMGSwipe.setImageResource(R.drawable.swipe_inactive);
				mLBLSwipe.setTextColor(getResources().getColor(R.color.dark_grey));

				mIMGEnter.setImageResource(R.drawable.tap_inactive);
				mLBLEnter.setTextColor(getResources().getColor(R.color.dark_grey));

				mCardActivity.mCardInputType = McardSaleEnum.CardInputType.SCAN;
				mCardActivity.IsScan = true;
				Intent intent = new Intent(mCardActivity, ScannerActivity.class);
				startActivityForResult(intent, SCAN_REQUEST);
			}
		});


	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (SharedVariable.IS_DEBUGGING_ON)
			Logs.v(SharedVariable.packName, "requestCode " + requestCode, true, true);


		if (requestCode == SCAN_REQUEST) {

			if (resultCode == mCardActivity.RESULT_OK) {

				mScanDetails = data.getData().toString();

				if (SharedVariable.IS_DEBUGGING_ON)
					Logs.v(SharedVariable.packName, "mScanDetails "  +mScanDetails, true, true);

				if(mScanDetails.matches("\\d+")){

					StringBuilder result = new StringBuilder();

					for (int i = 0; i < mScanDetails.length(); i++) {
						if (i % 4 == 0 && i != 0) {
							result.append("-");
						}
						result.append(mScanDetails.charAt(i));
					}

					if (SharedVariable.IS_DEBUGGING_ON)
						Logs.v(SharedVariable.packName, "mETCardNo "  +result, true, true);

					mCardActivity.mTopUpScanCardNum = (result);
					mCardActivity.showScreen(McardSaleEnum.McardSaleScreens.MCARD_CARDSALE_INPUT_CARD_NUM);


				} else{
					Constants.showDialog(mCardActivity, "", "scan valid card number");
					return;
				}
			}
		}
	}
}