package in.bigtree.vtree.view.summary;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.data.SummaryDetailsResponseData;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import java.util.Calendar;

import in.bigtree.vtree.R;
import in.bigtree.vtree.SharedVariable;
import in.bigtree.vtree.customviews.CustomProgressDialog;
import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.util.Constants;
import in.bigtree.vtree.view.BaseTitleActivity;

public class QRSaleSummary extends BaseTitleActivity
{

    CustomProgressDialog mProgressActivity = null;

    private static final String[] MONTHS = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private int year;
    private int month;
    private int day;
    public String mSelectedDate = "";

    static final int DATE_DIALOG_ID = 999;

    TextView lblSummaryDate = null;
    SharedVariable applicationData = null;

    // this time will handle the duplicate transactions, by ignoring the click action for 5 seconds time interval
    private long lastRequestTime = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.cardsummary);
        applicationData = (SharedVariable) getApplicationContext();

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        initViews();
    }


    private void initViews() {
        ((TextView) findViewById(R.id.topbar_LBL_heading)).setText("qr sale summary");
        ((TextView) findViewById(R.id.topbar_LBL_heading)).setTypeface(applicationData.font);

        lblSummaryDate = (TextView) findViewById(R.id.cardsummary_LBL_date);
        lblSummaryDate.setText(day + "/" + MONTHS[month] + "/" + year);
        mSelectedDate = year + "-" + (month + 1) + "-" + day;

        lblSummaryDate.setTypeface(applicationData.font);
       

        Button btnSubmit = (Button) findViewById(R.id.cardsummary_BTN_submit);
        btnSubmit.setTypeface(applicationData.font);

        btnSubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (System.currentTimeMillis() - lastRequestTime >= SharedVariable.REQUEST_THRESHOLD_TIME) {

                    getCardSummary();
                }

                lastRequestTime = System.currentTimeMillis();

            }
        });


    }

    public void getCardSummary() {

        try {

            final MSWisepadController wisepadController = MSWisepadController.
                    getSharedMSWisepadController(QRSaleSummary.this,
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment(),
                            AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource(),
                            null);

            MSWisepadController.setNetworkSource(AppSharedPrefrences.getAppSharedPrefrencesInstace().getNetworkSource());

            wisepadController.getQRSaleSummary(
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getReferenceId(),
                    AppSharedPrefrences.getAppSharedPrefrencesInstace().getSessionToken(),
                    mSelectedDate,
                    new MSWisepadControllerResponseListenerObserver());

            mProgressActivity = null;
            mProgressActivity = new CustomProgressDialog(QRSaleSummary.this, "Fetching Summary...");
            mProgressActivity.show();

        }
        catch (Exception e)
        {
            Constants.showDialog(QRSaleSummary.this, "qr sale summary", "The qr sale summary was not processed successfully please try again");
            e.printStackTrace();
        }

    }



    /**
     * MswipeWisepadControllerResponseListenerObserver
     * The mswipe overridden class  observer which listens to the response to the mswipe sdk function requests

     */
    class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener
    {

        /**
         * onReponseData
         * The response data notified back to the call back function
         * @param
         * aMSDataStore
         * 		the generic mswipe data store, this instance refers to LastTransactionResponseData, so this
         * need be type cast back to LastTransactionResponseData to access the  response data
         * @return
         */
        public void onReponseData(MSDataStore aMSDataStore)
        {

            SummaryDetailsResponseData cardSummaryDetailsResponseData = (SummaryDetailsResponseData) aMSDataStore;
            mProgressActivity.dismiss();

            if (cardSummaryDetailsResponseData.getResponseStatus())
            {

                Intent intent = new Intent(QRSaleSummary.this, QRSaleSummaryDetails.class);
                intent.putExtra("CardSaleAmount", cardSummaryDetailsResponseData.getSaleAmount());
                intent.putExtra("VoidSaleAmount", cardSummaryDetailsResponseData.getVoidAmount());
                intent.putExtra("CardSaleCount", cardSummaryDetailsResponseData.getCardCount());
                intent.putExtra("VoidSaleCount", cardSummaryDetailsResponseData.getVoidCount());
                intent.putExtra("TotalAmount", cardSummaryDetailsResponseData.getTotalAmount());
                intent.putExtra("TotalSwipesAndVoids", cardSummaryDetailsResponseData.getTotalSalesAndVoids());

                intent.putExtra("SummaryDate", lblSummaryDate.getText().toString());

                startActivity(intent);
            }
            else {

                Constants.showDialog(QRSaleSummary.this, "qr sale summary", cardSummaryDetailsResponseData.getResponseFailureReason());


            }
        }
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            lblSummaryDate.setText(day + "/" + MONTHS[month] + "/" + year);
            mSelectedDate = year + "-" + (month + 1) + "-" + day;


        }
    };

    public void onDateClicked(View v){

        showDialog(DATE_DIALOG_ID);
    }

}
