package in.bigtree.vtree.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import in.bigtree.vtree.R;

/**
 * HelpView 
 * HelpView activity hosts the details of the help support and the version details of the SDK version 
 */

public class HelpView extends Dialog implements android.view.View.OnClickListener
{
	public HelpView(Context context){
		super(context, R.style.styleCustDlg);
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_help);
		
			
		Button button = (Button) findViewById(R.id.helpview_BTN_ok);
		button.setOnClickListener(this);
	}
 
	@Override
	public void onClick(View v)
	{
		switch (v.getId()) 
		{
		
			case R.id.helpview_BTN_ok:
				this.dismiss();
				break;
	
		}
	}
}
