package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SYNCCONRESPONSEDATA {

    @SerializedName("Cinema")
    @Expose
    private List<Cinema> cinema = null;

    public List<Cinema> getCinema() {
        return cinema;
    }

    public void setCinema(List<Cinema> cinema) {
        this.cinema = cinema;
    }
}