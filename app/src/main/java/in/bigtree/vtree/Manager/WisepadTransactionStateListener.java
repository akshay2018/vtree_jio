package in.bigtree.vtree.Manager;

import com.mswipetech.wisepad.sdk.device.WisePadConnection;
import com.mswipetech.wisepad.sdk.device.WisePadTransactionState;

import java.util.Hashtable;

import in.bigtree.vtree.Manager.services.CreditSaleMessageEnum.WisePosStateInfoType;


public interface WisepadTransactionStateListener {

	public void onReturnWisepadConnectionState(WisePadConnection aWisePadConnection);

	public void onReturnWisepadTransactionState(WisePadTransactionState aWisePadTransactionState, String aMessage, WisePosStateInfoType aWisePosStateInfoType);

	public void onReturnWisepadDeviceInfo(Hashtable<String, String> deviceInfoData);

}
