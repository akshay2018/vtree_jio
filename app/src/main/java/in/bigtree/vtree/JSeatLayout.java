package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JSeatLayout {

    @SerializedName("SEAT_RESPONSE_CODE")
    @Expose
    private String sEATRESPONSECODE;
    @SerializedName("SEAT_RESPONSE_DATA")
    @Expose
    private SEATRESPONSEDATA sEATRESPONSEDATA;

    public String getSEATRESPONSECODE() {
        return sEATRESPONSECODE;
    }

    public void setSEATRESPONSECODE(String sEATRESPONSECODE) {
        this.sEATRESPONSECODE = sEATRESPONSECODE;
    }

    public SEATRESPONSEDATA getSEATRESPONSEDATA() {
        return sEATRESPONSEDATA;
    }

    public void setSEATRESPONSEDATA(SEATRESPONSEDATA sEATRESPONSEDATA) {
        this.sEATRESPONSEDATA = sEATRESPONSEDATA;
    }

}