package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

public class SetUp extends AppCompatActivity {

    Button btnSaveSetup;
    Context context;
    String strGSlayout = "";
    EditText edServerIp,tvWCode, edCId;
    Switch swHttps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        try{
            context = this;
            btnSaveSetup = (Button)findViewById(R.id.btnSaveSetup);
            strGSlayout = "full";
            edServerIp = (EditText) findViewById(R.id.edServerIp);
            tvWCode = (EditText) findViewById(R.id.edWCode);
            edCId = (EditText) findViewById(R.id.edCId);
            swHttps = (Switch)findViewById(R.id.swHttps);
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(SetUp.this,"Error","Something went wrong");
        }
    }

    public void SaveSetup(View v){
        String strIp = "", strWCode = "", strClientID = "", strFileContent = "", strFileName = "", isHttps = "";
        boolean blnWorking = false;
        try {
            //strFileName = getResources().getString(R.string.config_file_name);
            strFileName = getResources().getString(R.string.connection_config_file);


            strIp = edServerIp.getText().toString();
            strWCode = tvWCode.getText().toString();
            strClientID = edCId.getText().toString();
            isHttps = swHttps.isChecked() ? "Y" : "N";
            WifiManager wifiMan = (WifiManager) getApplicationContext().getSystemService(getApplicationContext().WIFI_SERVICE);
            WifiInfo wifiInf = wifiMan.getConnectionInfo();
            //int ipAddress = wifiInf.getIpAddress();
            //strClientID = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));

            if(strIp.equals("") || strClientID.equals("") || strWCode.equals("")) {
                CustomDialog cDialog = new CustomDialog();
                cDialog.ShowErrorDialog(getApplicationContext(),"Values cannot be blank","");
            }
            else{
                FileIO obFileIO = new FileIO();
                //check for ip connectivity
                blnWorking = obFileIO.checkURL(strIp);
                if(!blnWorking){
                    CustomDialog cDialog = new CustomDialog();
                    cDialog.ShowErrorDialog(SetUp.this,"No Connection","SERVER'S IP IS NOT REACHABLE");
                    return;
                }
                strFileContent += String.format("%s|%s|%s|%s", strIp, strWCode, strClientID,isHttps);
                //strFileName = getResources().getString(R.string.config_file_name);
                strFileName = getResources().getString(R.string.connection_config_file);
                Bundle bundle = new Bundle();
                bundle.putString("prev","setup");
                obFileIO.createFile(strFileName, strFileContent, getApplicationContext());
                Intent mainIntent = new Intent(getApplicationContext(), Login.class);
                mainIntent.putExtras(bundle);
                startActivity(mainIntent);
            }
            //Intent intLogin = new Intent(SetUp.this, Login.class);
            //startActivity(intLogin);
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(SetUp.this,"Error","Unable to set up");
        }
    }


    public void setSeatLayoutType(View v){
        try{
            Button mBtn = (Button) v;
            if(mBtn.getTag().toString().equals("fullsl")){
                Button pBtn = (Button)findViewById(R.id.btnPartialSL);
                pBtn.setBackground(ContextCompat.getDrawable(context,R.drawable.menu_curved_back));
                pBtn.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));

                mBtn.setBackground(ContextCompat.getDrawable(context,R.drawable.btn_curved_back_filled));
                mBtn.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));

                strGSlayout = "full";
            }
            else{
                Button fBtn = (Button)findViewById(R.id.btnFullSL);
                fBtn.setBackground(ContextCompat.getDrawable(context,R.drawable.menu_curved_back));
                fBtn.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));

                mBtn.setBackground(ContextCompat.getDrawable(context,R.drawable.btn_curved_back_filled));
                mBtn.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));

                strGSlayout = "partial";
            }
        }
        catch (Exception Ex){
            CustomDialog cDiag= new CustomDialog();
            cDiag.ShowErrorDialog(SetUp.this,"Error","Something went wrong");
        }
    }
}
