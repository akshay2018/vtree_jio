package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SEATRESPONSEDATA {

    @SerializedName("TempTransID")
    @Expose
    private String tempTransID;
    @SerializedName("Area")
    @Expose
    private List<Area> area = null;

    public String getTempTransID() {
        return tempTransID;
    }

    public void setTempTransID(String tempTransID) {
        this.tempTransID = tempTransID;
    }

    public List<Area> getArea() {
        return area;
    }

    public void setArea(List<Area> area) {
        this.area = area;
    }

}