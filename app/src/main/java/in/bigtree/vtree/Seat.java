package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Seat {

    @SerializedName("strGridSeatNum")
    @Expose
    private String strGridSeatNum;
    @SerializedName("strSeatNumber")
    @Expose
    private String strSeatNumber;
    @SerializedName("strSeatStatus")
    @Expose
    private String strSeatStatus;

    public String getStrGridSeatNum() {
        return strGridSeatNum;
    }

    public void setStrGridSeatNum(String strGridSeatNum) {
        this.strGridSeatNum = strGridSeatNum;
    }

    public String getStrSeatNumber() {
        return strSeatNumber;
    }

    public void setStrSeatNumber(String strSeatNumber) {
        this.strSeatNumber = strSeatNumber;
    }

    public String getStrSeatStatus() {
        return strSeatStatus;
    }

    public void setStrSeatStatus(String strSeatStatus) {
        this.strSeatStatus = strSeatStatus;
    }

}