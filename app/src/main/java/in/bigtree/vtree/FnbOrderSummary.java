package in.bigtree.vtree;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pnsol.sdk.auth.AccountValidator;
import com.pnsol.sdk.interfaces.DeviceType;
import com.pnsol.sdk.interfaces.PaymentTransactionConstants;
import com.pnsol.sdk.vo.response.LoginResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.bigtree.vtree.PayU_Printer.PaymentTransactionActivity;

public class FnbOrderSummary extends AppCompatActivity implements AsyncResponse, CustomBottomSheetDialogFragment.BottomSheetListener, PaymentTransactionConstants {
    JSONArray jArray;
    JSONObject jObject;
    TableLayout tblOS;
    TextView tvCinema, tvTotalAmt, tvTotalQty;
    Bundle bundle;
    String strFileContent = "", strTotalAmt = "", strAdvFileContent = "", strIP = "", strGAction = "", strJsonArray = "", strBookingNo = "", strTempTransId = "", strTotalQty = "", strGPType = "", strGPayMode="", strStockQty = "", strCounterEnabled = "", strWCODE = "";
    AlertDialog dialogProg;
    JSessions obJsonSession;
    BottomSheetDialogFragment bottomSheetDialogFragment;
    ArrayAdapter<String> arrSpnFilmAdapter;
    Button btnCounter, btnProceedCash,btnProceedCard;
    LinearLayout llBack;
    EditText edComments, edPhone;
    FrameLayout flIgPaymentList;

    //PAYU
    private static String DEVICE_COMMUNICATION_MODE = "transactionmode";
    private static String DEVICE_TYPE = "devicetype";
    private static String MAC_ADDRESS = "macAddress";
    private static String DEVICE_NAME = "devicename";
    private String deviceType, deviceName;
    private int deviceCommMode;
    private String mkey, pkey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fnb_order_summary);
        try {
            flIgPaymentList = findViewById(R.id.flIgPaymentList);
            bottomSheetDialogFragment = new CustomBottomSheetDialogFragment();
            strJsonArray = getIntent().getStringExtra("jsonArray");
            tblOS = (TableLayout) findViewById(R.id.tblOrderSummary);
            tvCinema = (TextView)findViewById(R.id.tvCinema);
            tvTotalAmt = (TextView) findViewById(R.id.tvTotalPrice);
            tvTotalQty = (TextView) findViewById(R.id.tvTotalQty);
            //btnCounter = (Button)findViewById(R.id.btnCounter);
            btnProceedCash = (Button)findViewById(R.id.btnProceedCash);
            btnProceedCard = (Button)findViewById(R.id.btnProceedCard);
            edComments = (EditText) findViewById(R.id.tvComments);
            edPhone = (EditText)findViewById(R.id.tvMobileNo);

            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];
            if (strFileContent.split("\\|").length > 1) {strWCODE = strFileContent.split("\\|")[1];} else {strWCODE = "";}

            llBack = (LinearLayout) findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToMain(v));
            SharedVariable obShared = (SharedVariable)getApplicationContext();
            //tvCinema.setText(obShared.getCID());
            bundle = getIntent().getExtras();
            tvTotalQty.setText(bundle.getString("totalqty"));
            strTotalAmt = bundle.getString("totalamt");
            tvTotalAmt.setText(strTotalAmt);
            jArray = new JSONArray(strJsonArray);
            for(int iCount = 0; iCount < jArray.length(); iCount++){
                jObject = (JSONObject) jArray.get(iCount);
                Log.d("","");
                TableRow tblRowOrder = (TableRow) LayoutInflater.from(FnbOrderSummary.this).inflate(R.layout.tablerow_ordersummary,null);
                ((TextView) tblRowOrder.findViewById(R.id.tvOrderItemName)).setText(jObject.get("itemname").toString());
                ((TextView) tblRowOrder.findViewById(R.id.tvOrderQty)).setText(jObject.get("qty").toString());
                ((TextView) tblRowOrder.findViewById(R.id.tvOrderAmt)).setText(jObject.get("totalprice").toString());

                ((ImageView) tblRowOrder.findViewById(R.id.ivOrderRemove)).setTag("remove" + "|" + jObject.get("itemid").toString()+ "|" + jObject.get("qty").toString() + "|" + jObject.get("price").toString() + "|" + jObject.get("totalprice").toString() + "|" + jObject.get("stockqty").toString());
                ((ImageView) tblRowOrder.findViewById(R.id.ivOrderAdd)).setTag("add" + "|" + jObject.get("itemid").toString()+ "|" + jObject.get("qty").toString() + "|" + jObject.get("price").toString() + "|" + jObject.get("totalprice").toString() + "|" + jObject.get("stockqty").toString());

                ((ImageView) tblRowOrder.findViewById(R.id.ivOrderAdd)).setOnClickListener(AddItemListener);
                ((ImageView) tblRowOrder.findViewById(R.id.ivOrderRemove)).setOnClickListener(RemoveItemListener);

                tblOS.addView(tblRowOrder);
            }

            edPhone.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        if (s.length() < 10) {
                            btnProceedCash.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
                            btnProceedCash.setClickable(false);
                            btnProceedCard.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
                            btnProceedCard.setClickable(false);
                        }
                        else{
                            btnProceedCash.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorJwcDarkBrown));
                            btnProceedCash.setClickable(true);
                            btnProceedCard.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorJwcDarkBrown));
                            btnProceedCard.setClickable(true);
                            closeKeyboard();
                        }

                    } catch (Exception Ex) {
                        Ex.toString();
                    }
                }
            });
        } catch (JSONException JEx) {
            ShowErrorDialog(FnbOrderSummary.this,"Error","JSON Error");
        }
        catch (Exception Ex) {
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    public void backToMain(View view) {
        try{
            Intent resultIntent = new Intent();
            resultIntent.putExtra("jsonArray", jArray.toString());
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    public void proceedToCart(View view){
        String strItemVarietyQty = "", strItemId = "", strITemQty = "", strItemDesc = "", strWcode = "";
        try{
            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];
            if (strFileContent.split("\\|").length > 1) {strWcode = strFileContent.split("\\|")[1];} else {strWcode = "";}
            strItemVarietyQty = Integer.toString(jArray.length());
            strItemDesc = "|" + strItemVarietyQty + "|";

            for(int iCount = 0; iCount < jArray.length(); iCount++) {
                jObject = (JSONObject) jArray.get(iCount);
                strItemId = jObject.get("itemid").toString();
                strITemQty = jObject.get("qty").toString();

                strItemDesc += strItemId + "|" + strITemQty + "|-1|";
            }
            ShowProgressDialog(FnbOrderSummary.this,"Processing","");
            strGAction = "ADDCONITEM";
            SoapCall obSc = new SoapCall(FnbOrderSummary.this);
            obSc.delegate = this;
            obSc.execute("ADDCONITEM",strIP, strItemVarietyQty, strItemDesc, strWcode,"","","","","","","","","","","","");
        }
        catch (Exception Ex){
            dialogProg.dismiss();
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    @Override
    public void processFinish(String output) {
        try {
            //dialogProg.dismiss();
            if (!output.split("\\|")[0].equals("1")) {
                if (strGAction.equals("ADDCONITEM")) {
                    //subProceedToPickupType(output);
                    if(strGPType.equals("seat")){
                        subProceedToPickupTypeEx(output);
                    }
                    else if(strGPType.equals("counter")){
                        processCounter(output);
                    }
                }
                else if (strGAction.equals("SESSION")) {
                    processShowFilms(output);
                }
                else if (strGAction.equals("SETFNBPICKUPORDELIVERYEX")) {
                    processPickupDelivery(output, "S");
                }
                else if(strGAction.equals("COMMIT")){
                    proceedConfirmation(output);
                }
                else if(strGPayMode.equals("CARDDETAILS")){
                    processmSwipeCreds(output);
                }
            } else {
                ShowErrorDialog(FnbOrderSummary.this,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    private void proceedConfirmation(String output) {
        String strTotal = "", strCat = "", strTotalAmount = "", strResp = "", strFSummData = "", strFnBPrintData = "", strTransNo = "";
        try {
            JCResp obJsonCommit = new Gson().fromJson(output, JCResp.class);
            if (obJsonCommit.getRESPONSECODE().equals("0")) {
                strResp = obJsonCommit.getRESPONSEDATA();
                strFSummData = strResp.split("\\@")[0];
                strFnBPrintData = strResp.split("\\@")[1];
                Bundle pcBundle = new Bundle();
                strTotal = strFSummData.split("\\|")[8];
                Intent intBookingConfirmation = new Intent(FnbOrderSummary.this, FnbConfirmation.class);
                pcBundle.putString("price", strTotal);
                pcBundle.putString("printdata", strFnBPrintData);
                pcBundle.putString("wcode", strWCODE);
                pcBundle.putString("transno", strFSummData.split("\\|")[11]);
                pcBundle.putString("bookingno", strFSummData.split("\\|")[10]);
                pcBundle.putString("receiptno", strFSummData.split("\\|")[13]);
                intBookingConfirmation.putExtra("jsonArray", strJsonArray);
                intBookingConfirmation.putExtras(pcBundle);
                startActivity(intBookingConfirmation);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    private void processCounter(String output){
        String strResponseCode = "", strResponseData="";
        try{
            dialogProg.dismiss();
            JCResp obJsonResponse = new Gson().fromJson(output, JCResp.class);
            strResponseCode = obJsonResponse.getRESPONSECODE();
            strResponseData = obJsonResponse.getRESPONSEDATA();
            if(strResponseCode.equals("0")) {
                strTempTransId = strResponseData.split("\\|")[0];
                strTotalQty = strResponseData.split("\\|")[1];
                strBookingNo = strResponseData.split("\\|")[2];
                //processPickupDelivery(output,"C");
                if(strGPayMode.equals("CARD")){
                    deviceType = DeviceType.N910;
                    deviceName = DeviceType.N910;
                    try {
                        CheckAccountActivation();

                    }
                    catch (RuntimeException Rex){
                        Toast.makeText(FnbOrderSummary.this,Rex.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    proceedToCommit();
                }
            }
            else{
                ShowErrorDialog(FnbOrderSummary.this,"Error",strResponseData);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    private void CheckAccountActivation() {
        try{
            getMSwipeCred();
            AccountValidator validator = new AccountValidator(getApplicationContext());
            if (!validator.isAccountActivated()) {
                mkey = "39B769745201";
                pkey = "A0F613B23B8B";
                validator.accountActivation(handler, mkey, pkey);
            }
            else{
                GoToPayUPaymentActivity();
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error",Ex.getMessage());
        }
    }

    private void GoToPayUPaymentActivity(){
        try{
            Intent intent = new Intent(FnbOrderSummary.this, PaymentTransactionActivity.class);

            intent.putExtra(MAC_ADDRESS, "");
            intent.putExtra(DEVICE_NAME, deviceName);
            intent.putExtra(DEVICE_COMMUNICATION_MODE, deviceCommMode);
            intent.putExtra(PAYMENT_TYPE, "Sale");
            intent.putExtra("referanceno", strTempTransId);
            intent.putExtra("amount", "1500");
            intent.putExtra("cashBackAmoumt", "0");
            startActivity(intent);
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error",Ex.getMessage());
        }
    }

    private void subProceedToPickupTypeEx(String output) {
        String strResponseCode = "", strResponseData="";
        try{
            dialogProg.dismiss();
            JCResp obJsonResponse = new Gson().fromJson(output, JCResp.class);
            strResponseCode = obJsonResponse.getRESPONSECODE();
            strResponseData = obJsonResponse.getRESPONSEDATA();
            if(strResponseCode.equals("0")) {
                strTempTransId = strResponseData.split("\\|")[0];
                strTotalQty = strResponseData.split("\\|")[1];
                strBookingNo = strResponseData.split("\\|")[2];
                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }
            else{
                ShowErrorDialog(FnbOrderSummary.this,"Error",strResponseData);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    private void processShowFilms(String output) {
        int intFilmCount = 0;
        try{
            List<String> list = new ArrayList<String>();
            obJsonSession = new Gson().fromJson(output, JSessions.class);
            if(obJsonSession.getSYNCRESPONSECODE().equals("0")){
                intFilmCount = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().size();
                for(int iCount = 0; iCount < intFilmCount; iCount++){
                    list.add(obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().get(iCount).getFilmName());
                }
                arrSpnFilmAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
                //spnFilms.setAdapter(arrSpnFilmAdapter);
                //llSeatDeatils.setVisibility(View.VISIBLE);
            }
            else{
                ShowErrorDialog(FnbOrderSummary.this,"Error",output);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    /*
    private void subProceedToPickupType(String output){
        String strResponseCode = "", strResponseData="", strTempTransId = "", strTotalQty = "", strBookingNo = "";
        try{
            JCResp obJsonResponse = new Gson().fromJson(output, JCResp.class);
            strResponseCode = obJsonResponse.getRESPONSECODE();
            strResponseData = obJsonResponse.getRESPONSEDATA();
            if(strResponseCode.equals("0")) {
                strTempTransId = strResponseData.split("\\|")[0];
                strTotalQty = strResponseData.split("\\|")[1];
                strBookingNo = strResponseData.split("\\|")[2];
                Bundle bundle = new Bundle();
                bundle.putString("totalamt", tvTotalAmt.getText().toString());
                bundle.putString("temptransid", strTempTransId);
                bundle.putString("totalqty", strTotalQty);
                bundle.putString("bookingno",strBookingNo);
                Intent intPickup = new Intent(FnbOrderSummary.this, FnbPickupType.class);
                intPickup.putExtra("jsonArray", jArray.toString());
                intPickup.putExtras(bundle);
                startActivity(intPickup);
            }
            else{
                ShowErrorDialog(FnbOrderSummary.this,"Error",strResponseData);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }
*/
    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    finish();
                }
            });
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogProg = mBuilder.create();
            dialogProg.show();

            dialogProg.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    public View.OnClickListener AddItemListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String strTagData = "", strItemId = "", strQty = "", strJItemId = "", strOldPrice = "", strNewPrice ="", strOldTotal = "", strNewTotal = "", strCartTotal = "", strCartQty = "", strStockQty = "";
            int intOldQty = 0, intNewQty = 0;
            TextView tvOsPrice, tvOsQty;
            LinearLayout llIvParent, llTvParent;
            ImageView ivSender, ivRemove;
            TableRow tblRow;
            try{
                ivSender = (ImageView) v;
                strTagData = ivSender.getTag().toString();
                strItemId = strTagData.split("\\|")[1];
                for(int i = 0; i < jArray.length(); i++) {
                    strJItemId = jArray.getJSONObject(i).get("itemid").toString();
                    if (strItemId.equals(strJItemId)) {
                        intOldQty = Integer.parseInt(jArray.getJSONObject(i).get("qty").toString());
                        if(intOldQty == Double.parseDouble(jArray.getJSONObject(i).get("stockqty").toString())){
                            return;
                        }
                        strOldPrice = strTagData.split("\\|")[3];
                        strOldTotal = strTagData.split("\\|")[4];
                        strNewTotal = Double.toString(Double.parseDouble(strOldPrice) + Double.parseDouble(strOldTotal));
                        strCartTotal = tvTotalAmt.getText().toString();
                        strCartQty = tvTotalQty.getText().toString();

                        intNewQty = intOldQty + 1;
                        jArray.getJSONObject(i).put("qty", Integer.toString(intNewQty));
                        jArray.getJSONObject(i).put("totalprice",strNewTotal);
                        tvTotalAmt.setText(Double.toString(Double.parseDouble(strCartTotal) + Double.parseDouble(strOldPrice)));
                        tvTotalQty.setText(Integer.toString(Integer.parseInt(strCartQty) + 1));

                        llIvParent = (LinearLayout)v.getParent();
                        tvOsQty = (TextView) llIvParent.getChildAt(llIvParent.indexOfChild(ivSender)-1);

                        tvOsQty.setText(Integer.toString(intNewQty));

                        tblRow = (TableRow)llIvParent.getParent();

                        llTvParent = (LinearLayout) tblRow.getChildAt(tblRow.indexOfChild(llIvParent)+1);
                        tvOsPrice = (TextView) llTvParent.getChildAt(1);
                        tvOsPrice.setText(Double.toString(Double.parseDouble(strNewTotal)));

                        ivRemove = (ImageView)llIvParent.getChildAt(llIvParent.indexOfChild(ivSender)-2);
                        strTagData = strTagData.split("\\|")[0] + "|" + strItemId + "|" + Integer.toString(intNewQty) + "|" + strOldPrice + "|" + strNewTotal;
                        ivSender.setTag(strTagData);
                        ivRemove.setTag(strTagData);
                    }
                }
            }
            catch (Exception Ex){
                ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
            }
        }
    };

    public View.OnClickListener RemoveItemListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String strTagData="", strItemId= "", strJItemId= "",strOldPrice = "", strNewPrice ="", strOldTotal = "", strNewTotal ="", strCartTotal ="", strCartQty ="";
            int intOldQty = 0, intNewQty = 0;
            TextView tvOsPrice, tvOsQty;
            LinearLayout llIvParent, llTvParent;
            ImageView ivSender, ivAdd;
            TableRow tblRow;
            try{
                ivSender = (ImageView) v;
                strTagData = ivSender.getTag().toString();
                strItemId = strTagData.split("\\|")[1];

                llIvParent = (LinearLayout)v.getParent();
                tblRow = (TableRow)llIvParent.getParent();
                llTvParent = (LinearLayout) tblRow.getChildAt(tblRow.indexOfChild(llIvParent)+1);
                tvOsPrice = (TextView) llTvParent.getChildAt(1);

                for(int i = 0; i < jArray.length(); i++) {
                    strJItemId = jArray.getJSONObject(i).get("itemid").toString();
                    if (strItemId.equals(strJItemId)) {
                        intOldQty = Integer.parseInt(jArray.getJSONObject(i).get("qty").toString());
                        intNewQty = intOldQty - 1;
                        if(intNewQty > 0) {
                            //jArray.getJSONObject(i).put("qty", Integer.toString(intNewQty));
                            //jArray.getJSONObject(i).put("totalprice", strNewPrice);

                            strOldPrice = strTagData.split("\\|")[3];
                            strOldTotal = strTagData.split("\\|")[4];
                            strNewTotal = Double.toString(Double.parseDouble(strOldTotal) - Double.parseDouble(strOldPrice));
                            strCartTotal = tvTotalAmt.getText().toString();
                            strCartQty = tvTotalQty.getText().toString();

                            jArray.getJSONObject(i).put("qty", Integer.toString(intNewQty));
                            jArray.getJSONObject(i).put("totalprice",strNewTotal);
                            tvTotalAmt.setText(Double.toString(Double.parseDouble(strCartTotal) - Double.parseDouble(strOldPrice)));
                            tvTotalQty.setText(Integer.toString(Integer.parseInt(strCartQty) - 1));

                            tvOsQty = (TextView) llIvParent.getChildAt(llIvParent.indexOfChild(ivSender)+1);
                            tvOsQty.setText(Integer.toString(intNewQty));
                            tvOsPrice.setText(Double.toString(Double.parseDouble(strNewTotal)));

                            ivAdd = (ImageView) llIvParent.getChildAt(llIvParent.indexOfChild(ivSender)+2);
                            strTagData = strTagData.split("\\|")[0] + "|" + strItemId + "|" + Integer.toString(intNewQty) + "|" + strOldPrice + "|" + strNewTotal;
                            ivSender.setTag(strTagData);
                            ivAdd.setTag(strTagData);
                        }
                        else {
                            llIvParent = (LinearLayout)v.getParent();
                            tblRow = (TableRow) llIvParent.getParent();
                            tblOS.removeView(tblRow);

                            strOldPrice = strTagData.split("\\|")[3];
                            strCartQty = tvTotalQty.getText().toString();
                            strCartTotal = tvTotalAmt.getText().toString();
                            tvTotalQty.setText(Integer.toString(Integer.parseInt(strCartQty) - 1));
                            tvTotalAmt.setText(Double.toString(Double.parseDouble(strCartTotal) - Double.parseDouble(strOldPrice)));

                            //remove item from array
                            JSONArray list = new JSONArray();
                            int len = jArray.length();
                            if (jArray != null) {
                                for (int j=0;j<len;j++)
                                {
                                    if (j != i)
                                    {
                                        list.put(jArray.getJSONObject(j));
                                    }
                                }
                                jArray = new JSONArray();
                                for (int k = 0; k < list.length(); k++) {
                                    jArray.put(list.getJSONObject(k));
                                }
                            }
                            //
                            ivAdd = (ImageView) llIvParent.getChildAt(llIvParent.indexOfChild(ivSender)+2);
                            strTagData = strTagData.split("\\|")[0] + "|" + strItemId + "|" + Integer.toString(intNewQty) + "|" + strOldPrice + "|" + strNewTotal;
                            ivSender.setTag(strTagData);
                            ivAdd.setTag(strTagData);
                        }
                    }

                    if(jArray.length() == 0) {
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("jsonArray", jArray.toString());
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                    }
                }
            }
            catch (Exception Ex){
                ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
            }
        }
    };

    @Override
    public void onBackPressed() {

    }

    public void SetSeatDetails(View v){
        String strItemVarietyQty="",strItemId="",strItemDesc="",strITemQty="", strWcode = "";
        try{
            strGPType = "seat";
            //BottomSheetDialogFragment bottomSheetDialogFragment = new CustomBottomSheetDialogFragment();
            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];
            if (strFileContent.split("\\|").length > 1) {strWcode = strFileContent.split("\\|")[1];} else {strWcode = "";}
            strItemVarietyQty = Integer.toString(jArray.length());
            strItemDesc = "|" + strItemVarietyQty + "|";

            for(int iCount = 0; iCount < jArray.length(); iCount++) {
                jObject = (JSONObject) jArray.get(iCount);
                strItemId = jObject.get("itemid").toString();
                strITemQty = jObject.get("qty").toString();

                strItemDesc += strItemId + "|" + strITemQty + "|-1|";
            }
            ShowProgressDialog(FnbOrderSummary.this,"Processing","");
            strGAction = "ADDCONITEM";
            SoapCall obSc = new SoapCall(FnbOrderSummary.this);
            obSc.delegate = this;
            obSc.execute("ADDCONITEM",strIP, strItemVarietyQty, strItemDesc, strWcode,"","","","","","","","","","","","");

            //bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    public void ProceedWithCash(View v){
        try{
            strGPayMode = v.getTag().toString();
            addConcessionCall();

        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    private void ProceedWithIntegratedPayment(View v){
        try{

            flIgPaymentList.setVisibility(View.VISIBLE);
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }


    private void addConcessionCall(){
        String strTotalAmt = "", strItemVarietyQty = "", strItemDesc = "", strItemId = "", strITemQty = "", strReferenceNo="";
        try{
            strGPType = "counter";
            strItemVarietyQty = Integer.toString(jArray.length());
            strItemDesc = "|" + strItemVarietyQty + "|";

            for(int iCount = 0; iCount < jArray.length(); iCount++) {
                jObject = (JSONObject) jArray.get(iCount);
                strItemId = jObject.get("itemid").toString();
                strITemQty = jObject.get("qty").toString();

                strItemDesc += strItemId + "|" + strITemQty + "|-1|";
            }
            ShowProgressDialog(FnbOrderSummary.this,"Processing","");
            strGAction = "ADDCONITEM";
            SoapCall obSc = new SoapCall(FnbOrderSummary.this);
            obSc.delegate = this;
            obSc.execute("ADDCONITEM",strIP, strItemVarietyQty, strItemDesc, strWCODE,"","","","","","","","","","","","");

        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }


    private void fetchData() {
        SoapCall obSc;
        try{
            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];

            strGAction = "SESSION";
            obSc = new SoapCall(FnbOrderSummary.this);
            obSc.delegate = this;
            obSc.execute("SESSIONS", strIP, "", "", "","","","","","","","","","","","","");
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Error while retreiving films");
        }
    }


    @Override
    public void SetOnSeat(String strSessionId, String strSeatDetails, String strDeliveryAt, String strTransDetails) {
        try{
            strGAction = "SETFNBPICKUPORDELIVERYEX";
            SoapCall obSc = new SoapCall(FnbOrderSummary.this);
            obSc.delegate = this;
            obSc.execute("SETFNBPICKUPORDELIVERYEX", strIP, strBookingNo, "Y", strDeliveryAt, strTransDetails, "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){

        }
    }

    private void processPickupDelivery(String output, String pickupType) {
        String strRespCode = "", strRespData = "", strTotalAmt = "";
        try{
            JCResp obResp = new Gson().fromJson(output,JCResp.class);
            strRespCode = obResp.getRESPONSECODE();
            strRespData = obResp.getRESPONSEDATA();
            strTotalAmt = tvTotalAmt.getText().toString();
            if(strRespCode.equals("0")) {
                Bundle poBundle = new Bundle();
                poBundle.putString("appmode","fnb");
                poBundle.putString("price",strTotalAmt);
                poBundle.putString("temptransid",strTempTransId);
                poBundle.putString("qty",strTotalQty);
                poBundle.putString("pickuptype",pickupType);
                poBundle.putString("bookingno",strBookingNo);
                Intent intPaymentOption = new Intent(FnbOrderSummary.this,PaymentOptions.class);
                intPaymentOption.putExtra("jsonArray", jArray.toString());
                intPaymentOption.putExtras(poBundle);
                startActivity(intPaymentOption);
            }
            else {
                ShowErrorDialog(FnbOrderSummary.this,"Error",strRespData);
            }
        }
        catch (Exception Ex) {
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    private void proceedToCommit(){
        String strCustDetails = "", strUID = "", strTransType = "", strProgTitle = "", strLoc = "", strJItems="", strSessionId = "", strComments = "", strPhone = "";
        SharedVariable obShared = ((SharedVariable)getApplicationContext());
        JSONObject inner_jObject;
        try{

            if(!strJsonArray.equals("")){
                jArray = new JSONArray(strJsonArray);
                for(int iCount = 0; iCount < jArray.length(); iCount++){
                    inner_jObject = (JSONObject) jArray.get(iCount);
                    strJItems += inner_jObject.getString("itemid") + "_" +inner_jObject.getString("qty") + "|";
                }
                strJItems = strJItems.substring(0,strJItems.length()-1);
            }
            strUID = obShared.getUID();
            strTransType = "C";
            strProgTitle = "Commiting transaction";
            strSessionId = strJItems;
            strLoc = obShared.getLOC();
            strComments = edComments.getText().toString();
            strPhone = edPhone.getText().toString();
            strCustDetails = strComments + "|" +strPhone + "|" + "VTREE" + "|" + strComments;
            strGAction = "COMMIT";

            SoapCall obSc = new SoapCall(FnbOrderSummary.this);
            obSc.delegate = this;
            ShowProgressDialog(FnbOrderSummary.this,strProgTitle,"");
            String strPickupType = "C";
            obSc.execute("COMMITTRANS", strIP, strSessionId, strTempTransId, "TRUE","5123456789012346|VISA|05|2021|123",strCustDetails,"C",strUID,strWCODE,"","",strTotalQty,strTransType,strLoc,strPickupType,"");

        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this,"Error","Something went wrong");
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void getMSwipeCred(){
        try{
            strGAction = "CARDDETAILS";
            SoapCall obSc = new SoapCall(FnbOrderSummary.this);
            obSc.delegate = this;
            obSc.execute("CARDDETAILS", strIP, "", "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this, "Error", "Something went wrong");
        }
    }

    private void processmSwipeCreds(String output) {
        String strResponseCode = "", strResponseData = "";
        try{
            JCResp obJsonResponse = new Gson().fromJson(output, JCResp.class);
            strResponseCode = obJsonResponse.getRESPONSECODE();
            strResponseData = obJsonResponse.getRESPONSEDATA();
            if(strResponseCode.equals("0")){
                mkey = strResponseData.split("\\|")[0];
                pkey = strResponseData.split("\\|")[1];
            }
            else{
                ShowErrorDialog(FnbOrderSummary.this, "Error", strResponseData.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this, "Error", "Something went wrong");
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == SUCCESS) {
                //LoginResponse vo = (LoginResponse) msg.obj;
                //Toast.makeText(FnbOrderSummary.this, "" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();
                GoToPayUPaymentActivity();
            }

            if (msg.what == FAIL) {
                Toast.makeText(FnbOrderSummary.this, (String) msg.obj + "|FAIL", Toast.LENGTH_SHORT).show();
            } else if (msg.what == ERROR_MESSAGE) {
                Toast.makeText(FnbOrderSummary.this, (String) msg.obj + "|ERROR_MESSAGE", Toast.LENGTH_LONG).show();
            }

        }
    };

    public void integratedPaymentSelected(View view) {
        try{
            flIgPaymentList.setVisibility(View.GONE);
            switch (view.getTag().toString()){
                case "card":
                    strGPayMode = "CARD";
                    break;
                case "upi":
                    strGPayMode = "UPI";
                    break;
            }
            addConcessionCall();
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbOrderSummary.this, "Error", "Something went wrong");
        }
    }
}
