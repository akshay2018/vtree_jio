package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JConData {

    @SerializedName("SYNCCON_RESPONSE_CODE")
    @Expose
    private String sYNCCONRESPONSECODE;
    @SerializedName("SYNCCON_RESPONSE_DATA")
    @Expose
    private SYNCCONRESPONSEDATA sYNCCONRESPONSEDATA;

    public String getSYNCCONRESPONSECODE() {
        return sYNCCONRESPONSECODE;
    }

    public void setSYNCCONRESPONSECODE(String sYNCCONRESPONSECODE) {
        this.sYNCCONRESPONSECODE = sYNCCONRESPONSECODE;
    }

    public SYNCCONRESPONSEDATA getSYNCCONRESPONSEDATA() {
        return sYNCCONRESPONSEDATA;
    }

    public void setSYNCCONRESPONSEDATA(SYNCCONRESPONSEDATA sYNCCONRESPONSEDATA) {
        this.sYNCCONRESPONSEDATA = sYNCCONRESPONSEDATA;
    }
}