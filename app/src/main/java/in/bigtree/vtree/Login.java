package in.bigtree.vtree;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.bigtree.vtree.view.settings.PermissionsActivity;

public class Login extends AppCompatActivity implements AsyncResponse {

    EditText edUID, edPWD;
    Button btnLogin;
    String strGAction = "", strWcode ="", strUID = "", strPWD = "", strIP = "", strFileContent = "", strConsSMS = "", strIbPrinting = "", strCCIntegrated = "";
    SoapCall obSc;
    AlertDialog dialogErr, dialogPro;
    boolean doubleBackToExitPressedOnce = false;
    RelativeLayout rlSParent;
    HashMap<String, String> mapLocations = new HashMap<>();
    Switch swPrinting;
    FrameLayout flSettingsMenu,flTransperent;
    private int REQUEST_CODE_PERMISSIONS = 2002;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        try {
            requestPermissions();
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                String strPrev = extras.getString("prev","");
                if (strPrev.equals("setup")) {
                    //fetchStockLocation();
                } else {
                    // Do something else
                }
            }

            edUID = (EditText)findViewById(R.id.tvUid);
            edPWD = (EditText)findViewById(R.id.tvPwd);
            rlSParent = (RelativeLayout)findViewById(R.id.rlSParent);
            btnLogin = (Button) findViewById(R.id.btnLogin);
            btnLogin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
            btnLogin.setClickable(false);
            //swPrinting = (Switch)findViewById(R.id.swPrinting);
            flSettingsMenu = (FrameLayout)findViewById(R.id.flSettingsMenu);
            flTransperent = (FrameLayout)findViewById(R.id.flTransperent);

            edUID.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        if (s.length() == 0) {
                            btnLogin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
                            btnLogin.setClickable(false);
                        }
                        else{
                            if(!btnLogin.isClickable() == true){
                                if(edPWD.getText().length() != 0) {
                                    btnLogin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorJwcDarkBrown));
                                    btnLogin.setClickable(true);
                                }
                            }
                        }
                        if(s.length() == 4){
                            edPWD.requestFocus();
                        }
                    } catch (Exception Ex) {
                        Ex.toString();
                    }
                }
            });
            edPWD.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        if (s.length() == 0) {
                            btnLogin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
                            btnLogin.setClickable(false);
                        }
                        else{
                            if(!btnLogin.isClickable() == true){
                                if(edUID.getText().length() != 0) {
                                    btnLogin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorJwcDarkBrown));
                                    btnLogin.setClickable(true);
                                }
                            }
                        }
                        if(s.length() == 4){
                            closeKeyboard();
                        }
                    } catch (Exception Ex) {
                        Ex.toString();
                    }
                }
            });

            final SharedVariable obShared = ((SharedVariable)getApplicationContext());
            obShared.setPrintChecked(false);
/*
            swPrinting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked != true) {
                        swPrinting.setText("Printing is disabled");
                        obShared.setPrintChecked(false);
                    } else {
                        swPrinting.setText("Printing is enabled");
                        obShared.setPrintChecked(true);
                    }
                }
            });
            */
            requestFocusOnEdUID();
        }
        catch (Exception Ex) {
            if(dialogErr.isShowing()) {
                dialogErr.dismiss();
            }
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(Login.this,"Error","Something went wrong");
        }
    }

    private void requestFocusOnEdUID() {
        try {
            edUID.setFocusable(true);
            edUID.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        catch (Exception Ex) {

        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings_menu,menu);
    }

    private void fetchStockLocation() {
        try{
            FileIO obFileIO = new FileIO();
            //strFileContent = obFileIO.readFile(getResources().getString(R.string.config_file_name),getApplicationContext());
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];

            strGAction = "STOCKLOCATION";
            obSc = new SoapCall(Login.this);
            obSc.delegate = this;
            obSc.execute("STOCKLOCATION",strIP, "", "", "","","","","","","","","","","","","");
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(Login.this,"Error","Something went wrong");
        }
    }

    public void getLogin(View v){

        String strCID = "", IMEI="", SERIAL = "";
        TelephonyManager tm;
        try{
            FileIO obFileIO = new FileIO();
            //strFileContent = obFileIO.readFile(getResources().getString(R.string.config_file_name),getApplicationContext());
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());

            strIP = strFileContent.split("\\|")[0];
            strWcode = strFileContent.split("\\|")[1];
            strCID = strFileContent.split("\\|")[2];
            //strSLayout = strFileContent.split("\\|")[2];
            strUID = edUID.getText().toString();
            strPWD = edPWD.getText().toString();

            //WifiManager wifiMan = (WifiManager) getApplicationContext().getSystemService(getApplicationContext().WIFI_SERVICE);
            //WifiInfo wifiInf = wifiMan.getConnectionInfo();
            //int ipAddress = wifiInf.getIpAddress();
            //strCID = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));

            tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            }
            else{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    IMEI = tm.getImei();
                } else {
                    IMEI = tm.getDeviceId();
                }

                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
                    SERIAL = android.os.Build.SERIAL;
                }
                else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    SERIAL = android.os.Build.getSerial();
                }
            }

            ShowProgressDialog(Login.this,"Logging on "+strWcode,"");
            strGAction = "LOGIN";
            obSc = new SoapCall(Login.this);
            obSc.delegate = this;
            obSc.execute("LOGIN",strIP, strUID, strPWD, strCID,strWcode,SERIAL,IMEI,"","","","","","","","","");
            /*
            Intent intMain = new Intent(Login.this,MainScreen.class);
            startActivity(intMain);
            */
        }
        catch (Exception Ex){
            //Ex.toString();
            if(dialogErr.isShowing()) {
                dialogErr.dismiss();
            }
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(Login.this,"Error","Something went wrong");
        }
    }

    @Override
    public void processFinish(String output) {
        String strResponseCode = "", strResponseData = "";
        Bundle bundle;
        try{
            prcessFinishNext(output);
        }
        catch (Exception Ex){
            if(dialogErr.isShowing()) {
                dialogErr.dismiss();
            }
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(Login.this,"Error","Something went wrong");
        }
    }

    public void prcessFinishNext(String output){
        String strResponseCode = "", strResponseData = "", strCinemaName = "", strStockLoc = "", strSalesProfile = "", strPrintingChk = "";
        Bundle bundle;
        final String[] strLocation = {""};
        try{

            if(strGAction.equals("LOGIN")) {
                if (!output.split("\\|")[0].equals("1")) {
                    JCResp obJsonResponse = new Gson().fromJson(output, JCResp.class);
                    strResponseCode = obJsonResponse.getRESPONSECODE();
                    strResponseData = obJsonResponse.getRESPONSEDATA();

                    if (strResponseCode.equals("0")) {

                        FileIO obFileIO = new FileIO();
                        //strFileContent = obFileIO.readFile(getResources().getString(R.string.config_file_name),getApplicationContext());
                        strFileContent = obFileIO.readFile(getResources().getString(R.string.advance_config_file),getApplicationContext());
                        //strStockLoc = (strFileContent.split("\\|")[0]).split("\\_")[1];
                        if (strFileContent.split("\\|").length > 3) {strConsSMS = strFileContent.split("\\|")[3];} else {strConsSMS = "";}
                        if (strFileContent.split("\\|").length > 4) {strIbPrinting = strFileContent.split("\\|")[4];} else {strIbPrinting = "";}
                        if (strFileContent.split("\\|").length > 5) {strCCIntegrated = strFileContent.split("\\|")[5];} else {strCCIntegrated = "";}
                        if (strFileContent.split("\\|").length > 1) {strPrintingChk = strFileContent.split("\\|")[1];} else {strPrintingChk = "";}
                        strStockLoc = strResponseData.split("\\$")[7];
                        strSalesProfile = strResponseData.split("\\$")[8];;
                        SharedVariable obShared = ((SharedVariable)getApplicationContext());
                        obShared.setUID(strUID);
                        obShared.setLOC(strStockLoc);
                        obShared.setMode(strSalesProfile);
                        //if (strConsSMS.equals("Y")) { obShared.setConsSMS(true); } else { obShared.setConsSMS(false); }
                        if (strIbPrinting.equals("Y")) { obShared.setIbPrinting(true); } else { obShared.setIbPrinting(false); }
                        if(strPrintingChk.equals("Y")){ obShared.setPrintChecked(true); } else {obShared.setPrintChecked(false);}
                        if(strCCIntegrated.equals("Y") || strCCIntegrated.equals("")){ obShared.setCCIntegratedStat(true); } else {obShared.setCCIntegratedStat(false);}

                        subSyncData();
                        //subConSyncData();
                        strCinemaName = strResponseData.split("\\$")[4];
                        bundle = new Bundle();
                        bundle.putString("uid", strUID);
                        bundle.putString("pwd", strPWD);
                        bundle.putString("wcode", strWcode);
                        bundle.putString("cinema", strCinemaName);
                        Intent intMain = null;
                        //if(obShared.getMode() == "fnb"){
                        //    intMain = new Intent(Login.this, FnbMain.class);
                        //    intMain.putExtras(bundle);
                        //}
                        //else if(obShared.getMode() == "movie"){
                        //    intMain = new Intent(Login.this, Movies.class);
                        //    intMain.putExtras(bundle);
                        //}
                        //else {
                        if(dialogPro != null) {
                            dialogPro.dismiss();
                        }
                            intMain = new Intent(Login.this, MainScreen.class);
                            intMain.putExtras(bundle);
                        //}
                        finish();
                        startActivity(intMain);
                    }
                    else{
                        edUID.setText("");
                        edPWD.setText("");
                        edUID.requestFocus();
                        CustomDialog cDialog = new CustomDialog();
                        cDialog.ShowErrorDialog(Login.this,"Error",strResponseData);
                        if(dialogPro != null) {
                            if (dialogPro.isShowing()) {
                                dialogPro.dismiss();
                            }
                        }
                    }
                }
                else{
                    edUID.setText("");
                    edPWD.setText("");
                    edUID.requestFocus();
                    CustomDialog cDialog = new CustomDialog();
                    cDialog.ShowErrorDialog(Login.this,"Error",output.split("\\|")[1]);
                    if(dialogPro != null) {
                        if (dialogPro.isShowing()) {
                            dialogPro.dismiss();
                        }
                    }
                }
            }
            if(strGAction.equals("SYNC") || strGAction.equals("SYNCCONITEM")){

            }
            if(strGAction.equals("STOCKLOCATION")){
                if (!output.split("\\|")[0].equals("1")) {
                    JCResp obJsonResponse = new Gson().fromJson(output, JCResp.class);
                    strResponseCode = obJsonResponse.getRESPONSECODE();
                    strResponseData = obJsonResponse.getRESPONSEDATA();
                    if(strResponseCode.equals("0")){
                        final String[] arrLocations = strResponseData.split("\\$");
                        List<String> locList = new ArrayList<>();
                        for (int iCount = 0; iCount < arrLocations.length; iCount++){
                            locList.add(arrLocations[iCount].split("\\_")[1]);
                        }
                        String[] arrLocNames = locList.toArray(new String[locList.size()]);
                        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                        builder.setTitle("Choose stock location");
                        //int checkedItem = 1; // cow
                        builder.setSingleChoiceItems(arrLocNames, 0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // user checked an item
                            }
                        });
                        // add OK and Cancel buttons
                        final String finalStrResponseData = strResponseData;
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //user clicked OK
                                ListView lw = ((AlertDialog)dialog).getListView();
                                storeLocation(arrLocations[lw.getCheckedItemPosition()], finalStrResponseData);
                            }
                        });

                        //builder.setNegativeButton("Cancel", null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        dialog.setCancelable(false);
                        dialog.setCanceledOnTouchOutside(false);
                    }
                    else{
                        CustomDialog cDialog = new CustomDialog();
                        cDialog.ShowErrorDialog(Login.this,"Error","Cannot get stock location");
                    }
                }
            }
        }
        catch (Exception Ex){
            edUID.setText("");
            edPWD.setText("");
            edUID.requestFocus();
            if(dialogErr.isShowing()) {
                dialogErr.dismiss();
            }
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(Login.this,"Error","Something went wrong");
        }
    }

    private void storeLocation(String strSelLocation, String arrLocations) {
        String strFileName = "", strLIp = "", strLWCode = "", strGSlayout = "", strLocationCode = "", strStoredLoc = "", strPrefMode = "", strSLLayout = "", strPrintMode = "", strCounterChk = "";
        try{
            //strFileName = getResources().getString(R.string.config_file_name);
            strFileName = getResources().getString(R.string.advance_config_file);
            FileIO obFileIO = new FileIO();

            strFileContent = obFileIO.readFile(strFileName,getApplicationContext());
            if(strFileContent.split("\\|").length > 0){strStoredLoc = strFileContent.split("\\|")[0];}
            if(strFileContent.split("\\|").length > 1){strSLLayout = strFileContent.split("\\|")[1];}
            if(strFileContent.split("\\|").length > 2){strPrefMode = strFileContent.split("\\|")[2];}
            if(strFileContent.split("\\|").length > 3){strPrintMode = strFileContent.split("\\|")[3];}
            if(strFileContent.split("\\|").length > 5) {strCounterChk = strFileContent.split("\\|")[5];} else {strCounterChk = "";}

            strFileContent = strSelLocation.split("\\_")[1] +"_" + strSelLocation.split("\\_")[0] + "|"+ strSLLayout + "|" + strPrefMode + "|" + strPrintMode + "|" + arrLocations + "|" + strCounterChk;
            obFileIO.writeFile(strFileName, strFileContent, getApplicationContext());
            SharedVariable obShare = ((SharedVariable)getApplicationContext());
            obShare.setLOC(strSelLocation.split("\\_")[0]);
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(Login.this,"Error","Error while storing stock location");
        }
    }

    private void subConSyncData() {
        String strStockLoc = "";
        try{
            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            //strStockLoc = obShared.getLOC();
            if(strIP.equals("")){
                Toast toast = Toast.makeText(getApplicationContext(), "URL empty, check settings.", Toast.LENGTH_LONG);
                return;
            }
            strGAction = "SYNCCONITEM";

            obSc = new SoapCall(Login.this);
            obSc.delegate = this;
            //Replace stock location with work station
            obSc.execute("SYNCCONITEM", strIP, strWcode, "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            if(dialogErr.isShowing()) {
                dialogErr.dismiss();
            }
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(Login.this,"Error","Something went wrong");
        }
    }

    private void subSyncData() {
        try{
            if(strIP.equals("")){
                Toast toast = Toast.makeText(getApplicationContext(), "URL empty, check settings.", Toast.LENGTH_LONG);
                return;
            }
            strGAction = "SYNC";

            obSc = new SoapCall(Login.this);
            obSc.delegate = this;
            obSc.execute("SYNC", strIP, "", "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(Login.this,"Error","Error while syncing data");
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogPro = mBuilder.create();
            //final AlertDialog dialogErr = mBuilder.create();
            dialogPro.show();

            dialogPro.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogPro.dismiss();
                }
            });
        }
        catch (Exception Ex){
            Ex.toString();
        }
    }

    public void getSettings(View view) {
        try{
            /*
            Bundle bundle = new Bundle();
            bundle.putString("prev","login");
            bundle.putString("uid",strUID);
            bundle.putString("pwd",strPWD);
            bundle.putString("wcode",strWcode);

            Intent intSettings = new Intent(Login.this,Settings.class);
            intSettings.putExtras(bundle);
            startActivity(intSettings);
            */
            flSettingsMenu.setVisibility(View.VISIBLE);
            flTransperent.setVisibility(View.VISIBLE);
        }
        catch (Exception Ex){
            if(dialogErr.isShowing()) {
                dialogErr.dismiss();
            }
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(Login.this,"Error","Something went wrong");
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please tap BACK again to exit", Toast.LENGTH_SHORT).show();
        //Snackbar snackbar = (Snackbar) Snackbar.make(rlSParent, "Please tap BACK again to exit", Snackbar.LENGTH_SHORT);
        //snackbar.show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void closeContextMenu(View view) {
        try{
            flSettingsMenu.setVisibility(View.GONE);
            flTransperent.setVisibility(View.GONE);
        }
        catch (Exception Ex){

        }
    }

    public void settingsSelected(View v){
        try{
            closeKeyboard();
            Intent intGSettings = null;
            Intent intASettings = null;
            Bundle bundle = new Bundle();
            bundle.putString("prev", "login");

            flSettingsMenu.setVisibility(View.GONE);
            flTransperent.setVisibility(View.GONE);

            edUID.setText("");
            edPWD.setText("");

            if(v.getTag().toString().equals("general")){
                intGSettings = new Intent(Login.this,GeneralSettings.class);
                intGSettings.putExtras(bundle);
                startActivity(intGSettings);
            }
            if(v.getTag().toString().equals("advance")){
                intASettings = new Intent(Login.this,AdvanceSettings.class);
                intASettings.putExtras(bundle);
                startActivity(intASettings);
            }
            if(v.getTag().toString().equals("about")){
                Intent aboutIntent = new Intent(Login.this,About.class);
                startActivity(aboutIntent);
            }
        }
        catch (Exception Ex){

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeKeyboard();
    }

    protected void requestPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {

            if ((ContextCompat.checkSelfPermission(this, "android.permission.BBPOS") != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.VIBRATE) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                // Permission is not granted

                Intent intent = new Intent(this, PermissionsActivity.class);
                startActivityForResult(intent, REQUEST_CODE_PERMISSIONS);

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
/*
        Logs.v(ApplicationData.packName, "requestCode: " + requestCode, true, true);
        Logs.v(ApplicationData.packName, "resultCode: " + resultCode, true, true);
*/
        if (requestCode == REQUEST_CODE_PERMISSIONS) {


        } else if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                this.finish();
                startActivity(this.getIntent());
            }
        }
        /*
        else if (requestCode == CHANGE_PASSWORD) {
            if (resultCode == RESULT_OK) {

                clearDeviceCache();
                Intent intent = new Intent(Launcher.this, LoginView.class);
                startActivity(intent);
            }
        }
         */
    }
}
