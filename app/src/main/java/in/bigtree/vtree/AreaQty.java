package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AreaQty {

    @SerializedName("AreaCode")
    @Expose
    private String areaCode;
    @SerializedName("AreaDesc")
    @Expose
    private String areaDesc;
    @SerializedName("AvailableSeats")
    @Expose
    private String availableSeats;
    @SerializedName("tCode")
    @Expose
    private String tCode;
    @SerializedName("tDesc")
    @Expose
    private String tDesc;
    @SerializedName("tPrice")
    @Expose
    private String tPrice;
    @SerializedName("package")
    @Expose
    private String _package;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaDesc() {
        return areaDesc;
    }

    public void setAreaDesc(String areaDesc) {
        this.areaDesc = areaDesc;
    }

    public String getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(String availableSeats) {
        this.availableSeats = availableSeats;
    }

    public String getTCode() {
        return tCode;
    }

    public void setTCode(String tCode) {
        this.tCode = tCode;
    }

    public String getTDesc() {
        return tDesc;
    }

    public void setTDesc(String tDesc) {
        this.tDesc = tDesc;
    }

    public String getTPrice() {
        return tPrice;
    }

    public void setTPrice(String tPrice) {
        this.tPrice = tPrice;
    }

    public String getPackage() {
        return _package;
    }

    public void setPackage(String _package) {
        this._package = _package;
    }
}