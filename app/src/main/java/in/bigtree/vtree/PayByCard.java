package in.bigtree.vtree;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mswipetech.wisepad.sdk.MSWisepadController;
import com.mswipetech.wisepad.sdk.data.EMITransactionDetailsResponseData;
import com.mswipetech.wisepad.sdk.data.MSDataStore;
import com.mswipetech.wisepad.sdk.listeners.MSWisepadControllerResponseListener;

import org.json.JSONArray;
import org.json.JSONObject;

import in.bigtree.vtree.data.AppSharedPrefrences;
import in.bigtree.vtree.pinelabs.IncomingHandler;
import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.view.MenuView;
import in.bigtree.vtree.view.cardpaymentactivity.cardsaleactivityintegration.MswipeCardSaleActivity;
import in.bigtree.vtree.view.cardpaymentactivity.cardsaleactivityintegration.MswipeDeclineActivity;
import in.bigtree.vtree.view.cardpaymentactivity.cardsaleactivityintegration.MswipePreauthSaleActivity;
import in.bigtree.vtree.view.cardpaymentactivity.cardsaleactivityintegration.MswipeSignatureActivity;


public class PayByCard extends AppCompatActivity implements AsyncResponse {
    String strTempTransId = "", strIP = "", strFileContent = "", strGAction = "", strFileName = "", strSessionId = "", strTicketType = "", strSeatInfo = "", strQty = "", strBookingId = "",
            strFilmName = "", strShowDateTime = "", strPrice = "", strCinemaname = "", strScreenName = "", strAreaCat = "", strSeats = "", strBookingNo = "", strJsonArray = "",strAppMode = "", strWCODE = "",
            strJItems = "", strPickupType = "", strmSwpUID = "", strmSwpPWD = "", strCinemaBranch = "";
    String strmSwipeStatus = "";
    LinearLayout llBack;
    Button btnProccedCard;
    TextInputEditText tvComments, tvMobileNo;
    TextView tvFilmName, tvShowDate, tvPrice, tvSkip;
    AlertDialog dialogProg;

    Messenger mServerMessenger;
    JSONArray jArray;
    JSONObject jObject;

    boolean isSaleWithCash = false;
    boolean isPreAuth = false;
    boolean isEmiSale = false;

    private static final String MS_CARDSALE_ACTIVITY_INTENT_ACTION = "mswipe.wisepad.sdk.CardSaleAction";
    private static final String MS_CASHATPOS_ACTIVITY_INTENT_ACTION = "mswipe.wisepad.sdk.CashAtPosAction";
    private static final String MS_EMISALE_ACTIVITY_INTENT_ACTION = "mswipe.wisepad.sdk.EmiAction";
    private static final String MS_PREAUTH_ACTIVITY_INTENT_ACTION = "mswipe.wisepad.sdk.PreauthAction";
    public static final int MS_CARDSALE_ACTIVITY_REQUEST_CODE = 1003;
    public static final int MS_CASHATPOS_ACTIVITY_REQUEST_CODE = 1004;
    public static final int MS_EMISALE_ACTIVITY_REQUEST_CODE = 1005;
    public static final int MS_PREAUTH_ACTIVITY_REQUEST_CODE = 1006;
    public static final int MSWIPE_CARDSALE_ACTIVITY_REQUEST_CODE = 1003;
    public static final int MSWIPE_CASHATPOS_ACTIVITY_REQUEST_CODE = 1004;
    public static final int MSWIPE_EMISALE_ACTIVITY_REQUEST_CODE = 1005;
    public static final int MSWIPE_PREAUTH_ACTIVITY_REQUEST_CODE = 1006;
    public static final int MSWIPE_CARDSALE_SIGNATURE_REQUEST = 1007;

    public static final int MSWIPE_CARDSALE_DECLINE_REQUEST = 1011;

    Context ctx;
    private boolean isBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_by_card);
        try {
            ctx = PayByCard.this;
            llBack = (LinearLayout) findViewById(R.id.llBack);
            btnProccedCard = (Button) findViewById(R.id.btnProcedCard);
            tvComments = (TextInputEditText) findViewById(R.id.tvComments);
            tvMobileNo = (TextInputEditText) findViewById(R.id.tvMobileNo);
            tvPrice = (TextView) findViewById(R.id.tvPrice);
            llBack.setOnClickListener((v) -> backToPaymentSelection(v));

            strJsonArray = getIntent().getStringExtra("jsonArray");
            Bundle poBundle = getIntent().getExtras();
            strTempTransId = poBundle.getString("temptransid");
            strAppMode = poBundle.getString("appmode");
            strSessionId = poBundle.getString("sessionid");
            strTicketType = poBundle.getString("ttype");
            strSeatInfo = poBundle.getString("seatinfo");
            strQty = poBundle.getString("qty");
            strBookingId = poBundle.getString("bookingid");
            strFilmName = poBundle.getString("filmname");
            strShowDateTime = poBundle.getString("showdatetime");
            strPrice = poBundle.getString("price");
            strCinemaname = poBundle.getString("cinemaname");
            strScreenName = poBundle.getString("screen");
            strAreaCat = poBundle.getString("areacat");
            strSeats = poBundle.getString("seats");
            strBookingNo = poBundle.getString("bookingno");
            strPickupType = poBundle.getString("pickuptype");

            llBack = (LinearLayout)findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToPaymentSelection(v));
            tvShowDate = (TextView) findViewById(R.id.tvShowDateTime);
            tvPrice = (TextView) findViewById(R.id.tvPrice);
            tvFilmName = (TextView)findViewById(R.id.tvFilmName);
            tvSkip = (TextView) findViewById(R.id.tvSkip);

            FileIO obFile = new FileIO();
            strFileName = getResources().getString(R.string.connection_config_file);
            strFileContent = obFile.readFile(strFileName, getApplicationContext());
            strIP = strFileContent.split("\\|")[0];
            strWCODE = strFileContent.split("\\|")[1];

            btnProccedCard.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
            btnProccedCard.setClickable(false);

            //showBlock();
            tvPrice.setText(strPrice);

            /*
            if(strAppMode.equals("fnb")) {
                tvSkip.setVisibility(View.VISIBLE);
            }
            */
            tvShowDate.setText(strShowDateTime);
            tvFilmName.setText("Payment for " + strFilmName);
            tvPrice.setText(strPrice);

            if(!strJsonArray.equals("")){
                jArray = new JSONArray(strJsonArray);
                for(int iCount = 0; iCount < jArray.length(); iCount++){
                    jObject = (JSONObject) jArray.get(iCount);
                    strJItems += jObject.getString("itemid") + "_" +jObject.getString("qty") + "|";
                }
                strJItems = strJItems.substring(0,strJItems.length()-1);
            }

            tvMobileNo.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        if (s.length() < 10) {
                            btnProccedCard.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
                            btnProccedCard.setClickable(false);
                        }
                        else{
                            btnProccedCard.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                            btnProccedCard.setClickable(true);
                            closeKeyboard();
                        }
                        /*
                        if(s.length() == 10){
                            closeKeyboard();
                        }
                        */

                    } catch (Exception Ex) {
                        Ex.toString();
                    }
                }
            });

            //Remove this after testing
            //strPrice = "5.00";
            getMSwipeCred();
            btnProccedCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub

                    if (AppSharedPrefrences.getIntegrationEnviroment().equals("PINELABS")) {
                        initPinelabs();
                    } else {

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null)
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                        double amount = 0;
                        try {
                            amount = Double.parseDouble(strPrice);
                        } catch (Exception ex) {
                            amount = 0;
                        }
                    /*
                    if (mTxtUserName.getText().toString().trim().startsWith("0")) {

                        showDialog("the userid cannot start with 0.");
                        mTxtUserName.requestFocus();
                        return;
                    }
                    else if(mTxtUserName.getText().toString().length() < 10)
                    {
                        showDialog("enter valid userid");
                        mTxtUserName.requestFocus();
                        return;
                    }
                    else if(mTxtPassword.getText().toString().length() == 0)
                    {
                        showDialog("enter a valid password");
                        mTxtPassword.requestFocus();
                        return;
                    }
                    else if (amount < 1)
                    {
                        showDialog("invalid amount! minimum amount should be inr 1.00 to proceed.");
                        return;
                    }
                    else if(isEmiSale && mTxtFirstSixDigits.getText().toString().length() == 0){
                        showDialog("enter a valid first six digits");
                        mTxtFirstSixDigits.requestFocus();
                        return;
                    }

                    else if(isEmiSale && mTxtFirstSixDigits.getText().toString().length() < 6) {
                        showDialog("enter a valid first six digits");
                        mTxtFirstSixDigits.requestFocus();
                        return;
                    }
                    else if (mTxtPhoneNum.getText().toString().trim().length() != 10) {

                        showDialog("required length of the mobile number is 10 digits.");
                        mTxtPhoneNum.requestFocus();
                        return;
                    }
                    else
                    */
                        if (tvMobileNo.getText().toString().trim().startsWith("0")) {
                            Toast.makeText(getApplicationContext(), "the mobile number cannot start with 0.", Toast.LENGTH_SHORT).show();
                            tvMobileNo.requestFocus();
                            return;
                        }
                        SharedVariable obShared = ((SharedVariable) getApplicationContext());
                        if (obShared.getCCIntegratedStat() == true) {
                            processCardSale();
                        } else {
                            proceedCommit("");
                        }
                    }
                }
            });

            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            strCinemaBranch = obShared.getBranchCode();

        } catch (Exception Ex) {
            ShowErrorDialog(ctx, "Error", "Something went wrong");
        }
    }

    private void initPinelabs() {
        JSONObject obj = new JSONObject();
        JSONObject objHeader = new JSONObject();
        JSONArray arr = new JSONArray();
        try{
            obj.put("ApplicationId","abcdefg");
            obj.put("UserId","1000");
            obj.put("MethodId","1001");
            obj.put("VersionNo","1.0");
            objHeader.put("Header",obj);
            objHeader.toString();
            Intent intent = new Intent();
            intent.setAction(AppSharedPrefrences.getPlutusSmartAction());
            intent.setPackage(AppSharedPrefrences.getPlutusSmartPackage());
            bindService(intent, connection, BIND_AUTO_CREATE);

            Message message = Message.obtain(null,AppSharedPrefrences.getMessageCode());
            Bundle data = new Bundle();
            String value = "";//{ "Header": { "ApplicationId": "abcdefgh", "UserId": "user1234", "MethodId": "1004", "VersionNo": "1.0"} }";
            data.putString(AppSharedPrefrences.getBillingRequestTag(), objHeader.toString());
            message.setData(data);

            message.replyTo = new Messenger(new IncomingHandler());
            mServerMessenger.send(message);
        }
        catch (Exception Ex){

        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void getMSwipeCred(){
        try{
            strGAction = "CARDDETAILS";
            SoapCall obSc = new SoapCall(ctx);
            obSc.delegate = this;
            obSc.execute("CARDDETAILS", strIP, "", "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            ShowErrorDialog(ctx, "Error", "Something went wrong");
        }
    }

    private void showBlock() {
        try {
            ShowErrorDialog(ctx, "Message from developer", "This type of payment is not configured yet.");
        } catch (Exception Ex) {
            ShowErrorDialog(ctx, "Error", "Something went wrong");
        }
    }


    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    subCancelTrans();
                }
            });
        } catch (Exception Ex) {
            ShowErrorDialog(ctx, "Error", "Something went wrong");
        }
    }

    private void subCancelTrans() {
        try {
            strGAction = "CANCELTRANS";
            SoapCall obSc = new SoapCall(ctx);
            obSc.delegate = this;
            obSc.execute("CANCELTRANS", strIP, strTempTransId, "", "", "", "", "", "", "", "", "", "", "", "", "","");
        } catch (Exception Ex) {
            ShowErrorDialog(ctx, "Error", "Something went wrong");
        }
    }

    public void proceedCommit(String receiptDetail){
        try{
            String strCustDetails = "", strUID = "", strTransType = "", strProgTitle = "", strLoc = "",strComments="",strPhone="";
            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            strUID = obShared.getUID();
            if(strAppMode.equals("fnb")) {
                strTransType = "C";
                strProgTitle = "Commiting transaction";
                strSessionId = strJItems;
                strLoc = obShared.getLOC();
            }
            else{
                strTransType = "T";
                strProgTitle = "Booking your ticket";
            }
            strComments = tvComments.getText().toString().trim();
            strPhone = tvMobileNo.getText().toString().trim();
            strCustDetails = strComments + "|" +strPhone + "|" + "VTREE" + "|" + strComments;
            strGAction = "COMMIT";

            SoapCall obSc = new SoapCall(ctx);
            obSc.delegate = this;
            ShowProgressDialog(PayByCard.this,strProgTitle,"");
            obSc.execute("COMMITTRANS", strIP, strSessionId, strTempTransId, "TRUE","5123456789012346|VISA|05|2021|123",strCustDetails,"R",strUID,strWCODE,strBookingId,strTicketType,strQty,strTransType,strLoc,strPickupType,receiptDetail);
            /*
            Intent intBookingConfirmation = new Intent(PayByCash.this,BookingConfirmation.class);
            startActivity(intBookingConfirmation);
            */
        }
        catch (Exception Ex){

            ShowErrorDialog(PayByCard.this,"Error","Something went wrong");
        }
    }

    //skip comments and mobile no
    public void proceedCommit(View view) {
        try{
            btnProccedCard.performClick();
        }
        catch (Exception Ex){

        }
    }

    @Override
    public void processFinish(String output) {
        try {
            if (!output.split("\\|")[0].equals("1")) {
                if (strGAction.equals("COMMIT")) {
                    proceedConfirmation(output);
                }
                if (strGAction.equals("CANCELTRANS")) {
                    Intent intent = new Intent(ctx, Movies.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                }
                if(strGAction.equals("CARDDETAILS")){
                    processmSwipeCreds(output);
                }
            } else {
                //Provisions for void transaction
                ShowErrorDialog(ctx, "Error", output.split("\\|")[1]);
            }
        } catch (Exception Ex) {
            if(strGAction.equals("COMMIT")){

            }
            ShowErrorDialog(ctx, "Error", "Something went wrong");
        }
    }

    private void processmSwipeCreds(String output) {
        String strResponseCode = "", strResponseData = "";
        try{
            JCResp obJsonResponse = new Gson().fromJson(output, JCResp.class);
            strResponseCode = obJsonResponse.getRESPONSECODE();
            strResponseData = obJsonResponse.getRESPONSEDATA();
            if(strResponseCode.equals("0")){
                strmSwpUID = strResponseData.split("\\|")[0];
                strmSwpPWD = strResponseData.split("\\|")[1];
            }
            else{
                ShowErrorDialog(ctx, "Error", strResponseData.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(ctx, "Error", "Something went wrong");
        }
    }

    public void backToPaymentSelection(View view) {
        try {
            finish();
        } catch (Exception Ex) {
            ShowErrorDialog(ctx, "Error", "Something went wrong");
        }
    }

    @Override
    public void onBackPressed() {

    }

    private void processCardSale() {
/*
        double amt = Double.parseDouble(strPrice);
        String totalAmt = String.format("%.2f", amt);
*/
        if (isPreAuth) {

            if (SharedVariable.IS_DEBUGGING_ON)
                Logs.v(SharedVariable.packName, "isPreAuth" + isPreAuth, true, true);

            Intent intent_preauth = new Intent(ctx, MswipePreauthSaleActivity.class);

            intent_preauth.putExtra("preauthsale", true);

            intent_preauth.putExtra("username", strmSwpUID);
            intent_preauth.putExtra("password", strmSwpPWD);
            intent_preauth.putExtra("amount", strPrice);
            intent_preauth.putExtra("mobileno", tvMobileNo.getText().toString().trim());
            intent_preauth.putExtra("receiptno", strCinemaBranch + strBookingNo);
            intent_preauth.putExtra("notes", strCinemaBranch + "|" + tvComments.getText().toString().trim());
            intent_preauth.putExtra("production", true);
/*
            if (AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment() == MSWisepadController.GATEWAY_ENVIRONMENT.LABS)

                intent_preauth.putExtra("production", false);
            else
            */
            //startActivity(intent_preauth);
            startActivityForResult(intent_preauth, MSWIPE_PREAUTH_ACTIVITY_REQUEST_CODE);
            //finish();


        } else if (isSaleWithCash) {
/*
            if (SharedVariable.IS_DEBUGGING_ON)
                Logs.v(SharedVariable.packName, "isSaleWithCash" + isSaleWithCash, true, true);

            Intent intent_cashAtpos = new Intent(ctx, MswipeCahAtPosSaleActivity.class);
            intent_cashAtpos.putExtra("salewithcash", true);

            intent_cashAtpos.putExtra("username", mTxtUserName.getText().toString().trim());
            intent_cashAtpos.putExtra("password", mTxtPassword.getText().toString().trim());
            intent_cashAtpos.putExtra("amount", totalAmt);
            intent_cashAtpos.putExtra("mobileno", mTxtPhoneNum.getText().toString().trim());
            intent_cashAtpos.putExtra("receiptno", mTxtReceipt.getText().toString().trim());
            intent_cashAtpos.putExtra("notes", mTxtNotes.getText().toString().trim());

            if (AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment() == MSWisepadController.GATEWAY_ENVIRONMENT.LABS)

                intent_cashAtpos.putExtra("production", false);
            else
                intent_cashAtpos.putExtra("production", true);

            //startActivity(intent_cashAtpos);
            startActivityForResult(intent_cashAtpos, MSWIPE_CASHATPOS_ACTIVITY_REQUEST_CODE);
            //finish();
*/
        } else if (isEmiSale) {
/*
            if (SharedVariable.IS_DEBUGGING_ON)
                Logs.v(SharedVariable.packName, "isEmiSale" + isEmiSale, true, true);

            getEmiDetails();
*/
        } else {

            Intent intent_cardsale = new Intent(PayByCard.this, MswipeCardSaleActivity.class);
            intent_cardsale.putExtra("cardsale", true);
            intent_cardsale.putExtra("username", strmSwpUID);
            intent_cardsale.putExtra("password", strmSwpPWD);
            intent_cardsale.putExtra("amount", strPrice);
            intent_cardsale.putExtra("mobileno", tvMobileNo.getText().toString().trim());
            intent_cardsale.putExtra("receiptno", strCinemaBranch+strBookingNo);
            intent_cardsale.putExtra("notes",strCinemaBranch + "|" + tvComments.getText().toString().trim());
            intent_cardsale.putExtra("extra1", "".trim());
            intent_cardsale.putExtra("extra2", "".trim());
            intent_cardsale.putExtra("extra3", "".trim());
            intent_cardsale.putExtra("extra4", "".trim());
            intent_cardsale.putExtra("extra5", "".trim());
            intent_cardsale.putExtra("extra6", "".trim());
            intent_cardsale.putExtra("extra7", "".trim());
            intent_cardsale.putExtra("extra8", "".trim());
            intent_cardsale.putExtra("extra9", "".trim());
            intent_cardsale.putExtra("extra10", "".trim());
            //if (AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment() == MSWisepadController.GATEWAY_ENVIRONMENT.LABS)
            intent_cardsale.putExtra("production", true);

            startActivityForResult(intent_cardsale, MSWIPE_CARDSALE_ACTIVITY_REQUEST_CODE);
            //finish();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == MSWIPE_CARDSALE_ACTIVITY_REQUEST_CODE) {

                boolean status = data.getBooleanExtra("status", false);
                String statusMessage = data.getStringExtra("statusMessage");
                String receiptDetail = data.getStringExtra("receiptDetail");

                Log.e(PayByCard.class.getSimpleName(),"Status"+status);
                Log.e(PayByCard.class.getSimpleName(),"statusMessage"+ statusMessage);
                if (resultCode == RESULT_OK) {
                    /*
                    if (AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired()) {
                        Intent intent = new Intent(ctx, MswipeSignatureActivity.class);
                        intent.putExtra("Title", "card sale");
                        intent.putExtra("receiptDetail", receiptDetail);
                        intent.putExtra("cardSaleResponseData", data.getSerializableExtra("cardSaleResponseData"));
                        startActivityForResult(intent, MSWIPE_CARDSALE_SIGNATURE_REQUEST);
                    } else {
                        showapproveDialog(Boolean.toString(status), data.getExtras().getString("AuthCode"),
                                data.getExtras().getString("RRNo"), statusMessage);
                    }
                    */
                    strmSwipeStatus = "true";
                    proceedCommit(receiptDetail);
                } else {

                    Intent intent = new Intent(ctx, MswipeDeclineActivity.class);
                    intent.putExtra("statusMessage", statusMessage);
                    intent.putExtra("cardSaleResponseData", data.getSerializableExtra("cardSaleResponseData"));
                    intent.putExtra("Title", getResources().getString(R.string.card_sale));
                    startActivityForResult(intent, MSWIPE_CARDSALE_DECLINE_REQUEST);
                }
            } else if (requestCode == MSWIPE_CASHATPOS_ACTIVITY_REQUEST_CODE) {

                boolean status = data.getBooleanExtra("status", false);
                String statusMessage = data.getStringExtra("statusMessage");

                if (resultCode == RESULT_OK) {
                    if (AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired()) {
                        Intent intent = new Intent(ctx, MswipeSignatureActivity.class);
                        intent.putExtra("Title", getResources().getString(R.string.cash_at_pos));
                        intent.putExtra("cardSaleResponseData", data.getSerializableExtra("cardSaleResponseData"));
                        startActivityForResult(intent, MSWIPE_CARDSALE_SIGNATURE_REQUEST);
                    } else {
                        showapproveDialog(Boolean.toString(status), data.getExtras().getString("AuthCode"),
                                data.getExtras().getString("RRNo"), statusMessage);
                    }
                } else {

                    Intent intent = new Intent(ctx, MswipeDeclineActivity.class);
                    intent.putExtra("statusMessage", statusMessage);
                    intent.putExtra("cardSaleResponseData", data.getSerializableExtra("cardSaleResponseData"));
                    intent.putExtra("Title", getResources().getString(R.string.cash_at_pos));
                    startActivityForResult(intent, MSWIPE_CARDSALE_DECLINE_REQUEST);
                }
            } else if (requestCode == MSWIPE_EMISALE_ACTIVITY_REQUEST_CODE) {

                boolean status = data.getBooleanExtra("status", false);
                String statusMessage = data.getStringExtra("statusMessage");

                if (resultCode == RESULT_OK) {
                    if (AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired()) {
                        Intent intent = new Intent(ctx, MswipeSignatureActivity.class);
                        intent.putExtra("Title", getResources().getString(R.string.emi));
                        intent.putExtra("cardSaleResponseData", data.getSerializableExtra("cardSaleResponseData"));
                        startActivityForResult(intent, MSWIPE_CARDSALE_SIGNATURE_REQUEST);
                    } else {
                        showapproveDialog(Boolean.toString(status), data.getExtras().getString("AuthCode"),
                                data.getExtras().getString("RRNo"), statusMessage);
                    }
                } else {

                    Intent intent = new Intent(ctx, MswipeDeclineActivity.class);
                    intent.putExtra("statusMessage", statusMessage);
                    intent.putExtra("cardSaleResponseData", data.getSerializableExtra("cardSaleResponseData"));
                    intent.putExtra("Title", getResources().getString(R.string.emi));
                    startActivityForResult(intent, MSWIPE_CARDSALE_DECLINE_REQUEST);
                }
            } else if (requestCode == MSWIPE_PREAUTH_ACTIVITY_REQUEST_CODE) {

                boolean status = data.getBooleanExtra("status", false);
                String statusMessage = data.getStringExtra("statusMessage");

                if (resultCode == RESULT_OK) {
                    if (AppSharedPrefrences.getAppSharedPrefrencesInstace().isSignatureRequired()) {
                        Intent intent = new Intent(ctx, MswipeSignatureActivity.class);
                        intent.putExtra("Title", getResources().getString(R.string.preauth));
                        intent.putExtra("cardSaleResponseData", data.getSerializableExtra("cardSaleResponseData"));
                        startActivityForResult(intent, MSWIPE_CARDSALE_SIGNATURE_REQUEST);
                    } else {
                        showapproveDialog(Boolean.toString(status), data.getExtras().getString("AuthCode"),
                                data.getExtras().getString("RRNo"), statusMessage);
                    }
                } else {

                    Intent intent = new Intent(ctx, MswipeDeclineActivity.class);
                    intent.putExtra("statusMessage", statusMessage);
                    intent.putExtra("cardSaleResponseData", data.getSerializableExtra("cardSaleResponseData"));
                    intent.putExtra("Title", getResources().getString(R.string.preauth));
                    startActivityForResult(intent, MSWIPE_CARDSALE_DECLINE_REQUEST);
                }
            }
        }
        catch (Exception Ex) {
            Log.d("PayByCard",Ex.toString());
        }
    }

    public void showapproveDialog(String status, String authcode, String rrno, String reason) {

        final Dialog dialog = new Dialog(ctx, R.style.styleCustDlg);
        dialog.setContentView(R.layout.cardsale_status_customdlg);
        dialog.setCanceledOnTouchOutside(false);

        dialog.setCancelable(true);


        TextView txtstatusmsg = (TextView) dialog.findViewById(R.id.customdlg_Txt_status);
        txtstatusmsg.setText(status);

        TextView txtauthcode = (TextView) dialog.findViewById(R.id.customdlg_Txt_authcode);
        txtauthcode.setText(authcode);

        TextView txtrrno = (TextView) dialog.findViewById(R.id.customdlg_Txt_rrno);
        txtrrno.setText(rrno);

        TextView txtreason = (TextView) dialog.findViewById(R.id.customdlg_Txt_reason);
        txtreason.setText(reason);

        Button yes = (Button) dialog.findViewById(R.id.customdlg_BTN_yes);
        yes.setBackgroundColor(getResources().getColor(R.color.topbar_activity_background));
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                Intent intent = new Intent(ctx, MenuView.class);
                startActivity(intent);
            }
        });

        dialog.show();
    }



    class MSWisepadControllerResponseListenerObserver implements MSWisepadControllerResponseListener {


        @Override
        public void onReponseData(MSDataStore mswipeDataStore) {

            if (mswipeDataStore instanceof EMITransactionDetailsResponseData) {
                /*

                EMITransactionDetailsResponseData emiTransactionDetailsResponseData = (EMITransactionDetailsResponseData) mswipeDataStore;

                if (ApplicationData.IS_DEBUGGING_ON)
                    Logs.v(ApplicationData.packName, "getResponseStatus " + emiTransactionDetailsResponseData.getResponseStatus(), true, true);

                if (ApplicationData.IS_DEBUGGING_ON)
                    Logs.v(ApplicationData.packName, "getResponseStatus " + emiTransactionDetailsResponseData.getResponseFailureReason(), true, true);

                if (emiTransactionDetailsResponseData.getResponseStatus()){

                    final ArrayList<EMIData> arrayList = emiTransactionDetailsResponseData.getEmiTxtArrayListData();

                    Dialog dialog = showEmiOptionsDialog(MswipePaymentView.this);

                    ListView listView = (ListView)dialog.findViewById(R.id.emisale_list_emi_selection);

                    final EmiAdapter emiAdapter = new EmiAdapter(MswipePaymentView.this, arrayList);
                    listView.setAdapter(emiAdapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                            selectedPosition = position;

                            mEmiPeriod = arrayList.get(position).emiTenure;
                            mEmiBankCode = arrayList.get(position).emiBankCode;
                            mEmiRate = arrayList.get(position).emiRate;
                            mEmiAmount = arrayList.get(position).emiAmt;

                            emiAdapter.notifyDataSetChanged();
                        }
                    });

                    ((Button)dialog.findViewById(R.id.customdlg_BTN_yes)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(selectedPosition == -1){

                                showDialog("please select an option");

                                return;
                            }

                            double amt = Double.parseDouble(mTxtCreditAmount.getText().toString().trim());
                            String totalAmt = String.format("%.2f", amt);

                            Intent intent_emisale = new Intent(MswipePaymentView.this, MswipeEMISaleActivity.class);
                            intent_emisale.putExtra("emisale", true);

                            intent_emisale.putExtra("username", mTxtUserName.getText().toString().trim());
                            intent_emisale.putExtra("password", mTxtPassword.getText().toString().trim());

                            if (AppSharedPrefrences.getAppSharedPrefrencesInstace().getGatewayEnvironment() == MSWisepadController.GATEWAY_ENVIRONMENT.LABS)

                                intent_emisale.putExtra("production", false);
                            else
                                intent_emisale.putExtra("production", true);

                            intent_emisale.putExtra("amount", totalAmt);
                            intent_emisale.putExtra("mobileno", mTxtPhoneNum.getText().toString().trim());
                            intent_emisale.putExtra("receiptno", mTxtReceipt.getText().toString().trim());
                            intent_emisale.putExtra("notes", mTxtNotes.getText().toString().trim());

                            intent_emisale.putExtra("firstSixDigits",mTxtFirstSixDigits.getText().toString());

                            intent_emisale.putExtra("emirate", mEmiRate);
                            intent_emisale.putExtra("emiamount", mEmiAmount);
                            intent_emisale.putExtra("emiperiod", mEmiPeriod);
                            intent_emisale.putExtra("emibankcode", mEmiBankCode);
                            startActivityForResult(intent_emisale, MSWIPE_EMISALE_ACTIVITY_REQUEST_CODE);
                            //finish();

                        }
                    });

                    dialog.show();
*/
            } else {

                android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(ctx);
                builder1.setMessage("Sattire");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();

                            }
                        });


                android.app.AlertDialog alert11 = builder1.create();
                alert11.show();
            }

        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogProg = mBuilder.create();
            dialogProg.show();

            dialogProg.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(PayByCard.this,"Error",Ex.toString());
        }
    }

    private void proceedConfirmation(String output) {
        try{
            String strTotal = "", strCat = "", strTotalAmount = "", strResp = "", strFSummData = "", strFnBPrintData = "", strTransNo = "";
            JCResp obJsonCommit = new Gson().fromJson(output, JCResp.class);
            if(obJsonCommit.getRESPONSECODE().equals("0")){
                strResp = obJsonCommit.getRESPONSEDATA();
                strFSummData = strResp.split("\\@")[0];
                strFnBPrintData = strResp.split("\\@")[1];
                Bundle pcBundle = new Bundle();
                strTotal = strFSummData.split("\\|")[8];
                if(strAppMode.equals("fnb")) {
                    Intent intBookingConfirmation = new Intent(PayByCard.this,FnbConfirmation.class);
                    pcBundle.putString("price",strTotal);
                    pcBundle.putString("printdata",strFnBPrintData);
                    pcBundle.putString("wcode",strWCODE);
                    pcBundle.putString("transno",strFSummData.split("\\|")[11]);
                    pcBundle.putString("bookingno",strFSummData.split("\\|")[10]);
                    pcBundle.putString("receiptno",strFSummData.split("\\|")[13]);
                    intBookingConfirmation.putExtra("jsonArray",strJsonArray);
                    intBookingConfirmation.putExtras(pcBundle);
                    startActivity(intBookingConfirmation);
                }
                else if(strAppMode.equals("ticket")){
                    pcBundle.putString("filmname",strFilmName);
                    pcBundle.putString("seats",strSeats);
                    pcBundle.putString("cinemaname",strCinemaname);
                    pcBundle.putString("showdatetime",strShowDateTime);
                    pcBundle.putString("qty",strQty);
                    pcBundle.putString("price",strTotal);
                    pcBundle.putString("screen",strScreenName);
                    pcBundle.putString("areacat",strAreaCat);
                    Intent intBookingConfirmation = new Intent(PayByCard.this,BookingConfirmation.class);
                    intBookingConfirmation.putExtras(pcBundle);
                    startActivity(intBookingConfirmation);
                }
            }
            else {
                ShowErrorDialog(PayByCard.this,"Error",obJsonCommit.getRESPONSEDATA());
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCard.this,"Error","Something went wrong");
        }
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mServerMessenger = new Messenger(service);
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mServerMessenger = null;
            isBound = false;
        }
    };
}

