package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupSeats {

    @SerializedName("strGroupArea")
    @Expose
    private String strGroupArea;
    @SerializedName("groupRows")
    @Expose
    private List<GroupRow> groupRows = null;

    public String getStrGroupArea() {
        return strGroupArea;
    }

    public void setStrGroupArea(String strGroupArea) {
        this.strGroupArea = strGroupArea;
    }

    public List<GroupRow> getGroupRows() {
        return groupRows;
    }

    public void setGroupRows(List<GroupRow> groupRows) {
        this.groupRows = groupRows;
    }

}