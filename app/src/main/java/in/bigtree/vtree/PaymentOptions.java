package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PaymentOptions extends AppCompatActivity implements AsyncResponse {
    String strTempTransId = "", strSessionId = "", strTicketType = "", strSeatInfo = "", strQty = "", strshowdatetime="", strFilmName = "", strBookingId="", strPrice = "", strCinemaName = "",
            strAreaCat = "", strScreenName = "", strSeats = "", strGAction = "", strAppMode = "", strJsonArray = "", strPickupType = "",strBookingNo="";
    Context context;
    TextView tvShowDateTime, tvFilmname, tvPrice;
    LinearLayout llBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_options);
        try{
            tvShowDateTime = findViewById(R.id.tvShowDateTime);
            tvFilmname = findViewById(R.id.tvFilmName);
            tvPrice = findViewById(R.id.tvPrice);
            llBack = (LinearLayout) findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> BackToPaySelectionConfirmation(v));
            context = this;
            Bundle poBundle = getIntent().getExtras();
            strAppMode = poBundle.getString("appmode");

            if(strAppMode.equals("fnb")){
                strPickupType = poBundle.getString("pickuptype");
                strBookingNo = poBundle.getString("bookingno");
                tvShowDateTime.setText("");
                tvFilmname.setText("Payment for "+ "concessions");
                strFilmName = "concessions";
                strJsonArray = getIntent().getStringExtra("jsonArray");
            }
            if(strAppMode.equals("ticket")){
                strSessionId = poBundle.getString("sessionid");
                strBookingNo = poBundle.getString("bookingno");
                strTicketType = poBundle.getString("ttype");
                strSeatInfo = poBundle.getString("seatinfo");
                strshowdatetime = poBundle.getString("showdatetime");
                strFilmName = poBundle.getString("filmname");
                strBookingId = poBundle.getString("bookingid");
                strPrice = poBundle.getString("price");
                strCinemaName = poBundle.getString("cinemaname");
                strAreaCat = poBundle.getString("areacat");
                strScreenName = poBundle.getString("screen");
                strSeats = poBundle.getString("seats");
                tvFilmname.setText("Payment for "+ strFilmName);
                tvShowDateTime.setText(strshowdatetime);
            }
            strTempTransId = poBundle.getString("temptransid");
            strPrice = poBundle.getString("price");
            strQty = poBundle.getString("qty");
            tvPrice.setText(strPrice);
        }
        catch(Exception Ex){
            ShowErrorDialog(PaymentOptions.this,"Error","Something went wrong");
        }
    }

    public void payByPayType(View v){
        try{
            String strTag = v.getTag().toString();
            Bundle pbBundle = new Bundle();
            pbBundle.putString("temptransid",strTempTransId);
            pbBundle.putString("sessionid",strSessionId);
            pbBundle.putString("ttype",strTicketType);
            pbBundle.putString("qty",strQty);
            pbBundle.putString("seatinfo",strSeatInfo);
            pbBundle.putString("bookingid",strBookingId);
            pbBundle.putString("showdatetime",strshowdatetime);
            pbBundle.putString("filmname",strFilmName);
            pbBundle.putString("price",strPrice);
            pbBundle.putString("cinemaname",strCinemaName);
            pbBundle.putString("areacat",strAreaCat);
            pbBundle.putString("screen",strScreenName);
            pbBundle.putString("seats",strSeats);
            pbBundle.putString("appmode",strAppMode);
            pbBundle.putString("pickuptype",strPickupType);
            pbBundle.putString("bookingno",strBookingNo);
            Intent intPayType;
            switch (strTag){
                case "paycash":
                    intPayType = new Intent(context,PayByCash.class);
                    intPayType.putExtra("jsonArray",strJsonArray);
                    intPayType.putExtras(pbBundle);
                    startActivity(intPayType);
                    break;
                case "paycard":
                    intPayType = new Intent(context,PayByCard.class);
                    intPayType.putExtra("jsonArray",strJsonArray);
                    intPayType.putExtras(pbBundle);
                    startActivity(intPayType);
                    break;
                default:
                    break;
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(PaymentOptions.this,"Error","Something went wrong");
        }
    }


    public void BackToPaySelectionConfirmation(View view) {
        try{
            subCancelTrans();
        }
        catch (Exception Ex){
            ShowErrorDialog(PaymentOptions.this,"Error","Something went wrong");
        }
    }

    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                        subCancelTrans();

                }
            });
        }
        catch (Exception Ex){
            Toast.makeText(PaymentOptions.this,Ex.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private void subCancelTrans() {
        String strFileContent = "", strIP = "";
        try{
            FileIO obFile = new FileIO();
            strFileContent = obFile.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];

            strGAction = "CANCELTRANS";
            SoapCall obSc = new SoapCall(PaymentOptions.this);
            obSc.delegate = this;
            obSc.execute("CANCELTRANS", strIP, strTempTransId, "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            ShowErrorDialog(PaymentOptions.this,"Error","Something went wrong");
            //ShowErrorDialog(PaySelectionConfirmation.this,"Error",Ex.toString());
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void processFinish(String output) {
        try{
            if(!output.split("\\|")[0].equals("1")){
                if(strGAction.equals("CANCELTRANS")) {
                    finish();
                }
            }
            else {
                ShowErrorDialog(PaymentOptions.this,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(PaymentOptions.this,"Error","Something went wrong");
        }
    }
}
