package in.bigtree.vtree;

import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.mswipetech.wisepad.sdk.device.MSWisepadDeviceController;
import com.mswipetech.wisepad.sdk.device.MSWisepadDeviceControllerResponseListener;
import com.mswipetech.wisepad.sdk.device.WisePadConnection;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import in.bigtree.vtree.util.Logs;
import in.bigtree.vtree.util.ReceiptBitmap;

public class MSwipePrinter {

    String strPrintData,strCinemaName,strWCode,strTransNo,strBookingNo,strReceiptNo;
    Context ctx;
    public MSwipePrinter(Context ctx){
        this.ctx = ctx;
        //doBindMswipeWisepadDeviceService();
    }

    public MSWisepadDeviceController mMSWisepadDeviceController = null;

    public void printVoucher(String strPrintData, String strCinemaName, String strWCode, String strTransNo, String strBookingNo, String strReceiptNo) {
        //Print.StartPrinting();
        try{
            String strDate ="", strItemDesc = "", strQty = "", strNettVal = "", strTax1="", strTax2="",strGrossVal="",strTotalNett="",strTotalTax1="",strTotalTax2="",strTotalGross="";
            /*
            String[] arrPrintData = strPrintData.split("\\|");
            Double dblNett = 0.00, dblTax1 = 0.00, dblTax2 = 0.00, dblGross=0.00;
            String msg="";
            byte[] arrayOfByte1 = { 27, 33, 0 };
            byte[] format = { 27, 33, 0 };
            format[2] = ((byte)(0x8 | arrayOfByte1[1]));

            Date dtmDate = new Date();
            String strDateFormat = "dd-MM-yyyy hh:mm a";
            DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
            strDate= dateFormat.format(dtmDate);
*/
            this.strPrintData = strPrintData;
            this.strCinemaName = strCinemaName;
            this.strWCode = strWCode;
            this.strTransNo = strTransNo;
            this.strBookingNo = strBookingNo;
            this.strReceiptNo = strReceiptNo;
            doBindMswipeWisepadDeviceService();
            /*
            if (mMSWisepadDeviceController.getWisepadConnectionState() == WisePadConnection.WisePadConnection_CONNECTED) {
                AsyncTaskRunner runner = new AsyncTaskRunner();
                runner.execute(strPrintData, strCinemaName, strWCode, strTransNo);

            } else {
                Log.e(MSwipePrinter.class.getSimpleName(), "device not connected");
                //mTXTStatus.setText("device not connected");
            }
        */

/*
            msg="RETAIL INVOICE";
            Print.StartPrinting(msg+"<br>",FontLattice.TWENTY_FOUR, true, Align.CENTER, true);

            msg=strCinemaName;
            Print.StartPrinting(msg+"<br>", FontLattice.TWENTY_FOUR, true, Align.CENTER, true);

            msg="**********CASH MEMO*********";
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.CENTER, true);

            msg=strDate;
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.LEFT, true);

            msg="From: "+strWCode;
            msg+= "    "+"Inv No:"+strTransNo;
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.LEFT, true);

            msg="----------------------------------------------";
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.CENTER, true);

            //msg="Items/Qty(Rate)   CGST   SGST   Amount";
            msg = String.format("%1$15s %2$8s %3$8s %4$8s", "Items/Qty(Rate)", "CGST", "SGST", "Amount");
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.CENTER, true);

            msg="----------------------------------------------";
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.CENTER, true);

            for(int iCount = 0; iCount < arrPrintData.length; iCount++){
                strItemDesc = arrPrintData[iCount].split("\\_")[0];
                strQty = arrPrintData[iCount].split("\\_")[1];
                strNettVal = arrPrintData[iCount].split("\\_")[2];
                strTax1 = arrPrintData[iCount].split("\\_")[3];
                strTax2 = arrPrintData[iCount].split("\\_")[4];
                strGrossVal = arrPrintData[iCount].split("\\_")[5];

                msg=strItemDesc + " x"+ strQty;
                Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.LEFT, true);

                msg = String.format("%1$15s %2$8s %3$8s %4$8s", strNettVal, strTax1, strTax2, strGrossVal);
                Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.CENTER, true);
                //writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

                dblNett = dblNett + Double.parseDouble(strNettVal);
                dblTax1 = dblTax1 + Double.parseDouble(strTax1);
                dblTax2 = dblTax2 + Double.parseDouble(strTax2);
                dblGross = dblGross + Double.parseDouble(strGrossVal);
            }

            msg="----------------------------------------------";
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.CENTER, true);

            strTotalNett = String.format("%.2f",dblNett);
            strTotalTax1 = String.format("%.2f",dblTax1);
            strTotalTax2 = String.format("%.2f",dblTax2);
            strTotalGross = String.format("%.2f",dblGross);

            msg = String.format("%1$7s %2$7s %3$8s %4$7s %5$8s", "Total", strTotalNett, strTotalTax1, strTotalTax2, strTotalGross);
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.CENTER, true);

            msg="----------------------------------------------";
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.CENTER, true);

            msg="CGST Code    -24133456655";
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.LEFT, true);
            //writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());
            msg="SGST Code    -24133456655";
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.LEFT, true);
            //writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

            Print.StartPrinting();

            msg="This is a computer generated invoice and \n hence does not require any signature";
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.CENTER, true);
            //writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.centerAlign());

            msg="Thank you";
            Print.StartPrinting(msg+"<br>", FontLattice.SIXTEEN, true, Align.CENTER, true);
            //writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.centerAlign());
            Print.StartPrinting();
            Print.StartPrinting();
            Print.StartPrinting();
*/
            //Typeface tf = Typeface.SERIF;

            /*
            ReceiptBitmap receiptBitmap = new ReceiptBitmap().getInstance();
            msg="RETAIL INVOICE";
            receiptBitmap.setTextSize(24);
            //receiptBitmap.setTypeface(Typeface.create(tf, Typeface.BOLD));
            receiptBitmap.drawCenterText(msg);

            msg=strCinemaName;
            receiptBitmap.drawCenterText(msg+"<br>");

            msg="**********CASH MEMO*********";
            receiptBitmap.setTextSize(16);
            receiptBitmap.drawCenterText(msg+"<br>");

            msg=strDate;
            receiptBitmap.drawLeftText(msg+"<br>");

            msg="From: "+strWCode;
            msg+= "    "+"Inv No:"+strTransNo;
            receiptBitmap.drawLeftText(msg+"<br>");

            msg="----------------------------------------------";
            receiptBitmap.drawCenterText(msg+"<br>");

            //msg="Items/Qty(Rate)   CGST   SGST   Amount";
            msg = String.format("%1$15s %2$8s %3$8s %4$8s", "Items/Qty(Rate)", "CGST", "SGST", "Amount");
            receiptBitmap.drawCenterText(msg+"<br>");

            msg="----------------------------------------------";
            receiptBitmap.drawCenterText(msg+"<br>");

            for(int iCount = 0; iCount < arrPrintData.length; iCount++){
                strItemDesc = arrPrintData[iCount].split("\\_")[0];
                strQty = arrPrintData[iCount].split("\\_")[1];
                strNettVal = arrPrintData[iCount].split("\\_")[2];
                strTax1 = arrPrintData[iCount].split("\\_")[3];
                strTax2 = arrPrintData[iCount].split("\\_")[4];
                strGrossVal = arrPrintData[iCount].split("\\_")[5];

                msg=strItemDesc + " x"+ strQty;
                receiptBitmap.drawLeftText(msg+"<br>");

                msg = String.format("%1$15s %2$8s %3$8s %4$8s", strNettVal, strTax1, strTax2, strGrossVal);
                receiptBitmap.drawCenterText(msg+"<br>");
                //writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

                dblNett = dblNett + Double.parseDouble(strNettVal);
                dblTax1 = dblTax1 + Double.parseDouble(strTax1);
                dblTax2 = dblTax2 + Double.parseDouble(strTax2);
                dblGross = dblGross + Double.parseDouble(strGrossVal);
            }

            msg="----------------------------------------------";
            receiptBitmap.drawCenterText(msg+"<br>");

            strTotalNett = String.format("%.2f",dblNett);
            strTotalTax1 = String.format("%.2f",dblTax1);
            strTotalTax2 = String.format("%.2f",dblTax2);
            strTotalGross = String.format("%.2f",dblGross);

            msg = String.format("%1$7s %2$7s %3$8s %4$7s %5$8s", "Total", strTotalNett, strTotalTax1, strTotalTax2, strTotalGross);
            receiptBitmap.drawCenterText(msg+"<br>");

            msg="----------------------------------------------";
            receiptBitmap.drawCenterText(msg+"<br>");

            msg="CGST Code    -24133456655";
            receiptBitmap.drawLeftText(msg+"<br>");
            //writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());
            msg="SGST Code    -24133456655";
            receiptBitmap.drawLeftText(msg+"<br>");
            //writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

            Print.StartPrinting();

            msg="This is a computer generated invoice and \n hence does not require any signature";
            receiptBitmap.drawCenterText(msg+"<br>");
            //writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.centerAlign());

            msg="Thank you";
            receiptBitmap.drawCenterText(msg+"<br>");
            //writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.centerAlign());
            receiptBitmap.drawNewLine();
            receiptBitmap.drawNewLine();
            receiptBitmap.drawNewLine();

             */
        }
        catch (Exception Ex)
        {
            Log.e(MSwipePrinter.class.getSimpleName(),"Error: " + Ex.toString());
        }
    }

    private ArrayList<byte[]> mPrintData;
    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String strPrintData = params[0], strCinemaName = params[1], strWCode = params[2], strTransNo = params[3];
            mPrintData = new ArrayList<>();
            byte[] receiptData = getReceipt(strPrintData,strCinemaName,strWCode,strTransNo, strBookingNo, strReceiptNo);
            mPrintData.add(receiptData);

            return "";
        }

        public byte[] getReceipt(String strPrintData, String strCinemaName, String strWCode, String strTransNo, String strBookingNo, String strReceiptNo){
            Log.e(MSwipePrinter.class.getSimpleName(),"strPrintdata"+strPrintData);
            String gstInfo = strPrintData.split("\\#")[1];
            String printInfo = strPrintData.split("\\#")[0];
            String[] arrPrintData = printInfo.split("\\|");
            Double dblNett = 0.00, dblTax1 = 0.00, dblTax2 = 0.00, dblGross=0.00;
            String msg="", strDate = "",  strItemDesc = "", strQty = "", strNettVal = "", strTax1="", strTax2="",strGrossVal="",strTotalNett="",strTotalTax1="",strTotalTax2="",strTotalGross="";
            Date dtmDate = new Date();
            String strDateFormat = "dd-MM-yyyy hh:mm a";
            DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
            strDate= dateFormat.format(dtmDate);
            Typeface tf = Typeface.SERIF;
            Log.e(MSwipePrinter.class.getSimpleName(),"strPrintData"+strPrintData.length());
            try {
                ReceiptBitmap receiptBitmap = new ReceiptBitmap().getInstance();

                receiptBitmap.init(2000);
                msg="RETAIL INVOICE";
                receiptBitmap.setTextSize(24);
                //receiptBitmap.setTypeface(Typeface.create(tf, Typeface.BOLD));
                receiptBitmap.drawCenterText(msg);

                msg=strCinemaName;
                receiptBitmap.drawCenterText(msg);

                msg="**********CASH MEMO*********";
                receiptBitmap.setTextSize(16);
                receiptBitmap.drawCenterText(msg);

                msg=strDate;
                receiptBitmap.drawLeftText(msg);

                receiptBitmap.drawLeftAndRightText("From: "+strWCode,"Invoice No:"+strReceiptNo);

                receiptBitmap.drawLeftAndRightText("Transaction No: "+strTransNo,"Order No:"+strBookingNo);

                msg="-------------------------------------------------------------------------";
                receiptBitmap.drawCenterText(msg);

                //msg = String.format("%1$15s %2$8s %3$8s %4$8s", "Items/Qty(Rate)", "CGST", "SGST", "Amount");
                msg = String.format("%1$15s %2$12s %3$12s %4$12s", "Items/Qty(Rate)", "CGST", "SGST", "Amount");
                receiptBitmap.drawCenterText(msg);

                msg="-------------------------------------------------------------------------";
                receiptBitmap.drawCenterText(msg);

                for(int iCount = 0; iCount < arrPrintData.length; iCount++){
                    strItemDesc = arrPrintData[iCount].split("\\_")[0];
                    strQty = arrPrintData[iCount].split("\\_")[1];
                    strNettVal = arrPrintData[iCount].split("\\_")[2];
                    strTax1 = arrPrintData[iCount].split("\\_")[3];
                    strTax2 = arrPrintData[iCount].split("\\_")[4];
                    strGrossVal = arrPrintData[iCount].split("\\_")[5];

                    msg=strItemDesc + " x"+ strQty;
                    receiptBitmap.drawLeftText(msg);

                    msg = String.format("%1$16s %2$13s %3$13s %4$13s", strNettVal, strTax1, strTax2, strGrossVal);
                    receiptBitmap.drawCenterText(msg);

                    dblNett = dblNett + Double.parseDouble(strNettVal);
                    dblTax1 = dblTax1 + Double.parseDouble(strTax1);
                    dblTax2 = dblTax2 + Double.parseDouble(strTax2);
                    dblGross = dblGross + Double.parseDouble(strGrossVal);
                }

                msg="-------------------------------------------------------------------------";
                receiptBitmap.drawCenterText(msg);

                strTotalNett = String.format("%.2f",dblNett);
                strTotalTax1 = String.format("%.2f",dblTax1);
                strTotalTax2 = String.format("%.2f",dblTax2);
                strTotalGross = String.format("%.2f",dblGross);

                msg = String.format("%1$7s %2$11s %3$12s %4$11s %5$12s", "Total", strTotalNett, strTotalTax1, strTotalTax2, strTotalGross);
                receiptBitmap.drawCenterText(msg);

                msg="-------------------------------------------------------------------------";
                receiptBitmap.drawCenterText(msg);

                msg="GSTIN:       "+gstInfo.split("\\~")[0];
                receiptBitmap.drawLeftText(msg);
                String cin = (gstInfo.split("\\~").length > 1) ? gstInfo.split("\\~")[1] : "";
                msg="CIN:            "+cin;
                receiptBitmap.drawLeftText(msg);
                String sac = (gstInfo.split("\\~").length > 2) ? gstInfo.split("\\~")[2] : "";
                msg="SAC:            "+sac;
                receiptBitmap.drawLeftText(msg);
                receiptBitmap.drawNewLine();
                msg="This is a computer generated invoice and <br/> hence does not require any signature";
                for(String m : msg.split("<br/>")){
                    receiptBitmap.drawCenterText(m);
                }

                msg="Thank you";
                receiptBitmap.drawCenterText(msg);

                receiptBitmap.drawNewLine();
                receiptBitmap.drawNewLine();
                Bitmap canvasbitmap = receiptBitmap.getReceiptBitmap();

                Bitmap croppedBmp = Bitmap.createBitmap(canvasbitmap, 0, 0, canvasbitmap.getWidth(), canvasbitmap.getHeight());

                byte[] imageCommand = mMSWisepadDeviceController.getPrintData(croppedBmp, 150);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                baos.write(imageCommand, 0, imageCommand.length);

                Log.e(MSwipePrinter.class.getSimpleName(),"receipt height: "+receiptBitmap.getReceiptHeight());

                return baos.toByteArray();


            } catch (Exception e) {
                e.printStackTrace();
                Log.e(MSwipePrinter.class.getSimpleName(),"Error:" + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if(mPrintData != null && mPrintData.size() > 0)
            {
                mMSWisepadDeviceController.print(mPrintData);
            }
            else{
                doUnbindMswipeWisepadDeviceService();
                //mTXTStatus.setText("unable to print");
            }
        }

        @Override
        protected void onPreExecute() { }

        @Override
        protected void onProgressUpdate(String... text) { }
    }

    private ServiceConnection mMSWisepadDeviceControllerService = new ServiceConnection(){
        public void onServiceConnected(ComponentName className, IBinder service)
        {
            try
            {
                MSWisepadDeviceController.LocalBinder localBinder = (MSWisepadDeviceController.LocalBinder) service;
                mMSWisepadDeviceController = localBinder.getService();

                if(mMSWisepadDeviceController != null) {
                    mMSWisepadDeviceController.initMswipeWisepadDeviceController(new MSWisepadDeviceObserver(),true, false, false,false, null);
                }
            }
            catch (Exception e) {
                Logs.v(SharedVariable.packName, "exception."+e.toString(), true, true);
            }
        }

        public void onServiceDisconnected(ComponentName className)
        {
                Logs.v(SharedVariable.packName, "Wisepad servcie un-binded and wisepad is disconnected...", true, true);
            /**
             * This is called when the connection with the service has been
             * unexpectedly disconnected - process crashed.
             *
             */

            mMSWisepadDeviceController = null;

        }
    };

    class MSWisepadDeviceObserver implements MSWisepadDeviceControllerResponseListener{

        @Override
        public void onReturnWisepadConnection(WisePadConnection wisePadConntection, BluetoothDevice bluetoothDevice) {

            if (mMSWisepadDeviceController.getWisepadConnectionState() == WisePadConnection.WisePadConnection_CONNECTED) {
                AsyncTaskRunner runner = new AsyncTaskRunner();
                runner.execute(strPrintData, strCinemaName, strWCode, strTransNo);

            } else {
                Log.e(MSwipePrinter.class.getSimpleName(), "device not connected");
                Log.e(MSwipePrinter.class.getSimpleName(), mMSWisepadDeviceController.getWisepadConnectionState().toString());
            }
            /*
            String msg = getString(R.string.unknown);

            if(wisePadConntection == WisePadConnection.WisePadConnection_CONNECTED)
            {
                msg = getString(R.string.device_connected);
            }
            else if(wisePadConntection == WisePadConnection.WisePadConnection_CONNECTING){

                msg = getString(R.string.device_connecting);
            }
            else if(wisePadConntection == WisePadConnection.WisePadConnection_NOT_CONNECTED){

                msg = getString(R.string.device_not_connected);
            }
            else if(wisePadConntection == WisePadConnection.WisePadConnection_DIS_CONNECTED){

                msg = getString(R.string.device_disconnected);
            }
*/

        }

        @Override
        public void onRequestWisePadCheckCardProcess(CheckCardProcess checkCardProcess, ArrayList<String> dataList) {

        }

        @Override
        public void onReturnWisePadOfflineCardTransactionResults(CheckCardProcessResults checkCardResults, Hashtable<String, Object> paramHashtable) {

        }

        @Override
        public void onError(Error error, String errorMsg) {

            Log.e(MSwipePrinter.class.getSimpleName(),"onError: "+error.toString());
        }

        @Override
        public void onRequestDisplayWispadStatusInfo(DisplayText msg) {

        }

        @Override
        public void onReturnDeviceInfo(Hashtable<String, String> paramHashtable) {

        }

        @Override
        public void onReturnFunctionKeyResult(FunctionKeyResult keyType) {

        }

        @Override
        public void onReturnWispadNetwrokSettingInfo(WispadNetwrokSetting wispadNetwrokSetting, boolean status, Hashtable<String, Object> netwrokSettingInfo) {

        }

        @Override
        public void onReturnNfcDetectCardResult(NfcDetectCardResult nfcDetectCardResult, Hashtable<String, Object> hashtable) {

        }

        @Override
        public void onReturnNfcDataExchangeResult(boolean isSuccess, Hashtable<String, String> data) {

        }

        @Override
        public void onPrintResult(PrintResult printResult) {
            doUnbindMswipeWisepadDeviceService();
/*
            if (SharedVariable.IS_DEBUGGING_ON)
                Logs.e(SharedVariable.packName,  "printResult " + printResult, true, true);

            String msg = getString(R.string.unknown);

            if(printResult == PrintResult.PRINT_SUCCESS)
            {
                msg = R.string.printer_command_success;

            }
            else if(printResult == PrintResult.PRINT_NO_PAPER_OR_COVER_OPENED)
            {
                msg = getString(R.string.no_paper);

            }
            else if(printResult == PrintResult.PRINT_WRONG_PRINTER_COMMAND)
            {
                msg = getString(R.string.printer_command_not_available);

            }
            else if(printResult == PrintResult.PRINT_OVERHEAT)
            {
                msg = getString(R.string.printer_overheat);

            }
            else if(printResult == PrintResult.PRINT_PRINTER_ERROR)
            {
                msg = getString(R.string.printer_command_not_available);

            }

            mTXTStatus.setText(msg);
*/

        }
    }

    void doBindMswipeWisepadDeviceService(){
        ctx.bindService(new Intent(ctx, MSWisepadDeviceController.class), mMSWisepadDeviceControllerService, Context.BIND_AUTO_CREATE);
    }

    public void doUnbindMswipeWisepadDeviceService(){
        ctx.unbindService(mMSWisepadDeviceControllerService);
    }


}
