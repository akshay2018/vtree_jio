package in.bigtree.vtree;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

public class PrintFnB {
    BluetoothAdapter btAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    OutputStream mmOutputStream;
    InputStream mmInputStream;

    public String checkDevice(Context ctx, String strPrinterName) {
        String strReturn = "";
        try {
            btAdapter = BluetoothAdapter.getDefaultAdapter();
            if (btAdapter == null) {
                Toast.makeText(ctx, "No bluetooth adapter available", Toast.LENGTH_LONG).show();
                strReturn = "2|bluetooth adapter not available";
                return strReturn;
            }
            if (!btAdapter.isEnabled()) {
                //Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                //startActivityForResult(enableBluetooth, 0);
                strReturn = "3|bluetooth is disabled";
                return strReturn;
            }
            Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    // RPP300 is the name of the bluetooth printer device
                    // we got this name from the list of paired devices
                    if (device.getName().equals(strPrinterName)) {
                        mmDevice = device;
                        if (mmDevice != null) {
                            //Toast.makeText(ctx, "Device found: " + device.getName(), Toast.LENGTH_LONG).show();
                            strReturn = "0|"+strPrinterName;
                        }
                        break;
                    }
                }
            }
        } catch (Exception Ex) {
            strReturn = "1|"+Ex.toString();
        }
        return strReturn;
    }

    public void openBT(String strPrintData, String strCinemaName, String strWCode, String strTransNo) throws IOException {
        try {
            Method getUuidsMethod = btAdapter.getClass().getDeclaredMethod("getUuids", null);
            ParcelUuid[] uuids = (ParcelUuid[]) getUuidsMethod.invoke(btAdapter, null);
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();

            sendData(strPrintData, strCinemaName, strWCode, strTransNo);
            //beginListenForData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String openDevice(String strPrintData, String strCinemaName, String strWCode, String strTransNo, String strPrinterName) {
        String strReturn = "";
        try {
            btAdapter = BluetoothAdapter.getDefaultAdapter();
            if (btAdapter == null) {
                strReturn = "2|bluetooth adapter not available";
                return strReturn;
            }
            if (!btAdapter.isEnabled()) {
                strReturn = "3|bluetooth is disabled";
                return strReturn;
            }
            Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device.getName().equals(strPrinterName)) {
                        mmDevice = device;
                        if (mmDevice != null) {
                            strReturn = "0|"+strPrinterName;
                            openBT(strPrintData, strCinemaName, strWCode, strTransNo);
                        }
                        break;
                    }
                }
            }
        }
        catch (Exception Ex){
            strReturn="1|"+Ex.toString();
        }
        return strReturn;
    }

    public  void sendData(String strPrintData, String strCinemaName, String strWCode, String strTransNo) throws IOException {
        String msg="";
        try {
            String strDate ="", strItemDesc = "", strQty = "", strNettVal = "", strTax1="", strTax2="",strGrossVal="",strTotalNett="",strTotalTax1="",strTotalTax2="",strTotalGross="";
            Double dblNett = 0.00, dblTax1 = 0.00, dblTax2 = 0.00, dblGross=0.00;
            String[] arrPrintData = strPrintData.split("\\|");
            byte[] arrayOfByte1 = { 27, 33, 0 };
            byte[] format = { 27, 33, 0 };
            format[2] = ((byte)(0x8 | arrayOfByte1[1]));

            Date dtmDate = new Date();
            String strDateFormat = "dd-MM-yyyy hh:mm a";
            DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
            strDate= dateFormat.format(dtmDate);

            // the text typed by the user
            msg="RETAIL INVOICE";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.centerAlign());

            msg=strCinemaName;
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().height().small().get(), PrintFormatter.centerAlign());

            msg="**********CASH MEMO*********";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.centerAlign());

            msg="Date:"+strDate;
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

            msg="From:"+strWCode;
            msg+= "    "+"Inv No:"+strTransNo;
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

            msg="--------------------------------";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().get(), PrintFormatter.centerAlign());

            //msg="Items/Qty(Rate)   CGST   SGST   Amount";
            msg = String.format("%1$15s %2$8s %3$8s %4$8s", "Items/Qty(Rate)", "CGST", "SGST", "Amount");
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

            msg="--------------------------------";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().get(), PrintFormatter.centerAlign());

            for(int iCount = 0; iCount < arrPrintData.length; iCount++){
                strItemDesc = arrPrintData[iCount].split("\\_")[0];
                strQty = arrPrintData[iCount].split("\\_")[1];
                strNettVal = arrPrintData[iCount].split("\\_")[2];
                strTax1 = arrPrintData[iCount].split("\\_")[3];
                strTax2 = arrPrintData[iCount].split("\\_")[4];
                strGrossVal = arrPrintData[iCount].split("\\_")[5];

                msg=strItemDesc + " x"+ strQty;
                msg += "\n";
                writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

                msg = String.format("%1$15s %2$8s %3$8s %4$8s", strNettVal, strTax1, strTax2, strGrossVal);
                msg += "\n";
                writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

                dblNett = dblNett + Double.parseDouble(strNettVal);
                dblTax1 = dblTax1 + Double.parseDouble(strTax1);
                dblTax2 = dblTax2 + Double.parseDouble(strTax2);
                dblGross = dblGross + Double.parseDouble(strGrossVal);
            }
/*
            msg="BURGER";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

            msg = String.format("%1$15s %2$8s %3$8s %4$8s", "66.50", "1.75", "1.75", "70.00");
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

            msg="PEPSI 250ml x3";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

            msg = String.format("%1$15s %2$8s %3$8s %4$8s", "142.50", "3.75", "3.75", "150.00");
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());
*/
            msg="--------------------------------";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().get(), PrintFormatter.centerAlign());

            strTotalNett = String.format("%.2f",dblNett);
            strTotalTax1 = String.format("%.2f",dblTax1);
            strTotalTax2 = String.format("%.2f",dblTax2);
            strTotalGross = String.format("%.2f",dblGross);

            msg = String.format("%1$7s %2$7s %3$8s %4$7s %5$8s", "Total", strTotalNett, strTotalTax1, strTotalTax2, strTotalGross);
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

            msg="--------------------------------";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().get(), PrintFormatter.centerAlign());

            msg="CGST Code    -24133456655";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());
            msg="SGST Code    -24133456655";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.leftAlign());

            msg="This is a computer generated invoice and \n hence does not require any signature";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.centerAlign());

            msg="Thank you";
            msg += "\n";
            writeWithFormat(msg.getBytes(), new PrintFormatter().small().get(), PrintFormatter.centerAlign());

            //printKOT();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void printKOT(){

    }

    public boolean writeWithFormat(byte[] buffer, final byte[] pFormat, final byte[] pAlignment) {
        try {
            // Notify printer it should be printed with given alignment:
            mmOutputStream.write(pAlignment);
            // Notify printer it should be printed in the given format:
            mmOutputStream.write(pFormat);
            // Write the actual data:
            mmOutputStream.write(buffer, 0, buffer.length);

            // Share the sent message back to the UI Activity
            //App.getInstance().getHandler().obtainMessage(MESSAGE_WRITE, buffer.length, -1, buffer).sendToTarget();
            return true;
        } catch (IOException e) {
            Log.e("", "Exception during write", e);
            return false;
        }
    }

}
