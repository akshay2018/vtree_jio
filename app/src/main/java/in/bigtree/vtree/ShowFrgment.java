package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


public class ShowFrgment extends Fragment {

    GridView gvShowTImes;
    public ShowFrgment(){

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_grid, container, false);
        String strShows = "", strShowDate = "";

        try {
            gvShowTImes = (GridView) view.findViewById(R.id.gvShowTimes);
            Bundle bundle = this.getArguments();
            strShows = bundle.getString("shows");
            strShowDate = bundle.getString("showdate");
            final String strCinemaName = bundle.getString("cinemaname");
            final String strFilmName = bundle.getString("filmname");
            bundle.remove("shows");
            bundle.remove("cinemaname");
            bundle.remove("filmname");
            bundle.remove("showdate");
            CustomGridAdapter gridAdapter = new CustomGridAdapter(getActivity(), strShows,strShowDate);
            gvShowTImes.setAdapter(gridAdapter);

            gvShowTImes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String strFileContent = "", strSLayout = "", strScreen = "", strShowTime = "", strShowDate = "", isFreeSeating = "", ticketTypeCodes = "";
                    String strSessionId = view.getTag().toString().split("\\|")[0];
                    strShowTime = view.getTag().toString().split("\\|")[1];
                    strScreen = view.getTag().toString().split("\\|")[2];
                    strShowDate = view.getTag().toString().split("\\|")[3];
                    isFreeSeating = view.getTag().toString().split("\\|")[4];
                    ticketTypeCodes = view.getTag().toString().split("\\|",-1)[5];
                    //Toast.makeText(parent.getContext(),strSessionId, Toast.LENGTH_LONG).show();
                    FileIO obFileIO = new FileIO();
                    strFileContent = obFileIO.readFile(getResources().getString(R.string.advance_config_file),parent.getContext());
                    if(strFileContent.split("\\|").length > 0){ strSLayout = strFileContent.split("\\|")[0]; }
                    //SharedVariable obShared = ((SharedVariable)parent.getContext());
                    //strSLayout = obShared.getSeatLayout();
                    //isFreeSeating = "Y";
                    //ticketTypeCodes = "0040~Free Seating$0040~Free Seating";
                    if(isFreeSeating.equals("Y")){

                        //showFreeSeatingDialog(parent.getContext(), ticketTypeCodes, strSessionId, strCinemaName, strShowTime, strFilmName, strShowDate, strScreen);
                    }
                    else {
                        switch (strSLayout) {
                            case "full":
                                GoToSeatLayout(parent.getContext(), strSessionId, strCinemaName, strShowTime, strScreen, strFilmName, strShowDate);
                                break;
                            case "partial":
                                GoToTicketTypeSelection(parent.getContext(), strSessionId, strCinemaName, strShowTime, strScreen, strFilmName, strShowDate);
                                break;
                            case "":
                                GoToSeatLayout(parent.getContext(), strSessionId, strCinemaName, strShowTime, strScreen, strFilmName, strShowDate);
                                break;
                        }
                    }
                }
            });

        }
        catch (Exception Ex){
            Ex.toString();
        }
        return view;
    }

    public void showFreeSeatingDialog(Context ctx, String ticketTypeCodes, String strSessionId, String strCinemaName, String strShowTime, String strFilmName, String strShowDate, String strScreen){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
        try{
            Button btnProceed;
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.free_seating_layout, null);
            btnProceed = (Button) mView.findViewById(R.id.btnProceed);
            LinearLayout llFSeatQty = (LinearLayout)mView.findViewById(R.id.llFSeatQty);
            llFSeatQty.removeAllViews();
            RadioGroup rdGrpTTpye = (RadioGroup)mView.findViewById(R.id.rdGrpTTpye);
            String[] arrTTypes = ticketTypeCodes.split("\\$");
            final String[] TQty = {""};
            final String[] TType = {""};
            for(int t = 0; t < arrTTypes.length; t++){
                //RadioButton rdTButton = new RadioButton(ctx,null,R.style.Widget_AppCompat_CompoundButton_CheckBox);
                RadioButton rdTButton = new RadioButton(ctx,null);
                rdTButton.setId(t+999);
                rdTButton.setTag(arrTTypes[t].split("\\~")[0]);
                rdTButton.setText(arrTTypes[t].split("\\~")[1]);
                if(t == 0){
                    rdTButton.setChecked(true);
                }
                else {
                    RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.setMargins(10, 0, 0, 0);
                    rdTButton.setLayoutParams(params);
                }
                rdGrpTTpye.addView(rdTButton);
            }
            for(int q = 1; q <= 8; q++){
                LinearLayout llQtyP = new LinearLayout(ctx);
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, getResources().getDisplayMetrics());
                llQtyP.setLayoutParams(new LinearLayout.LayoutParams(width,width));
                llQtyP.setTag(q+"");

                TextView txtQty = new TextView(ctx);
                txtQty.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                txtQty.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER);
                txtQty.setText(q+"");
                txtQty.setTag("tqty_"+q);
                txtQty.setTextSize(16);
                txtQty.setTextColor(ContextCompat.getColor(ctx,R.color.black));
                if(q==2){
                    TQty[0] = "2";

                    llQtyP.setBackground(ContextCompat.getDrawable(ctx,R.drawable.btn_curved_back_filled));
                    txtQty.setTextColor(ContextCompat.getColor(ctx,R.color.colorWhite));
                }
                llQtyP.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        LinearLayout llPrevSelected = (mView).findViewWithTag(TQty[0]+"");
                        llPrevSelected.setBackground(ContextCompat.getDrawable(ctx,R.color.colorWhite));
                        TextView tOldView = (mView).findViewWithTag("tqty_"+TQty[0]);
                        tOldView.setTextColor(ContextCompat.getColor(ctx,R.color.black));

                        TQty[0] = (String)view.getTag();
                        view.setBackground(ContextCompat.getDrawable(ctx,R.drawable.btn_curved_back_filled));
                        TextView tNewView = (mView).findViewWithTag("tqty_"+TQty[0]);
                        tNewView.setTextColor(ContextCompat.getColor(ctx, R.color.colorWhite));

                    }
                });
                llQtyP.addView(txtQty);
                llFSeatQty.addView(llQtyP);
            }

            btnProceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int selectedId = rdGrpTTpye.getCheckedRadioButtonId();

                    // find the radiobutton by returned id
                    RadioButton rdButtonSelected = (RadioButton) mView.findViewById(selectedId);
                    TType[0] = (String) rdButtonSelected.getTag();
                    Log.d("","Qty :" + TQty[0] + ", TType : " + TType[0]);
                    ProceedToFreeSeating(ctx, TQty[0], TType[0], strSessionId, strCinemaName, strShowTime, strScreen, strFilmName, strShowDate);
                }
            });

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();
        }
        catch (Exception Ex){
            Ex.toString();
        }
    }

    private void ProceedToFreeSeating(Context ctx, String qty, String strTktType, String strSessionId, String strCinemaName, String strShowTime, String strScreenName, String strFilmName, String strShowDate) {
        try{
            Bundle stBundle = new Bundle();
            stBundle.putString("cinemaname",strCinemaName);
            stBundle.putString("showtime",strShowTime);
            stBundle.putString("screen",strScreenName);
            stBundle.putString("filmname",strFilmName);
            stBundle.putString("areacat","");
            stBundle.putString("ttype",strTktType); // tickettype code
            stBundle.putString("seats","");
            stBundle.putString("qty",qty);
            stBundle.putString("temptransid","");
            stBundle.putString("sessionid",strSessionId);
            stBundle.putString("seatinfo","");
            stBundle.putString("showdate",strShowDate);
            stBundle.putString("ispackage","");
            stBundle.putString("isFreeSeating","Y");

            Intent intConfirm = new Intent(ctx,PaySelectionConfirmation.class);
            intConfirm.putExtras(stBundle);
            startActivity(intConfirm);
        }
        catch (Exception Ex){
            //ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
        }
    }


    public void onTicketButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        String strSelectedCode = view.getTag().toString();
    }
    private void GoToSeatLayout(Context context, String strSessionId, String strCinemaName, String strShowTime, String strScreen, String strFilmName, String strShowDate) {
        try{
            Bundle stBundle = new Bundle();
            stBundle.putString("sessionid",strSessionId);
            stBundle.putString("cinemaname",strCinemaName);
            stBundle.putString("showtime",strShowTime);
            stBundle.putString("screen",strScreen);
            stBundle.putString("filmname",strFilmName);
            stBundle.putString("showdate",strShowDate);
            stBundle.putString("stype","full");
            Intent intSeatLayout = new Intent(context,SeatLayout.class);
            intSeatLayout.putExtras(stBundle);
            startActivity(intSeatLayout);
        }
        catch (Exception Ex){
            Ex.toString();
        }
    }

    private void GoToTicketTypeSelection(Context context, String strSessionId, String strCinemaName, String strShowTime, String strScreen, String strFilmName, String strShowDate) {
        try{
            Bundle stBundle = new Bundle();
            stBundle.putString("sessionid",strSessionId);
            stBundle.putString("cinemaname",strCinemaName);
            stBundle.putString("showtime",strShowTime);
            stBundle.putString("screen",strScreen);
            stBundle.putString("filmname",strFilmName);
            stBundle.putString("showdate",strShowDate);
            Intent intSeatLayout = new Intent(context,TicketTypeSelection.class);
            intSeatLayout.putExtras(stBundle);
            startActivity(intSeatLayout);
        }
        catch (Exception Ex){
            Ex.toString();
        }
    }
}

    class CustomGridAdapter extends BaseAdapter{

        Context mContext;
        String[] strShows;
        String strShowDate = "";
        public CustomGridAdapter(FragmentActivity context, String strShows, String strShowDate) {
            this.mContext = context;
            this.strShows = strShows.split("\\$");
            this.strShowDate = strShowDate;
        }


        @Override
        public int getCount() {
            return strShows.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            String strShowTime = "", strSessionId = "", strScreenName = "", strAvailability = "", isFreeSeating = "", ticketTypes = "";
            TextView tvShowTime, tvScreen, tvStatus;
            LinearLayout llShowCell,llBottomPart, llShowParent;
            try {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.fragment_show, null);

                strSessionId = strShows[position].split("\\|")[0];
                strShowTime = strShows[position].split("\\|")[1];
                strScreenName = strShows[position].split("\\|")[2];
                strAvailability = strShows[position].split("\\|")[3];
                isFreeSeating = strShows[position].split("\\|")[6];
                ticketTypes = strShows[position].split("\\|",-1)[7];
                tvShowTime = (TextView) convertView.findViewById(R.id.tvShowTime);
                //tvScreen = (TextView) convertView.findViewById(R.id.tvScreen);
                tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
                llShowCell = (LinearLayout) convertView.findViewById(R.id.llShowContainer);
                //llBottomPart = (LinearLayout) convertView.findViewById(R.id.llBottomPartShow);
                llShowParent = (LinearLayout) convertView.findViewById(R.id.llParentShowCell);
                llShowParent.setTag(strSessionId + "|" + strShowTime + "|" + strScreenName + "|" + strShowDate + "|" + isFreeSeating + "|" + ticketTypes);
                tvShowTime.setText(strShowTime);
                //tvScreen.setText(strScreenName);
                if (strAvailability.equals("A")) {
                    strAvailability = "Available";
                    llShowCell.setBackgroundResource(R.drawable.border_show_available);
                    //llBottomPart.setBackgroundResource(R.drawable.border_show_top);
                    tvShowTime.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
                    //tvScreen.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
                } else {
                    strAvailability = "Sold out";
                }
                tvStatus.setText(strAvailability);
            }
            catch (Exception Ex)
            {
                Ex.toString();
            }
            return convertView;
        }



    }
