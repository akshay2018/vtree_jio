package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

public class FnbMain extends AppCompatActivity implements AsyncResponse,ActivityCommunicator{
    TabLayout tbClassLayout;
    TextView tvTotalQty, tvTotalPrice, tvStockQty;
    ConFragmentAdapter adapter;
    ViewPager mPager;
    RelativeLayout rlsParent;
    String strJsonSession = "", strIP = "", strFileContent = "", strFileName = "", strGAction = "", strOutput = "";
    SoapCall obSc;
    JSONObject jObject;
    JSONArray jArray;
    AlertDialog dialogProg;
    String strOSModifiedData = "";
    LinearLayout llBack;
    private BottomSheetBehavior bottomSheetBehavior;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fnb_main);
        try{

            llBack = (LinearLayout) findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToMain(v));
            tbClassLayout = (TabLayout)findViewById(R.id.tbClassLayout);
            //tvCinema = (TextView)findViewById(R.id.tvCinema);
            tvTotalPrice = (TextView)findViewById(R.id.tvTotalPrice);
            tvTotalQty = (TextView)findViewById(R.id.tvTotalQty);
            tvStockQty = (TextView)findViewById(R.id.tvStockQty);

            bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            FileIO obFile = new FileIO();
            strFileContent = obFile.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];
            jArray = new JSONArray();
            subConSyncData();
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbMain.this,"Error","Something went wrong");
        }
    }

    private void initGetData(String output) {
        String strTabName = "", strCinemaName = "", strCinemaBranch = "";
        int intClassCount = 0;
        try{
            mPager = (ViewPager)findViewById(R.id.vpPager);
            JConData obJsonConsession = new Gson().fromJson(output, JConData.class);
            if(obJsonConsession.getSYNCCONRESPONSECODE().equals("0")) {
                intClassCount = obJsonConsession.getSYNCCONRESPONSEDATA().getCinema().get(0).getItemDetails().size();
                strCinemaName = obJsonConsession.getSYNCCONRESPONSEDATA().getCinema().get(0).getStrCinemaName();
                strCinemaBranch = obJsonConsession.getSYNCCONRESPONSEDATA().getCinema().get(0).getStrCinemaBranch();
                //'tvCinema.setText(strCinemaName);
                SharedVariable obShared = ((SharedVariable)getApplicationContext());
                obShared.setCID(strCinemaName);
                obShared.setBranchCode(strCinemaBranch);

                for (int i = 0; i < intClassCount; i++) {
                    strTabName = obJsonConsession.getSYNCCONRESPONSEDATA().getCinema().get(0).getItemDetails().get(i).getStrClassDesc();
                    tbClassLayout.addTab(tbClassLayout.newTab().setText(strTabName));
                }
                adapter = new ConFragmentAdapter(getSupportFragmentManager(), intClassCount, obJsonConsession.getSYNCCONRESPONSEDATA().getCinema().get(0).getItemDetails(), output, strOSModifiedData);
                mPager.setAdapter(adapter);
                int intLimit = adapter.getCount();
                mPager.setOffscreenPageLimit(intLimit);

                tbClassLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        mPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {}

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {}
                });

                mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, float v, int i1) {}

                    @Override
                    public void onPageSelected(int i) {
                        tbClassLayout.getTabAt(i).select();
                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {}
                });
            }
            else if(obJsonConsession.getSYNCCONRESPONSECODE().equals("2")) {
                ShowErrorDialog(FnbMain.this,"Error","Stock is not available for configured profile");
            }
            else {
                ShowErrorDialog(FnbMain.this,"Error","Something went wrong");
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbMain.this,"Error","Something went wrong");
        }
    }

    public void backToMain(View view) {
        try{
            finish();
        }
        catch (Exception Ex) {
            ShowErrorDialog(FnbMain.this,"Error","Something went wrong");
        }
    }

    @Override
    public void processFinish(String output) {
        try{
            if (dialogProg != null) {
                dialogProg.dismiss();
            }
            strOutput = output;
            if (!output.split("\\|")[0].equals("1")) {

                if (strGAction.equals("SYNCCONITEM")) {
                    initGetData(output);
                }
            }
            else {
                ShowErrorDialog(FnbMain.this,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbMain.this,"Error","Something went wrong");
        }
    }

    @Override
    public void passDataToActivity(String strOp, int val, String strItemId, String strItemName, String strItemPrice, String finalStockQty, String finalListStockQty) {
        int intQty = 0, intExistingQty = 0, intNewQty = 0, intJItemQty = 0, intNewCartQty = 0, intNewCartTotal=0,intCartTotal=0;
        String strJItemId = "", strNewPrice = "";
        boolean blnFound = false;
        double dblCartTotal = 0.0, dblNewCartTotal = 0.0;
        double dblNewPrice = Double.parseDouble(strItemPrice);
        try {
            intQty = val;
            intExistingQty = Integer.parseInt(tvTotalQty.getText().toString());

            if(jArray.length() == 0){
                jObject = new JSONObject();
                jObject.put("itemid",strItemId);
                jObject.put("itemname",strItemName);
                jObject.put("price",strItemPrice);
                jObject.put("totalprice",strItemPrice);
                jObject.put("qty",Integer.toString(intQty));
                jObject.put("stockqty",finalStockQty);
                jObject.put("liststockqty",finalListStockQty);

                jArray.put(jObject);
                intNewQty = intExistingQty + val;
                tvTotalQty.setText(Integer.toString(intNewQty));
                tvTotalPrice.setText(Double.toString(dblNewPrice));
            }
            else{
                for(int i = 0; i < jArray.length(); i++) {
                    strJItemId = jArray.getJSONObject(i).get("itemid").toString();
                    if(strItemId.equals(strJItemId)){
                        intJItemQty = Integer.parseInt(jArray.getJSONObject(i).get("qty").toString());
                        if (strOp.equals("add")) {
                            intNewQty = intJItemQty + val;
                            intNewCartQty = intExistingQty + val;
                            dblCartTotal = Double.parseDouble(tvTotalPrice.getText().toString());
                            dblNewCartTotal = dblCartTotal + dblNewPrice;
                        } else {
                            intNewQty = intJItemQty - val;
                            intNewCartQty = intExistingQty - val;
                            dblCartTotal = Double.parseDouble(tvTotalPrice.getText().toString());
                            dblNewCartTotal = dblCartTotal - dblNewPrice;
                        }

                        if(intNewQty > 0){
                            strNewPrice = Double.toString(dblNewPrice * intNewQty);
                            tvTotalQty.setText(Integer.toString(intNewCartQty));
                            tvTotalPrice.setText(Double.toString(dblNewCartTotal));
                            jArray.getJSONObject(i).put("qty", Integer.toString(intNewQty));
                            jArray.getJSONObject(i).put("totalprice",strNewPrice);
                            jArray.getJSONObject(i).put("stockqty",finalStockQty);
                            jArray.getJSONObject(i).put("liststockqty",finalListStockQty);
                        }
                        else {
                            JSONArray list = new JSONArray();
                            int len = jArray.length();
                            if (jArray != null) {
                                for (int j=0;j<len;j++)
                                {
                                    if (j != i)
                                    {
                                        list.put(jArray.getJSONObject(j));
                                    }
                                }
                                jArray = new JSONArray();
                                for (int k = 0; k < list.length(); k++) {
                                    jArray.put(list.getJSONObject(k));
                                }
                            }
                            tvTotalQty.setText(Integer.toString(intNewCartQty));
                            tvTotalPrice.setText(Double.toString(dblNewCartTotal));
                            if(intNewCartQty == 0){
                                tvTotalPrice.setText("0");
                                tvTotalQty.setText("0");
                                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                mPager.setPadding(0,0,0,0);
                            }
                        }
                        blnFound = true;
                    }
                }

                if(blnFound == false){
                    if (strOp.equals("add")) {
                        intNewCartQty = intExistingQty + intQty;
                        tvTotalQty.setText(Integer.toString(intNewCartQty));

                        dblCartTotal = Double.parseDouble(tvTotalPrice.getText().toString());
                        dblNewCartTotal = dblCartTotal + Double.parseDouble(strItemPrice);
                        tvTotalPrice.setText(Double.toString(dblNewCartTotal));

                        jObject = new JSONObject();
                        jObject.put("itemid",strItemId);
                        jObject.put("itemname",strItemName);
                        jObject.put("price",strItemPrice);
                        jObject.put("totalprice",strItemPrice);
                        jObject.put("qty",Integer.toString(intQty));
                        jObject.put("stockqty",finalStockQty);
                        jObject.put("liststockqty",finalListStockQty);
                        jArray.put(jObject);
                    }
                }
            }
            if(strOp.equals("add")){
                if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    int intMarginBottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics());
                    mPager.setPadding(0,0,0,intMarginBottom);
                }
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbMain.this,"Error","Something went wrong");
        }
    }


    public void proceedToCart(View v){
        String strTotalQty = "", strTotalAmt = "";
        try{
            Bundle bundle = new Bundle();

            strTotalQty = tvTotalQty.getText().toString();
            strTotalAmt = tvTotalPrice.getText().toString();

            bundle.putString("totalamt",strTotalAmt);
            bundle.putString("totalqty",strTotalQty);
            Intent intOrderSummary = new Intent(FnbMain.this, FnbOrderSummary.class);
            //intOrderSummary.putExtras(bundle);
            intOrderSummary.putExtra("jsonArray", jArray.toString());
            intOrderSummary.putExtras(bundle);
            //startActivity(intOrderSummary);
            startActivityForResult(intOrderSummary,0);
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbMain.this,"Error", Ex.toString());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        int intNewQty = 0;
        Double dblNewPrice = 0.0;
        JSONArray jOSArray; JSONObject jOSObject;
        String strItemId = "", strItemDesc = "", strItemPrice = "";
        super.onActivityResult(requestCode,resultCode,data);
        try{
            Log.d("",data.getExtras().getString("jsonArray"));
            mPager.removeAllViews();
            tbClassLayout.removeAllTabs();
            strOSModifiedData = data.getExtras().getString("jsonArray");
            jOSArray = new JSONArray(strOSModifiedData);
            if(jOSArray.length() == 0) {
                strOSModifiedData = "";
                tvTotalPrice.setText("0");
                tvTotalQty.setText("0");
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mPager.setPadding(0,0,0,0);
            }
            subConSyncData();

            jArray = new JSONArray();
            for(int iCount = 0; iCount < jOSArray.length(); iCount++){
                jOSObject = (JSONObject) jOSArray.get(iCount);
                intNewQty = intNewQty + Integer.parseInt(jOSObject.getString("qty"));
                dblNewPrice = dblNewPrice + Double.parseDouble(jOSObject.getString("totalprice"));

                strItemId = jOSObject.getString("itemid");
                strItemDesc = jOSObject.getString("itemname");
                strItemPrice = jOSObject.getString("price");

                jObject = new JSONObject();
                jObject.put("itemid",strItemId);
                jObject.put("itemname",strItemDesc);
                jObject.put("price",strItemPrice);
                jObject.put("totalprice",jOSObject.getString("totalprice"));
                jObject.put("qty",jOSObject.getString("qty"));
                jObject.put("stockqty",jOSObject.getString("stockqty"));
                jObject.put("liststockqty",jOSObject.getString("liststockqty"));
                jArray.put(jObject);
                //change existing jarray
                /*
                for (int jCount = 0; jCount < jArray.length(); jCount++){
                    if(jOSObject.getString("itemid").equals(jArray.getJSONObject(jCount).getString("itemid"))) {
                        jArray.getJSONObject(jCount).put("qty", jOSObject.getString("qty"));
                        jArray.getJSONObject(jCount).put("totalprice", jOSObject.getString("totalprice"));
                    }
                }
                */
            }
            tvTotalQty.setText(Integer.toString(intNewQty));
            tvTotalPrice.setText(Double.toString(dblNewPrice));

        } catch (Exception Ex) {
            Ex.printStackTrace();
        }
        /*
        for (int iCount = 0; iCount < tbClassLayout.getTabCount(); iCount++){

        }
        */
    }

    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    finish();
                }
            });
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbMain.this,"Error", Ex.toString());
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogProg = mBuilder.create();
            dialogProg.show();

            dialogProg.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(FnbMain.this,"Error", Ex.toString());
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            //initGetData(strOutput);
            int intjArrayLength = jArray.length();
            for(int i = 0; i < intjArrayLength; i++) {
                jArray.remove(0);
            }
            tvTotalPrice.setText("0");
            tvTotalQty.setText("0");
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mPager.setPadding(0,0,0,0);
            mPager.removeAllViews();
            tbClassLayout.removeAllTabs();
            subConSyncData();
        } catch (Exception Ex) {
            ShowErrorDialog(FnbMain.this,"Error", Ex.toString());
        }
    }

    @Override
    public void onBackPressed() {

    }
    //

    private void subConSyncData() {
        String  strWCode = "";
        try{
            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            FileIO obFile = new FileIO();
            strFileContent = obFile.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strWCode = strFileContent.split("\\|")[1];
            if(strIP.equals("")){
                Toast toast = Toast.makeText(getApplicationContext(), "URL empty, check settings.", Toast.LENGTH_LONG);
                return;
            }
            strGAction = "SYNCCONITEM";

            obSc = new SoapCall(FnbMain.this);
            obSc.delegate = this;
            obSc.execute("SYNCCONITEM", strIP, strWCode, "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(FnbMain.this,"Error","Something went wrong");
        }
    }
}


