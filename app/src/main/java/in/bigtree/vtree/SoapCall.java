package in.bigtree.vtree;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class SoapCall extends AsyncTask<String, String, String> {
    public AsyncResponse delegate = null;

    public  final String OPERATION_NAME = "subExecute";
    public  final String SOAP_ACTION = "http://tempuri.org/subExecute";
    public  final String WSDL_TARGET_NAMESPACE = "http://tempuri.org/";
    public  String SOAP_ADDRESS = "";
    private int TimeOut=180000;
    private ProgressDialog dialog;
    private ProgressBar progressBar;

    Context mContext;
    CustomDialog cDialog;
    public SoapCall(Context context)
    {
        try
        {
            this.mContext = context;
            //this.dialog = new ProgressDialog(context);
/*
            progressBar = new ProgressBar(context,null,android.R.attr.progressBarStyleLarge);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            layout.addView(progressBar,params);
            progressBar.setVisibility(View.VISIBLE);
            */

            // DO NOTHING HERE.
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        String strResp = "", strErr = "";
        Object response=null;
        try {
            String strCommand = strings[0], strURL = strings[1], Param1 = strings[2], Param2 = strings[3], Param3 = strings[4], Param4 =strings[5], Param5 = strings[6], Param6 = strings[7], Param7 = strings[8],
                    Param8 = strings[9], Param9 = strings[10], Param10 = strings[11], Param11 = strings[12], Param12 = strings[13], Param13 = strings[14], Param14 = strings[15], Param15 = strings[16];
            //strResp = Call(strings[0], strings[1], strings[2], strings[3], strings[4], strings[5], strings[6], strings[7], strings[8], strings[9], strings[10], strings[11], strings[12], strings[13], strings[14], strings[15]);
            SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);

            PropertyInfo objProperty = new PropertyInfo();
            objProperty.setName("strCommand");
            objProperty.setValue(strCommand);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty = new PropertyInfo();
            objProperty.setName("Param1");
            objProperty.setValue(Param1);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty = new PropertyInfo();
            objProperty.setName("Param2");
            objProperty.setValue(Param2);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param3");
            objProperty.setValue(Param3);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param4");
            objProperty.setValue(Param4);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param5");
            objProperty.setValue(Param5);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param6");
            objProperty.setValue(Param6);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param7");
            objProperty.setValue(Param7);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param8");
            objProperty.setValue(Param8);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param9");
            objProperty.setValue(Param9);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param10");
            objProperty.setValue(Param10);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param11");
            objProperty.setValue(Param11);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param12");
            objProperty.setValue(Param12);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param13");
            objProperty.setValue(Param13);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param14");
            objProperty.setValue(Param14);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param15");
            objProperty.setValue(Param15);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            FileIO obFileIO = new FileIO();
            String strFileContent = obFileIO.readFile(mContext.getResources().getString(R.string.connection_config_file),mContext);
            String strIP = strFileContent.split("\\|")[0];
            String protocol = strFileContent.split("\\|")[3] == "Y" ? "https" : "http";

            SOAP_ADDRESS = protocol + "://" + strIP + mContext.getResources().getString(R.string.webservice_name);

            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS,TimeOut);

            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();

        }
        catch (SocketTimeoutException STEx) {
            System.err.println("SocketTimeoutException: " + STEx.toString());
            strErr = STEx.toString();
        }
        catch (IOException IOEx) {
            //IOEx.printStackTrace();
            System.err.println("IOException: " + IOEx.toString());
            strErr = IOEx.toString();
        }
        catch (Exception Ex){
            //Ex.printStackTrace();
            System.err.println("Exception: " + Ex.toString());
            strErr = Ex.toString();
        }
        finally {
        }
        if(response != null) {
            return response.toString();
        }
        else {
            return "1|"+strErr;
        }
    }

    /*
    public String Call(String strCommand, String strURL, String Param1, String Param2, String Param3, String Param4, String Param5, String Param6, String Param7, String Param8,
                       String Param9, String Param10, String Param11, String Param12, String Param13, String Param14)
    {
        Object response=null;
        try
        {
            SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);

            PropertyInfo objProperty = new PropertyInfo();
            objProperty.setName("strCommand");
            objProperty.setValue(strCommand);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty = new PropertyInfo();
            objProperty.setName("Param1");
            objProperty.setValue(Param1);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty = new PropertyInfo();
            objProperty.setName("Param2");
            objProperty.setValue(Param2);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param3");
            objProperty.setValue(Param3);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param4");
            objProperty.setValue(Param4);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param5");
            objProperty.setValue(Param5);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param6");
            objProperty.setValue(Param6);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param7");
            objProperty.setValue(Param7);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param8");
            objProperty.setValue(Param8);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param9");
            objProperty.setValue(Param9);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param10");
            objProperty.setValue(Param10);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param11");
            objProperty.setValue(Param12);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param13");
            objProperty.setValue(Param13);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            objProperty=new PropertyInfo();
            objProperty.setName("Param14");
            objProperty.setValue(Param14);
            objProperty.setType(String.class);
            request.addProperty(objProperty);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            SOAP_ADDRESS = "http://" + strURL + "/BGSmartPoSAPI/wsTransWrapper.asmx";
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);

            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        }
        catch(Exception exception)
        {
            response = "1"+"|"+exception.toString();
        }
        return response.toString();
    }
*/
    @Override
    protected void onPostExecute(String result) {
        /*if(progDiag.isShowing()){
            progDiag.dismiss();
        }*/
        delegate.processFinish(result);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //cDialog = new CustomDialog();
        //progDiag = cDialog.GetProgressDialog(mContext,"","");
        //progDiag.show();
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }
}
