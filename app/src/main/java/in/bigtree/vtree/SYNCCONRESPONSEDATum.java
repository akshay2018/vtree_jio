package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SYNCCONRESPONSEDATum {

    @SerializedName("strClassCode")
    @Expose
    private String strClassCode;
    @SerializedName("strClassDesc")
    @Expose
    private String strClassDesc;
    @SerializedName("Items")
    @Expose
    private List<Item> items = null;

    public String getStrClassCode() {
        return strClassCode;
    }

    public void setStrClassCode(String strClassCode) {
        this.strClassCode = strClassCode;
    }

    public String getStrClassDesc() {
        return strClassDesc;
    }

    public void setStrClassDesc(String strClassDesc) {
        this.strClassDesc = strClassDesc;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}