package in.bigtree.vtree.tsc_bt;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.widget.Toast;

import com.example.tscdll.TSCActivity;

public class Printer {
    Context mContext;

    public Printer(Context ctx){
        this.mContext = ctx;
    }

    TSCActivity TscDll = new TSCActivity();
    public void Print(String BT_printer_mac, String printData){
        String event_name = "", event_date = "", event_time = "", qty = "", booking_id="", date_time="";
        try {
            event_name = (printData.split("\\|")[0]).toUpperCase();
            event_date = (printData.split("\\|")[1]).toUpperCase();
            event_time = printData.split("\\|")[2];
            date_time = event_date +"|"+ event_time;
            //date_time = date_time.replace(",","");
            qty = printData.split("\\|")[3];
            booking_id = printData.split("\\|")[4];
            TSCActivity TscDll = new TSCActivity();

            TscDll.openport(BT_printer_mac);
            //TscDll.openport("64:A2:F9:C6:B9:30");
            if (!TscDll.IsConnected) {
                Toast.makeText(mContext, "BT printer not connected", Toast.LENGTH_SHORT).show();
                return;
            }
            Toast.makeText(mContext, "Initiating BT Printing", Toast.LENGTH_SHORT).show();
            //String status = TscDll.printerstatus(300);

            //TscDll.sendcommand("SIZE 75 mm, 50 mm\r\n");
            //TscDll.sendcommand("GAP 2 mm, 0 mm\r\n");//Gap media
            //TscDll.sendcommand("BLINE 2 mm, 0 mm\r\n");//blackmark media
            TscDll.clearbuffer();
            //TscDll.sendcommand("SPEED 4\r\n");
            //TscDll.sendcommand("DENSITY 12\r\n");
            //TscDll.sendcommand("CODEPAGE UTF-8\r\n");
            TscDll.sendcommand("SET TEAR ON\r\n");
            TscDll.sendcommand("SET COUNTER @1 1\r\n");
            TscDll.sendcommand("@1 = \"0001\"\r\n");
            TscDll.sendcommand("TEXT 20,400,\"ROMAN.TTF\",0,18,18,\""+event_name+"\"\r\n");
            TscDll.sendcommand("TEXT 10,470,\"ROMAN.TTF\",0,12,12,\""+"A MAGICAL EXPERIENCE WITH"+"\"\r\n");
            TscDll.sendcommand("TEXT 0,530,\"ROMAN.TTF\",0,10,10,\""+"WATER, LIGHT, MIST, FIRE AND MUSIC"+"\"\r\n");
            TscDll.sendcommand("TEXT 0,590,\"ROMAN.TTF\",0,10,10,\""+date_time+"\"\r\n");
            TscDll.sendcommand("TEXT 70,650,\"ROMAN.TTF\",0,10,10,\"ADMISSION PASS|Valid for "+qty+" persons\"\r\n");
            TscDll.sendcommand("TEXT 80,710,\"ROMAN.TTF\",0,9,9,\""+"Gates open 30 minutes prior "+"\"\r\n");
            //TscDll.sendcommand("TEXT 0,740,\"ROMAN.TTF\",0,9,9,\""+"to another date or show time."+"\"\r\n");
            TscDll.sendcommand("TEXT 160,780,\"ROMAN.TTF\",0,12,12,\""+booking_id+"\"\r\n");
            ////TscDll.sendcommand("TEXT 20,500,\"ROMAN.TTF\",0,12,12,\"Valid for: "+qty+" persons\"\r\n");
            //TscDll.barcode(60, 860, "128", 100, 0, 0, 3, 3, booking_id);
            TscDll.qrcode(150, 840, "L", "8", "A", "0", "M2", "S3", booking_id);


            //TscDll.printerfont(100, 250, "2", 0, 1, 1, "Event Name: Hardcoded Event Name");
            //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

            TscDll.printlabel(1, 1);

            TscDll.closeport(5000);
        }
        catch (Exception Ex){
            Toast.makeText(mContext, "Error while BT Printing", Toast.LENGTH_SHORT).show();
        }
    }

    public void PrintFnBReceipt(String BT_printer_mac, String printData){
        try{
            TSCActivity TscDll = new TSCActivity();

            TscDll.openport(BT_printer_mac);
            if (!TscDll.IsConnected) {
                Toast.makeText(mContext, "BT printer not connected", Toast.LENGTH_SHORT).show();
                return;
            }
            Toast.makeText(mContext, "Initiating BT Printing", Toast.LENGTH_SHORT).show();
            TscDll.clearbuffer();
            TscDll.sendcommand("SET TEAR ON\r\n");
            TscDll.sendcommand("SET COUNTER @1 1\r\n");
            TscDll.sendcommand("@1 = \"0001\"\r\n");

            TscDll.sendcommand("TEXT 160,20,\"ROMAN.TTF\",0,10,10,\"TAX INVOICE\"\r\n");
            TscDll.sendcommand("TEXT 0,35,\"ROMAN.TTF\",0,10,10,\"--------------------------------------------------\"\r\n");
            TscDll.sendcommand("TEXT 80,50,\"ROMAN.TTF\",0,10,10,\"Reliance Industries Limited\"\r\n");
            TscDll.sendcommand("TEXT 0,80,\"ROMAN.TTF\",0,10,10,\"3rd Floor, Maker Chambers IV,\"\r\n");
            TscDll.sendcommand("TEXT 0,95,\"ROMAN.TTF\",0,10,10,\"222, Nariman Point,\"\r\n");
            TscDll.sendcommand("TEXT 0,110,\"ROMAN.TTF\",0,10,10,\"Mumbai 400 021\"\r\n");
            TscDll.sendcommand("TEXT 0,125,\"ROMAN.TTF\",0,10,10,\"Telephone No: +91 22 3555 500\"\r\n");
            TscDll.sendcommand("TEXT 0,140,\"ROMAN.TTF\",0,10,10,\"Fax No: 22 2204 2268; 22 2285 2214\"\r\n");
            TscDll.sendcommand("TEXT 0,155,\"ROMAN.TTF\",0,10,10,\"Email: investor.relations@ril.com\"\r\n");
            TscDll.sendcommand("TEXT 0,170,\"ROMAN.TTF\",0,10,10,\"Website: www.ril.com\"\r\n");
            //heading
            TscDll.sendcommand("TEXT 0,200,\"ROMAN.TTF\",0,10,10,\"--------------------------------------------------\"\r\n");
            TscDll.sendcommand("TEXT 0,210,\"ROMAN.TTF\",0,10,10,\"HSN\"\r\n");
            TscDll.sendcommand("TEXT 120,210,\"ROMAN.TTF\",0,10,10,\"HSN\"\r\n");
            TscDll.sendcommand("TEXT 360,210,\"ROMAN.TTF\",0,10,10,\"CGST%\"\r\n");
            TscDll.sendcommand("TEXT 480,210,\"ROMAN.TTF\",0,10,10,\"SGST%\"\r\n");
            //TscDll.sendcommand("TEXT 0,225,\"ROMAN.TTF\",0,10,10,\"\"\r\n");
            TscDll.sendcommand("TEXT 120,225,\"ROMAN.TTF\",0,10,10,\"QTY\"\r\n");
            TscDll.sendcommand("TEXT 240,225,\"ROMAN.TTF\",0,10,10,\"Net\"\r\n");
            TscDll.sendcommand("TEXT 360,225,\"ROMAN.TTF\",0,10,10,\"CGST\"\r\n");
            TscDll.sendcommand("TEXT 480,225,\"ROMAN.TTF\",0,10,10,\"SGST\"\r\n");
            TscDll.sendcommand("TEXT 600,225,\"ROMAN.TTF\",0,10,10,\"TOTAL\"\r\n");
            TscDll.sendcommand("TEXT 0,235,\"ROMAN.TTF\",0,10,10,\"--------------------------------------------------\"\r\n");
            //loop

        }
        catch (Exception Ex){

        }
    }

}
