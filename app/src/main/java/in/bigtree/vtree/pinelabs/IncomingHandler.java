package in.bigtree.vtree.pinelabs;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import in.bigtree.vtree.data.AppSharedPrefrences;

public class IncomingHandler extends Handler {
    @Override
    public void handleMessage(Message msg) {
        Bundle bundle = msg.getData();
        String value = bundle.getString(AppSharedPrefrences.getBillingResponseTag());
        Log.d("",value);
// process the response Json as required.
    }
}
