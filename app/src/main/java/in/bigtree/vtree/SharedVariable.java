package in.bigtree.vtree;

import android.content.Context;
import android.graphics.Typeface;
import android.support.multidex.MultiDexApplication;


public class SharedVariable extends MultiDexApplication {
    private String strUID;
    private String strCinemaName;
    private String strLocation;
    private boolean isPrintingChecked;
    private String strPrefMode;
    private String strSLayout;
    private String strBranchCode;
    private boolean isCounterPickEnabled;
    private boolean IbPrinting;
    private boolean blnCCIntegrated;

    //-----------mswipe shared values------------
    public final static boolean IS_DEBUGGING_ON = true;
    public final static String packName = "in.bigtree.vtree";
    public static final String SERVER_NAME = "Mswipe";

    /*The application global attributes */
    public static final int PhoneNoLength = 10;
    public static final String Currency_Code = "INR.";
    public static final String smsCode = "+91";
    public static final String mTipRequired = "false";

    public Typeface font = null;
    public Typeface fontBold = null;
    public Typeface fontMedium = null;
    public Typeface fontLight = null;
    public Typeface fontLightItalic = null;

    public static String mCurrency = "inr";

    public static final int mCashAtPosAmountLimit = 2000;



    /*The application context statically available to the entire application */
    private static Context mContext = null;

    private static SharedVariable mSharedVariable = null;


    public static final int font_size_amount_small = 40 ;
    public static final int font_size_amount_normal = 60;

    public static final int REQUEST_THRESHOLD_TIME = 2000;
    //-----------End mswipe shared values------------

    public String getUID(){
        return strUID;
    }
    public String getCID(){
        return strCinemaName;
    }
    public String getLOC(){
        return strLocation;
    }
    public boolean getPrintChecked(){
        return isPrintingChecked;
    }
    public String getMode() { return strPrefMode; }
    public String getSeatLayout() { return strSLayout; }
    public boolean getCounterPickupStat() { return isCounterPickEnabled; }
    public boolean getIbPrinting(){return IbPrinting; }
    public String getBranchCode(){
        return strBranchCode;
    }
    public boolean getCCIntegratedStat(){
        return blnCCIntegrated;
    }


    public void setUID(String u){
        strUID = u;
    }
    public void setCID(String c){
        strCinemaName = c;
    }
    public void setLOC(String l){
        strLocation = l;
    }
    public void setPrintChecked(boolean c){ isPrintingChecked = c; }
    public void setMode(String m) { strPrefMode = m; }
    public void setSeatLayout(String s) { strSLayout = s; }
    public void setCounterPickupStat(boolean s) { isCounterPickEnabled = s; }
    public void setIbPrinting(boolean Ip) { IbPrinting= Ip; }
    public void setBranchCode(String cb){
        strBranchCode = cb;
    }
    public void setCCIntegratedStat(boolean ci){
        blnCCIntegrated = ci;
    }

    //-----------------------mswipe required code

    private static SharedVariable mApplicationData = null;


    @Override
    public void onCreate()
    {
        // TODO Auto-generated method stub
        super.onCreate();
        mContext = getApplicationContext();

        font = Typeface.createFromAsset(mContext.getAssets(),"fonts/english/roboto/Roboto-Regular.ttf");
        fontBold = Typeface.createFromAsset(mContext.getAssets(),"fonts/english/roboto/Roboto-Bold.ttf");
        fontMedium = Typeface.createFromAsset(mContext.getAssets(),"fonts/english/roboto/Roboto-Medium.ttf");
        fontLight = Typeface.createFromAsset(mContext.getAssets(),"fonts/english/roboto/Roboto-Light.ttf");
        fontLightItalic = Typeface.createFromAsset(mContext.getAssets(),"fonts/english/roboto/Roboto-LightItalic.ttf");

    }

    /**
     * The  context of the application that can be accesses
     * @param
     *
     * @return
     * the application context
     */
    public static Context getApplicationContex()
    {
        return mContext;

    }

    /**
     * single instance.
     * @return
     */
    public static SharedVariable getApplicationDataSharedInstance()
    {
        if(mApplicationData == null)
        {
            mApplicationData = new SharedVariable();
        }
        return mApplicationData;
    }
}
