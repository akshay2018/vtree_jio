package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import in.bigtree.vtree.tsc_bt.Printer;

public class FnbConfirmation extends AppCompatActivity {
    Bundle bundle;
    LinearLayout llPaidItems;
    TextView tvPrice;
    String strJsonArray, strItemName = "", strItemQty = "", strItemPrice = "",strPrintChk="", strPrintData = "", strCinemaName = "", strWCode = "", strTransNo="", strPrinterName = "", strBookingNo = "", strReceiptNo="";
    JSONArray jArray;
    JSONObject jObject;
    Context ctx;
    SharedPreferences obPreferences;
    SharedPreferences.Editor editor;
    AlertDialog dialogProg;
    LinearLayout llBack;
    Button btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fnb_confirmation);
        try {
            ctx = this;
            bundle = getIntent().getExtras();
            llBack = (LinearLayout) findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToHome(v));
            btnBack = (Button)findViewById(R.id.btnBack);
            btnBack.setOnClickListener((v) -> backToHome(v));
            strJsonArray = getIntent().getStringExtra("jsonArray");
            obPreferences = getSharedPreferences(getResources().getString(R.string.appPrefs), Context.MODE_PRIVATE);
            llPaidItems = (LinearLayout)findViewById(R.id.llPaidItems);
            tvPrice = (TextView)findViewById(R.id.tvFnbPaymentTotal);
            tvPrice.setText(bundle.getString("price"));
            strPrintData = bundle.getString("printdata");
            strWCode = bundle.getString("wcode");
            strTransNo = bundle.getString("transno");
            strBookingNo = bundle.getString("bookingno");
            strReceiptNo = bundle.getString("receiptno");
            jArray = new JSONArray(strJsonArray);
            for(int iCount = 0; iCount < jArray.length(); iCount++) {
                jObject = (JSONObject) jArray.get(iCount);
                strItemName = jObject.get("itemname").toString();
                strItemQty = jObject.get("qty").toString();
                strItemPrice = jObject.get("totalprice").toString();

                LinearLayout llPaidItemRec = new LinearLayout(this);

                int intTopMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
                int intStartMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
                int intEndMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
                int intBottomPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
                LinearLayout.LayoutParams llPaidItemsRecParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                llPaidItemsRecParams.setMargins(intStartMargin, intTopMargin,intEndMargin,0);

                llPaidItemRec.setLayoutParams(llPaidItemsRecParams);
                llPaidItemRec.setPadding(0,0,0,intBottomPadding);
                llPaidItemRec.setBackground(ContextCompat.getDrawable(ctx,R.drawable.border_bottom));

                LinearLayout llItemName = new LinearLayout(this);
                llItemName.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,2.0f));

                TextView tvItemName = new TextView(this);
                int inttvItemMarginStart = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
                int intTvSize15 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
                LinearLayout.LayoutParams llTvItemParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                llTvItemParams.setMargins(inttvItemMarginStart,0,0,0);
                tvItemName.setLayoutParams(llTvItemParams);
                tvItemName.setText(strItemName);
                tvItemName.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvItemName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
                Typeface typefaceps = ResourcesCompat.getFont(ctx, R.font.panton_semibold);
                tvItemName.setTypeface(typefaceps);

                llItemName.addView(tvItemName);

                LinearLayout llItemQty = new LinearLayout(this);
                llItemQty.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));
                llItemQty.setGravity(Gravity.CENTER);

                TextView tvItemQty = new TextView(this);
                tvItemQty.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvItemQty.setText(strItemQty);
                tvItemQty.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvItemQty.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
                tvItemQty.setTypeface(typefaceps);

                llItemQty.addView(tvItemQty);

                LinearLayout llItemPriceParent = new LinearLayout(this);
                int intPriceEndMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
                LinearLayout.LayoutParams llPriceParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
                llPriceParams.setMargins(0,0,intPriceEndMargin,0);
                llItemPriceParent.setLayoutParams(llPriceParams);
                llItemPriceParent.setGravity(Gravity.END);

                LinearLayout llItemPrice = new LinearLayout(this);
                llItemPrice.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                TextView tvRupee = new TextView(this);
                tvRupee.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvRupee.setText(getResources().getString(R.string.Rs));
                tvRupee.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvRupee.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
                Typeface typefacerm = ResourcesCompat.getFont(ctx, R.font.roboto_medium);
                tvRupee.setTypeface(typefacerm);

                TextView tvPrice = new TextView(this);
                int intTvSize16 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                tvPrice.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvPrice.setText(strItemPrice);
                tvPrice.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
                tvPrice.setTypeface(typefaceps);

                llItemPrice.addView(tvRupee);
                llItemPrice.addView(tvPrice);
                llItemPriceParent.addView(llItemPrice);

                llPaidItemRec.addView(llItemName);
                llPaidItemRec.addView(llItemQty);
                llPaidItemRec.addView(llItemPriceParent);

                llPaidItems.addView(llPaidItemRec);
            }


            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            if(obShared.getIbPrinting() == true){
                SharedVariable obShare = ((SharedVariable)getApplicationContext());
                strCinemaName = obShare.getCID();
                try {
                    //Print.StartPrinting();
                    MSwipePrinter obMSwipePrinter = new MSwipePrinter(getApplicationContext());
                    obMSwipePrinter.printVoucher(strPrintData, strCinemaName, strWCode, strTransNo, strBookingNo, strReceiptNo);
                }
                catch (RuntimeException iEx) {
                    Log.d("Inbuilt printer",iEx.toString());
                }
            }
            if(obShared.getPrintChecked() == true){
                ShowProgressDialog(ctx,"Printing","This should take less than a minute");
                strPrinterName = obPreferences.getString("printer","");
                //Printer printer = new Printer(ctx);
                //printer.Print();
                /*
                PrintFnB obPrint = new PrintFnB();
                strPrintChk = obPrint.checkDevice(FnbConfirmation.this,strPrinterName);
                if(strPrintChk.equals("")){
                    Toast.makeText(getApplicationContext(),"No printer found",Toast.LENGTH_SHORT).show();
                }
                else if(strPrintChk.split("\\|")[0].equals("0")){
                    SharedVariable obShare = ((SharedVariable)getApplicationContext());
                    strCinemaName = obShare.getCID();
                    obPrint.openDevice(strPrintData,strCinemaName,strWCode,strTransNo,strPrinterName);
                }
                */

                dialogProg.dismiss();
            }
        }
        catch (Exception Ex) {
            CustomDialog obDiag = new CustomDialog();
            obDiag.ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogProg = mBuilder.create();
            dialogProg.show();

            dialogProg.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error",Ex.toString());
        }
    }
    public void backToHome(View view) {
        try{
            Intent intent = new Intent(FnbConfirmation.this, FnbMain.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        }
        catch (Exception Ex){
            Ex.toString();
        }
    }
}
