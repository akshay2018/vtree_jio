package in.bigtree.vtree.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper
{
	public final static String log_tab = "DataBaseHelper=>";

	Context context = null;
	private static DataBaseHelper mDBConnection;
	public static String Lock = "dblock";
	public static final int db_version = 1;

	private DataBaseHelper(Context context)
	{
		super(context, "mswipesdkdemo.db", null, db_version);
		this.context = context;

	}

	public static synchronized DataBaseHelper getDBAdapterInstance(Context context)
	{
		
		synchronized(Lock)
		{
			if (mDBConnection == null) {
				mDBConnection = new DataBaseHelper(context);
	    }
	    return mDBConnection;
		}

	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{

		db.execSQL("create table cardsaleofflinedata(trx_id integer primary key autoincrement, " +
				"userid varchar(20), status varchar(250), status_message varchar(250), " +
				"trx_data varchar(5000), flight_no varchar(250), crew_no varchar(250), seat_no varchar(250), depart_from varchar(250), " +
				"arriving_to varchar(250), total_amount varchar(250),card_type varchar(250), trx_date varchar(250), " +
				"card_holder_name varchar(250), last_four_digits varchar(250), phone_no varchar(250), " +
				"tlv_data varchar(250), ksn varchar(250), trx_message varchar(250));");

		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{

	}
}