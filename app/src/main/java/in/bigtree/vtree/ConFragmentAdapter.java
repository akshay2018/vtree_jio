package in.bigtree.vtree;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.io.Serializable;
import java.util.List;

public class ConFragmentAdapter extends FragmentStatePagerAdapter {
    public int noOfTabs;
    private List<ItemDetail> lstClass;
    public String strCinemaNames, strFilmName, strOSModifiedData;

    public ConFragmentAdapter(FragmentManager fm, int noOfTabs, List<ItemDetail> lstClass, String strJson, String strOSModifiedData) {
        super(fm);
        this.noOfTabs = noOfTabs;
        this.lstClass = lstClass;
        this.strOSModifiedData = strOSModifiedData;
    }

    @Override
    public Fragment getItem(int position) {
        ItemFragment fragment = new ItemFragment();
        Bundle bundle;
        for (int i = 0; i < noOfTabs ; i++) {
            if (i == position) {
                bundle = new Bundle();
                bundle.putSerializable("lstitems",(Serializable) lstClass.get(i).getItems());
                bundle.putString("classcode",lstClass.get(i).getStrClassCode());
                bundle.putSerializable("classname",lstClass.get(i).getStrClassDesc());
                bundle.putString("ordersummarydata",strOSModifiedData);
                fragment.setArguments(bundle);
                break;
            }
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return noOfTabs;
    }
}
