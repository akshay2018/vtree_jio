package in.bigtree.vtree;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.io.Serializable;
import java.util.List;

public class FragmentAdapter extends FragmentStatePagerAdapter {
    public int noOfTabs;
    private List<ShowDate> arrItems;
    private String strJson = "";


    public FragmentAdapter(FragmentManager fragmentManager, int noOfTabs, List<ShowDate> arrItems, String strJson) {
        super(fragmentManager);
        this.noOfTabs = noOfTabs;
        this.arrItems = arrItems;
        this.strJson = strJson;
    }

    @Override
    public Fragment getItem(int position) {
        MovieFragment fragment = new MovieFragment();
        Bundle bundle = new Bundle();

        for (int i = 0; i < noOfTabs ; i++) {
            if (i == position) {
                bundle = new Bundle();
                // PASS THE LIST IN THE BUNDLE
                bundle.putSerializable("FilmList", (Serializable) arrItems.get(i).getFilms());
                bundle.putString("sDate", arrItems.get(i).getSessionDate());
                bundle.putString("strjson",strJson);
                fragment.setArguments(bundle);
                break;
            }
        }
        return fragment;
    }


    @Override
    public int getCount() {
        return noOfTabs;
    }
}
