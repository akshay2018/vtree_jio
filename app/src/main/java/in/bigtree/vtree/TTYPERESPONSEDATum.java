package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TTYPERESPONSEDATum {

    @SerializedName("AreaQty")
    @Expose
    private List<AreaQty> areaQty = null;

    public List<AreaQty> getAreaQty() {
        return areaQty;
    }

    public void setAreaQty(List<AreaQty> areaQty) {
        this.areaQty = areaQty;
    }
}