package in.bigtree.vtree;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class About extends AppCompatActivity {
    String versionName = "";
    TextView tvVersion;
    LinearLayout llBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        try {
            llBack = (LinearLayout) findViewById(R.id.llIvBack);
            llBack.setOnClickListener((v) -> backFromAbout(v));
            tvVersion = (TextView) findViewById(R.id.tvVersion);
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            tvVersion.setText(versionName);
        }
        catch (Exception Ex) {

        }
    }

    public void backFromAbout(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {

    }
}
