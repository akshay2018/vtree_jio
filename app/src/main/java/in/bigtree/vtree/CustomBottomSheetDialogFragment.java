package in.bigtree.vtree;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomBottomSheetDialogFragment extends BottomSheetDialogFragment implements AsyncResponse{
    private BottomSheetListener mListener;
    Spinner sprFilm, sprShowTime;
    TextInputEditText tvSeats;
    Context ctx;
    String strFileContent = "",strIP="",strGAction="";
    JSessions obJsonSession;
    List<String> lstSession;
    Button btnProceed;
    RadioButton rdI,rdB;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottomsheet_onseat_details, container, false);
        sprFilm = (Spinner)v.findViewById(R.id.spr_films);
        sprShowTime = (Spinner)v.findViewById(R.id.spr_showtime);
        tvSeats = (TextInputEditText)v.findViewById(R.id.tvSeat);
        btnProceed = (Button)v.findViewById(R.id.btnProceed);
        rdI = (RadioButton)v.findViewById(R.id.rdI);
        rdB = (RadioButton)v.findViewById(R.id.rdB);
        btnProceed.setBackgroundColor(ContextCompat.getColor(ctx,R.color.colorDisabledButton));
        btnProceed.setClickable(false);

        tvSeats.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

        tvSeats.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.length() <= 1) {
                        tvSeats.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                    }
                    if(s.length() > 1){
                        KeyListener keyListener = DigitsKeyListener.getInstance("1234567890");
                        tvSeats.setKeyListener(keyListener);
                    }
                } catch (Exception Ex) {
                    Ex.toString();
                }
            }
        });

        sprFilm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String strSelectedFilm = (String) parent.getItemAtPosition(position);
                updateShowTime(strSelectedFilm);
            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sprShowTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //mListener.clickers(sprFilm);
        tvSeats.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if(s.length() > 0){
                        //tvSeats.setText(tvSeats.getText().toString().toUpperCase());
                        btnProceed.setBackgroundColor(ContextCompat.getColor(ctx,R.color.colorPrimary));
                        btnProceed.setClickable(true);
                    }
                    else {
                        btnProceed.setBackgroundColor(ContextCompat.getColor(ctx,R.color.colorDisabledButton));
                        btnProceed.setClickable(false);
                    }
                } catch (Exception Ex) {
                    Ex.toString();
                }
            }
        });

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String strDeliveryAt = "", strSessionId = "",strSeatDetails="",strTransDetails="", strRowId = "", strSeatId = "";
                try{
                    strSessionId = lstSession.get(sprShowTime.getSelectedItemPosition());
                    strSeatDetails = tvSeats.getText().toString();
                    strSeatDetails = strSeatDetails.replaceAll("\\s", "");
                    strSeatDetails = strSeatDetails.toUpperCase();

                    Pattern p = Pattern.compile("\\d+");
                    Matcher m = p.matcher(strSeatDetails);
                    while(m.find()) {
                        strSeatId = m.group();
                    }
                    strRowId = strSeatDetails.replace(strSeatId,"");

                    strTransDetails = "|"+"BOOKINGID="+"|"+"SESSIONID="+strSessionId+"|"+"ROWID="+strRowId+"|"+"SEATID="+strSeatId+"|";
                    if(rdB.isChecked()){
                        strDeliveryAt = "B";
                    }
                    else if(rdI.isChecked()){
                        strDeliveryAt = "I";
                    }
                    mListener.SetOnSeat(strSessionId,strSeatDetails,strDeliveryAt,strTransDetails);
                }
                catch (Exception Ex){
                    Log.d("","");
                }
            }
        });

        fetchData();
        return v;
    }

    private void updateShowTime(String strSelectedFilm) {
        int intFilmCount = 0, intShowDateCount=0;
        String strFilmName = "", strShowTime = "";
        try{
            lstSession = new ArrayList<String>();
            List<String> list = new ArrayList<String>();
            intFilmCount = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().size();
            for(int iCount = 0; iCount < intFilmCount; iCount++){
                strFilmName = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().get(iCount).getFilmName();
                if(strFilmName.equals(strSelectedFilm)){
                    intShowDateCount = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().get(iCount).getShowTimes().size();
                    for(int iSCount = 0; iSCount < intShowDateCount; iSCount++) {
                        strShowTime = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().get(iCount).getShowTimes().get(iSCount).getShowDetails();
                        list.add(strShowTime.split("\\|")[1]);
                        lstSession.add(strShowTime.split("\\|")[0]);
                    }
                }
            }

            ArrayAdapter<String> arrSpnTimeAdapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, list);
            sprShowTime.setAdapter(arrSpnTimeAdapter);
        }
        catch (Exception Ex){
            //ShowErrorDialog(FnbPickupType.this,"Error",Ex.toString());
            Log.d("","");
        }
    }

    public interface BottomSheetListener{
        void SetOnSeat(String strSessionId, String strSeatDetails, String strDeliveryAt, String strTransDetails);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mListener = (BottomSheetListener) context;
            ctx = context;
        }
        catch (ClassCastException CEx){
            Log.d("","");
        }
    }

    //fetch shows for onseat concessions
    private void fetchData() {
        SoapCall obSc;
        try{
            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),ctx);
            strIP = strFileContent.split("\\|")[0];

            strGAction = "SYNCONSEATSESSIONS";
            obSc = new SoapCall(ctx);
            obSc.delegate = this;
            obSc.execute("SYNCONSEATSESSIONS", strIP, "", "", "","","","","","","","","","","","","");

        }
        catch (Exception Ex){
            //ShowErrorDialog(FnbOrderSummary.this,"Error",Ex.toString());
            Log.d("","");
        }
    }


    @Override
    public void processFinish(String output) {
        try {
            //dialogProg.dismiss();
            if (!output.split("\\|")[0].equals("1")) {
                if (strGAction.equals("SYNCONSEATSESSIONS")) {
                    processShowFilms(output);
                }
            } else {
                //ShowErrorDialog(FnbOrderSummary.this,"Error",output.split("\\|")[1]);
                Log.d("","");
            }
        }
        catch (Exception Ex){
            //ShowErrorDialog(FnbOrderSummary.this,"Error",Ex.toString());
            Log.d("","");
        }
    }

    private void processShowFilms(String output) {
        String strRespCode = "", strRespData="", strFilmName = "", strShowTime="",strShowDate = "";
        int intFilmCount = 0;
        try{
            List<String> list = new ArrayList<String>();
            obJsonSession = new Gson().fromJson(output, JSessions.class);

            strRespCode = obJsonSession.getSYNCRESPONSECODE();
            if(obJsonSession.getSYNCRESPONSECODE().equals("0")){
                intFilmCount = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().size();
                for(int iCount = 0; iCount < intFilmCount; iCount++){
                    list.add(obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().get(iCount).getFilmName());
                }
                ArrayAdapter<String> arrSpnFilmAdapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, list);
                sprFilm.setAdapter(arrSpnFilmAdapter);
            }
            else{
                //ShowErrorDialog(FnbOrderSummary.this,"Error",output);
                Log.d("","");
            }
        }
        catch (Exception Ex){
            Log.d("","");
            //ShowErrorDialog(FnbOrderSummary.this,"Error",Ex.toString());
        }
    }

    public void proceedOnSeatPickup(View v){
        String strDeliveryAt = "", strSessionId = "",strSeatDetails="",strTransDetails="";
        try{
            strSessionId = lstSession.get(sprShowTime.getSelectedItemPosition());
            strSeatDetails = tvSeats.getText().toString();
            strTransDetails = "|"+"BOOKINGID="+"|"+"SESSIONID="+strSessionId+"|"+"ROWID="+strSeatDetails.split("\\-")[0]+"|"+"SEATID="+strSeatDetails.split("\\-")[1]+"|";
            if(rdB.isChecked()){
                strDeliveryAt = "B";
            }
            else if(rdI.isChecked()){
                strDeliveryAt = "I";
            }
            mListener.SetOnSeat(strSessionId,strSeatDetails,strDeliveryAt,strTransDetails);
        }
        catch (Exception Ex){
            Log.d("","");
        }
    }
}