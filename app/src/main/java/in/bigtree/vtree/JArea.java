package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JArea {

    @SerializedName("TTYPE_RESPONSE_CODE")
    @Expose
    private String tTYPERESPONSECODE;
    @SerializedName("TTYPE_RESPONSE_DATA")
    @Expose
    private List<TTYPERESPONSEDATum> tTYPERESPONSEDATA = null;

    public String getTTYPERESPONSECODE() {
        return tTYPERESPONSECODE;
    }

    public void setTTYPERESPONSECODE(String tTYPERESPONSECODE) {
        this.tTYPERESPONSECODE = tTYPERESPONSECODE;
    }

    public List<TTYPERESPONSEDATum> getTTYPERESPONSEDATA() {
        return tTYPERESPONSEDATA;
    }

    public void setTTYPERESPONSEDATA(List<TTYPERESPONSEDATum> tTYPERESPONSEDATA) {
        this.tTYPERESPONSEDATA = tTYPERESPONSEDATA;
    }

}