package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Area {

    @SerializedName("strAreaDesc")
    @Expose
    private String strAreaDesc;
    @SerializedName("strAreaNum")
    @Expose
    private String strAreaNum;
    @SerializedName("strAreaCode")
    @Expose
    private String strAreaCode;
    @SerializedName("strHasCurrentOrder")
    @Expose
    private String strHasCurrentOrder;
    @SerializedName("strGroupSeatsData")
    @Expose
    private String strGroupSeatsData;
    @SerializedName("strTicketType")
    @Expose
    private String strTicketType;
    @SerializedName("strPackageTicket")
    @Expose
    private String strPackageTicket;
    @SerializedName("strChildTicket")
    @Expose
    private String strChildTicket;
    @SerializedName("rows")
    @Expose
    private List<Row> rows = null;

    public String getStrAreaDesc() {
        return strAreaDesc;
    }

    public void setStrAreaDesc(String strAreaDesc) {
        this.strAreaDesc = strAreaDesc;
    }

    public String getStrAreaNum() {
        return strAreaNum;
    }

    public void setStrAreaNum(String strAreaNum) {
        this.strAreaNum = strAreaNum;
    }

    public String getStrAreaCode() {
        return strAreaCode;
    }

    public void setStrAreaCode(String strAreaCode) {
        this.strAreaCode = strAreaCode;
    }

    public String getStrHasCurrentOrder() {
        return strHasCurrentOrder;
    }

    public void setStrHasCurrentOrder(String strHasCurrentOrder) {
        this.strHasCurrentOrder = strHasCurrentOrder;
    }

    public String getStrGroupSeatsData() {
        return strGroupSeatsData;
    }

    public void setStrGroupSeatsData(String strGroupSeatsData) {
        this.strGroupSeatsData = strGroupSeatsData;
    }

    public String getStrTicketType() {
        return strTicketType;
    }

    public void setStrTicketType(String strTicketType) {
        this.strTicketType = strTicketType;
    }

    public String getStrPackageTicket() {
        return strPackageTicket;
    }

    public void setStrPackageTicket(String strPackageTicket) {
        this.strPackageTicket = strPackageTicket;
    }

    public String getStrChildTicket() {
        return strChildTicket;
    }

    public void setStrChildTicket(String strChildTicket) {
        this.strChildTicket = strChildTicket;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

}