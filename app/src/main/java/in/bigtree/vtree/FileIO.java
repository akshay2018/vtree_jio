package in.bigtree.vtree;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

public class FileIO {
    public void createFile(String strFileName, String strFileContent, Context applicationContext){
        try{
            FileOutputStream outputStream;
            outputStream = applicationContext.openFileOutput(strFileName, Context.MODE_APPEND);
            outputStream.write(strFileContent.getBytes());
            outputStream.close();
        }
        catch (Exception Ex){
            Ex.printStackTrace();
        }
    }

    public String readFile(String strFileName, Context applicationContext){
        String strLine = "";
        try{
            File directory = applicationContext.getFilesDir();
            File file = new File(directory, strFileName);
            FileInputStream fileInputStream = new FileInputStream (file);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ( (strLine = bufferedReader.readLine()) != null )
            {
                stringBuilder.append(strLine);
            }
            fileInputStream.close();
            strLine = stringBuilder.toString();

            bufferedReader.close();
        }
        catch (Exception Ex){
            Log.e(FileIO.class.getSimpleName(),Ex.toString());
        }
        return strLine;
    }

    public void writeFile(String strFileName, String strFileContent, Context applicationContext){
        FileWriter fw = null;
        try{
            FileOutputStream outputStream;
            outputStream = applicationContext.openFileOutput(strFileName, Context.MODE_PRIVATE);
            outputStream.write(strFileContent.getBytes());
            outputStream.close();
        }
        catch (Exception Ex){
            Log.e(FileIO.class.getSimpleName(),Ex.toString());
        }
    }

    public void deleteFile(String strFileName, Context applicationContext){
        try{
            File directory = applicationContext.getFilesDir();
            File file = new File(directory, strFileName);
            file.delete();
        }
        catch (Exception Ex){
            Log.e(FileIO.class.getSimpleName(),Ex.toString());
        }
    }

    public boolean checkURL(String strURL){

        boolean blnResp = false;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL url = new URL("http://"+strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(8000);
            connection.connect();
            int code = connection.getResponseCode();
            if(code == 200) {
                blnResp = true;
            } else {
                blnResp = false;
            }
        }
        catch (Exception Ex){
            Ex.printStackTrace();
            Log.e(FileIO.class.getSimpleName(),Ex.toString());
            blnResp = false;
        }
        return blnResp;
    }

    public String readLocalJson(Context ctx, String strFileName){
        String jsonString = ""; InputStream is = null;
        try {
            if(strFileName.equals("sessions")) {
                is = ctx.getResources().openRawResource(R.raw.sessions);
            }
            if(strFileName.equals("seatlayout")) {
                is = ctx.getResources().openRawResource(R.raw.seatlayout);
            }
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            jsonString = writer.toString();

        }
        catch (Exception Ex){
            Ex.printStackTrace();
        }
        return  jsonString;
    }

}
