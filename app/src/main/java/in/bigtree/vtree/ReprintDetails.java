package in.bigtree.vtree;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ReprintDetails extends AppCompatActivity implements AsyncResponse{

    String strBookingNo = "", strBookingId = "", strFilmName = "", strQty = "", strDateTime = "", strScreen = "", strCat = "", strSeats = "", strPrice = "", strAction = "", strCinemaName = "";
    TextView tvbid, tvfname, tvqty, tvdtime, tvscreen, tvcat, tvseats, tvprice,tvseatsqty,tvCinema;
    AlertDialog dialogErr;
    LinearLayout llBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reprint_details);

        try{
            llBack = (LinearLayout)findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToLastTrans(v));
            Bundle rtBundle = getIntent().getExtras();
            strBookingNo = rtBundle.getString("bookingno");
            strBookingId = rtBundle.getString("bookingid");
            strFilmName = rtBundle.getString("filmname");
            strQty = rtBundle.getString("qty");
            strDateTime = rtBundle.getString("datetime");
            strScreen = rtBundle.getString("screen");
            strCat = rtBundle.getString("cat");
            strSeats = rtBundle.getString("seats");
            strPrice = rtBundle.getString("price");
            strCinemaName = rtBundle.getString("cinema");

            tvbid = (TextView)findViewById(R.id.tvBID);
            tvfname = (TextView)findViewById(R.id.tvFilmName);
            tvqty = (TextView)findViewById(R.id.tvTkQty);
            tvdtime = (TextView)findViewById(R.id.tvDateTime);
            tvscreen = (TextView)findViewById(R.id.tvScreenName);
            tvcat = (TextView)findViewById(R.id.tvCat);
            tvseats = (TextView)findViewById(R.id.tvSeats);
            tvprice = (TextView)findViewById(R.id.tvTotal);
            tvseatsqty = (TextView)findViewById(R.id.tvSeatQty);
            tvCinema = (TextView)findViewById(R.id.tvBigCinemaName);

            tvbid.setText(strBookingId);
            tvfname.setText(strFilmName);
            tvqty.setText(strQty + " Tickets");
            tvdtime.setText(strDateTime);
            tvscreen.setText(strScreen);
            tvcat.setText(strCat);
            tvseats.setText(strSeats);
            tvprice.setText(strPrice);
            tvseatsqty.setText("Seats(" + strQty + ")");
            tvCinema.setText(strCinemaName);
        }
        catch(Exception Ex){
            ShowErrorDialog(ReprintDetails.this,"Error","Something went wrong");
        }
    }

    public void proceedReprint(View view) {
        try{
            String strFileContent = "",strIP="";
            FileIO obFile = new FileIO();
            strFileContent = obFile.readFile(getResources().getString(R.string.connection_config_file),ReprintDetails.this);
            strIP = strFileContent.split("\\|")[0];
            strAction = "REPRINT";
            SoapCall obSc = new SoapCall(ReprintDetails.this);
            obSc.delegate = this;
            ShowProgressDialog(ReprintDetails.this,"Resending","");
            obSc.execute("REPRINT", strIP, strBookingNo, "", "","","","","","","","","","","","","");
        }
        catch (Exception Ex){
            ShowErrorDialog(ReprintDetails.this,"Error","Something went wrong");
        }
    }

    public void backToLastTrans(View view) {
        try{
            finish();
        }
        catch (Exception Ex){
            ShowErrorDialog(ReprintDetails.this,"Error","Something went wrong");
        }
    }

    @Override
    public void processFinish(String output) {
        try{
            dialogErr.dismiss();
            if(!output.split("\\|")[0].equals("1")){
                finish();
            }
            else {
                CustomDialog cDialog = new CustomDialog();
                cDialog.ShowErrorDialog(ReprintDetails.this,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(ReprintDetails.this,"Error","Something went wrong");
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            Ex.toString();
        }
    }

    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    finish();

                }
            });
        }
        catch (Exception Ex){
            Toast.makeText(ReprintDetails.this,Ex.toString(), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onBackPressed() {

    }
}
