package in.bigtree.vtree;

public interface AsyncResponse {
    void processFinish(String output);
}
