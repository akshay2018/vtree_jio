package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FnbPickupType extends AppCompatActivity implements AsyncResponse {
    Context context;
    ImageView ivSeat,ivCounter;
    TextView tvOnSeat, tvCounter, tvPrice, tvSeat;
    String strSelectedMode = "", strTotalAmt = "", strTempTransId = "", strTotalQty = "", strGAction = "", strIP = "", strFileContent = "", strGSessionId = "", strBookingNo = "";
    List<String> lstSession;
    LinearLayout llSeat, llCounter, llSeatDeatils, llNextToPickupEx;
    JSONArray jArray;
    JSONObject jObject;
    AlertDialog dialogProg;
    Spinner spnFilms, spnShowDate;
    Bundle bundle;
    JSessions obJsonSession;
    RadioButton rdB, rdI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fnb_pickup_type);
        try {
            bundle = getIntent().getExtras();
            strTotalAmt = bundle.getString("totalamt");
            strTempTransId = bundle.getString("temptransid");
            strTotalQty = bundle.getString("totalqty");
            strBookingNo = bundle.getString("bookingno");
            ivSeat = (ImageView) findViewById(R.id.ivSeat);
            tvOnSeat = (TextView) findViewById(R.id.tvOnSeat);
            ivCounter = (ImageView) findViewById(R.id.ivCounter);
            tvCounter = (TextView) findViewById(R.id.tvCounter);
            tvPrice = (TextView) findViewById(R.id.tvPrice);
            tvSeat = (TextView) findViewById(R.id.tvSeat);
            llSeat = (LinearLayout) findViewById(R.id.llOnSeat);
            llCounter = (LinearLayout) findViewById(R.id.llCounter);
            llSeatDeatils = (LinearLayout) findViewById(R.id.llContainerSeatDetails);

            rdB = (RadioButton)findViewById(R.id.rdB);
            rdI = (RadioButton)findViewById(R.id.rdI);

            llNextToPickupEx = (LinearLayout) findViewById(R.id.llNextToPickupEx);
            FileIO obFile = new FileIO();
            strFileContent = obFile.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];

            spnFilms = (Spinner)findViewById(R.id.spr_films);
            spnShowDate = (Spinner)findViewById(R.id.spr_showtime);
            tvPrice.setText(strTotalAmt);
            String jsonArray = getIntent().getStringExtra("jsonArray");
            jArray = new JSONArray(jsonArray);
            context = this;
            //spnFilms.setOnItemSelectedListener(spnFilmSelected);
            spnFilms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String strSelectedFilm = (String) parent.getItemAtPosition(position);
                    updateShowTime(strSelectedFilm);
                }
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            spnShowDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                }
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            strGSessionId = "";

            tvSeat.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void afterTextChanged(Editable s) {
                    try {

                        if(s.length() > 0){
                            llNextToPickupEx.setVisibility(View.VISIBLE);
                        }
                        else {
                            llNextToPickupEx.setVisibility(View.GONE);
                        }
                    } catch (Exception Ex) {
                        Ex.toString();
                    }
                }
            });
        }
        catch (JSONException e) {
            ShowErrorDialog(FnbPickupType.this,"Error","Something went wrong");
        }
    }

    private void updateShowTime(String strSelectedFilm) {
        int intFilmCount = 0, intShowDateCount=0;
        String strFilmName = "", strShowTime = "";
        try{
            lstSession = new ArrayList<String>();
            List<String> list = new ArrayList<String>();
            intFilmCount = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().size();
            for(int iCount = 0; iCount < intFilmCount; iCount++){
                strFilmName = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().get(iCount).getFilmName();
                if(strFilmName.equals(strSelectedFilm)){
                    intShowDateCount = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().get(iCount).getShowTimes().size();
                    for(int iSCount = 0; iSCount < intShowDateCount; iSCount++) {
                        strShowTime = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().get(iCount).getShowTimes().get(iSCount).getShowDetails();
                        list.add(strShowTime.split("\\|")[1]);
                        lstSession.add(strShowTime.split("\\|")[0]);
                    }
                }
            }

            ArrayAdapter<String> arrSpnTimeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
            spnShowDate.setAdapter(arrSpnTimeAdapter);
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbPickupType.this,"Error","Something went wrong");
        }
    }

    public void BackToOs(View view) {
        try {
            subCancelTrans();
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbPickupType.this,"Error","Something went wrong");
        }
    }

    public void tapFromCounter(View view) {
        try {
            llCounter.setBackground(ContextCompat.getDrawable(context, R.drawable.btn_curved_back_filled));
            ivCounter.setColorFilter(ContextCompat.getColor(context, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
            tvCounter.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));

            if (strSelectedMode.equals("seat")) {
                ivSeat.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
                tvOnSeat.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                llSeat.setBackground(ContextCompat.getDrawable(context,R.drawable.ripple));
            }
            strSelectedMode = "counter";
            llSeatDeatils.setVisibility(View.GONE);

            Bundle poBundle = new Bundle();
            poBundle.putString("appmode","fnb");
            poBundle.putString("price",strTotalAmt);
            poBundle.putString("temptransid",strTempTransId);
            poBundle.putString("qty",strTotalQty);
            Intent intPaymentOption = new Intent(context,PaymentOptions.class);
            intPaymentOption.putExtra("jsonArray", jArray.toString());
            intPaymentOption.putExtras(poBundle);
            startActivity(intPaymentOption);
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbPickupType.this,"Error","Something went wrong");
        }
    }

    public void tapOnSeat(View view){

        try {
            llSeat.setBackground(ContextCompat.getDrawable(context, R.drawable.btn_curved_back_filled));
            ivSeat.setColorFilter(ContextCompat.getColor(context, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
            tvOnSeat.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));

            strSelectedMode = "seat";
            fetchData();
        }
        catch (Exception Ex) {
            ShowErrorDialog(FnbPickupType.this,"Error",Ex.toString());
        }
    }

    private void fetchData() {
        SoapCall obSc;
        try{
            if(strIP.equals("")){
                Toast toast = Toast.makeText(getApplicationContext(), "URL empty, check settings.", Toast.LENGTH_LONG);
                return;
            }
            strGAction = "SESSION";
            obSc = new SoapCall(FnbPickupType.this);
            obSc.delegate = this;
            obSc.execute("SESSIONS", strIP, "", "", "","","","","","","","","","","","","");

        }
        catch (Exception Ex){
            ShowErrorDialog(FnbPickupType.this,"Error",Ex.toString());
        }
    }

    private void subCancelTrans() {
        String strFileContent = "";
        try{
            ShowProgressDialog(FnbPickupType.this,"Going back","");
            strGAction = "CANCELTRANS";
            SoapCall obSc = new SoapCall(FnbPickupType.this);
            obSc.delegate = this;
            obSc.execute("CANCELTRANS", strIP, strTempTransId, "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbPickupType.this,"Error",Ex.toString());
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogProg = mBuilder.create();
            dialogProg.show();

            dialogProg.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbPickupType.this,"Error",Ex.toString());
        }
    }

    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    subCancelTrans();
                }
            });
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbPickupType.this,"Error",Ex.toString());
        }
    }

    @Override
    public void processFinish(String output) {
        try{
            if(!output.split("\\|]")[0].equals("1")) {
                if (strGAction.equals("SESSION")) {
                    processShowFilms(output);
                }
                else if(strGAction.equals("SETFNBPICKUPORDELIVERYEX")){
                    processPickupDelivery(output);
                }
                else if(strGAction.equals("CANCELTRANS")){
                    finish();
                }
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbPickupType.this,"Error",Ex.toString());
        }
    }

    private void processPickupDelivery(String output) {
        String strRespCode = "", strRespData = "";
        try{
            JCResp obResp = new Gson().fromJson(output,JCResp.class);
            strRespCode = obResp.getRESPONSECODE();
            strRespData = obResp.getRESPONSEDATA();
            if(strRespCode.equals("0")) {
                Bundle poBundle = new Bundle();
                poBundle.putString("appmode","fnb");
                poBundle.putString("price",strTotalAmt);
                poBundle.putString("temptransid",strTempTransId);
                poBundle.putString("qty",strTotalQty);
                Intent intPaymentOption = new Intent(context,PaymentOptions.class);
                intPaymentOption.putExtra("jsonArray", jArray.toString());
                intPaymentOption.putExtras(poBundle);
                startActivity(intPaymentOption);
            }
            else {
                ShowErrorDialog(FnbPickupType.this,"Error",strRespData);
            }
        }
        catch (Exception Ex) {
            ShowErrorDialog(FnbPickupType.this,"Error",Ex.toString());
        }
    }

    private void processShowFilms(String output) {
        String strRespCode = "", strRespData="", strFilmName = "", strShowTime="",strShowDate = "";
        int intFilmCount = 0;
        try{
            List<String> list = new ArrayList<String>();
            obJsonSession = new Gson().fromJson(output, JSessions.class);

            strRespCode = obJsonSession.getSYNCRESPONSECODE();
            if(obJsonSession.getSYNCRESPONSECODE().equals("0")){
                intFilmCount = obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().size();
                for(int iCount = 0; iCount < intFilmCount; iCount++){
                    list.add(obJsonSession.getSYNCRESPONSEDATA().getSessions().get(0).getShowDates().get(0).getFilms().get(iCount).getFilmName());
                }
                ArrayAdapter<String> arrSpnFilmAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
                spnFilms.setAdapter(arrSpnFilmAdapter);
                llSeatDeatils.setVisibility(View.VISIBLE);
            }
            else{
                ShowErrorDialog(FnbPickupType.this,"Error",output);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbPickupType.this,"Error",Ex.toString());
        }
    }

    public void proceedOnSeatPickup(View view) {
        String strSeatDetails = "", strSession = "", strTransDetails = "", strDeliveryAt = "";
        try{
            //|BOOKINGID=|SESSIONID=36915| ROWID=L|SEATID=16|
            if(rdB.isChecked()){
                strDeliveryAt = "B";
            }
            else if(rdI.isChecked()){
                strDeliveryAt = "I";
            }
            strSession = lstSession.get(spnShowDate.getSelectedItemPosition());
            strSeatDetails = tvSeat.getText().toString();
            ShowProgressDialog(FnbPickupType.this,"Going back","");
            strTransDetails = "|"+"BOOKINGID="+"|"+"SESSIONID="+strSession+"|"+"ROWID="+strSeatDetails.split("\\-")[0]+"|"+"SEATID="+strSeatDetails.split("\\-")[1]+"|";
            strGAction = "SETFNBPICKUPORDELIVERYEX";
            SoapCall obSc = new SoapCall(FnbPickupType.this);
            obSc.delegate = this;
            obSc.execute("SETFNBPICKUPORDELIVERYEX", strIP, strBookingNo, "Y", strDeliveryAt, strTransDetails, "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            ShowErrorDialog(FnbPickupType.this,"Error",Ex.toString());
        }
    }

    @Override
    public void onBackPressed() {

    }

}
