package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupSeat {

    @SerializedName("seatId")
    @Expose
    private String seatId;
    @SerializedName("strSeatPlace")
    @Expose
    private String strSeatPlace;

    public String getSeatId() {
        return seatId;
    }

    public void setSeatId(String seatId) {
        this.seatId = seatId;
    }

    public String getStrSeatPlace() {
        return strSeatPlace;
    }

    public void setStrSeatPlace(String strSeatPlace) {
        this.strSeatPlace = strSeatPlace;
    }

}