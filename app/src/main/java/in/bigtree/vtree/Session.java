package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Session {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("ShowDates")
    @Expose
    private List<ShowDate> showDates = null;

    @SerializedName("Branch")
    @Expose
    private String Branch;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getBranch() {
        return Branch;
    }

    public void setBranch(String Branch) {
        this.Branch = Branch;
    }

    public List<ShowDate> getShowDates() {
        return showDates;
    }

    public void setShowDates(List<ShowDate> showDates) {
        this.showDates = showDates;
    }

}