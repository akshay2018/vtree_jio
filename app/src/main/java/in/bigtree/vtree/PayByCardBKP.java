package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PayByCardBKP extends AppCompatActivity implements AsyncResponse{
    String strTempTransId = "", strIP = "", strFileContent = "", strGAction = "",strFileName="", strSessionId = "", strTicketType = "", strSeatInfo = "", strQty = "", strBookingId = "",
            strFilmName = "", strShowDateTime = "", strPrice = "", strCinemaname = "", strScreenName = "", strAreaCat = "", strSeats = "";
    LinearLayout llBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_by_card);
        try{
            llBack = (LinearLayout)findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToPaymentSelection(v));

            Bundle poBundle = getIntent().getExtras();
            strTempTransId = poBundle.getString("temptransid");
            strSessionId = poBundle.getString("sessionid");
            strTicketType = poBundle.getString("ttype");
            strSeatInfo = poBundle.getString("seatinfo");
            strQty = poBundle.getString("qty");
            strBookingId = poBundle.getString("bookingid");
            strFilmName = poBundle.getString("filmname");
            strShowDateTime = poBundle.getString("showdatetime");
            strPrice = poBundle.getString("price");
            strCinemaname = poBundle.getString("cinemaname");
            strScreenName = poBundle.getString("screen");
            strAreaCat = poBundle.getString("areacat");
            strSeats = poBundle.getString("seats");

            FileIO obFile = new FileIO();
            strFileName = getResources().getString(R.string.connection_config_file);
            strFileContent = obFile.readFile(strFileName,getApplicationContext());
            strIP = strFileContent.split("\\|")[0];
            showBlock();
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCardBKP.this,"Error","Something went wrong");
        }
    }

    private void showBlock() {
        try{
            ShowErrorDialog(PayByCardBKP.this, "Message from developer","This type of payment is not configured yet.");
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCardBKP.this,"Error","Something went wrong");
        }
    }


    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    subCancelTrans();
                }
            });
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCardBKP.this,"Error","Something went wrong");
        }
    }

    private void subCancelTrans() {
        try{
            strGAction = "CANCELTRANS";
            SoapCall obSc = new SoapCall(PayByCardBKP.this);
            obSc.delegate = this;
            obSc.execute("CANCELTRANS", strIP, strTempTransId, "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCardBKP.this,"Error","Something went wrong");
        }
    }

    @Override
    public void processFinish(String output) {
        try{
            if(!output.split("\\|")[0].equals("1")){
                if(strGAction.equals("COMMIT")){
                    //proceedConfirmation(output);
                }
                if(strGAction.equals("CANCELTRANS")){
                    Intent intent = new Intent(PayByCardBKP.this, Movies.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                }
            }
            else {
                ShowErrorDialog(PayByCardBKP.this,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCardBKP.this,"Error","Something went wrong");
        }
    }

    public void backToPaymentSelection(View view) {
        try{
            finish();
        }
        catch (Exception Ex){
            ShowErrorDialog(PayByCardBKP.this,"Error","Something went wrong");
        }
    }

    @Override
    public void onBackPressed() {

    }
}
