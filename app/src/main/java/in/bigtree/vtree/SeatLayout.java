package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.opengl.Matrix;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SeatLayout extends AppCompatActivity implements AsyncResponse {

    Matrix matrix = new Matrix();
    Float scale = 1f;
    ScaleGestureDetector SGD;
    LinearLayout llSeatParent, llBack;
    int currentX;
    int currentY;
    String strSessionId = "", strPrevArea = "", strIP = "", strFileContent = "", strAction = "", strCinemaName = "",strShowTime = "", strScreenName = "", strFilmName = "", strTempTransId = "", strShowDate = "",
            strStype = "", strSelectedArea = "";
    List arrSelectedSeats = new ArrayList<String>();
    //Map<Integer,String> mapSelectedSeats = new HashMap<Integer, String>();
    AlertDialog dialogErr;
    //TextView tvFilmName;
    JSONObject jsonGroupArea;

    private float mScale = 1f;
    private ScaleGestureDetector mScaleDetector;
    //GestureDetector gestureDetector;
    ScrollView dScrollView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seat_layout);
        try{

            //gestureDetector = new GestureDetector(this, new GestureListener());
            //dScrollView = (ScrollView)findViewById(R.id.dScrollView);
            Bundle sBundle = getIntent().getExtras();
            strSessionId = sBundle.getString("sessionid");
            strCinemaName = sBundle.getString("cinemaname") != null ? sBundle.getString("cinemaname") : "";
            strShowTime = sBundle.getString("showtime") != null ? sBundle.getString("showtime") : "";
            strScreenName = sBundle.getString("screen") != null ? sBundle.getString("screen") : "";
            strFilmName = sBundle.getString("filmname") != null ? sBundle.getString("filmname") : "";
            strShowDate = sBundle.getString("showdate") != null ? sBundle.getString("showdate") : "";
            strStype = sBundle.getString("stype") != null ? sBundle.getString("stype") : "";

            strSelectedArea = sBundle.getString("areacode") != null ? sBundle.getString("areacode") : "";

            llSeatParent = (LinearLayout)findViewById(R.id.llseatparent);
            llBack = (LinearLayout)findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToShows(v));
            //SGD = new ScaleGestureDetector(this, new ScaleListener());
            //tvFilmName = (TextView)findViewById(R.id.tvFilmName);
            //tvFilmName.setText(strFilmName);

            GroupSeats obGroupSeat = new GroupSeats();
            jsonGroupArea = new JSONObject();

            callSeatLayout(strSessionId);
        /*
            mScaleDetector = new ScaleGestureDetector(this, new ScaleGestureDetector.SimpleOnScaleGestureListener()
            {
                @Override
                public boolean onScale(ScaleGestureDetector detector)
                {
                    float scale = 1 - detector.getScaleFactor();

                    float prevScale = mScale;
                    mScale += scale;

                    if (mScale < 0.5f) // Minimum scale condition:
                        mScale = 0.5f;

                    if (mScale > 1f) // Maximum scale condition:
                        mScale = 1f;
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1f / prevScale, 1f / mScale, 1f / prevScale, 1f / mScale, detector.getFocusX(), detector.getFocusY());
                    scaleAnimation.setDuration(0);
                    scaleAnimation.setFillAfter(true);
                    ScrollView layout =(ScrollView) findViewById(R.id.dScrollView);
                    //HorizontalScrollView hLayout = (HorizontalScrollView)findViewById(R.id.scrollView);
                    layout.startAnimation(scaleAnimation);
                    return true;
                }
            });
        */
            //subDrawSeatLayout(strSeatData);
        }
        catch (Exception Ex){
            ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
        }
    }

    /*
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        super.dispatchTouchEvent(event);
        mScaleDetector.onTouchEvent(event);
        gestureDetector.onTouchEvent(event);
        return gestureDetector.onTouchEvent(event);
    }
    */
    /*
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            // double tap fired.
            return true;
        }
    }
*/

    private void callSeatLayout(String strSessionId) {
        try{
            FileIO obFile = new FileIO();
            strFileContent = obFile.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];

            strAction = "SEATLAYOUT";
            SoapCall obSc = new SoapCall(SeatLayout.this);
            obSc.delegate = this;
            //strResponse = obCs.Call("SEATLAYOUT", strIP, strSession, "", "","","","","","","","","","","","");

            //CustomDialog cDialog = new CustomDialog();
            //progDiag = cDialog.GetProgressDialog(SeatLayout.this,"","");
            //progDiag.show();
            ShowProgressDialog(SeatLayout.this,"Loading Seat Layout","This should take less than an minute");
            obSc.execute("SEATLAYOUT", strIP, strSessionId, "", "","","","","","","","","","","","","");

            //String strSeatData = obFile.readLocalJson(SeatLayout.this,"seatlayout");
        }
        catch (Exception Ex){
            ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
        }
    }

    private void subDrawSeatLayout(String strSeatData) {
        String strRespCode = "", strGroupSeatArea = "";
        boolean blnFoundGrpSeat = false;
        try{
            int globInt = 0;
            JSeatLayout obJsonSeats = new Gson().fromJson(strSeatData, JSeatLayout.class);
            if(obJsonSeats.getSEATRESPONSECODE().equals("0")) {
                int intTotalAreaCount = obJsonSeats.getSEATRESPONSEDATA().getArea().size();
                strTempTransId = obJsonSeats.getSEATRESPONSEDATA().getTempTransID();
                strRespCode = obJsonSeats.getSEATRESPONSECODE();

                LinearLayout llParent;
                llParent = (LinearLayout) findViewById(R.id.llseatparent);

                //remove previouse child views
                llParent.removeAllViews();

                for (int intAreaCount = 0; intAreaCount < intTotalAreaCount; intAreaCount++) {
                    int intMarginStartArea = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
                    final LinearLayout llAreaCategry = new LinearLayout(this);
                    LinearLayout.LayoutParams layoutAreaParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutAreaParameters.setMarginStart(intMarginStartArea);
                    llAreaCategry.setLayoutParams(layoutAreaParameters);
                    llAreaCategry.setOrientation(LinearLayout.HORIZONTAL);

                    String strAreaDesc = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getStrAreaDesc();
                    String strAreaNum = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getStrAreaNum();
                    String strAreaCode = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getStrAreaCode();
                    String strGroupSeatsData = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getStrGroupSeatsData();
                    String strHasCurrentOrder = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getStrHasCurrentOrder();
                    String strTicketCode = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getStrTicketType();
                    String strIsPakageTicket = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getStrPackageTicket();

                    //Binding group seat data to utilize when assigning seats
                    if(!strGroupSeatsData.equals("")){
                        bindGroupSeatsData(strAreaCode,strGroupSeatsData);
                        for(int iCount = 0; iCount < jsonGroupArea.length(); iCount++){
                            strGroupSeatArea = jsonGroupArea.getString("groupArea");
                        }
                    }

                    int intPhyBlankRowWidth = 0;
                    int intPhyBlankRowHeight = 0;
                    int intPhyBlankRowLeftMargin = 0;

                    final TextView vwAreaCategroyDesc = new TextView(this);
                    vwAreaCategroyDesc.setText(strAreaDesc);
                    vwAreaCategroyDesc.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                    llAreaCategry.addView(vwAreaCategroyDesc);
                    llParent.addView(llAreaCategry);


                    int intTotalRowCount = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getRows().size();

                    for (int intRowCount = intTotalRowCount - 1; intRowCount >= 0; intRowCount--) {

                        final LinearLayout llRowParent = new LinearLayout(this);
                        llRowParent.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        llRowParent.setOrientation(LinearLayout.HORIZONTAL);

                        final LinearLayout llRow = new LinearLayout(this);
                        //llRow.setLayoutParams(new LinearLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT, GridLayout.LayoutParams.WRAP_CONTENT));
                        llRow.setLayoutParams(new LinearLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT, GridLayout.LayoutParams.WRAP_CONTENT,1.0f));
                        llRow.setOrientation(LinearLayout.HORIZONTAL);
                        llRow.setBackgroundColor(Color.WHITE);

                        String intGridRowID = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getRows().get(intRowCount).getIntGridRowID();
                        String strRowPhyID = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getRows().get(intRowCount).getStrRowPhyID();

                        int intPhyRowWidth = 0;
                        int intPhyRowHeight = 0;
                        int intPhyRowLeftMargin = 0;
                        int intRowIdColor = Color.parseColor("#dfdfdf");
                        intPhyRowWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                        intPhyRowHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 18, getResources().getDisplayMetrics());
                        intPhyRowLeftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());

                        final LinearLayout llRowPhyId = new LinearLayout(this);
                        LinearLayout.LayoutParams layoutRowParameters = new LinearLayout.LayoutParams(intPhyRowWidth, intPhyRowHeight);
                        layoutRowParameters.setMargins(0, 0, intPhyRowLeftMargin, 0);
                        llRowPhyId.setLayoutParams(layoutRowParameters);
                        llRowPhyId.setGravity(Gravity.CENTER_HORIZONTAL);
                        llRowPhyId.setBackgroundColor(intRowIdColor);

                        int RowIDTextColor = Color.parseColor("#606060");
                        final TextView vwRowPhyId = new TextView(this);
                        vwRowPhyId.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
                        vwRowPhyId.setBackgroundColor(intRowIdColor);
                        vwRowPhyId.setTextColor(RowIDTextColor);
                        vwRowPhyId.setText(strRowPhyID);
                        llRowPhyId.addView(vwRowPhyId);

                        //llRow.addView(llRowPhyId);
                        llRowParent.addView(llRowPhyId);

                        int intTotalSeatCount = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getRows().get(intRowCount).getSeats().size();
                        for (int intSeatCount = 0; intSeatCount < intTotalSeatCount; intSeatCount++) {
                            int intSeatWidth = 0;
                            int intSeatHeight = 0;
                            int intSeatLeftMargin = 0;

                            intSeatWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                            intSeatHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                            intSeatLeftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());

                            final LinearLayout llSeatLayout = new LinearLayout(this);
                            LinearLayout.LayoutParams layoutRowParamsSeat = new LinearLayout.LayoutParams(intSeatWidth, intSeatHeight);
                            layoutRowParamsSeat.setMargins(intSeatLeftMargin, 0, 0, 2);
                            llSeatLayout.setGravity(Gravity.CENTER_HORIZONTAL);
                            llSeatLayout.setLayoutParams(layoutRowParamsSeat);
                            llSeatLayout.setId(globInt++);

                            String strGridSeatNum = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getRows().get(intRowCount).getSeats().get(intSeatCount).getStrGridSeatNum();
                            String strSeatNumber = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getRows().get(intRowCount).getSeats().get(intSeatCount).getStrSeatNumber();
                            String strSeatStatus = obJsonSeats.getSEATRESPONSEDATA().getArea().get(intAreaCount).getRows().get(intRowCount).getSeats().get(intSeatCount).getStrSeatStatus();

                            final TextView vwSeat = new TextView(this);
                        /*
                        if(strPrefLayout.equals("PARTIAL")) {
                            if (!strAreaCode.equals(strSelectedArea)) {
                                //disable other areas
                                strSeatStatus = "11";
                            }
                        }
                        */
                            if(strStype.equals("partial")) {
                                if (!strAreaCode.equals(strSelectedArea)) {
                                    //disable other areas
                                    strSeatStatus = "11";
                                }
                            }
                            blnFoundGrpSeat = false;
                            switch (strSeatStatus) {
                                case "0":
                                    //AVAILABLE SEAT.
                                    llSeatLayout.setBackgroundResource(R.drawable.border_seat_available);
                                    vwSeat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);

                                    //vwSeat.setTextColor(getResources().getColor(R.color.colorSeatText));
                                    vwSeat.setGravity(Gravity.CENTER_HORIZONTAL);
                                    vwSeat.setText(strSeatNumber);
                                    vwSeat.setTextColor(ContextCompat.getColor(SeatLayout.this, R.color.colorText));
                                    llSeatLayout.addView(vwSeat);

                                    llSeatLayout.setTag(strSeatNumber);
                                    llSeatLayout.setClickable(true);

                                    if(!strGroupSeatArea.equals(strAreaCode)){
                                        llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus + "#" + strTicketCode + "#" + strAreaDesc + "#" + "N" + "#" +strIsPakageTicket);
                                    }
                                    else{
                                        JSONArray jARRAY = jsonGroupArea.getJSONArray("groupRows");
                                        for(int intGroupRowCount = 0; intGroupRowCount < jARRAY.length(); intGroupRowCount++) {
                                            JSONObject jOBJECT = (JSONObject) jARRAY.get(intGroupRowCount);
                                            String strGroupRowId = jOBJECT.getString("groupRowId");
                                            if(intGridRowID.equals(strGroupRowId)){
                                                JSONArray jSeatArray = jOBJECT.getJSONArray("groupSeats");
                                                for(int intGroupSeatCount = 0; intGroupSeatCount < jSeatArray.length(); intGroupSeatCount++){
                                                    JSONObject jGSeatObj = (JSONObject) jSeatArray.get(intGroupSeatCount);
                                                    String strGroupSeatNo = jGSeatObj.getString("groupSeatNo");
                                                    String strSeatPlace = jGSeatObj.getString("seatPlace");
                                                    blnFoundGrpSeat = true;
                                                    if(strGroupSeatNo.equals(strGridSeatNum)){
                                                        llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus + "#" + strTicketCode + "#" + strAreaDesc + "#" + strSeatPlace + "#" + strIsPakageTicket);
                                                    }
                                                }
                                            }
                                            if(blnFoundGrpSeat == false){
                                                llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus + "#" + strTicketCode + "#" + strAreaDesc + "#" + "N" + "#" +strIsPakageTicket);
                                            }
                                            Log.d("","");
                                        }
                                    }
                                    llSeatLayout.setOnClickListener(SeatClickListerner);

                                    break;
                                case "1":
                                    //BOOKED SEAT.
                                    llSeatLayout.setBackgroundResource(R.drawable.border_seat_sold);
                                    vwSeat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9);
                                    vwSeat.setTextColor(Color.WHITE);
                                    vwSeat.setGravity(Gravity.CENTER_HORIZONTAL);
                                    vwSeat.setText(strSeatNumber);
                                    llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus);
                                    llSeatLayout.addView(vwSeat);
                                    break;
                                case "3":
                                    //SPECAIL SEAT.
                                    llSeatLayout.setBackgroundResource(R.drawable.border_seat_special);
                                    //vwSeat.setTextColor(Color.WHITE);
                                    vwSeat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9);
                                    vwSeat.setTextColor(ContextCompat.getColor(SeatLayout.this, R.color.colorText));
                                    vwSeat.setGravity(Gravity.CENTER_HORIZONTAL);
                                    vwSeat.setText(strSeatNumber);
                                    llSeatLayout.setClickable(true);
                                    if(!strGroupSeatArea.equals(strAreaCode)) {
                                        llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus + "#" + strTicketCode + "#" + strAreaDesc + "#" + "N" + "#" + strIsPakageTicket);
                                    }
                                    else {
                                        JSONArray jARRAY = jsonGroupArea.getJSONArray("groupRows");
                                        for(int intGroupRowCount = 0; intGroupRowCount < jARRAY.length(); intGroupRowCount++) {
                                            JSONObject jOBJECT = (JSONObject) jARRAY.get(intGroupRowCount);
                                            String strGroupRowId = jOBJECT.getString("groupRowId");
                                            if(intGridRowID.equals(strGroupRowId)) {
                                                JSONArray jSeatArray = jOBJECT.getJSONArray("groupSeats");
                                                for(int intGroupSeatCount = 0; intGroupSeatCount < jSeatArray.length(); intGroupSeatCount++) {
                                                    JSONObject jGSeatObj = (JSONObject) jSeatArray.get(intGroupSeatCount);
                                                    String strGroupSeatNo = jGSeatObj.getString("groupSeatNo");
                                                    String strSeatPlace = jGSeatObj.getString("seatPlace");
                                                    blnFoundGrpSeat = true;
                                                    if(strGroupSeatNo.equals(strGridSeatNum)){
                                                        llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus + "#" + strTicketCode + "#" + strAreaDesc + "#" + strSeatPlace + "#" + strIsPakageTicket);
                                                    }
                                                }
                                            }
                                            if(blnFoundGrpSeat == false){
                                                llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus + "#" + strTicketCode + "#" + strAreaDesc + "#" + "N" + "#" + strIsPakageTicket);
                                            }
                                        }
                                    }
                                    llSeatLayout.addView(vwSeat);
                                    llSeatLayout.setOnClickListener(SeatClickListerner);
                                    break;
                                case "4":
                                    //INHOUSE SEAT.
                                    llSeatLayout.setBackgroundResource(R.drawable.border_seat_special);
                                    vwSeat.setTextColor(Color.WHITE);
                                    vwSeat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9);
                                    //vwSeat.setTextColor(getResources().getColor(R.color.colorSeatText));
                                    vwSeat.setGravity(Gravity.CENTER_HORIZONTAL);
                                    vwSeat.setText(strSeatNumber);
                                    llSeatLayout.setClickable(true);
                                    if(!strGroupSeatArea.equals(strAreaCode)) {
                                        llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus + "#" + strTicketCode + "#" + strAreaDesc + "#" + "N" + "#" + strIsPakageTicket);
                                    }
                                    else {
                                        JSONArray jARRAY = jsonGroupArea.getJSONArray("groupRows");
                                        for(int intGroupRowCount = 0; intGroupRowCount < jARRAY.length(); intGroupRowCount++) {
                                            JSONObject jOBJECT = (JSONObject) jARRAY.get(intGroupRowCount);
                                            String strGroupRowId = jOBJECT.getString("groupRowId");
                                            if(intGridRowID.equals(strGroupRowId)) {
                                                JSONArray jSeatArray = jOBJECT.getJSONArray("groupSeats");
                                                for(int intGroupSeatCount = 0; intGroupSeatCount < jSeatArray.length(); intGroupSeatCount++) {
                                                    JSONObject jGSeatObj = (JSONObject) jSeatArray.get(intGroupSeatCount);
                                                    String strGroupSeatNo = jGSeatObj.getString("groupSeatNo");
                                                    String strSeatPlace = jGSeatObj.getString("seatPlace");
                                                    blnFoundGrpSeat = true;
                                                    if(strGroupSeatNo.equals(strGridSeatNum)){
                                                        llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus + "#" + strTicketCode + "#" + strAreaDesc + "#" + strSeatPlace + "#" + strIsPakageTicket);
                                                    }
                                                }
                                            }
                                            if(blnFoundGrpSeat == false){
                                                llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus + "#" + strTicketCode + "#" + strAreaDesc + "#" + "N" + "#" + strIsPakageTicket);
                                            }
                                        }
                                    }
                                    llSeatLayout.addView(vwSeat);
                                    llSeatLayout.setOnClickListener(SeatClickListerner);
                                    break;
                                case "5":
                                    //CUREENT ORDER.
                                    llSeatLayout.setBackgroundResource(R.drawable.border_seat_selected);
                                    vwSeat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
                                    //vwSeat.setTextColor(getResources().getColor(R.color.colorSeatText));
                                    vwSeat.setGravity(Gravity.CENTER_HORIZONTAL);
                                    llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus);
                                    llSeatLayout.addView(vwSeat);
                                    break;
                                case "10":
                                    llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus);
                                    break;
                                case "11":
                                    llSeatLayout.setBackgroundResource(R.drawable.border_seat_sold);
                                    vwSeat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
                                    vwSeat.setTextColor(Color.WHITE);
                                    vwSeat.setGravity(Gravity.CENTER_HORIZONTAL);
                                    vwSeat.setText(strSeatNumber);
                                    llSeatLayout.setTag(strAreaCode + "_" + strAreaNum + "_" + intGridRowID + "_" + strGridSeatNum + "#" + (strRowPhyID + "-" + strSeatNumber) + "#" + strSeatStatus);
                                    llSeatLayout.addView(vwSeat);
                                    break;
                                default:
                            }
                            llRow.addView(llSeatLayout);
                        }

                        llRowParent.addView(llRow);
                        //llRow.addView(llRowPhyIdLast);

                        final LinearLayout llRowPhyId2 = new LinearLayout(this);
                        LinearLayout.LayoutParams layoutRowParameters2 = new LinearLayout.LayoutParams(intPhyRowWidth, intPhyRowHeight);
                        layoutRowParameters2.setMargins(intPhyRowLeftMargin, 0, 0, 0);
                        llRowPhyId2.setLayoutParams(layoutRowParameters2);
                        llRowPhyId2.setGravity(Gravity.CENTER_HORIZONTAL);
                        llRowPhyId2.setBackgroundColor(intRowIdColor);

                        int RowIDTextColor2 = Color.parseColor("#606060");
                        final TextView vwRowPhyId2 = new TextView(this);
                        vwRowPhyId2.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
                        vwRowPhyId2.setBackgroundColor(intRowIdColor);
                        vwRowPhyId2.setTextColor(RowIDTextColor2);
                        vwRowPhyId2.setText(strRowPhyID);
                        llRowPhyId2.addView(vwRowPhyId2);

                        //llRow.addView(llRowPhyId);
                        llRowParent.addView(llRowPhyId2);
                        llParent.addView(llRowParent);
                    }
                }

                LinearLayout llScreenContainer = new LinearLayout(this);
                LinearLayout.LayoutParams llScreenContainerParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                int intMarginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
                int intScreenWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getResources().getDisplayMetrics());
                int intScreenHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
                llScreenContainerParams.setMargins(0,intMarginTop,0,intMarginTop);
                llScreenContainer.setLayoutParams(llScreenContainerParams);
                llScreenContainer.setGravity(Gravity.CENTER|Gravity.CENTER_VERTICAL);

                LinearLayout llScreen = new LinearLayout(this);
                llScreen.setLayoutParams(new LinearLayout.LayoutParams(intScreenWidth,intScreenHeight));
                llScreen.setBackground(ContextCompat.getDrawable(SeatLayout.this,R.drawable.screen_outline));

                TextView tvScreen = new TextView(this);
                tvScreen.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvScreen.setText("Screen this way");
                tvScreen.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                tvScreen.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
                llScreen.addView(tvScreen);
                llScreenContainer.addView(llScreen);

                llParent.addView(llScreenContainer);
            }
            else{
                ShowErrorDialog(SeatLayout.this,"Error",obJsonSeats.getSEATRESPONSEDATA().toString());
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(SeatLayout.this,"Error",strSeatData);
        }
    }

    private void bindGroupSeatsData(String strAreaCode, String strGroupSeatsData) {
        String[] arrRecords = null;
        String[] arrRowDetail = null;
        String[] strRow = null;
        String strRowDetails = "", intGridRowId = "", strSeatNo = "";
        try{
            JSONObject jsonGroupRow = null;
            JSONArray jsonArrGroupRow = new JSONArray();
            arrRecords = strGroupSeatsData.split("\\:");
            for(int iCount = 0; iCount < arrRecords.length; iCount++){
                jsonGroupRow = new JSONObject();
                JSONArray jsonArrGroupSeats = new JSONArray();
                arrRowDetail = arrRecords[iCount].split("\\-");
                for(int intRowDetails = 0; intRowDetails < arrRowDetail.length; intRowDetails++){
                    JSONObject jsonGroupSeats = new JSONObject();
                    strRow = arrRowDetail[intRowDetails].split("\\,");
                    intGridRowId = strRow[0];
                    strSeatNo = strRow[1];

                    if(intRowDetails == 0){
                        jsonGroupSeats.put("groupSeatNo",strSeatNo);
                        jsonGroupSeats.put("seatPlace","F");
                    }
                    else{
                        jsonGroupSeats.put("groupSeatNo",strSeatNo);
                        jsonGroupSeats.put("seatPlace","L");
                    }
                    jsonArrGroupSeats.put(jsonGroupSeats);
                }
                jsonGroupRow.put("groupRowId",intGridRowId);
                jsonGroupRow.put("groupSeats",jsonArrGroupSeats);

                jsonArrGroupRow.put(jsonGroupRow);
            }
            jsonGroupArea.put("groupArea",strAreaCode);
            jsonGroupArea.put("groupRows",jsonArrGroupRow);
        }
        catch (Exception Ex){
            ShowErrorDialog(SeatLayout.this,"Error while binding group seat data",Ex.toString());
        }
    }

    public View.OnClickListener SeatClickListerner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try{
                if(arrSelectedSeats.size() < 10) {
                    selectSeat(v);
                }
                else{
                    clearSeats(v);
                }
            }
            catch (Exception Ex){
                ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
            }
        }
    };

    private void clearSeats(View v) {
        try{
            String strSeatStatus = "";
            for(int iCount = 0; iCount < arrSelectedSeats.size(); iCount++){
                int lId = Integer.parseInt(arrSelectedSeats.get(iCount).toString().split("\\|")[0]);
                LinearLayout llSeatToRemove = (LinearLayout)findViewById(lId);
                TextView tSeatToRemove;
                View vwToRemove = llSeatToRemove.getChildAt(0);
                strSeatStatus = llSeatToRemove.getTag().toString().split("\\#")[2];
                if(strSeatStatus.equals("3") || strSeatStatus.equals("4")) {
                    llSeatToRemove.setBackground(ContextCompat.getDrawable(SeatLayout.this, R.drawable.border_seat_special));
                }
                else {
                    llSeatToRemove.setBackground(ContextCompat.getDrawable(SeatLayout.this, R.drawable.border_seat_available));
                }
                if (vwToRemove instanceof TextView) {
                    tSeatToRemove = (TextView) vwToRemove;
                    tSeatToRemove.setTextColor(ContextCompat.getColor(SeatLayout.this,R.color.colorText));
                }
            }
            arrSelectedSeats.clear();
            selectSeat(v);
        }
        catch (Exception Ex){
            ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
        }
    }

    private void selectSeat(View v) {
        String strCurArea = "", strSeatStatus = "", strSeatPlace = "";
        try{
            String strCurSelectionTag = "";
            TextView tView;
            LinearLayout lLSelectedSeat = (LinearLayout)v;
            View vw = lLSelectedSeat.getChildAt(0);
            strCurSelectionTag = Integer.toString(v.getId()) + "|" + v.getTag().toString();
            strCurArea = v.getTag().toString().split("\\#")[0].split("\\_")[0];
            strSeatStatus = v.getTag().toString().split("\\#")[2];
            strSeatPlace = v.getTag().toString().split("\\#")[5];

            if(strSeatPlace.equals("N")) {

                if (arrSelectedSeats.contains(strCurSelectionTag)) {
                    TextView tSeatToRemove;
                    View vwToRemove = lLSelectedSeat.getChildAt(0);

                    if (strSeatStatus.equals("3") || strSeatStatus.equals("4")) {
                        lLSelectedSeat.setBackground(ContextCompat.getDrawable(SeatLayout.this, R.drawable.border_seat_special));
                    } else {
                        lLSelectedSeat.setBackground(ContextCompat.getDrawable(SeatLayout.this, R.drawable.border_seat_available));
                    }

                    if (vwToRemove instanceof TextView) {
                        tSeatToRemove = (TextView) vwToRemove;
                        tSeatToRemove.setTextColor(ContextCompat.getColor(SeatLayout.this, R.color.colorText));
                    }
                    arrSelectedSeats.remove(strCurSelectionTag);
                    return;
                }

                if (!strCurArea.equals(strPrevArea)) {
                    strPrevArea = strCurArea;
                    clearSeats(v);
                } else {
                    arrSelectedSeats.add(strCurSelectionTag);
                    lLSelectedSeat.setBackground(ContextCompat.getDrawable(SeatLayout.this, R.drawable.border_seat_selected));
                    if (vw instanceof TextView) {
                        tView = (TextView) vw;
                        tView.setTextColor(ContextCompat.getColor(SeatLayout.this, R.color.colorWhite));
                    }
                }
            }
            else{
                selectGroupSeats(v);
            }

        }
        catch (Exception Ex){
            ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
        }
    }

    private void selectGroupSeats(View v) {
        String strCurSelectionTag = "", strCurArea = "", strSeatStatus = "", strSeatPlace = "";
        try{
            TextView tView;
            LinearLayout lLSelectedSeat = (LinearLayout)v;
            View vw = lLSelectedSeat.getChildAt(0);
            strCurSelectionTag = Integer.toString(v.getId()) + "|" + v.getTag().toString();
            strCurArea = v.getTag().toString().split("\\#")[0].split("\\_")[0];
            strSeatStatus = v.getTag().toString().split("\\#")[2];
            strSeatPlace = v.getTag().toString().split("\\#")[5];

            if(arrSelectedSeats.size() > 0) {
                clearSeats(v);
            }
            else{
                if (!strCurArea.equals(strPrevArea)) {
                    strPrevArea = strCurArea;
                    clearSeats(v);
                }else {
                    arrSelectedSeats.add(strCurSelectionTag);
                    lLSelectedSeat.setBackground(ContextCompat.getDrawable(SeatLayout.this, R.drawable.border_seat_selected));
                    if (vw instanceof TextView) {
                        tView = (TextView) vw;
                        tView.setTextColor(ContextCompat.getColor(SeatLayout.this, R.color.colorWhite));
                    }
                    LinearLayout llParentRow = (LinearLayout)lLSelectedSeat.getParent();
                    LinearLayout llSibling;
                    if(strSeatPlace.equals("F")){
                        llSibling = (LinearLayout)llParentRow.getChildAt(llParentRow.indexOfChild(lLSelectedSeat)+1);
                    }
                    else{
                        llSibling = (LinearLayout)llParentRow.getChildAt(llParentRow.indexOfChild(lLSelectedSeat)-1);
                    }
                    //Adding sibling for group seat type
                    strCurSelectionTag = Integer.toString(llSibling.getId()) + "|" + llSibling.getTag().toString();
                    arrSelectedSeats.add(strCurSelectionTag);
                    vw = llSibling.getChildAt(0);

                    llSibling.setBackground(ContextCompat.getDrawable(SeatLayout.this, R.drawable.border_seat_selected));
                    if (vw instanceof TextView) {
                        tView = (TextView) vw;
                        tView.setTextColor(ContextCompat.getColor(SeatLayout.this, R.color.colorWhite));
                    }
                }
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
        }
    }

    public void confirmSeats(View v){
        try{
            String strData = "", strCat = "", strSeat = "", strTktType = "", strQty = "", strAreaCode = "", strSeatsInfo = "", strAreaNo = "", strRowId = "", strSeatId = "", strIsPackageTicket = "";
            //Log.d("",arrSelectedSeats.get(0).toString());
            if(arrSelectedSeats.size() < 1){

                return;
            }
            strQty = Integer.toString(arrSelectedSeats.size());

            strSeatsInfo = "|" + strQty + "|";
            for(int iCount = 0; iCount < arrSelectedSeats.size(); iCount++){
                String[] arrSelectedData = arrSelectedSeats.get(iCount).toString().split("\\#");

                strAreaCode = arrSelectedData[0].split("\\_")[0].split("\\|")[1];
                strAreaNo = arrSelectedData[0].split("\\_")[1];
                strRowId = arrSelectedData[0].split("\\_")[2];
                strSeatId = arrSelectedData[0].split("\\_")[3];
                strCat = arrSelectedData[4];
                strTktType = arrSelectedData[3];
                strIsPackageTicket = arrSelectedData[6];

                strSeat += arrSelectedData[1] + ",";
                strSeatsInfo += strAreaCode + "|" + strAreaNo + "|" + strRowId + "|" + strSeatId + "|";
            }
            strSeat = strSeat.replace("-","");
            strSeat = strSeat.substring(0,strSeat.length()-1);
            strSeat = strSeat.replace(",",", ");

            Bundle stBundle = new Bundle();
            stBundle.putString("cinemaname",strCinemaName);
            stBundle.putString("showtime",strShowTime);
            stBundle.putString("screen",strScreenName);
            stBundle.putString("filmname",strFilmName);
            stBundle.putString("areacat",strCat);
            stBundle.putString("ttype",strTktType); // tickettype code
            stBundle.putString("seats",strSeat);
            stBundle.putString("qty",strQty);
            stBundle.putString("temptransid",strTempTransId);
            stBundle.putString("sessionid",strSessionId);
            stBundle.putString("seatinfo",strSeatsInfo);
            stBundle.putString("showdate",strShowDate);
            stBundle.putString("ispackage",strIsPackageTicket);
            Intent intConfirm = new Intent(SeatLayout.this,PaySelectionConfirmation.class);
            intConfirm.putExtras(stBundle);
            startActivity(intConfirm);
        }
        catch (Exception Ex){
            ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
        }
    }

    @Override
    public void processFinish(String output) {
        try{
            //progDiag.dismiss();
            dialogErr.dismiss();
            if (!output.split("\\|")[0].equals("1")) {
                if (strAction.equals("SEATLAYOUT")) {
                    subDrawSeatLayout(output);
                }
            }
            else {
                ShowErrorDialog(SeatLayout.this,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
        }
    }

    public void backToShows(View view) {
        try{
            finish();
        }
        catch (Exception Ex){
            ShowErrorDialog(SeatLayout.this,"Error","Something went wrong");
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            //btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();ShowErrorDialog(SeatLayout.this,"Error",Ex.toString());
        }
    }

    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    finish();
                }
            });
        }
        catch (Exception Ex){
            Toast.makeText(SeatLayout.this,Ex.toString(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {

    }
}
