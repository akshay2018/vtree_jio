package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PaySelectionConfirmation extends AppCompatActivity implements AsyncResponse{

    TextView tvCinemaName, tvBigCinemaName, tvFilmName, tvScreenName, tvAreaCat, tvSeats, tvShowTime, tvTQty, tvSeatQty, tvTotal;
    String strCinemaName = "", strFilmName = "", strScreen = "", strAreaCat = "", strTicketType = "", strSeats = "", strShowTime = "", strQty = "", strTempTransId = "", strSessionId = "",
    strSeatInfo = "", strGAction = "",strIP = "", strWCODE="",strFileName = "", strFileContent = "", strDateTime = "", strBookingId = "", strShowDate = "", strFreeSeating = "", strIsPackageTicket = "", strBookingNo = "",
    strPrint = "N";
    AlertDialog dialogErr;
    boolean blnSeatsAdded = false;
    LinearLayout llBack;
    Button btnProceed;
    EditText edComments, edPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_selection_confirmation);
        try{

            FileIO obFile = new FileIO();
            strFileName = getResources().getString(R.string.connection_config_file);
            strFileContent = obFile.readFile(strFileName,getApplicationContext());
            strIP = strFileContent.split("\\|")[0];
            strWCODE = strFileContent.split("\\|")[1];

            Bundle psBundle = getIntent().getExtras();
            strCinemaName = psBundle.getString("cinemaname");
            strFilmName = psBundle.getString("filmname");
            strScreen = psBundle.getString("screen");
            strAreaCat = psBundle.getString("areacat");
            strTicketType = psBundle.getString("ttype");   //tickettype code
            strSeats = psBundle.getString("seats");
            strShowTime = psBundle.getString("showtime");
            strQty = psBundle.getString("qty");
            strTempTransId = psBundle.getString("temptransid");
            strSessionId = psBundle.getString("sessionid");
            strSeatInfo = psBundle.getString("seatinfo");
            strShowDate = psBundle.getString("showdate");
            strIsPackageTicket = psBundle.getString("ispackage");
            strFreeSeating = psBundle.getString("isFreeSeating");

            SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy", java.util.Locale.ENGLISH);
            Date myDate = sdf.parse(strShowDate);
            sdf.applyPattern("EEE, d MMM yyyy");
            strDateTime = sdf.format(myDate) + "|" + strShowTime;
            llBack = (LinearLayout)findViewById(R.id.llBack);
            llBack.setOnClickListener((v) -> backToSeats(v));
            //tvCinemaName = (TextView) findViewById(R.id.tvCinemaName);
            //tvBigCinemaName = (TextView) findViewById(R.id.tvBigCinemaName);
            tvFilmName = (TextView) findViewById(R.id.tvFilmName);
            //tvScreenName = (TextView) findViewById(R.id.tvScreenName);
            //tvAreaCat = (TextView) findViewById(R.id.tvCat);
            //tvSeats = (TextView) findViewById(R.id.tvSeats);
            tvShowTime = (TextView) findViewById(R.id.tvDateTime);
            tvTQty = (TextView)findViewById(R.id.tvTkQty);
            //tvSeatQty = (TextView) findViewById(R.id.tvSeatQty);
            tvTotal = (TextView) findViewById(R.id.tvTotal);
            btnProceed = (Button)findViewById(R.id.btnProccedToCommit);
            btnProceed.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
            btnProceed.setClickable(false);
            callAddSeats();

            //tvCinemaName.setText(strCinemaName);
            //tvAreaCat.setText(strAreaCat);
            //tvBigCinemaName.setText(strCinemaName);
            tvFilmName.setText(strFilmName);
            //tvScreenName.setText(strScreen);
            //tvSeats.setText(strSeats);
            //tvShowTime.setText(strShowTime);
            tvTQty.setText(strQty + " Tickets");
            //tvSeatQty.setText("Seats"+"("+strQty+")");

            edComments = (EditText) findViewById(R.id.tvComments);
            edPhone = (EditText)findViewById(R.id.tvMobileNo);

            edPhone.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        if (s.length() < 10) {
                            btnProceed.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
                            btnProceed.setClickable(false);
                        }
                        else{
                            btnProceed.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorJwcDarkBrown));
                            btnProceed.setClickable(true);
                            closeKeyboard();
                        }
                        /*
                        if(s.length() == 10){
                            closeKeyboard();
                        }
                        */
                    } catch (Exception Ex) {
                        Ex.toString();
                    }
                }
            });
        }
        catch (Exception Ex){
            ShowErrorDialog(PaySelectionConfirmation.this,"Error","Something went wrong");
        }
    }

    public void callAddSeats(){
        try{
            strGAction = "ADDSEATS";
            SoapCall obSc = new SoapCall(PaySelectionConfirmation.this);
            obSc.delegate = this;
            ShowProgressDialog(PaySelectionConfirmation.this,"Getting information","");
            obSc.execute("ADDSEATS", strIP, strSessionId, strTempTransId, strTicketType,strQty,strSeatInfo,strIsPackageTicket,"N","",strFreeSeating,"","","","","","");
        }
        catch (Exception Ex){
            ShowErrorDialog(PaySelectionConfirmation.this,"Error","Something went wrong");
        }
    }

    /*
    public void proceedPaymentSelection(View v){
        try{
            Bundle poBundle = new Bundle();
            poBundle.putString("sessionid",strSessionId);
            poBundle.putString("temptransid",strTempTransId);
            poBundle.putString("qty",strQty);
            poBundle.putString("ttype",strTicketType);
            poBundle.putString("seatinfo",strSeatInfo);
            poBundle.putString("showdatetime",strDateTime);
            poBundle.putString("filmname",strFilmName);
            poBundle.putString("bookingid",strBookingId);
            poBundle.putString("cinemaname",strCinemaName);
            poBundle.putString("price",tvTotal.getText().toString());
            poBundle.putString("screen",strScreen);
            poBundle.putString("areacat",strAreaCat);
            poBundle.putString("seats",strSeats);
            poBundle.putString("appmode","ticket");
            poBundle.putString("bookingno",strBookingNo);

            Intent intPaymentOptions = new Intent(PaySelectionConfirmation.this,PaymentOptions.class);
            intPaymentOptions.putExtras(poBundle);
            startActivity(intPaymentOptions);
        }
        catch (Exception Ex){
            ShowErrorDialog(PaySelectionConfirmation.this,"Error","Unable to proceed");
        }
    }

     */

    //added to direct proceed to commit without showing payment options
    public void proceedPaymentSelection(){
        try{

            String strCustDetails = "", strUID = "", strTransType = "", strProgTitle = "", strLoc = "", strComments = "", strPhone = "";


            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            strUID = obShared.getUID();

                strTransType = "T";
                strProgTitle = "Booking your ticket";

            strComments = edComments.getText().toString();
            strPhone = edPhone.getText().toString();
            strCustDetails = strComments + "|" +strPhone + "|" + "VTREE" + "|" + strComments;
            strGAction = "COMMIT";

            SoapCall obSc = new SoapCall(PaySelectionConfirmation.this);
            obSc.delegate = this;
            ShowProgressDialog(PaySelectionConfirmation.this,strProgTitle,"");
            obSc.execute("COMMITTRANS", strIP, strSessionId, strTempTransId, "TRUE","5123456789012346|VISA|05|2021|123",strCustDetails,"C",strUID,strWCODE,strBookingId,strTicketType,strQty,strTransType,strLoc,"","");
            /*
            Intent intBookingConfirmation = new Intent(PayByCash.this,BookingConfirmation.class);
            startActivity(intBookingConfirmation);
            */
        }
        catch (Exception Ex){
            ShowErrorDialog(PaySelectionConfirmation.this,"Error","Something went wrong");
        }
    }

    public void backToSeats(View view) {
        try{
            subCancelTrans();

        }
        catch (Exception Ex){
            ShowErrorDialog(PaySelectionConfirmation.this,"Error","Unable to proceed");
        }
    }

    @Override
    public void processFinish(String output) {
        try{
            dialogErr.dismiss();
            if(!output.split("\\|")[0].equals("1")){
                if(strGAction.equals("ADDSEATS")) {
                    proceedNext(output);
                }
                if(strGAction.equals("CANCELTRANS")) {
                    finish();
                }
                if(strGAction.equals("COMMIT")) {
                    proceedConfirmation(output);
                }
            }
            else {
                dialogErr.dismiss();
                ShowErrorDialog(PaySelectionConfirmation.this,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(PaySelectionConfirmation.this,"Error","Unable to process");
        }
    }

    private void proceedConfirmation(String output) {
        try{
            String strTotal = "", strResp = "", strFSummData = "", strFnBPrintData = "", strBookingId = "";
            JCResp obJsonCommit = new Gson().fromJson(output, JCResp.class);
            if(obJsonCommit.getRESPONSECODE().equals("0")){
                strResp = obJsonCommit.getRESPONSEDATA();
                strFSummData = strResp.split("\\@")[0];
                strFnBPrintData = strResp.split("\\@")[1];
                Bundle pcBundle = new Bundle();
                strTotal = strFSummData.split("\\|")[8];
                strBookingId = strFSummData.split("\\|")[12];

                pcBundle.putString("filmname",strFilmName);
                pcBundle.putString("seats",strSeats);
                pcBundle.putString("cinemaname",strCinemaName);
                pcBundle.putString("showdatetime",strDateTime);
                pcBundle.putString("qty",strQty);
                pcBundle.putString("price",strTotal);
                pcBundle.putString("screen",strScreen);
                pcBundle.putString("areacat",strAreaCat);
                pcBundle.putString("print",strPrint);
                pcBundle.putString("printData", strFnBPrintData);
                pcBundle.putString("bookingid",strBookingId);
                Intent intBookingConfirmation = new Intent(PaySelectionConfirmation.this,BookingConfirmation.class);
                intBookingConfirmation.putExtras(pcBundle);
                startActivity(intBookingConfirmation);
            }
            else {
                ShowErrorDialog(PaySelectionConfirmation.this,"Error",obJsonCommit.getRESPONSEDATA());
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(PaySelectionConfirmation.this,"Error","Something went wrong");
        }
    }

    private void proceedNext(String output) {
        try{
            JCResp obJsonSeats = new Gson().fromJson(output, JCResp.class);
            if(obJsonSeats.getRESPONSECODE().equals("0")){
                String strPrice = "";
                blnSeatsAdded = true;
                /*
                strDateTime = obJsonSeats.getRESPONSEDATA().split("\\|")[3];
                strDateTime = strDateTime.replace("_"," ");
                strDateTime = strDateTime.replace(",",", ");
                */
                if(strFreeSeating == null)
                    strFreeSeating = "";
                if(strFreeSeating.equals("Y"))
                    strPrice = obJsonSeats.getRESPONSEDATA().split("\\|")[7];
                else
                    strPrice = obJsonSeats.getRESPONSEDATA().split("\\|")[5];
                strBookingNo = obJsonSeats.getRESPONSEDATA().split("\\|")[1];
                strTempTransId = obJsonSeats.getRESPONSEDATA().split("\\|")[13];
                tvShowTime.setText(strDateTime);
                tvTotal.setText(strPrice);
            }
            else {
                ShowErrorDialog(PaySelectionConfirmation.this,"Error",obJsonSeats.getRESPONSEDATA());
            }
            //tvShowTime.setText(strShowTime);
        }
        catch (Exception Ex){
            ShowErrorDialog(PaySelectionConfirmation.this,"Error","Unable to proceed");
        }
    }

    private void subCancelTrans() {
        String strFileContent = "";
        try{
            strGAction = "CANCELTRANS";
            SoapCall obSc = new SoapCall(PaySelectionConfirmation.this);
            obSc.delegate = this;
            obSc.execute("CANCELTRANS", strIP, strTempTransId, "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){
            ShowErrorDialog(PaySelectionConfirmation.this,"Error","Something went wrong");
            //ShowErrorDialog(PaySelectionConfirmation.this,"Error",Ex.toString());
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            ShowErrorDialog(PaySelectionConfirmation.this,"Error","Unable to show progress dialog");
        }
    }

    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    if(blnSeatsAdded == false){
                        finish();
                    }
                    if(blnSeatsAdded == true){
                        subCancelTrans();
                    }
                }
            });
        }
        catch (Exception Ex){
            Toast.makeText(PaySelectionConfirmation.this,"Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {

    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showPrintOrNotDialog(View v){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(PaySelectionConfirmation.this);
        try{

            SharedVariable obShared = ((SharedVariable) getApplicationContext());
            if (obShared.getIbPrinting() != true && obShared.getPrintChecked() != true) {
                proceedPaymentSelection();
                return;
            }
            Button btnYes, btnSkip;
            LayoutInflater inflater = (LayoutInflater) (PaySelectionConfirmation.this).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.dialog_print_or_not, null);
            btnYes = (Button) mView.findViewById(R.id.btnPrint);
            btnSkip = (Button) mView.findViewById(R.id.btnSkip);


            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    strPrint = "Y";
                    proceedPaymentSelection();
                    dialogErr.dismiss();
                }
            });
            btnSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    proceedPaymentSelection();
                    dialogErr.dismiss();
                }
            });

            dialogErr.show();
        }
        catch (Exception Ex){
            Ex.toString();
        }
    }

}

