package in.bigtree.vtree;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class ReprintLastCon extends AppCompatActivity implements AsyncResponse {
    String strIP = "", strFileContent = "", strWCODE = "", strGAction = "", strCinemaname ="", strUID = "";
    SoapCall obSc;
    Context ctx;
    TextView tvWCODE,tvTickets,tvConcession;
    LinearLayout llBack;
    ScrollView scrollTicketParent,scrollConParent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reprint_last_con);

        llBack = (LinearLayout)findViewById(R.id.llBack);
        scrollTicketParent = (ScrollView)findViewById(R.id.scrollTicketParent);
        scrollConParent = (ScrollView)findViewById(R.id.scrollConParent);
        tvTickets = (TextView)findViewById(R.id.tvTickets);
        tvConcession = (TextView)findViewById(R.id.tvConcession);
        llBack.setOnClickListener((v) -> backToMain(v));
        Bundle rLBundle = getIntent().getExtras();
        strCinemaname = rLBundle.getString("cinema");
        ctx = this;
        tvWCODE = (TextView)findViewById(R.id.tvWCODE);
        FileIO obFile = new FileIO();
        strFileContent = obFile.readFile(getResources().getString(R.string.connection_config_file),ctx);
        strIP = strFileContent.split("\\|")[0];
        strWCODE = strFileContent.split("\\|")[1];
        SharedVariable obShared = ((SharedVariable)getApplicationContext());
        strUID = obShared.getUID();

        tvWCODE.setText("Last 10 prints for "+strWCODE);

        callTransactionHistoryTickets();

    }

    //calling tickets history
    private void callTransactionHistoryTickets() {
        try{
            strGAction = "TRANSHISTORY";
            obSc = new SoapCall(ReprintLastCon.this);
            obSc.delegate = this;
            obSc.execute("TRANSHISTORY", strIP, strWCODE, strUID, "","","","","","","","","","","","","");
        }
        catch (Exception Ex){
            ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }

    //calling concession history
    private void callTransactionHistory() {
        try{
            strGAction = "TRANSHISTORYCONCESSION";
            obSc = new SoapCall(ReprintLastCon.this);
            obSc.delegate = this;
            obSc.execute("TRANSHISTORYCONCESSION", strIP, strWCODE, strUID, "","","","","","","","","","","","","");
        }
        catch (Exception Ex){
            ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }

    private void backToMain(View v) {
        try{
            finish();
        }
        catch (Exception Ex){

        }
    }

    @Override
    public void processFinish(String output) {
        try{
            if(!output.split("\\|")[0].equals("1")){
                if(strGAction.equals("TRANSHISTORY")){
                    callTransactionHistory();
                    showTransHistoryTickets(output);
                }
                else if(strGAction.equals("TRANSHISTORYCONCESSION")){
                    showTransHistory(output);
                }
            }
            else {
                if(strGAction.equals("TRANSHISTORY")){
                    callTransactionHistory();
                }
                ShowErrorDialog(ctx,"Error",output.split("\\|")[1]);
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(ctx,"Error","Something went wrong");
            if(strGAction.equals("TRANSHISTORY")){
                callTransactionHistory();
            }
        }
    }

    private void showTransHistory(String output) {
        String strBookingNo = "", strItemDesc = "", strItemQty = "", strNetAmt = "", strGrossAmt = "", strNetTotal = "", strGrossTotal = "", strMobileNo = "", strItemField = "",
                strTotalPrice = "", strDisplayItemString = "", strReceiptNo = "", strTransNo = "";
        try{
            JCResp obResponse = new Gson().fromJson(output, JCResp.class);
            if(obResponse.getRESPONSECODE().equals("0")){
                LinearLayout llParent = (LinearLayout)findViewById(R.id.llConParent);
                String itemsInfo =  obResponse.getRESPONSEDATA ().split("\\#")[0];
                String gstInfo =  obResponse.getRESPONSEDATA ().split("\\#")[1];
                for(int iCount = 0; iCount < itemsInfo.split("\\$").length; iCount++) {
                    String[] arrFields = itemsInfo.split("\\$")[iCount].split("\\|");
                    strTransNo = arrFields[0];
                    strReceiptNo = arrFields[1];
                    strBookingNo = arrFields[2];
                    strMobileNo = arrFields[4];
                    strItemField = arrFields[5];
                    strTotalPrice = arrFields[7];
                    strDisplayItemString = "";
                    String[] arrItemFields = strItemField.split("\\,");
                    for(int jCount = 0; jCount < arrItemFields.length; jCount++){
                        strDisplayItemString += arrItemFields[jCount].split("\\~")[0] + " x" + arrItemFields[jCount].split("\\~")[1] + ",";
                    }
                    strDisplayItemString = strDisplayItemString.substring(0, strDisplayItemString.length() - 1);
                    int intContainerheight = 0, intCSMargin = 0, intCEMargin = 0, intCTMargin = 0;
                    intContainerheight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, getResources().getDisplayMetrics());
                    intCSMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                    intCEMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                    intCTMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                    LinearLayout llItemContainer = new LinearLayout(this);
                    LinearLayout.LayoutParams llContainerParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, intContainerheight);
                    llContainerParams.setMargins(intCSMargin, intCTMargin, intCEMargin, 0);
                    llItemContainer.setLayoutParams(llContainerParams);
                    llItemContainer.setBackground(ContextCompat.getDrawable(ctx, R.drawable.menu_curved_back));
                    llItemContainer.setGravity(Gravity.CENTER_VERTICAL);
                    llItemContainer.setOrientation(LinearLayout.HORIZONTAL);

                    llItemContainer.setTag(strTransNo +"_"+strItemField+"_"+strMobileNo+"_"+strTotalPrice+"_"+gstInfo+"_"+strReceiptNo+"_"+strBookingNo);
                    llParent.addView(llItemContainer);

                    LinearLayout llLsubContainer = new LinearLayout(this);
                    LinearLayout.LayoutParams llLsubParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,7.0f);
                    llLsubContainer.setLayoutParams(llLsubParams);
                    llLsubContainer.setOrientation(LinearLayout.VERTICAL);
                    llItemContainer.addView(llLsubContainer);

                    int intSMargin = 0, intEMargin = 0, intTMargin = 0;
                    TextView tvConItemsName = new TextView(this);
                    TextView tvTotalText = new TextView(this);
                    TextView tvTotalPrice = new TextView(this);
                    TextView tvRsSymbol = new TextView(this);

                    intSMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
                    intEMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());

                    Typeface tfCS = ResourcesCompat.getFont(ctx, R.font.calibre_semibold);

                    LinearLayout.LayoutParams llTvConItemsParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    llTvConItemsParams.setMarginStart((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics()));
                    llTvConItemsParams.setMarginEnd((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));
                    tvConItemsName.setTypeface(tfCS);
                    tvConItemsName.setLayoutParams(llTvConItemsParams);
                    tvConItemsName.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                    tvConItemsName.setTextSize(20);
                    tvConItemsName.setMaxLines(1);
                    tvConItemsName.setEllipsize(TextUtils.TruncateAt.END);
                    tvConItemsName.setText(strDisplayItemString);

                    LinearLayout llTotalPrice = new LinearLayout(this);
                    LinearLayout.LayoutParams llTotalPriceParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    llTotalPriceParams.setMargins(0,(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()),0,0);
                    llTotalPrice.setLayoutParams(llTotalPriceParams);
                    llTotalPrice.setOrientation(LinearLayout.HORIZONTAL);

                    LinearLayout.LayoutParams llTvTotalTextParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    llTvTotalTextParams.setMargins(intSMargin,0,intEMargin,0);
                    tvTotalText.setLayoutParams(llTvTotalTextParams);
                    tvTotalText.setText("Total: ");
                    tvTotalText.setTypeface(tfCS);
                    tvTotalText.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                    tvTotalText.setTextSize(17);

                    tvRsSymbol.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    tvRsSymbol.setText(R.string.Rs);
                    tvRsSymbol.setTypeface(tfCS);
                    tvRsSymbol.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                    tvRsSymbol.setTextSize(16);

                    LinearLayout.LayoutParams lltvTotalPriceParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lltvTotalPriceParams.setMargins(0,0,(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()),0);
                    tvTotalPrice.setTextSize(17);
                    tvTotalPrice.setTypeface(tfCS);
                    tvTotalPrice.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                    tvTotalPrice.setLayoutParams(lltvTotalPriceParams);
                    tvTotalPrice.setText(strTotalPrice);

                    llTotalPrice.addView(tvTotalText);
                    llTotalPrice.addView(tvRsSymbol);
                    llTotalPrice.addView(tvTotalPrice);

                    llLsubContainer.addView(tvConItemsName);
                    llLsubContainer.addView(llTotalPrice);

                    LinearLayout llLSubBottom = new LinearLayout(this);
                    llLSubBottom.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    LinearLayout llBIDContainer = new LinearLayout(this);
                    llBIDContainer.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));
                    llBIDContainer.setOrientation(LinearLayout.VERTICAL);

                    Typeface tfCR = ResourcesCompat.getFont(ctx, R.font.calibre_regular);

                    TextView tvLblBID = new TextView(this);
                    TextView tvTextBID = new TextView(this);
                    int intISMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
                    int intITMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
                    LinearLayout.LayoutParams tvLblBIDParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    tvLblBIDParams.setMargins(intISMargin,intITMargin,0,0);
                    tvLblBID.setLayoutParams(tvLblBIDParams);
                    tvLblBID.setTextColor(ContextCompat.getColor(ctx,R.color.colorDisabledText));
                    tvLblBID.setText("Booking No");
                    tvLblBID.setTextSize(14);
                    tvLblBID.setTypeface(tfCR);

                    LinearLayout.LayoutParams tvTextBIDParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    tvTextBIDParams.setMargins(intISMargin,0,0,0);
                    tvTextBID.setLayoutParams(tvTextBIDParams);
                    tvTextBID.setText(strTransNo);
                    tvTextBID.setTextColor(ContextCompat.getColor(ctx,R.color.colorAccent));
                    tvTextBID.setTextSize(20);
                    tvTextBID.setTypeface(tfCR);

                    llBIDContainer.addView(tvLblBID);
                    llBIDContainer.addView(tvTextBID);

                    LinearLayout llPhoneContainer = new LinearLayout(this);
                    llPhoneContainer.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,2.0f));
                    llPhoneContainer.setOrientation(LinearLayout.VERTICAL);

                    TextView tvLblPhone = new TextView(this);
                    TextView tvTextPhone = new TextView(this);

                    LinearLayout.LayoutParams tvLblPhoneParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    tvLblPhoneParams.setMargins(intISMargin,intITMargin,0,0);
                    tvLblPhone.setLayoutParams(tvLblPhoneParams);
                    tvLblPhone.setTextColor(ContextCompat.getColor(ctx,R.color.colorDisabledText));
                    tvLblPhone.setText("Mobile No");
                    tvLblPhone.setTextSize(14);
                    tvLblPhone.setTypeface(tfCR);

                    LinearLayout.LayoutParams tvTextPhoneParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    tvTextPhoneParams.setMargins(intISMargin,0,0,0);
                    tvTextPhone.setLayoutParams(tvTextPhoneParams);
                    tvTextPhone.setText(strMobileNo);
                    tvTextPhone.setTextColor(ContextCompat.getColor(ctx,R.color.colorAccent));
                    tvTextPhone.setTextSize(20);
                    tvTextPhone.setTypeface(tfCR);

                    llPhoneContainer.addView(tvLblPhone);
                    llPhoneContainer.addView(tvTextPhone);


                    llLSubBottom.addView(llBIDContainer);
                    llLSubBottom.addView(llPhoneContainer);

                    llLsubContainer.addView(llLSubBottom);

                    LinearLayout llArrowContainer = new LinearLayout(this);
                    ImageView ivArrow = new ImageView(this);

                    llArrowContainer.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1.2f));
                    llArrowContainer.setGravity(Gravity.CENTER);

                    ivArrow.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    ivArrow.setImageResource(R.drawable.ic_arrow_right);
                    llArrowContainer.addView(ivArrow);

                    llItemContainer.addView(llArrowContainer);
                    llItemContainer.setClickable(true);

                    llItemContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            goToReprint(v);
                        }
                    });

                    //llParent.addView(llItemContainer);
                }
            }
            else {

            }
        }
        catch (Exception Ex){
            ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }


    //show tickets history
    private void showTransHistoryTickets(String output) {
        String strBookingNo = "", strBookingID = "", strFilmName = "", strScreenName = "", strCat = "", strSeats = "", strQty = "", strPrice = "", strCName = "", strPhone = "", strDateTime = "", strTransNo = "";
        try{
            JCResp obResponse = new Gson().fromJson(output, JCResp.class);
            if(obResponse.getRESPONSECODE().equals("0")){
                LinearLayout llParent = (LinearLayout)findViewById(R.id.llParent);

                for(int iCount = 0; iCount < obResponse.getRESPONSEDATA ().split("\\|").length; iCount++) {
                    String[] arrFields = obResponse.getRESPONSEDATA().split("\\|")[iCount].split("\\$");
                    strBookingNo = arrFields[0];
                    strBookingID = arrFields[1];
                    strCName = arrFields[2];
                    strPhone = arrFields[3];
                    strDateTime = arrFields[5];
                    strFilmName = arrFields[7];
                    strQty = arrFields[8];
                    strSeats = arrFields[10];
                    if(strSeats.contains(",")){
                        strSeats = strSeats.replaceAll("\\,",", ");
                    }
                    strCat = arrFields[14];
                    strTransNo = arrFields[15];
                    strPrice = arrFields[16];
                    strScreenName = arrFields[17];

                    int intContainerheight = 0, intCSMargin = 0, intCEMargin = 0, intCTMargin = 0;
                    intContainerheight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, getResources().getDisplayMetrics());
                    intCSMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                    intCEMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                    intCTMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
                    LinearLayout llItemContainer = new LinearLayout(this);
                    LinearLayout.LayoutParams llContainerParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, intContainerheight);
                    llContainerParams.setMargins(intCSMargin, intCTMargin, intCEMargin, 0);
                    llItemContainer.setLayoutParams(llContainerParams);
                    llItemContainer.setBackground(ContextCompat.getDrawable(ctx, R.drawable.menu_curved_back));
                    llItemContainer.setGravity(Gravity.CENTER_VERTICAL);
                    llItemContainer.setOrientation(LinearLayout.HORIZONTAL);

                    llItemContainer.setTag(strBookingNo +"_"+ strBookingID+"_"+strFilmName+"_"+strQty+"_"+strDateTime+"_"+strScreenName+"_"+strCat+"_"+strSeats+"_"+strPrice);

                    llParent.addView(llItemContainer);

                    LinearLayout llLsubContainer = new LinearLayout(this);
                    LinearLayout.LayoutParams llLsubParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,7.0f);
                    //llLsubParams.addRule(LinearLayout.CENTER_HORIZONTAL);
                    llLsubContainer.setLayoutParams(llLsubParams);
                    llLsubContainer.setOrientation(LinearLayout.VERTICAL);

                    llItemContainer.addView(llLsubContainer);

                    int intSMargin = 0, intEMargin = 0, intTMargin = 0;
                    TextView tvMvName = new TextView(this);
                    TextView tvQtyDateTime = new TextView(this);

                    intSMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
                    intEMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
                    intTMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());

                    LinearLayout.LayoutParams llTvMvNameParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    llTvMvNameParams.setMargins(intSMargin,0,intEMargin,0);
                    Typeface tfCS = ResourcesCompat.getFont(ctx, R.font.calibre_semibold);

                    tvMvName.setLayoutParams(llTvMvNameParams);
                    tvMvName.setTextSize(20);
                    tvMvName.setTextColor(ContextCompat.getColor(ctx,R.color.colorText));
                    tvMvName.setTypeface(tfCS);
                    tvMvName.setText(strFilmName);

                    LinearLayout.LayoutParams llTvDateTimeParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    llTvDateTimeParams.setMargins(intSMargin,0,intEMargin,0);
                    Typeface tfCR = ResourcesCompat.getFont(ctx, R.font.calibre_regular);

                    tvQtyDateTime.setLayoutParams(llTvDateTimeParams);
                    tvQtyDateTime.setTextSize(14);
                    tvQtyDateTime.setTextColor(ContextCompat.getColor(ctx,R.color.colorDisabledText));
                    tvQtyDateTime.setMaxLines(1);
                    tvQtyDateTime.setTypeface(tfCR);
                    tvQtyDateTime.setText(strQty + " Tickets - " + strDateTime);

                    llLsubContainer.addView(tvMvName);
                    llLsubContainer.addView(tvQtyDateTime);

                    LinearLayout llLSubBottom = new LinearLayout(this);
                    llLSubBottom.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                    LinearLayout llBIDContainer = new LinearLayout(this);
                    llBIDContainer.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));
                    llBIDContainer.setOrientation(LinearLayout.VERTICAL);

                    TextView tvLblBID = new TextView(this);
                    TextView tvTextBID = new TextView(this);
                    int intISMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
                    int intITMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
                    LinearLayout.LayoutParams tvLblBIDParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    tvLblBIDParams.setMargins(intISMargin,intITMargin,0,0);
                    tvLblBID.setLayoutParams(tvLblBIDParams);
                    tvLblBID.setTextColor(ContextCompat.getColor(ctx,R.color.colorDisabledText));
                    tvLblBID.setText("Booking ID");
                    tvLblBID.setTextSize(14);
                    tvLblBID.setTypeface(tfCR);

                    LinearLayout.LayoutParams tvTextBIDParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    tvTextBIDParams.setMargins(intISMargin,0,0,0);
                    tvTextBID.setLayoutParams(tvTextBIDParams);
                    tvTextBID.setText(strBookingID);
                    tvTextBID.setTextColor(ContextCompat.getColor(ctx,R.color.colorAccent));
                    tvTextBID.setTextSize(20);
                    tvTextBID.setTypeface(tfCR);

                    llBIDContainer.addView(tvLblBID);
                    llBIDContainer.addView(tvTextBID);


                    LinearLayout llPhoneContainer = new LinearLayout(this);
                    llPhoneContainer.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,2.0f));
                    llPhoneContainer.setOrientation(LinearLayout.VERTICAL);

                    TextView tvLblPhone = new TextView(this);
                    TextView tvTextPhone = new TextView(this);

                    LinearLayout.LayoutParams tvLblPhoneParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    tvLblPhoneParams.setMargins(intISMargin,intITMargin,0,0);
                    tvLblPhone.setLayoutParams(tvLblPhoneParams);
                    tvLblPhone.setTextColor(ContextCompat.getColor(ctx,R.color.colorDisabledText));
                    tvLblPhone.setText("Mobile No");
                    tvLblPhone.setTextSize(14);
                    tvLblPhone.setTypeface(tfCR);

                    LinearLayout.LayoutParams tvTextPhoneParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    tvTextPhoneParams.setMargins(intISMargin,0,0,0);
                    tvTextPhone.setLayoutParams(tvTextPhoneParams);
                    tvTextPhone.setText(strPhone);
                    tvTextPhone.setTextColor(ContextCompat.getColor(ctx,R.color.colorAccent));
                    tvTextPhone.setTextSize(20);
                    tvTextPhone.setTypeface(tfCR);

                    llPhoneContainer.addView(tvLblPhone);
                    llPhoneContainer.addView(tvTextPhone);


                    llLSubBottom.addView(llBIDContainer);
                    llLSubBottom.addView(llPhoneContainer);

                    llLsubContainer.addView(llLSubBottom);

                    LinearLayout llArrowContainer = new LinearLayout(this);
                    ImageView ivArrow = new ImageView(this);

                    llArrowContainer.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT,1.2f));
                    llArrowContainer.setGravity(Gravity.CENTER);

                    ivArrow.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    ivArrow.setImageResource(R.drawable.ic_arrow_right);
                    llArrowContainer.addView(ivArrow);

                    llItemContainer.addView(llArrowContainer);
                    llItemContainer.setClickable(true);

                    llItemContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            goToReprintTicket(v);
                        }
                    });

                    //llParent.addView(llItemContainer);
                }
            }
        }
        catch (Exception Ex){
            ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }

    private void goToReprintTicket(View v) {
        try{
            //llItemContainer.setTag(strBookingID+"_"+strFilmName+"_"+strQty+"_"+strDateTime+"_"+strScreenName+"_"+strCat+"_"+strSeats+"_"+strPrice);
            Bundle bundle = new Bundle();
            bundle.putString("bookingno",v.getTag().toString().split("_")[0]);
            bundle.putString("bookingid",v.getTag().toString().split("_")[1]);
            bundle.putString("filmname",v.getTag().toString().split("_")[2]);
            bundle.putString("qty",v.getTag().toString().split("_")[3]);
            bundle.putString("datetime",v.getTag().toString().split("_")[4]);
            bundle.putString("screen",v.getTag().toString().split("_")[5]);
            bundle.putString("cat",v.getTag().toString().split("_")[6]);
            bundle.putString("seats",v.getTag().toString().split("_")[7]);
            bundle.putString("price",v.getTag().toString().split("_")[8]);
            bundle.putString("cinema",strCinemaname);

            Intent intReprint = new Intent(ctx,ReprintDetails.class);
            intReprint.putExtras(bundle);
            startActivity(intReprint);
            //Toast.makeText(ctx,v.getTag().toString(),Toast.LENGTH_LONG).show();
        }
        catch (Exception Ex){
            ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }

    private void goToReprint(View v) {
        try{
            Bundle bundle = new Bundle();
            //strBookingNo +"_"+strItemField+"_"+strMobileNo+"_"+strTotalPrice
            bundle.putString("transno",v.getTag().toString().split("\\_")[0]);
            bundle.putString("items",v.getTag().toString().split("\\_")[1]);
            bundle.putString("totalprice",v.getTag().toString().split("\\_")[3]);
            bundle.putString("gstinfo",v.getTag().toString().split("\\_")[4]);
            bundle.putString("receiptno",v.getTag().toString().split("\\_")[5]);
            bundle.putString("bookingno",v.getTag().toString().split("\\_")[6]);

            bundle.putString("wcode",strWCODE);

            Intent intReprint = new Intent(ctx,ReprintDetailsCon.class);
            intReprint.putExtras(bundle);
            startActivity(intReprint);
        }
        catch (Exception Ex){

        }
    }

    public void ShowErrorDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.error_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvErrDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvErrDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissErr);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            final AlertDialog dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogErr.dismiss();
                    finish();

                }
            });
        }
        catch (Exception Ex){
            Toast.makeText(ReprintLastCon.this,Ex.toString(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {

    }

    public void changeLayout(View v){
        try{
            if(v.getTag().toString().equals("t")){
                scrollTicketParent.setVisibility(View.VISIBLE);
                scrollConParent.setVisibility(View.GONE);
                tvTickets.setTextColor(ContextCompat.getColor(ctx,R.color.colorPrimary));
                tvTickets.setTypeface(null,Typeface.BOLD);
                tvConcession.setTextColor(ContextCompat.getColor(ctx,R.color.colorDisabledText));
                tvConcession.setTypeface(null,Typeface.NORMAL);
            }
            else{
                scrollTicketParent.setVisibility(View.GONE);
                scrollConParent.setVisibility(View.VISIBLE);
                tvConcession.setTextColor(ContextCompat.getColor(ctx,R.color.colorPrimary));
                tvConcession.setTypeface(null,Typeface.BOLD);
                tvTickets.setTextColor(ContextCompat.getColor(ctx,R.color.colorDisabledText));
                tvTickets.setTypeface(null,Typeface.NORMAL);
            }
        }
        catch (Exception Ex){

        }
    }

}
