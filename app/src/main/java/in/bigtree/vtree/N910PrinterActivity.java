package in.bigtree.vtree;
/*
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.pnsol.sdk.interfaces.DeviceType;
import com.pnsol.sdk.interfaces.ReceiptConst;
import com.pnsol.sdk.payment.PaymentInitialization;

import java.util.ArrayList;
import java.util.List;

import static com.pnsol.sdk.interfaces.PaymentTransactionConstants.SUCCESS;

public class N910PrinterActivity extends Activity {
    public Spinner fontSpinner, alignmentspinner;
    public Button printSample, printReceipt, printImage, print;
    String alignment, fontSize;
    EditText sampleText;
    private PaymentInitialization initialization;
    private Bitmap bitmap;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n910_printer);
        loadFontsize();
        loadAlignments();
        printSample = (Button) findViewById(R.id.printText);
        printReceipt = (Button) findViewById(R.id.printReceipt);
        printImage = (Button) findViewById(R.id.printImage);
        sampleText = (EditText) findViewById(R.id.ediText);
        print = (Button) findViewById(R.id.printEditText);
        fontSpinner = (Spinner) findViewById(R.id.spn_font_size);
        alignmentspinner = (Spinner) findViewById(R.id.spnAlignment);


        printSample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alignment = alignmentspinner.getSelectedItem().toString();
                fontSize = fontSpinner.getSelectedItem().toString();
                StringBuffer stringBuffer = printData(alignment, fontSize);
                initialization = new PaymentInitialization(N910PrinterActivity.this);
                initialization.printText(printerHandler, DeviceType.N910, stringBuffer);
            }
        });

        printReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printReceipt();
            }
        });
        printImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alignment = alignmentspinner.getSelectedItem().toString();
                alignment = setAlignment();
                bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.payswiff);
                initialization = new PaymentInitialization(N910PrinterActivity.this);
                initialization.printImage(printerHandler, DeviceType.N910, bitmap, alignment);
            }
        });
        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fontSize = fontSpinner.getSelectedItem().toString();
                alignment = alignmentspinner.getSelectedItem().toString();
                StringBuffer scriptBuffer = new StringBuffer();
                fontSize = setFontSize();
                alignment = setAlignment();
                scriptBuffer.append("!asc " + fontSize + "\n !asc " + fontSize + "\n !gray 5\n");//Set the title font to large
                scriptBuffer.append("!yspace 5\n");// Set the line spacing, the value is [0,60], the default is 6
                scriptBuffer.append("*text " + alignment + " " + sampleText.getText().toString() + "\n");
                initialization = new PaymentInitialization(N910PrinterActivity.this);
                initialization.printText(printerHandler, DeviceType.N910, scriptBuffer);


            }
        });
    }

    private String setFontSize() {
        if (fontSize == "Large(24)") {
            fontSize = "l";
        } else if (fontSize == "Medium(32)") {
            fontSize = "n";
        } else if (fontSize == "Small(48)") {
            fontSize = "s";
        }
        return fontSize;
    }

    private String setAlignment() {
        if (alignment.equals("left")) {
            alignment = ReceiptConst.LEFT;
        } else if (alignment.equals("right")) {
            alignment = ReceiptConst.RIGHT;
        } else if (alignment.equals("center")) {
            alignment = ReceiptConst.CENTER;
        }
        return alignment;
    }


    public void printReceipt() {
        printLogo(ReceiptConst.CENTER);
        printText();
    }

    private void printText() {
        StringBuffer scriptBuffer = new StringBuffer();
        scriptBuffer.append("!asc l\n");
        scriptBuffer.append("!yspace 6\n");
        scriptBuffer.append("!asc n\n");
        scriptBuffer.append("!yspace 1\n");
        scriptBuffer.append("*text l " + getfinalString("DATE:30-07-2020", "TIME:16:56:90", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("MID:12345", "TID: 098765", 32) + "\n");
        scriptBuffer.append("*text l " + "INVOICE:61349" + "\n");
        scriptBuffer.append("*text c " + "SALE" + "\n");
        scriptBuffer.append("*text c " + "CREDIT" + " " + "VISA" + "\n");
        scriptBuffer.append("*text l " + getfinalString("CARD:", "441962XXXX1912[CHIP]", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("AUTH CODE:", "61349", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("RRN:", "159566454868", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("AID:", "A0000000031010", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("TVR:0000000000", "TSI:1234", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("TC:", "28EDE192A8837CAA", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("AMOUNT:", "100.00", 32) + "\n");
        scriptBuffer.append("*line" + "\n");
        scriptBuffer.append("*text l " + getfinalString("TOTAL AMOUNT:", "100.00", 32) + "\n");
        scriptBuffer.append("*text c " + "\n");
        scriptBuffer.append("*text c " + "\n");
        scriptBuffer.append("*text c " + "PIN VERIFIED OK" + "\n");
        scriptBuffer.append("*text c " + "SIGNATURE REQUIRED" + "\n");
        scriptBuffer.append("*text c " + "I AGREE TO PAY AS PER" + "\n");
        scriptBuffer.append("*text c " + "CARD ISSUER AGREEMENT" + "\n");
        scriptBuffer.append("*text c " + "Powered by Payswiff" + "\n");
        scriptBuffer.append("*text c " + "\n");
        scriptBuffer.append("*text c " + "\n");
        initialization = new PaymentInitialization(N910PrinterActivity.this);
        initialization.printText(printerHandler, DeviceType.N910, scriptBuffer);
    }


    private void printLogo(String alignment) {
        bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.payswiff);
        initialization = new PaymentInitialization(N910PrinterActivity.this);
        initialization.printImage(printerHandler, DeviceType.N910, bitmap, alignment);
    }

    private String getfinalString(String string, String string2, int strlength) {
        int len1 = string.length();
        int len2 = string2.length();
        int totallen = strlength;
        StringBuffer sb = new StringBuffer();
        sb.append(string);
        if ((len1 + len2) < totallen) {
            int space = totallen - (len1 + len2);
            for (int i = 0; i < space; i++) {
                sb.append("\u0020");
            }
            sb.append(string2);
        } else {
            sb.append(string2);
        }

        return sb.toString();

    }

    private StringBuffer printData(String alignment, String fontSize) {
        StringBuffer scriptBuffer = new StringBuffer();
        fontSize = setFontSize();
        alignment = setAlignment();
        scriptBuffer.append(" !asc " + fontSize + "\n !gray 5\n");//Set font to large ,aignment left and gryscale 5
        scriptBuffer.append("!yspace 5\n");// Set the line spacing, the value is [0,60], the default is 6
        scriptBuffer.append("*text " + alignment + " ++++++++++++++ X ++++++++\n");
        scriptBuffer.append("*text " + alignment + " SampleData\n");
        scriptBuffer.append("!yspace 10\n");// Set content line spacing
        scriptBuffer.append("*text " + alignment + " DATE:25-07-2020 " + "\n");
        scriptBuffer.append("*text " + alignment + " MID:5245678659\n");
        scriptBuffer.append("*text " + alignment + " INVOICE:61349\n");
        scriptBuffer.append("*text " + alignment + " CARD:441962XXXX1912[CHIP]\n");
        scriptBuffer.append("*text " + alignment + " AUTH CODE:61349\n");
        scriptBuffer.append("*text " + alignment + " RRN:159566454868\n");
        scriptBuffer.append("*text " + alignment + " AID:A0000000031010\n");
        scriptBuffer.append("*text " + alignment + " TVR:0000000000\n");
        scriptBuffer.append("*text " + alignment + " TC:28EDE192A8837CAA\n");
        scriptBuffer.append("*line" + "\n");//Print dotted line
        return scriptBuffer;
    }


    private void loadFontsize() {
        fontSpinner = findViewById(R.id.spn_font_size);
        List<String> list = new ArrayList<String>();
        list.add("Large(24)");
        list.add("Medium(32)");
        list.add("Small(48)");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fontSpinner.setAdapter(dataAdapter);
    }

    private void loadAlignments() {
        alignmentspinner = (Spinner) findViewById(R.id.spnAlignment);
        List<String> list = new ArrayList<String>();
        list.add("left");
        list.add("center");
        list.add("right");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        alignmentspinner.setAdapter(dataAdapter);
    }


    @SuppressLint("HandlerLeak")
    private final Handler printerHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == SUCCESS) {
                Toast.makeText(N910PrinterActivity.this,
                        (String) msg.obj, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(N910PrinterActivity.this,
                        (String) msg.obj, Toast.LENGTH_SHORT).show();
            }
        }
    };

}
*/