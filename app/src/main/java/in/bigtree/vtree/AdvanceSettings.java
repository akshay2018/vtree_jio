package in.bigtree.vtree;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class AdvanceSettings extends AppCompatActivity implements AsyncResponse{

    Switch swPrinting, swCounter, swIbPrinting, swCCIntegrated;
    String strFileContent = "", strWcode = "", strSLayout = "", strGAction = "", strFetchLocBtn = "", finalGStrResponseData = "", strGLocation = "",
           strPrintingChk = "", strGSlayout = "", strGPrefMode = "", strGPrintMode = "", strCounterEnabled = "", strGCounterPick="", strConsSMS = "", strIbPrinting = "", strCCIntegrated = "";
    EditText edSLoc, edUid, edPwd;
    EditText edPrinterName;
    SoapCall obSc;
    AlertDialog dialogErr;
    Button btnChangeLoc, btnGetStockLoc, btnFnb, btnMovie, btnBoth, btnSLFull, btnSLPartial, btnLogin;
    FrameLayout frAdminLogin;
    SharedPreferences obPreferences;
    SharedPreferences.Editor editor;
    Context ctx;
    LinearLayout llBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advance_settings);
        ctx = AdvanceSettings.this;
        FileIO obFileIO = new FileIO();
        strFileContent = obFileIO.readFile(getResources().getString(R.string.advance_config_file),getApplicationContext());
        obPreferences = getSharedPreferences(getResources().getString(R.string.appPrefs), Context.MODE_PRIVATE);
        editor = obPreferences.edit();
        llBack = (LinearLayout) findViewById(R.id.llIvBack);

        if (strFileContent.split("\\|").length > 0) {strSLayout = strFileContent.split("\\|")[0];} else {strSLayout = "";}
        if (strFileContent.split("\\|").length > 1) {strPrintingChk = strFileContent.split("\\|")[1];} else {strPrintingChk = "";}
        if (strFileContent.split("\\|").length > 2) {strCounterEnabled = strFileContent.split("\\|")[2];} else {strCounterEnabled = "";}
        if (strFileContent.split("\\|").length > 3) {strConsSMS = strFileContent.split("\\|")[3];} else {strConsSMS = "";}
        if (strFileContent.split("\\|").length > 4) {strIbPrinting = strFileContent.split("\\|")[4];} else {strIbPrinting = "";}
        if (strFileContent.split("\\|").length > 5) {strCCIntegrated = strFileContent.split("\\|")[5];} else {strCCIntegrated = "";}

        //swConSMS is disabled
        strConsSMS = "Y";
        swPrinting = (Switch)findViewById(R.id.swPrinting);
        swCounter = (Switch)findViewById(R.id.swCounter);
        //swConsSMS = (Switch)findViewById(R.id.swConsSMS);
        swIbPrinting = (Switch)findViewById(R.id.swIbPrinting);
        swCCIntegrated = (Switch)findViewById(R.id.swCCIntegrated);

        btnChangeLoc = (Button)findViewById(R.id.btnChangeLoc);
        btnGetStockLoc = (Button)findViewById(R.id.btnGetLoc);
        btnFnb = (Button)findViewById(R.id.btnFnb);
        btnMovie = (Button)findViewById(R.id.btnMovie);
        btnBoth = (Button)findViewById(R.id.btnBoth);
        btnSLPartial = (Button)findViewById(R.id.btnPartialSSL);
        btnSLFull = (Button)findViewById(R.id.btnFullSSL);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        edSLoc = (TextInputEditText)findViewById(R.id.tvSLOC);
        edUid = (EditText) findViewById(R.id.tvUid);
        edPwd = (EditText)findViewById(R.id.tvPwd);
        edPrinterName = (EditText)findViewById(R.id.tvPrinter);

        frAdminLogin = (FrameLayout)findViewById(R.id.frAdminLogin);
        btnLogin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
        btnLogin.setClickable(false);

        llBack = (LinearLayout) findViewById(R.id.llIvBack);
        llBack.setOnClickListener((v) -> backFromSettings(v));

        //prompt admin login
        frAdminLogin.setVisibility(View.VISIBLE);

        final SharedVariable obShared = ((SharedVariable)getApplicationContext());
        swPrinting.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked != true) {
                swPrinting.setText("Bluetooth printing is disabled");
                obShared.setPrintChecked(false);
                strGPrintMode = "N";
                edPrinterName.setText("");
                edPrinterName.setEnabled(false);
            } else {
                swPrinting.setText("Bluetooth printing is enabled");
                obShared.setPrintChecked(true);
                strGPrintMode = "Y";
                edPrinterName.setEnabled(true);
            }
        });

        swCounter.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked != true) {
                obShared.setCounterPickupStat(false);
                strGCounterPick = "N";
            } else {
                obShared.setCounterPickupStat(true);
                strGCounterPick = "Y";
            }
        });
        /*
        swConsSMS.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked != true) {
                obShared.setConsSMS(false);
                strConsSMS = "N";
            } else {
                obShared.setConsSMS(true);
                strConsSMS = "Y";
            }
        });
        */
        swIbPrinting.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked != true) {
                swIbPrinting.setText("Inbuilt printing is disabled");
                obShared.setIbPrinting(false);
                strIbPrinting = "N";
            } else {
                swIbPrinting.setText("Inbuilt printing is enabled");
                obShared.setIbPrinting(true);
                strIbPrinting = "Y";
            }
        });

        swCCIntegrated.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked != true) {
                obShared.setCCIntegratedStat(false);
                strCCIntegrated = "N";
            } else {
                obShared.setCCIntegratedStat(true);
                strCCIntegrated = "Y";
            }
        });

        //set preferred seat layout
        switch (strSLayout){
            case "full":
                strGSlayout = "full";
                btnSLFull.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.back_btn_primary));
                btnSLFull.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorWhite));

                btnSLPartial.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ripple));
                btnSLPartial.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorText));
                break;
            case "partial":
                strGSlayout = "partial";
                btnSLPartial.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.back_btn_primary));
                btnSLPartial.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorWhite));

                btnSLFull.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ripple));
                btnSLFull.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorText));
                break;
            case "":
                strGSlayout = "full";
                break;
        }

        if(strCounterEnabled.equals("Y")){
            swCounter.setChecked(true);
        }
        else{
            swCounter.setChecked(false);
        }

        //set printing option
        if(strPrintingChk.equals("Y")) {
            swPrinting.setChecked(true);
            if(!obPreferences.getString("printer","").equals("")){
                edPrinterName.setText(obPreferences.getString("printer",""));
            }
        }
        else{
            edPrinterName.setEnabled(false);
            swPrinting.setChecked(false);
        }

        //set concession sms setting
        /*
        if(strConsSMS.equals("Y") || strConsSMS.equals("")){
            swConsSMS.setChecked(true);
        }
        else{
            swConsSMS.setChecked(false);
        }
*/
        //set inbuilt printing setting
        if(strIbPrinting.equals("Y") || strIbPrinting.equals("")){
            swIbPrinting.setChecked(true);
        }
        else{
            swIbPrinting.setChecked(false);
        }

        //set cc integration setting
        if(strCCIntegrated.equals("Y") || strCCIntegrated.equals("")){
            swCCIntegrated.setChecked(true);
        }
        else{
            swCCIntegrated.setChecked(false);
        }


        edUid.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.length() == 0) {
                        btnLogin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
                        btnLogin.setClickable(false);
                    }
                    else{
                        if(!btnLogin.isClickable() == true){
                            if(edPwd.getText().length() != 0) {
                                btnLogin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorJwcDarkBrown));
                                btnLogin.setClickable(true);
                            }
                        }
                    }
                    if(s.length() == 4){
                        edPwd.requestFocus();
                    }
                } catch (Exception Ex) {
                    Ex.toString();
                }
            }
        });

        edPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.length() == 0) {
                        btnLogin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorDisabledButton));
                        btnLogin.setClickable(false);
                    }
                    else{
                        if(!btnLogin.isClickable() == true){
                            if(edUid.getText().length() != 0) {
                                btnLogin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorJwcDarkBrown));
                                btnLogin.setClickable(true);
                            }
                        }
                    }
                    if(s.length() == 4){
                        closeKeyboard();
                    }
                } catch (Exception Ex) {
                    Ex.toString();
                }
            }
        });
    }

    public void setSeatLayoutType(View view) {
        try{
            Button mBtn = (Button) view;
            if(mBtn.getTag().toString().equals("fullsl")){
                Button pBtn = (Button)findViewById(R.id.btnPartialSSL);
                pBtn.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ripple));
                pBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorText));

                mBtn.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.back_btn_primary));
                mBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorWhite));

                strGSlayout = "full";
            }
            else{
                Button fBtn = (Button)findViewById(R.id.btnFullSSL);
                fBtn.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ripple));
                fBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorText));

                mBtn.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.back_btn_primary));
                mBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorWhite));

                strGSlayout = "partial";
            }
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Unable to set seat layout");
        }
    }

    public void SaveSettings(View view) {
        String strFileContent = "", strFileName = "", strPrinterStat = "";
        try{
            //EditText til = (TextInputLayout) findViewById(R.id.tiPrinter);
            if(strGPrintMode == "Y"){
                if(edPrinterName.getText().toString().equals("")){
                    //til.setError("Specify BT printer mac address");
                    Toast.makeText(AdvanceSettings.this,"Specify BT printer mac address", Toast.LENGTH_LONG).show();
                    return;
                }
                //PrintFnB obPrint = new PrintFnB();
                //strPrinterStat = obPrint.checkDevice(ctx,edPrinterName.getText().toString());
                //if(strPrinterStat.equals("")){
                //    til.setError("Printer not found");
                //    return;
                //}
                editor.putString("printer",edPrinterName.getText().toString());
                editor.commit();
            }
            else{
                editor.putString("printer","");
            }
            strFileName = getResources().getString(R.string.advance_config_file);
            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(strFileName, getApplicationContext());

            strFileContent = strGSlayout + "|" + strGPrintMode + "|" + strGCounterPick + "|" + strConsSMS + "|" + strIbPrinting + "|" + strCCIntegrated;
            obFileIO.writeFile(strFileName, strFileContent, getApplicationContext());

            finish();
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Can not save settings");
        }
    }

    public void setAppMode(View view) {
        String strAppMode = "";
        try{
            Button vBtn = (Button) view;
            Button fBtn, mBtn, bBtn;
            strAppMode = vBtn.getTag().toString();
            vBtn.setBackground(ContextCompat.getDrawable(ctx,R.drawable.back_btn_primary));
            vBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorWhite));
            if(strAppMode.equals("fnb")){
                mBtn = (Button)findViewById(R.id.btnMovie);
                bBtn = (Button)findViewById(R.id.btnBoth);

                mBtn.setBackground(ContextCompat.getDrawable(ctx,R.drawable.ripple));
                bBtn.setBackground(ContextCompat.getDrawable(ctx,R.drawable.ripple));
                mBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorText));
                bBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorText));
            }
            if(strAppMode.equals("movie")){
                fBtn = (Button)findViewById(R.id.btnFnb);
                bBtn = (Button)findViewById(R.id.btnBoth);
                bBtn.setBackground(ContextCompat.getDrawable(ctx,R.drawable.ripple));
                fBtn.setBackground(ContextCompat.getDrawable(ctx,R.drawable.ripple));
                fBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorText));
                bBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorText));
            }
            if (strAppMode.equals("both")){
                fBtn = (Button)findViewById(R.id.btnFnb);
                mBtn = (Button)findViewById(R.id.btnMovie);
                fBtn.setBackground(ContextCompat.getDrawable(ctx,R.drawable.ripple));
                mBtn.setBackground(ContextCompat.getDrawable(ctx,R.drawable.ripple));
                fBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorText));
                mBtn.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorText));
            }
            SharedVariable obShared = ((SharedVariable)getApplicationContext());
            obShared.setMode(strAppMode);
            strGPrefMode = strAppMode;
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Error while setting default app mode");
        }
    }

    public void ClearSettings(View view) {
    }

    public void fetchStockLocation(View v) {
        String strFileContent = "", strIP = "";
        try{
            strFetchLocBtn = "change";
            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());

            strIP = strFileContent.split("\\|")[0];
            ShowProgressDialog(ctx,"Fetching stock locations","This should take less than an minute");
            obSc = new SoapCall(ctx);
            strGAction = "STOCKLOCATION";
            obSc = new SoapCall(ctx);
            obSc.delegate = this;
            obSc.execute("STOCKLOCATION",strIP, "", "", "","","","","","","","","","","","","");
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Error while fetching stock location");
        }
    }

    public void getStockLocation(View v) {
        String strFileContent = "", strIP = "";
        boolean blnWorking = false;
        try{
            strFetchLocBtn = "get";
            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());
            strIP = strFileContent.split("\\|")[0];

            if(strIP.equals("")){
                return;
            }
            blnWorking = obFileIO.checkURL(strIP);
            if(!blnWorking){
                CustomDialog cDialog = new CustomDialog();
                cDialog.ShowErrorDialog(ctx,"No Connection","SERVER'S IP IS NOT REACHABLE");
                return;
            }
            ShowProgressDialog(ctx,"Getting stock locations","This should take less than an minute");
            obSc = new SoapCall(ctx);
            strGAction = "STOCKLOCATION";
            obSc = new SoapCall(ctx);
            obSc.delegate = this;
            obSc.execute("STOCKLOCATION",strIP, "", "", "","","","","","","","","","","","","");
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }

    public void backFromSettings(View view) {
        try{
            //this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
            //this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            finish();
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Can not go back");
        }
    }

    public void ShowProgressDialog(Context ctx, String strTitle, String strDesc) {
        try {
            TextView tvDialogTitle, tvDialogDesc;
            Button btnDissmiss;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(ctx);
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = inflater.inflate(R.layout.progress_dialog, null);
            tvDialogTitle = (TextView) mView.findViewById(R.id.tvProgDialogTitle);
            tvDialogDesc = (TextView) mView.findViewById(R.id.tvProgDialogDesc);
            btnDissmiss = (Button) mView.findViewById(R.id.btnDismissProg);
            //btnDissmiss.setVisibility(View.INVISIBLE);
            tvDialogTitle.setText(strTitle);
            tvDialogDesc.setText(strDesc);

            mBuilder.setView(mView);
            dialogErr = mBuilder.create();
            dialogErr.show();

            dialogErr.setCanceledOnTouchOutside(false);
            btnDissmiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    dialogErr.dismiss();
                }
            });
        }
        catch (Exception Ex){
            //CustomDialog cDialog = new CustomDialog();ShowErrorDialog(SeatLayout.this,"Error",Ex.toString());
        }
    }

    @Override
    public void processFinish(String output) {
        try{

            if(strGAction.equals("LOGIN")) {
                dialogErr.dismiss();
                processLogin(output);
            }
            if(strGAction.equals("LOGOUT")) {

            }
        }
        catch (Exception Ex){
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Error while getting stock location");
        }
    }

    private void processLogin(String output) {
        String strResponseCode = "", strResponseData = "";
        int intSecLevel = 0;
        try{
            if(strGAction.equals("LOGIN")) {
                if (!output.split("\\|")[0].equals("1")) {
                    JCResp obJsonResponse = new Gson().fromJson(output, JCResp.class);
                    strResponseCode = obJsonResponse.getRESPONSECODE();
                    strResponseData = obJsonResponse.getRESPONSEDATA();

                    if (strResponseCode.equals("0")) {
                        intSecLevel = Integer.parseInt(strResponseData.split("\\$")[6]);
                        if(intSecLevel <= 4){
                            adminLogOut();
                        }
                    }
                    else{
                        edUid.setText("");
                        edPwd.setText("");
                        edUid.requestFocus();
                        CustomDialog cDialog = new CustomDialog();
                        cDialog.ShowErrorDialog(ctx,"Error",strResponseData);
                        dialogErr.dismiss();
                    }
                }
                else{
                    edUid.setText("");
                    edPwd.setText("");
                    edUid.requestFocus();
                    CustomDialog cDialog = new CustomDialog();
                    cDialog.ShowErrorDialog(ctx,"Error",output.split("\\|")[1]);
                    dialogErr.dismiss();
                }
            }
        }
        catch (Exception Ex){
            edUid.setText("");
            edPwd.setText("");
            edUid.requestFocus();
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Something went wrong");
        }
    }

    private void adminLogOut() {
        String strIP = "", strWcode = "", strUID = "", strPWD = "", strCId = "";
        try{
            FileIO obFileIO = new FileIO();
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());

            strIP = strFileContent.split("\\|")[0];
            strWcode = strFileContent.split("\\|")[1];
            strUID = edUid.getText().toString();
            strPWD = edPwd.getText().toString();
            strCId = strFileContent.split("\\|")[2];
            edUid.setText("");
            edPwd.setText("");
            frAdminLogin.setVisibility(View.GONE);

            strGAction = "LOGOUT";
            obSc = new SoapCall(ctx);
            obSc.delegate = this;
            obSc.execute("LOGOUT", strIP, strUID, strPWD, strCId, strWcode, "", "", "", "", "", "", "", "", "", "","");
        }
        catch (Exception Ex){

        }
    }

    public void getLogin(View view) {
        String strCID = "", strIP = "", strUID = "", strPWD = "", IMEI = "", SERIAL = "";
        TelephonyManager tm;
        try{
            FileIO obFileIO = new FileIO();
            //strFileContent = obFileIO.readFile(getResources().getString(R.string.config_file_name),getApplicationContext());
            strFileContent = obFileIO.readFile(getResources().getString(R.string.connection_config_file),getApplicationContext());

            strIP = strFileContent.split("\\|")[0];
            strWcode = strFileContent.split("\\|")[1];
            strCID = strFileContent.split("\\|")[2];

            strUID = edUid.getText().toString();
            strPWD = edPwd.getText().toString();

            tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(AdvanceSettings.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            }
            else{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    IMEI = tm.getImei();
                } else {
                    IMEI = tm.getDeviceId();
                }
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
                    SERIAL = android.os.Build.SERIAL;
                }
                else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    SERIAL = android.os.Build.getSerial();
                }
            }

            ShowProgressDialog(ctx,"Logging on "+strWcode,"");
            strGAction = "LOGIN";
            obSc = new SoapCall(ctx);
            obSc.delegate = this;
            obSc.execute("LOGIN",strIP, strUID, strPWD, strCID,strWcode,SERIAL,IMEI,"","","","","","","","","");
            /*
            Intent intMain = new Intent(Login.this,MainScreen.class);
            startActivity(intMain);
            */
        }
        catch (Exception Ex){
            //Ex.toString();
            CustomDialog cDialog = new CustomDialog();
            cDialog.ShowErrorDialog(ctx,"Error","Error while login id");
        }
    }

    @Override
    public void onBackPressed() {

    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
