package in.bigtree.vtree.PayU_Printer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pnsol.sdk.interfaces.PaymentTransactionConstants;
import com.pnsol.sdk.payment.PaymentInitialization;
import com.pnsol.sdk.util.UtilManager;
import com.pnsol.sdk.vo.response.ICCTransactionResponse;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import in.bigtree.vtree.FnbMain;
import in.bigtree.vtree.R;

/**
 * @author pradeep.arige
 * updated by vasanthi.k  on 08/12/2018
 */

public class SignatureCaptureActivity extends Activity implements
        PaymentTransactionConstants {

    public static final String SIGNATURE = "/signature.bmp";
    private LinearLayout signatureCapture;
    private Signature mSignature;
    private Button save, clear;
    private ICCTransactionResponse iccTransactionResponse;
    private String paymentType;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payu_signature_capture);

        signatureCapture = (LinearLayout) findViewById(R.id.linearLayout1);
        mSignature = new Signature(this, null);
        mSignature.setBackgroundColor(Color.WHITE);
        signatureCapture.addView(mSignature, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        save = (Button) findViewById(R.id.save);
        save.setEnabled(false);
        clear = (Button) findViewById(R.id.clear);

        iccTransactionResponse = (ICCTransactionResponse) getIntent()
                .getSerializableExtra("vo");
        //mpaySDk 2.0
        paymentType = getIntent().getExtras().getString("paymentType");

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignature.clear();
                save.setEnabled(false);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSignature.save(signatureCapture);
                Bitmap bitmap = BitmapFactory.decodeFile(SignatureCaptureActivity.this.getFilesDir().getPath() + SIGNATURE);


                PaymentInitialization initialization = new PaymentInitialization(SignatureCaptureActivity.this);
                initialization.initiateSignatureCapture(handler, iccTransactionResponse.getReferenceNumber(), UtilManager.convertBitmapToByteArray(bitmap));


            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();

    }


    public class Signature extends View {
        public static final float STORKE_WIDTH = 5f;
        public static final float HALF_STROKE_WIDTH = STORKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();
        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();
        public static final String SIGNATURE = "signature.bmp";
        private Bitmap bitmap;

        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STORKE_WIDTH);
        }

        public void save(View view) {
            Log.v("log_tag", "Width: " + view.getWidth());
            Log.v("log_tag", "Height: " + view.getHeight());
            if (bitmap == null) {
                try {
                    bitmap = Bitmap.createBitmap(view.getWidth(),
                            view.getHeight(), Config.ARGB_8888);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
            Canvas canvas = new Canvas(bitmap);
            try {
                FileOutputStream fos = openFileOutput(SIGNATURE,
                        Context.MODE_PRIVATE);
                view.draw(canvas);
                bitmap.compress(Bitmap.CompressFormat.PNG, 45, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                Log.v("log_tag", e.toString());
            } catch (IOException e) {
                Log.v("log_tag", e.toString());
            }
        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            //save.setEnabled(true);
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:
                    resetDirtyRect(eventX, eventY);
                    int historysize = event.getHistorySize();
                    for (int i = 0; i < historysize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    //Enabling save button only on move that means after drawing somthing on screen instead of enabling after simple touch
                    save.setEnabled(true);
                    break;
                case MotionEvent.ACTION_UP:
                    break;
                default:
                    debug("Ignored touch event: " + event.toString());
                    break;
            }
            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));
            lastTouchX = eventX;
            lastTouchY = eventY;
            return true;
        }

        private void debug(String string) {
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }
            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.bottom = Math.min(lastTouchY, eventY);
            dirtyRect.top = Math.max(lastTouchY, eventY);
        }
    }

    @android.annotation.SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            if (msg.what == SUCCESS) {

                Toast.makeText(SignatureCaptureActivity.this, "Success", Toast.LENGTH_LONG).show();

                Intent i = new Intent(SignatureCaptureActivity.this,
                        TransactionDetails.class);

                i.putExtra("vo", iccTransactionResponse);

                finish();
                SignatureCaptureActivity.this.startActivity(i);
            } else if (msg.what == FAIL) {
                Toast.makeText(SignatureCaptureActivity.this, (String) msg.obj,
                        Toast.LENGTH_LONG).show();

            } else if (msg.what == ERROR_MESSAGE) {
                Toast.makeText(SignatureCaptureActivity.this, (String) msg.obj,
                        Toast.LENGTH_LONG).show();
            }

        }

        ;
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(SignatureCaptureActivity.this, FnbMain.class);
        startActivity(intent);
        finish();
    }
}
