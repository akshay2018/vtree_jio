package in.bigtree.vtree.PayU_Printer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import com.example.tscdll.TSCActivity;
import com.pnsol.sdk.interfaces.DeviceType;
import com.pnsol.sdk.payment.PaymentInitialization;

import static com.pnsol.sdk.interfaces.PaymentTransactionConstants.SUCCESS;

public class Printer {
    Context ctx = null;
    public Printer(Context ctx){
        this.ctx = ctx;
    }

    private PaymentInitialization initialization;

    public void printText() {
        StringBuffer scriptBuffer = new StringBuffer();
        scriptBuffer.append("!asc l\n");
        scriptBuffer.append("!yspace 6\n");
        scriptBuffer.append("!asc n\n");
        scriptBuffer.append("!yspace 1\n");
        scriptBuffer.append("*text l " + getfinalString("DATE:30-07-2020", "TIME:16:56:90", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("MID:12345", "TID: 098765", 32) + "\n");
        scriptBuffer.append("*text l " + "INVOICE:61349" + "\n");
        scriptBuffer.append("*text c " + "SALE" + "\n");
        scriptBuffer.append("*text c " + "CREDIT" + " " + "VISA" + "\n");
        scriptBuffer.append("*text l " + getfinalString("CARD:", "441962XXXX1912[CHIP]", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("AUTH CODE:", "61349", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("RRN:", "159566454868", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("AID:", "A0000000031010", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("TVR:0000000000", "TSI:1234", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("TC:", "28EDE192A8837CAA", 32) + "\n");
        scriptBuffer.append("*text l " + getfinalString("AMOUNT:", "100.00", 32) + "\n");
        scriptBuffer.append("*line" + "\n");
        scriptBuffer.append("*text l " + getfinalString("TOTAL AMOUNT:", "100.00", 32) + "\n");
        scriptBuffer.append("*text c " + "\n");
        scriptBuffer.append("*text c " + "\n");
        scriptBuffer.append("*text c " + "PIN VERIFIED OK" + "\n");
        scriptBuffer.append("*text c " + "SIGNATURE REQUIRED" + "\n");
        scriptBuffer.append("*text c " + "I AGREE TO PAY AS PER" + "\n");
        scriptBuffer.append("*text c " + "CARD ISSUER AGREEMENT" + "\n");
        scriptBuffer.append("*text c " + "Powered by Payswiff" + "\n");
        scriptBuffer.append("*text c " + "\n");
        scriptBuffer.append("*text c " + "\n");
        scriptBuffer.append("*text c " + "Extra Line 1" + "\n");
        scriptBuffer.append("*text c " + "Extra Line 2" + "\n");
        scriptBuffer.append("*text c " + "Extra Line 3" + "\n");

        scriptBuffer.append("*line" + "\n");//Print dotted line
        initialization = new PaymentInitialization(ctx);
        initialization.printText(printerHandler, DeviceType.N910, scriptBuffer);
    }

    public void Print(String printData){
        String event_name = "", event_date = "", event_time = "", qty = "", booking_id="";
        try {
            event_name = printData.split("\\|")[0];
            event_date = printData.split("\\|")[1];
            event_time = printData.split("\\|")[2];
            qty = printData.split("\\|")[3];
            booking_id = printData.split("\\|")[4];

            StringBuffer scriptBuffer = new StringBuffer();
            scriptBuffer.append("!asc l\n");
            scriptBuffer.append("!yspace 6\n");
            scriptBuffer.append("!asc n\n");
            scriptBuffer.append("!yspace 1\n");

            scriptBuffer.append("*text c " +event_name + "\n");
            scriptBuffer.append("*text l " +"A MAGICAL EXPERIENCE WITH"+ "\n");
            scriptBuffer.append("*text l " +"WATER, LIGHT, MIST, FIRE AND MUSIC"+ "\n");
            scriptBuffer.append("*text l " +event_date+" | "+event_time + "\n");
            scriptBuffer.append("*text l "+"ADMISSION PASS|Valid for " +qty + " persons" + "\n");
            scriptBuffer.append("*text c " +"Gates open 30 minutes prior"+ "\n");
            scriptBuffer.append("*text c " +booking_id + "\n");


            //TscDll.barcode(50, 300, "128", 100, 0, 0, 3, 3, booking_id);
            //TscDll.sendcommand("TEXT 100,400,\"ROMAN.TTF\",0,12,12,\""+booking_id+"\"\r\n");

            //TscDll.printerfont(100, 250, "2", 0, 1, 1, "Event Name: Hardcoded Event Name");
            //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

            initialization = new PaymentInitialization(ctx);
            initialization.printText(printerHandler, DeviceType.N910, scriptBuffer);


        }
        catch (Exception Ex){
            Toast.makeText(ctx, "Error while BT Printing", Toast.LENGTH_SHORT).show();
        }
    }

    private String getfinalString(String string, String string2, int strlength) {
        int len1 = string.length();
        int len2 = string2.length();
        int totallen = strlength;
        StringBuffer sb = new StringBuffer();
        sb.append(string);
        if ((len1 + len2) < totallen) {
            int space = totallen - (len1 + len2);
            for (int i = 0; i < space; i++) {
                sb.append("\u0020");
            }
            sb.append(string2);
        } else {
            sb.append(string2);
        }

        return sb.toString();

    }

    @SuppressLint("HandlerLeak")
    private final Handler printerHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == SUCCESS) {
                Toast.makeText(ctx,(String) msg.obj, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ctx,(String) msg.obj, Toast.LENGTH_SHORT).show();
            }
        }
    };
}
