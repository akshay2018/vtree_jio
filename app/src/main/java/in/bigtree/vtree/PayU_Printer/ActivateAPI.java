package in.bigtree.vtree.PayU_Printer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

import com.pnsol.sdk.interfaces.PaymentTransactionConstants;
import com.pnsol.sdk.vo.response.LoginResponse;

import in.bigtree.vtree.FnbOrderSummary;

public class ActivateAPI implements PaymentTransactionConstants {
    private Context ctx;
    public ActivateAPI(Context ctx){
        this.ctx = ctx;
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == SUCCESS) {
                LoginResponse vo = (LoginResponse) msg.obj;

                Toast.makeText(ctx, "" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();
                Intent i = new Intent(ctx, PaymentTransactionActivity.class);
                ctx.startActivity(i);
            }

            if (msg.what == FAIL) {
                Toast.makeText(ctx, (String) msg.obj, Toast.LENGTH_SHORT).show();
            } else if (msg.what == ERROR_MESSAGE) {
                Toast.makeText(ctx, (String) msg.obj, Toast.LENGTH_LONG).show();
            }

        }
    };

}
