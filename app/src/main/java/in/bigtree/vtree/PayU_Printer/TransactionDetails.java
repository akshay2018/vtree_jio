package in.bigtree.vtree.PayU_Printer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pnsol.sdk.interfaces.PaymentTransactionConstants;
import com.pnsol.sdk.payment.PaymentInitialization;
import com.pnsol.sdk.vo.response.ICCTransactionResponse;
import com.pnsol.sdk.vo.response.TransactionStatusResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.List;

import in.bigtree.vtree.R;

/**
 * @author pradeep.arige
 * updated by vasanthi.k  on 08/12/2018
 */

public class TransactionDetails extends Activity implements
        PaymentTransactionConstants {

    private Button details;
    private ICCTransactionResponse iccTransactionResponse;
    private RadioGroup radiodevice, radioComm;
    private TextView response_txn;
    private String paymentMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payu_transaction_details);
        radiodevice=(RadioGroup)findViewById(R.id.radiodevice);
        details = (Button) findViewById(R.id.details);
        response_txn = (TextView) findViewById(R.id.response_txn);
        iccTransactionResponse = (ICCTransactionResponse) getIntent()
                .getSerializableExtra("vo");

        String data = "";
        try {

            data = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(iccTransactionResponse);
        } catch (JsonGenerationException e1) {
            e1.printStackTrace();
        } catch (JsonMappingException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        response_txn.setText(data);
        details.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    PaymentInitialization initialization = new PaymentInitialization(TransactionDetails.this);
                    //initialization.initiateTransactionDetails(handler, iccTransactionResponse.getReferenceNumber(),null,"","");
                    initialization.initiateTransactionDetails(handler, iccTransactionResponse.getReferenceNumber(), null,paymentMode , null);
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        });
        radiodevice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.sale) {
                    paymentMode = SALE;
                } else if (checkedId == R.id.cashpos) {
                    paymentMode = CASH_AT_POS;
                } else if (checkedId == R.id.balanceEnquiry) {

                    paymentMode = BALANCE_ENQUIRY;
                } else if (checkedId == R.id.matm) {

                    paymentMode = MICRO_ATM;
                }
            }
        });

    }

    // The Handler that gets information back from the PaymentProcessThread
    @SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            if (msg.what == SUCCESS) {
                //
                List<TransactionStatusResponse> transactionStatusResponse = (List<TransactionStatusResponse>) msg.obj;

                Intent i = new Intent(TransactionDetails.this, PaymentDetails.class);
                i.putExtra("txnResponse", transactionStatusResponse.get(0));

                startActivity(i);

            }
            if (msg.what == FAIL) {
                Toast.makeText(TransactionDetails.this, (String) msg.obj,
                        Toast.LENGTH_LONG).show();
                //finish();
            } else if (msg.what == ERROR_MESSAGE) {
                Toast.makeText(TransactionDetails.this, (String) msg.obj,
                        Toast.LENGTH_LONG).show();
            }

        };
    };

}
