package in.bigtree.vtree.PayU_Printer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pnsol.sdk.interfaces.DeviceType;
import com.pnsol.sdk.interfaces.PaymentTransactionConstants;
import com.pnsol.sdk.payment.PaymentInitialization;
import com.pnsol.sdk.vo.DeviceInformation;
import com.pnsol.sdk.vo.response.ICCTransactionResponse;

import java.util.ArrayList;

import in.bigtree.vtree.R;

/**
 * @author pradeep.arige
 * updated by vasanthi.k  on 10/22/2018
 */

/*
 * This class is used to process the payment transaction. From the
 * SharedPreferences gets the Bluetooth MAC address. If the MAC address is
 * presents within SharedPreferences then it connects the BluetoothConnection
 * class and establish the BluetoothSocket between the PED and application to
 * process the payment transaction. Otherwise, if MAC address is not presents in
 * SharedPreferences then displays dialog box
 */

public class PaymentTransactionActivity extends Activity implements
        PaymentTransactionConstants {

    public static final int REQUEST_ENABLE_BT = 1;
    private static String DEVICE_COMMUNICATION_MODE = "transactionmode";
    private static String DEVICE_NAME = "devicename";
    private static String MAC_ADDRESS = "macAddress";
    private static String DEVICE_TYPE = "devicetype";
    private BluetoothAdapter mBtAdapter;
    private boolean bluetoothOnFlag, checkFlag;
    private String amount = "11.00";
    private String cashBackAmount = null;
    private String merchantRefNo, deviceName, paymentoptioncode, deviceMACAddress, paymentType;
    private ListView emvList;
    private int deviceCommMode;
    private PaymentInitialization initialization;
    private com.pnsol.sdk.vo.request.EMI emivo;
    //private int deviceType;
    private String deviceSerial;
    private TextView tvTotalAmt, tvCardInstruction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payu_payment);
        tvTotalAmt = (TextView)findViewById(R.id.totalAmt);
        tvCardInstruction = (TextView)findViewById(R.id.cardinstuction);
        deviceCommMode = getIntent().getIntExtra(DEVICE_COMMUNICATION_MODE, 0);
        deviceMACAddress = getIntent().getStringExtra(MAC_ADDRESS);
        paymentType = getIntent().getStringExtra(PAYMENT_TYPE);
        deviceName = getIntent().getStringExtra(DEVICE_NAME);
        amount = getIntent().getStringExtra("amount");
        cashBackAmount = getIntent().getStringExtra("cashBackAmount");
        merchantRefNo = getIntent().getStringExtra("referanceno");
        tvTotalAmt.setText(amount);
        if (merchantRefNo == null || merchantRefNo.equalsIgnoreCase("null") || merchantRefNo.equalsIgnoreCase("")) {
            merchantRefNo = String.valueOf(System.currentTimeMillis());
        }

        if (paymentType.equalsIgnoreCase(EMI)) {
            emivo = (com.pnsol.sdk.vo.request.EMI) getIntent().getSerializableExtra("vo");
            paymentoptioncode = getIntent().getStringExtra("paymentcode");
        }
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bluetooth is enabled or not if not then asks for enable
        if (!checkFlag) {
            if (mBtAdapter != null) {
                mBtAdapter.cancelDiscovery();
                if (!mBtAdapter.isEnabled()) {
                    bluetoothOnFlag = false;
                    Intent enableIntent = new Intent(
                            BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                } else {
                    bluetoothOnFlag = true;
                }
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!checkFlag) {
            if (bluetoothOnFlag) {
                initiateConnection();
            }
        }
    }

    private void initiateConnection() {
        if(deviceName.equalsIgnoreCase(DeviceType.N910)){
            deviceConnection(deviceMACAddress);
        }else{
            isDeviceON("Please check whether card reader available and its ON?");
        }

    }


    public void deviceConnection(String address) {
        checkFlag = true;
        // int device = getIntent().getIntExtra(DEVICE_TYPE, 0);
        if (paymentType.equalsIgnoreCase(EMI)) {
            if (paymentoptioncode.equalsIgnoreCase("emilist")) {
                initialization = new PaymentInitialization(
                        PaymentTransactionActivity.this);
                initialization.initiateTransaction(handler,
                        deviceName,
                        deviceMACAddress,
                        amount,
                        paymentType,
                        PaymentTransactionConstants.CREDIT,
                        null, null,
                        71.000001,
                        17.000001,
                        merchantRefNo,
                        null, emivo, deviceCommMode, merchantRefNo, null, null);
            } else {
                initialization = new PaymentInitialization(
                        PaymentTransactionActivity.this);
                initialization.initiateEMITransactionWithPaymentOptionCode(handler,
                        deviceName,
                        deviceMACAddress,
                        amount,
                        paymentType,
                        PaymentTransactionConstants.CREDIT,
                        null, null,
                        71.000001,
                        17.000001,
                        merchantRefNo,
                        null, paymentoptioncode, deviceCommMode);
            }

        } else if (paymentType.equalsIgnoreCase(DEVICE_STATUS)) {
            try {
                initialization = new PaymentInitialization(
                        PaymentTransactionActivity.this);
                initialization.isQPOSCardReaderAvailable(handler, deviceCommMode, address
                );
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        } else if ((paymentType.equalsIgnoreCase(SALE_WITH_CASH_BACK))) {
            initialization = new PaymentInitialization(
                    PaymentTransactionActivity.this);
            initialization.initiateTransaction(handler, deviceName, address, amount, paymentType, null,
                    null, null, 0.0, 0.0, merchantRefNo, cashBackAmount, deviceCommMode, merchantRefNo, "", "");
        } else if ((paymentType.equalsIgnoreCase(CASH))) {
            initialization = new PaymentInitialization(
                    PaymentTransactionActivity.this);
            initialization.initiateCashTransaction(handler, amount, paymentType, "POS", null, null, 71.000001, 17.000001, merchantRefNo, "", "");
        } else if ((paymentType.equalsIgnoreCase(VAS_SALE_DEBIT))) {
            initialization = new PaymentInitialization(
                    PaymentTransactionActivity.this);
            initialization.initiateTransaction(handler, deviceName, address, amount, paymentType, "POS",
                    null, null, 71.000001, 17.000001, merchantRefNo, null, deviceCommMode, merchantRefNo, "", "");
        }else if (paymentType.equalsIgnoreCase(DEVICEINFO)) {
            initialization = new PaymentInitialization(PaymentTransactionActivity.this);
            initialization.getDeviceInfo(handler, deviceName, deviceCommMode, address);

        } else if(paymentType.equalsIgnoreCase(APP_UPDATE)){
            initialization = new PaymentInitialization(PaymentTransactionActivity.this);
            initialization.appUpdate(handler,this,deviceName);
        }else if (paymentType.equalsIgnoreCase(MICRO_ATM)) {
            initialization = new PaymentInitialization(
                    PaymentTransactionActivity.this);
            initialization.initiateTransaction(handler, deviceName, address, amount, PaymentTransactionConstants.MICRO_ATM, "POS",
                    null, null, 71.000001, 17.000001, merchantRefNo, null, deviceCommMode, merchantRefNo, null, null);
        } else {
            try {
                //main initiate transaction req
                //Toast.makeText(PaymentTransactionActivity.this, "Initiating sale transaction...",Toast.LENGTH_LONG).show();
                alertMessage("initialization.initiateTransaction(handler, \""+deviceName+"\", \""+address+"\", \""+amount+"\", \""+paymentType+"\", \"POS\", null, null, 71.000001, 17.000001, \""+merchantRefNo+"\", null, \""+deviceCommMode+"\", \""+merchantRefNo+"\", \"\", \"\");");
                initialization = new PaymentInitialization(
                        PaymentTransactionActivity.this);
                initialization.initiateTransaction(handler, deviceName, address, amount, paymentType, "POS",
                        null, null, 71.000001, 17.000001, merchantRefNo, null, deviceCommMode, merchantRefNo, "", "");
            } catch (RuntimeException e) {
                e.printStackTrace();
            }


        }
    }


    // The Handler that gets information back from the PaymentProcessThread
    @SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {

        public void handleMessage(Message msg) {
            checkFlag = true;
            if (msg.what == SOCKET_NOT_CONNECTED) {
                Toast.makeText(PaymentTransactionActivity.this, "SOCKET NOT CONNECTED", Toast.LENGTH_LONG).show();
                alertMessage((String) msg.obj);
            }
            if (msg.what == QPOS_ID) {
                Toast.makeText(PaymentTransactionActivity.this, (String) msg.obj, Toast.LENGTH_LONG).show();
                finish();

            }else if(msg.what==DEVICE_INFO){
                DeviceInformation deviceInfo=(DeviceInformation) msg.obj;
                Toast.makeText(PaymentTransactionActivity.this, "Device Serail number: "+deviceInfo.getSerialNumer()+"\n"+"Model number:"+deviceInfo.getModelNumber(), Toast.LENGTH_LONG).show();
                finish();
            }  else if (msg.what == CHIP_TRANSACTION_APPROVED
                    || msg.what == SWIP_TRANSACTION_APPROVED) {
                ICCTransactionResponse iCCTransactionResponse = (ICCTransactionResponse) msg.obj;
                if (iCCTransactionResponse.isSignatureRequired()) {
                    Intent i = new Intent(PaymentTransactionActivity.this,
                            SignatureCaptureActivity.class);
                    i.putExtra("vo", iCCTransactionResponse);
                    //mpaysdk 2.0
                    i.putExtra("paymentType", paymentType);
                    finish();
                    PaymentTransactionActivity.this.startActivity(i);

                } else {
                    Intent i = new Intent(PaymentTransactionActivity.this,
                            TransactionDetails.class);

                    i.putExtra("vo", iCCTransactionResponse);
                    //mpaysdk 2.0
                    i.putExtra("paymentType", paymentType);
                    ;
                    finish();
                    PaymentTransactionActivity.this.startActivity(i);
                }

            } else if (msg.what == CHIP_TRANSACTION_DECLINED
                    || msg.what == SWIP_TRANSACTION_DECLINED) {
                ICCTransactionResponse vo = (ICCTransactionResponse) msg.obj;
                Intent i = new Intent(PaymentTransactionActivity.this,
                        TransactionDetails.class);

                i.putExtra("vo", vo);
                i.putExtra("paymentType", paymentType);
                PaymentTransactionActivity.this.startActivity(i);
                Toast.makeText(PaymentTransactionActivity.this, "Transaction Status : " + vo.getResponseCode() + ":" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();
                finish();
            } else if (msg.what == QPOS_DEVICE) {
                Toast.makeText(PaymentTransactionActivity.this, "QPOS_DEVICE", Toast.LENGTH_LONG).show();
                alertMessage((String) msg.obj);
            } else if (msg.what == TRANSACTION_FAILED) {
                ICCTransactionResponse vo = null;
                if (msg.obj instanceof ICCTransactionResponse) {
                    vo = (ICCTransactionResponse) msg.obj;
                }

                if (paymentType.equalsIgnoreCase(EMI)) {
                    Intent i = new Intent(PaymentTransactionActivity.this, TransactionDetails.class);
                    i.putExtra("vo", vo);
                    i.putExtra("paymentType", paymentType);
                    PaymentTransactionActivity.this.startActivity(i);
                    Toast.makeText(PaymentTransactionActivity.this, "Transaction Status : " + vo.getResponseCode() + ":" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Toast.makeText(PaymentTransactionActivity.this, "Transaction Status : " + vo.getResponseCode() + ":" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();

                    finish();
                }
            } else if (msg.what == TRANSACTION_INITIATED) {
                Toast.makeText(PaymentTransactionActivity.this, "TRANSACTION_INITIATED", Toast.LENGTH_LONG).show();
                tvCardInstruction.setText(msg.obj.toString());

            } else if (msg.what == ERROR_MESSAGE) {

                if (msg.obj instanceof ICCTransactionResponse) {
                    ICCTransactionResponse vo = (ICCTransactionResponse) msg.obj;
                    Toast.makeText(PaymentTransactionActivity.this, vo.getResponseCode() + ":" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();
                    finish();
                }
                else {
                    Toast.makeText(PaymentTransactionActivity.this, "instanceof ICCTransactionResponse err", Toast.LENGTH_LONG).show();
                    alertMessage((String) msg.obj);
                }
            } else if (msg.what == TRANSACTION_PENDING) {
                Toast.makeText(PaymentTransactionActivity.this,
                        (String) msg.obj + "Pending status", Toast.LENGTH_SHORT).show();
                finish();
            } else if (msg.what == DISPLAY_STATUS) {
                Toast.makeText(PaymentTransactionActivity.this,
                        (String) msg.obj, Toast.LENGTH_SHORT).show();
            } else if (msg.what == QPOS_EMV_MULITPLE_APPLICATION) {
                ArrayList<String> applicationList = (ArrayList<String>) msg.obj;
                emvList = (ListView) findViewById(R.id.application_list);
                emvList.setVisibility(View.VISIBLE);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(PaymentTransactionActivity.this, android.R.layout.simple_list_item_1, applicationList);
                emvList.setAdapter(adapter);
                emvList.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        if (initialization != null) {
                            initialization.getQposListener().executeSelectedEMVApplication(position);
                            emvList.setVisibility(View.GONE);
                        }
                    }
                });
            } else if (msg.what == SUCCESS) {

                Toast.makeText(PaymentTransactionActivity.this,"Going to exit", Toast.LENGTH_SHORT).show();
                /*  alertMessage((String) msg.obj);*/
                //Intent i = new Intent(PaymentTransactionActivity.this,MainActivity.class);
                finish();
                //PaymentTransactionActivity.this.startActivity(i);
            }

        }


    };

    public void alertMessage(String message) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Alert").setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        finish();
                    }
                }).show();

    }

    public void isDeviceON(String message) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);

        }
        builder.setTitle("Alert").setMessage(message)
                .setCancelable(false)
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        finish();
                    }
                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deviceConnection(deviceMACAddress);
                    }
                }).show();
    }
}
