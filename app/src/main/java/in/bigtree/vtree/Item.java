package in.bigtree.vtree;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("strItemCode")
    @Expose
    private String strItemCode;
    @SerializedName("strItemDesc")
    @Expose
    private String strItemDesc;
    @SerializedName("strItemPrice")
    @Expose
    private String strItemPrice;
    @SerializedName("strQuantity")
    @Expose
    private String strQuantity;



    public String getStrItemCode() {
        return strItemCode;
    }

    public void setStrItemCode(String strItemCode) {
        this.strItemCode = strItemCode;
    }

    public String getStrItemDesc() {
        return strItemDesc;
    }

    public void setStrItemDesc(String strItemDesc) {
        this.strItemDesc = strItemDesc;
    }

    public String getStrItemPrice() {
        return strItemPrice;
    }

    public void setStrItemPrice(String strItemPrice) {
        this.strItemPrice = strItemPrice;
    }

    public String getStrQuantity() {
        return strQuantity;
    }

    public void setStrQuantity(String strQuantity) {
        this.strQuantity = strQuantity;
    }

}